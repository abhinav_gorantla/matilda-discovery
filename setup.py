import os
import re
import codecs
from setuptools import setup, find_packages
from shutil import copyfile, copytree

copyfile('etc/matilda_discovery.service', '/etc/systemd/system/matilda_discovery.service')
if not os.path.exists('/etc/matilda_discovery'):
   os.makedirs(os.path.dirname('/etc/matilda_discovery'), exist_ok=True)
   copytree('etc/', '/etc/matilda_discovery')

if not os.path.exists('/var/log/matilda'):
   os.makedirs(os.path.dirname('/var/log/matilda'), exist_ok=True)

with open('requirements.txt') as f:
    required = f.read().splitlines()

here = os.path.abspath(os.path.dirname(__file__))


def read(*parts):
    return codecs.open(os.path.join(here, *parts), 'r').read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


setup(
    name="matilda-discovery",
    version=find_version("__init__.py"),
    packages=find_packages(),
    include_package_data=True,
    install_requires=required,
    package_data = {
        # If any package contains *.txt or *.rst files, include them:
        '': ['*.service'],
    },
)
