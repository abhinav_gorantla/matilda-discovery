# -*- coding: utf-8 -*-
from setuptools import setup
#from setuptools.extension import Extension
from distutils.extension import Extension
from Cython.Build import cythonize
from Cython.Distutils import build_ext
from setuptools.dist import Distribution
from pathlib import Path
import shutil
import configparser
import os,json
from fnmatch import fnmatch
from Cython.Compiler import Options


class BinaryDistribution(Distribution):
    def is_pure(self):
        return False


class MyBuildExt(build_ext):
    def run(self):
        build_ext.run(self)

        build_dir = Path(self.build_lib)
        root_dir = Path(__file__).parent
        target_dir = build_dir if not self.inplace else root_dir
        print("$$$$$$",root_dir,target_dir, build_dir)
        self.copy_file(Path('matilda_discovery')  / '__init__.py', root_dir, build_dir  )
        self.copy_file(Path('matilda_discovery')  / '__main__.py', root_dir, build_dir  )
        self.copy_file(Path('matilda_discovery/resources')  / 'ConfigFile.ini', root_dir, build_dir  )
        self.copy_file(Path('matilda_discovery/resources')  / 'matilda_logging.ini', root_dir, build_dir  )


    def copy_file(self, path, source_dir, destination_dir):
        if not (source_dir / path).exists():
            return

        shutil.copyfile(str(source_dir / path), str(destination_dir / path))

class BuildCython(MyBuildExt, BinaryDistribution):
    def __init__(self, dirs=None, compile_args=None, link_args=None, nthreads=None, gdb=None, keyword=None, \
                 language=None, data_files=None, zip=None):
        self.extpath1=[]
        self.extpath2=[]
        self.exts=[]

        self.extpath1work=[]
        self.extpath2work=[]
        self.extpath1plug=[]
        self.extpath2plug=[]
        self.dirs = dirs
        self.compile_args = compile_args
        self.link_args = link_args
        self.nthreads = int(nthreads)
        self.gdb = bool(gdb)
        self.keyword = bool(keyword)
        self.language = int(language)
        self.data_files = data_files
        self.zip = zip

    def find_files(self, root):
        pattern = "*.py"
        ommit = []
        file_ommit = ['__init__.py']
        s1 = set(ommit)
        temp = []
        temp_files = []
        for file in os.listdir(root):
            if fnmatch(file, pattern) and  file not in file_ommit:
                temp.append(os.path.join(root, file))
                temp_files.append(file)

        return (temp, temp_files)

    def BuildArray(self):
        Options.docstrings = True
        Options.fast_fail = True
        ommit = ['__pycache__','.git','.scannerwork','.idea','migrate_repo']
        s1 = set(ommit)
        for path, subdirs, files in os.walk("matilda_discovery"):
            for d in subdirs:
                path1=os.path.relpath(os.path.join(path, d), ".")
                if '/' in path1:
                    path2=path1.replace('/','.')
                    s2 = set(path1.split('/'))
                    if not s1.intersection(s2):
                        #print('path',path1,path2)
                        self.extpath1.append(path1)
                        self.extpath2.append(path2)

    def Buildexts(self):
        self.BuildArray()
        for (pth1,pth2) in zip(self.extpath1,self.extpath2):
            #print('pth1 {} pth2 {}'.format(pth1,pth2))
            (dirs,files) = self.find_files(os.path.join(os.path.dirname(os.path.abspath(__file__)) + '/' + pth1 + '/'))
            for (d,f) in zip(dirs,files):
                self.exts.append(Extension(
                    name= pth2 + '.' + f.split('.')[0],
                    sources=[d],
                    include_dirs=self.dirs,
                    extra_compile_args=self.compile_args,
                    extra_link_args=self.link_args))
                with open('requirements.txt') as f:
                   required = f.read().splitlines()

        print(self.exts)
        setup(
            name="matilda_discovery",
            install_requires=required,
            ext_modules=cythonize(
            self.exts,
            nthreads=self.nthreads,
            gdb_debug=self.gdb,
            build_dir="build",
            compiler_directives=dict(
            always_allow_keywords=self.keyword,
            language_level=self.language
            )),
            cmdclass=dict(
                build_ext=MyBuildExt
            ),

            packages=['matilda_discovery'],
            entry_points={
                'console_scripts': [
			    'matildadiscovery=matilda_discovery.api.controller.api:main',
			    'discoverysched=matilda_discovery.api.controller.scheduler:main',

			   ],
            },

            package_data={'etc': ['etc/matilda_discovery.conf','etc/matilda_discovery.ini','etc/matilda_discovery.service','etc/matilda_discovery_api.conf','etc/matilda_discovery_uwsgi.py']},
            data_files=self.data_files,
            distclass=BinaryDistribution,
            #include_package_data=True,
            zip_safe=self.zip
        )

def main():
    config = configparser.ConfigParser()
    config.read("settings.ini")
    bld = BuildCython(dirs=eval(config.get('compiler', 'dirs'), {}, {}),compile_args=eval(config.get('compiler', 'compile_args'), {}, {}),
                link_args=eval(config.get('compiler', 'link_args'), {}, {}), nthreads=config.get('compiler', 'nthreads'),
                gdb=config.get('compiler', 'gdb_debug'), keyword=config.get('compiler', 'keywords'),
                language=config.get('compiler', 'language'), data_files=eval(config.get('build', 'data_files'), {}, {}),
                zip=config.getboolean('compiler', 'zip_safe'))
    bld.Buildexts()
    #initialize_app(app)
    #app.run(host='0.0.0.0', port=5010, debug=True)


if __name__ == "__main__":
    main()
