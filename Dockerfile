FROM python:3.6-stretch
LABEL maintainer "devops@matildacloud.com" description "Matilda DEVOPS"
RUN apt-get update
RUN apt-get install gcc
RUN apt-get install python3-dev default-libmysqlclient-dev -y
RUN mkdir -p /opt/Discovery
COPY matilda_discovery/ /opt/Discovery/matilda_discovery/
COPY requirements.txt/ /opt/Discovery/matilda_discovery/
WORKDIR /opt/Discovery/
RUN pip3 install -r matilda_discovery/requirements.txt
RUN cp /opt/Discovery/matilda_discovery/package_dependencies/* /usr/local/lib/python3.6/site-packages/flask_restplus/
#COPY matilda_discovery.service /etc/systemd/system/
#ENTRYPOINT systemctl start matilda_discovery.service
ENV PYTHONPATH=/opt/Discovery/matilda-discovery
ENTRYPOINT python matilda_discovery/app.py
EXPOSE 5010
