$url = "https://dev.mysql.com/get/Downloads/MySQLInstaller/mysql-installer-community-5.7.27.0.msi"
$output = "C:\temp\MySQL.msi"
$start_time = Get-Date

Invoke-WebRequest -Uri $url -OutFile $output

msiexec /i $output /quiet

echo "MySQL Installation Complete"

mysql –version

Set-Content -Value “ALTER USER ‘root’@’%’ IDENTIFIED BY ‘Matilda3#’;” -Path c:\mysql\mysql-init.txt -Encoding Ascii

mysqld –init-file=c:\\mysql\\mysql-init.txt –console

mysqld –install

Get-Service MySQL
Start-Service MySQL
Get-Service MySQL