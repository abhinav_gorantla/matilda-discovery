#!/usr/bin/env bash

echo "Check if user exists"
if grep -Fxq "matilda" /etc/sudoers
then
    echo "Matilda User already exists. Skipping..."
else
    echo "Creating user..."
    useradd -m matilda -s /bin/bash
    usermod -aG sudo matilda
    sed -i '/#includedir/i matilda ALL=(ALL) NOPASSWD: ALL' /etc/sudoers
    echo "Matilda User created successfully"
fi

echo "Setting up matilda directory structure"
#mkdir -p /opt/matilda/matilda-discovery
#cp -R * /opt/matilda/matilda-discovery
mkdir -p /var/log/matilda

#echo "Changing permissions"
#chown -R matilda:matilda /opt/matilda
#chown -R matilda:matilda /var/log/matilda

echo "Installing Python 3.6"
echo | add-apt-repository ppa:deadsnakes/ppa


echo "Installing dependencies"
platform=`awk -F= '/^NAME/{print $2}' /etc/os-release`
if [[ $platform = *"Ubuntu"* ]]; then
  echo "Identified Operating System: Ubuntu"
  echo "Installing Python 3.6"
  echo | sudo add-apt-repository ppa:deadsnakes/ppa
  apt-get update
  apt-get install python3.6 python3-pip python3.6-dev build-essential libffi-dev gcc -y
  apt-get install libmysqlclient-dev uwsgi -y
  apt-get install uwsgi-plugin-python3 -y
  apt-get install python-mysqldb -y
else
  yum update
  yum install python36u python36u-pip python36u-devel python36u-libs -y
  yum install python python-pip python-setuptools -y
  yum install libmysqlclient-dev uwsgi -y
  yum install MySQL-python -y
fi

echo "Setting Python 3.6 as default"
y | rm /usr/bin/python
y | rm /usr/bin/python3
ln -s /usr/bin/python3.6 /usr/bin/python3
ln -s /usr/bin/python3 /usr/bin/python


pip3 install -r /opt/matilda/matilda-discovery/requirements.txt

echo "Setting up Matilda Discovery API service"
#export PYTHONPATH=$PYTHONPATH:/opt/matilda/matilda-discovery
#cp /opt/matilda/matilda-discovery/etc/matilda_discovery.service /etc/systemd/system/

#/usr/bin/uwsgi --socket 0.0.0.0:5010 --protocol=http --ini matilda_discovery.ini
#systemctl daemon-reload
#systemctl enable matilda_discovery
#systemctl start matilda_discovery
echo "Setup Complete"

