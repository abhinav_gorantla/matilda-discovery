import configparser
import datetime
import json
import math
import os
import re
import socket
import struct
from decimal import Decimal
import platform

from cryptography.fernet import Fernet
from netaddr import IPNetwork

from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.logger_engine import logger as logger_py_file

logger = logger_py_file.Logger()


def get_properties(property_file_path, section, property_name=None):
    """
    accepts ini file path, section name, an optional property name and returns contents under a given section
    :param property_file_path: relative path of the ini file we are trying to read from
    :param section: section in the ini file we want to read
    :param property_name: property_name under section in the ini file we want to read
    :return: dict containing section details in ini file. If property_name is given, returns the corresponding property as a string
    """
    try:
        config = configparser.RawConfigParser()
        config.optionxform = str
        config.read(os.path.normpath(os.path.join(os.path.dirname(__file__), property_file_path)))

        if property_name:
            data = config.get(section, property_name)
            return data

        data = config._sections[section]

        return data

    except Exception as e:
        logger.error(f"Failed to fetch contents. Error: {e}. Inputs: property_file_path: {property_file_path}. section: {section}. property_name: {property_name}")
        return None


def get_ip_list(ip_input):
    """
    retunrs list of ip_adress by taking ip_adddres range
    :param ip_input: ip_address Range
    :return: list of ip_address
    """
    logger.debug(f"Getting ip list from given ip range or cidr block")
    ip_list = []
    try:
        if '-' in ip_input:
            ip_range = ip_input.split('-')
            start_ip = ip_range[0]
            end_ip = ip_range[1]
            if '.' not in ip_range[1]:
                octates = ip_range[0].split('.')
                octates[3] = ip_range[1]
                end_ip = '.'.join(octates)
            start = struct.unpack('>I', socket.inet_aton(start_ip))[0]
            end = struct.unpack('>I', socket.inet_aton(end_ip))[0]
            ip_list = [socket.inet_ntoa(struct.pack('>I', i)) for i in range(start, end)]
        elif '/' in ip_input:
            for ip in IPNetwork(ip_input):
                ip_list.append(str(ip))
        elif ',' in ip_input:
            ip_list = ip_input.split(',')
        else:
            if _is_valid_ip(ip_input):
                ip_list.append(ip_input)
            else:
                raise Exception('Invalid input')

        logger.debug(f"Successfully got ip list from given ip")
        return ip_list

    except Exception as e:
        logger.error(f"Failed to get ip list. Error: {e}. Inputs: ip_input: {ip_input}")
        return []


def get_datetime(req_format=CreateInfra.default_datetime_format.value):
    """
    Returns current datetime in default format. If req_format is provided, corresponding datetime format will be used.
    :param req_format: constant from CreateInfra defining datetime format to be used
    :return: str, current datetime
    """
    try:
        return str(datetime.datetime.now().strftime(req_format))

    except Exception as e:
        logger.error(f"Failed to get datetime. Error: {e}. Input: {req_format}")


def savings_caluclation(value1,value2):
    """
    caluclates the savings of the latter value to the first value
    :return: float,Savings
    """
    try:
        logger.debug(f"finding savings value")
        response =None
        if value1 not in [0,None] and value2 not in [None,0]:
            response = round((100 - float(value2/value1*100)),2)
            logger.debug(f"Successfully found savings value")
        else:
            logger.debug(f"One of the two values were either empty or 0")
        return response
    except Exception as e:
        logger.error(f"Exception occured while caluclating saving value. Error {e}")
        return None

def str_concat(input1, input2):
    """
    Accepts 2 primitive data type values and returns concatenated value. If either of them are null, returns 'None'
    :param input1: primitive data type except None
    :param input2: primitive data type except None
    :return: string
    """
    logger.debug(f"Checking for null values and concatenating the inputs. input1: {input1}  | input2: {input2}")
    try:
        if input1 is None or input2 is None:
            logger.debug("One of the inputs is null. Returning 'None'")
            return None

        else:
            logger.debug(f"Successfully concatenated the inputs.")
            return str(input1) + str(input2)

    except Exception as e:
        logger.error(f"Failed to concatenate. Error: {e}. Inputs: input1: {input1}. input2: {input2}")
        return None


def none_check_return_null(item):
    """
    function checks if teh variable is NONE, then returns "NULL" else retunrs the variable
    :param item:
    :return:
    """
    try:
        logger.debug(f"Checking for NONE in the item: {item}")
        return_value = item if item is not None else "NULL"
        return return_value
    except Exception as e:
        logger.debug(f"Exception occurred while checking NONE value for the item {item}. Exception : {e}")
        return None


def none_check_return_zero(item):
    """
    function checks if teh variable is NONE, then returns "NULL" else retunrs the variable
    :param item:
    :return:
    """
    try:
        logger.debug(f"Checking for NONE in the item: {item}")
        return_value = item if item is not None else 0
        return return_value
    except Exception as e:
        logger.debug(f"Exception occurred while checking NONE value for the item {item}. Exception : {e}")
        return None


def round_up(param_1, param_2):
    """
    rounds the param_1 value to the nearest mutiple of param_2 which is higher than param_1
    """
    temp_1 = math.ceil(float(param_1 / param_2))
    return temp_1 * param_2


def percent(current_value, total_value):
    """
    Returns percentage of current value relative to total value with 2 decimal precision
    :param current_value: caluclated value,int or float
    :param total_value: total value ,int or float
    :return: percentage result
    """
    logger.debug(f"Calculating percent using percent function")

    try:
        if total_value == 0:
            logger.error("total value is 0")
            return None
        logger.debug(f"Successfully calculated percent")
        return round((current_value / total_value) * 100, 2)
    except Exception as e:
        logger.error(f"Error while calculating percent. Error: {e}. Inputs: current_value: {current_value}. total_value: {total_value}")
        return None


def get_time_difference(start_date, end_date):
    """
    returns the time difference between 2 given input time in  the following format "25 Days, hours:minutes:seconds"
    :param start_date:any date format
    :param end_date: any date format
    :return: resp : time difference between two times
    """
    logger.debug(f"getting difference between start_date: {start_date} and end_date: {end_date}")

    try:
        start_date = datetime.datetime.strptime(start_date, '%m-%d-%Y %I:%M:%S %p')
        end_date = datetime.datetime.strptime(end_date, '%m-%d-%Y %I:%M:%S %p')
        resp = str(end_date - start_date)
        logger.debug(f"Successfully got difference between start_date: {start_date} and end_date: {end_date}")
        return resp

    except Exception as e:
        logger.error(f"Failed to get difference between start_date: {start_date} and end_date: {end_date}. Error: {e}")
        return None


def _is_valid_ip(ip):
    """
    returns true or false to chekc if the IP is valid
    :param ip: ip_address
    :return: boolean which tells valid or not
    """
    try:
        socket.inet_aton(ip)
        return True
    except socket.error as e:
        logger.error(f"Failed to check if ip is valid. Error: {e}. Input: {ip}")
        return False


def null_check(value):
    """
    returns Null if item is None else return the value
    :param out:
    :return:
    """
    if value in [None, ""]:
        return "Null"
    return value


def shell_to_json(items, info=None, logger_ip=None):  # done
    try:
        logger.debug(logger_ip=logger_ip, message=f"Converting Shell format to Json format for {info} shell script. SHELL SCRIPT OUTPUT: {items}")
        resp_list = []
        for item in items:
            spl_ch_flag=False
            if "\\" in item:
                item = item.replace("\\", "*%*")
                spl_ch_flag = True
            item1 = item.replace("'", '"')
            resp = json.loads(item1)
            if spl_ch_flag:
                key, val = next(iter(resp.items()))
                resp[key.replace("*%*","\\")]=val.replace("*%*","\\")
            resp_list.append(resp)

        logger.debug(logger_ip=logger_ip, message=f"Successfully converted Shell format to Json format for {info} shell script. SHELL TO JSON OUTPUT: {resp_list}")
        return resp_list

    except Exception as e:
        logger.error(logger_ip=logger_ip, message=f"Exception occurred while converting shell format to Json format. Error: {e}. Inputs: items: {items}. info: {info}. logger_ip: {logger_ip}")


def shell_to_jsonlist(items, info=None,logger_ip=None):
    try:
        logger.debug(f"Converting shell script into Json list format for {info} shell script. SHELL SCRIPT OUTPUT: {items}")
        outputList = []
        for item in items[0][:-2].split('||'):
            if item.strip():
                itemJson = json.loads('{}')
                for itemDetails in item.split(','):
                    if itemDetails.strip() and ':' in itemDetails.strip():
                        key = itemDetails[0:itemDetails.index(':')]
                        value = itemDetails[itemDetails.index(':') + 1:]
                        itemJson[key.strip()] = value.strip()
                outputList.append(itemJson)
        logger.debug(logger_ip=logger_ip, message=f"Successfully converted Shell format to Json format for {info} shell script. SHELL TO JSONLIST OUTPUT: {outputList}")
        return outputList
    except Exception as e:
        logger.error(logger_ip=logger_ip, message=f"Exception occured while converting shell format into Json list format for {info} shell script. Exception :{e}. Inputs: items: {items}. info: {info}")


def get_script(name=None, version=None, operatingsystem=None, arguments=None):
    try:
        if version:
            ps_name = '../resources/powershell/' + version + "/" + name + ".ps1"
        elif operatingsystem:
            ps_name = '../resources/shell/' + ''.join([i for i in operatingsystem if not i.isdigit()]) + '/' + operatingsystem + '/' + name + '.sh'
        elif not name:
            ps_name = '../resources/shell/os.sh'
        file_path = os.path.normpath(os.path.join(os.path.dirname(__file__), ps_name))
        with open(file_path, 'r') as f:
            ps_script = f.read()
            if arguments:
                ps_script = ps_script.replace('ARGUMENTS', arguments)
        return ps_script

    except Exception as e:
        logger.error(f"Failed to get script. Error: {e}. Inputs: name: {name}. version: {version}. operatingsystem: {operatingsystem}")


def float_converter(value):
    try:
        if value not in [None, []]:
            return float(value)
        return 0.0
    except Exception as e:
        logger.error(f"Failed to convert to float. Error: {e}. Input: value: {value}")


def number_formatter(value, number_type=None):
    try:
        if "T" in value:
            res = float(re.sub(r"[a-zA-Z]", "", value)) * 1024
        elif "G" in value:
            res = float(re.sub(r"[a-zA-Z]", "", value))
        elif "M" in value:
            res = float(re.sub(r"[a-zA-Z]", "", value)) / 1024
        elif "K" in value:
            res = float(re.sub(r"[a-zA-Z]", "", value)) / (1024 * 1024)
        else:
            res = float(re.sub(r"[a-zA-Z%]", "", value))

        if number_type == 'int':
            return int(res)

        if number_type == 'float':
            return round(res, 2)

    except Exception as e:
        logger.error(f"Failed to format. Error: {e}. Inputs: value: {value}, number_type: {number_type}")


def create_dir_file(directory):
    if not os.path.exists(directory):
        try:
            logger.info(f"Creating Directory Path")
            os.makedirs(directory)
        except OSError as exception:
            logger.info(f"Failed to create Directory. Exception: {exception}. inputs: directory: {directory}")
    return None


def get_host_username(username, domain):
    """
    creates username from gievn username and domain details and along with user domain_user varible
    :param username:string, username
    :param domain:string, domain name
    :return: string, new username
    """
    logger.debug(f"Getting username")

    user_name = username
    try:
        const = get_properties(property_file_path=CreateInfra.etc_properties_path.value, section='domainuser')
        if domain != "":  # change this to is not None when changes come from front end
            user_name = username + str(const['domainuser']) + domain

        logger.debug(f"Successfully created a username")
        return user_name

    except Exception as e:
        logger.error(f" Failed to return a username. Error: {e}. Inputs: username: {user_name}. domain: {domain}")
        return None


def specialchar_check(value):
    regex = re.compile('[-@_!#$%^&*()<>?/\|}{~:]')
    if (regex.search(value) == None):
        return True
    else:
        return False


def nullcheck_return_list_firstentry(out):
    if out in [None, []]:
        return None
    return out[0]


def nullcheck(out):
    if out in [None, []]:
        return None
    return out


def input_formatter(request):
    """
    changes input dictionary or list of dictionaries to convert everything to lower case. when dealing with api calls
    :param request: dictionary or list
    :return: None
    """
    try:
        if type(request) == dict:
            for key, value in request.items():

                if type(value) == list:
                    for item in value:
                        input_formatter(item)

                elif type(value) == dict:
                    input_formatter(value)

                elif type(value) == str:
                    request[key] = value.lower()

        elif type(request) == list:
            for items in request:
                input_formatter(items)
        elif type(request) == str:
            request.lower()

    except Exception as e:
        logger.error(f"Failed to convert strings to lower case for request: {request}. Error: {e}")


def json_formatter(request):
    """
    changes input dictionary or list of dictionaries to avoid 'TypeError: Object of type datetime is not JSON serializable' when dealing with api calls
    :param request: dictionary or list
    :return: None
    """
    try:
        if type(request) == dict:
            for key, value in request.items():

                if value == '':
                    value = None

                if type(value) == list:
                    for item in value:
                        json_formatter(item)

                elif type(value) == dict:
                    json_formatter(value)

                elif type(value) == Decimal:
                    request[key] = round(float(value), 2)

                elif type(value) == datetime:
                    request[key] = str(value.strftime(CreateInfra.us_datetime_format.value))

                elif type(value) != str and type(value) != int and type(value) != float and value is not None:
                    request[key] = str(value)

        elif type(request) == list:
            for items in request:
                json_formatter(items)

    except Exception as e:
        logger.error(f"Failed to JSON format request for {request}. Error: {e}. Input: request: {request}")


def powershell_to_json(items, info=None,logger_ip=None):
    try:
        outputList = []
        for item in items.split('\r\n\r\n'):
            if item.strip():
                itemJson = json.loads('{}')
                for itemDetails in item.split('\r\n'):
                    if itemDetails.strip() and ':' in itemDetails.strip():
                        key = itemDetails[0:itemDetails.index(':')]
                        value = itemDetails[itemDetails.index(':') + 1:]
                        itemJson[key.strip()] = value.strip()
                outputList.append(itemJson)
        return outputList
    except Exception as e:
        logger.error(logger_ip=logger_ip, message=f"Failed to convert powershell to json for {info}. Error: {e}. Inputs: items: {items}")


def powershell_to_json_raw_files_format(items):
    try:
        outputList = []
        key = ''
        value = ''
        i = 0
        for item in items.split('\r\n\r\n'):
            if item.strip() and len(item.strip()) > 0:
                itemJson = json.loads('{}')
                for itemDetails in item.split('\r\n'):
                    # print(f"item Details:::{itemDetails}")
                    if itemDetails.strip() and ' : ' in itemDetails.strip():
                        i = i + 1
                        key = itemDetails[0:itemDetails.index(' : ')]
                        value = itemDetails[itemDetails.index(' : ') + 3:]
                    else:
                        if i > 0 and len(itemDetails.strip()) > 0:
                            value = itemJson[key.strip()] + itemDetails.strip()
                    if len(key.strip()) > 0:
                        itemJson[key.strip()] = value
                if len(itemJson) > 0:
                    outputList.append(itemJson)
        return outputList
    except Exception as e:
        logger.error(f"Failed to convert powershell to json raw file format. Error: {e}. Inputs: items: {items}")


def windows_version(version):
    try:
        ver_list = ['2003', '2008', '2012', '2016', '2019', '7']
        for version_item in ver_list:
            if version_item in version:
                if version_item == '7':
                    return 'windows7'
                return version_item
        return "2008"  # if the version is not obtained then the deafult version of the powershells is being considered as "2008"'

    except Exception as e:
        logger.error(f"Failed to get windows version. Error: {e}. Inputs: {version}")


def linux_os(operatingsystem):
    try:
        os_list = ['centos5', 'centos6', 'centos7', 'rhel5', 'rhel6', 'rhel7', 'solaris10', 'solaris11', 'ubuntu14', 'ubuntu16', 'ubuntu18', 'oel5', 'aix7', 'aix6']
        for os_item in os_list:
            if os_item in operatingsystem:
                return os_item

        return "centos7"  # if the version is not obtained then the deafult version of the powershells is being considered as "2008"'

    except Exception as e:
        logger.error(f"Failed to get windows version. Error: {e}. Inputs: {operatingsystem}")


def simple_encrypt(message):
    """
    Accepts a string message and returns an encrypted message using pre-defined key.
    :param message: String. Any message to be encrypted.
    :return: encrypted message
    """
    try:
        logger.debug(f"Encrypting the input message")

        if message is not None:

            key = CreateInfra.LOG_ENCRYPTION_KEY.value
            cipher_suite = Fernet(key)
            encoded_text = cipher_suite.encrypt(str.encode(message))

            logger.debug(f"Encryption successful")
            return encoded_text.decode()

        else:
            raise Exception(f"Null input given")

    except Exception as e:
        logger.error(f"Encryption failed. Error: {e}")


def simple_decrypt(encrypted_message):
    """
    Accepts a string message and returns an decrypted message using pre-defined key.
    :param encrypted_message: String. Any message to be decrypted.
    :return: encrypted message
    """
    try:
        logger.debug(f"Decryption the input message")

        if encrypted_message is not None:

            encrypted_message = str.encode(encrypted_message)

            key = CreateInfra.LOG_ENCRYPTION_KEY.value
            cipher_suite = Fernet(key)
            decoded_text = cipher_suite.decrypt(encrypted_message)

            logger.debug(f"Decryption successful")
            return decoded_text.decode()

        else:
            raise Exception(f"Null input given")

    except Exception as e:
        logger.error(f"Decryption failed. Error: {e}")


def utilization_formatter(request, logger_ip=None):
    """
    changes input dictionary or list of dictionaries to replace '' with None
    :param request: dictionary or list
    :return: None
    """
    try:
        if type(request) == dict:
            for key, value in request.items():

                if value == '':
                    value = None

                if type(value) == list:
                    for item in value:
                        json_formatter(item)

    except Exception as e:
        logger.error(logger_ip=logger_ip, message=f"Failed to format the data being sent to utilization table. Error: {e}")



def update_db_obj(table_data):
    """

    """
    resp = []
    try:
        for i in table_data:
            i.pop('_sa_instance_state', None)
            resp.append(i)
    except Exception as e:
        logger.error(f"Exception occurred while updating db object. EXCEPTION {e} ")
    return resp

def date_time_formatter(req, platform, logger_ip=None):
    """
    formats the date format for the certificates to standard date format
    :param request: string
    :return: date format

    """
    try:
        logger.debug(f"Converting date time to standard format for the request {req}")

        if platform == "Windows":
            date_time_obj = datetime.datetime.strptime(req, '%m/%d/%Y %H:%M:%S %p')
        else:
            date_time_obj = datetime.datetime.strptime(req, '%b %d %H:%M:%S %Y %Z')
        return date_time_obj

    except Exception as e:
        logger.error(logger_ip=logger_ip,
                     message=f"Failed to format the date time for {req}. \nError: {e}")


def UI_format_1(backend_param):
    """
    this function converts the format "in_progress" to "In Progress"
    """
    try:
        backend_param = backend_param.replace("_"," ")
        UI_format= backend_param.title()

        return UI_format
    except Exception as e:
        logger.error(f"Exception occurred while converting backend format to UI required format. Exception : {e}")
        return None


def raw_reports_dir(discoveryname, loggerip):
    """

    """
    try:
        hosted_platform = platform.system()
        directory = None

        if hosted_platform == 'Linux':
            directory = CreateInfra.linux_raw_data_path.value + discoveryname + "/" + loggerip
        if hosted_platform == 'Windows':
            directory = CreateInfra.win_raw_data_path.value + discoveryname + "\\" + loggerip

        create_dir_file(directory)
        return directory, hosted_platform
    except Exception as e:
        logger.error(f"Exception occurred while crating directory for RAW files. Exception : {e}")
        return None