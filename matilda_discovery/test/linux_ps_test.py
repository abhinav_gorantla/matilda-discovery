from matilda_discovery.connector import cmd_handler
from matilda_discovery.utils.util import get_script, shell_to_json, shell_to_jsonlist
from pprint import pprint


def linux_test(req):
       try:
              connector = cmd_handler.CMDHandler(req)

              ssh, out, flag, linux_error = connector.ssh_connect()
              print(f"LINUX EXCEPTION {linux_error}")
              pprint(f"LINUX VERSION : {out[0]}")

              script = "services"

              resp = get_script(name=script, operatingsystem=out[0])
              raw_resp, err = connector.execute_shell(resp, cmd_info=None, logger_ip=None)
              print("\nRAW RESPONSE \n")
              print(raw_resp)

              if script in ['packages', 'ports']:
               final_response = shell_to_jsonlist(raw_resp, info=None, logger_ip=None)

              elif script in ['host', 'memory', 'cpu']:
               final_response = shell_to_json(raw_resp, info=None, logger_ip=None)[0]

              else:
               final_response = shell_to_json(raw_resp, info=None, logger_ip=None)

              print("\nAFTER SHELL TO JSON CONVERSION \n")
              pprint(final_response)
       except Exception as e:
              print(f"EXCEPTION IN LINUX TEST CLASS {e}")


linux_test(req=dict(current_ip='192.168.20.204', username='root', password='openstack'))
