import json
#import pytest
import requests


def test_dashboard_get():
    url = 'http://localhost:5010/matilda/host_discovery/dashboard/summary'
    response = requests.get(url)
    assert response.status_code == 200

def test_dashboard_put():
    url = 'http://localhost:5010/matilda/host_discovery/dashboard/summary?'
    response = requests.post(url, data=json.dumps({'account_id':1,'task_id':2,'status':"Start"}))
    assert response.status_code == 201