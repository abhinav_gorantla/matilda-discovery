import paramiko
import re

def ssh_connect(req):
    try:
        ssh = paramiko.SSHClient()
        ssh.load_system_host_keys()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(req.get('ip') , username=req.get('username'), password=req.get('password'), allow_agent=False)

        out, err = execute_shell(ssh, cmd="cat /etc/os-release")
        print(f"HOST DETAILS \n {out}")
        if any("Unable to get valid context" in s for s in out) or not out:
            error_msg = "Unable to reach"
            print(f"UNABLE TO GET VALID CONTEXT")
            return ssh, out, error_msg
    except paramiko.ssh_exception.AuthenticationException as e:
        error_msg = "Authentication failure"
        print(f"ERROR {error_msg} \n ACTUAL ERROR {e}")
    except TimeoutError as e:
        error_msg = "Unable to reach"
        print(f"ERROR {error_msg} \n ACTUAL ERROR {e}")
    except paramiko.ssh_exception.NoValidConnectionsError as e:
        error_msg = "Unable to reach"
        print(f"ERROR {error_msg} \n ACTUAL ERROR {e}")
    except Exception as e:
        error_msg = "Unable to reach"
        print(f"ERROR {error_msg} \n ACTUAL ERROR {e}")
    return

def execute_shell(ssh, cmd):
    output = []
    try:
        (ssh_stdin, ssh_stdout, ssh_stderr) = ssh.exec_command(cmd)
        for line in ssh_stdout.readlines():
            ansi_escape = re.compile(r'(\x9B|\x1B\[)[0-?]*[ -/]*[@-~]')
            output.append(re.sub('[\n"]', '', line).strip())
        return output, None
    except Exception as e:
        print(f"EXCEPTION IN EXECUTING SCRIPT {e}")

    return output, None

ssh_connect(req=dict(ip='192.168.20.204', username='root', password='openstack'))