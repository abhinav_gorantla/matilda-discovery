import winrm
from winrm.exceptions import InvalidCredentialsError

def windows_session():
    ip = ''
    username = 'Administrator'
    password = ''
    flag = False
    error_msg = None
    acctual_error = None
    host_info = ""
    http = "https://"
    ws = "/wsman"
    port = "5986"
    win_host_details = 'Get-WmiObject Win32_OperatingSystem | Format-List Caption, Version, TotalVisibleMemorySize'

    url = http + ip + ':' + port + ws
    try:
        print(f"Establishing Remote Login of the Host: {ip}")
        session = winrm.Session(url, auth=(username, password),
                                server_cert_validation='ignore')
        host_resp = session.run_ps(win_host_details)
        host_info = host_resp.std_out.decode()
        print(f"HOST INFO {host_info}")
        flag = True
    except winrm.exceptions.InvalidCredentialsError as e:
        print(f"Invalid Credentials of the Host: {ip} \n EXCEPTION {e} ")
        error_msg = "Authentication Error"
        acctual_error = e
    except ConnectionError as e:
        print(f"Unable to connect to the Host: {ip} \n EXCEPTION {e} ")
        error_msg = "Unable to connect to Host"
        acctual_error = e
    except winrm.exceptions.WinRMTransportError as e:
        print(f"Windows Bad HTTP Response returned from the Host: {ip} \n EXCEPTION {e} ")
        error_msg = "Unable to connect to Host"
        acctual_error = e
    except Exception as e:
        print(f"Unable to connect to the Host: {ip} \n EXCEPTION {e}")
        acctual_error = e
    return host_info, flag, error_msg, acctual_error


host_info, flag, error_msg, acctual_error = windows_session()
print('flag: ', flag)
print('error_msg: ', error_msg)


