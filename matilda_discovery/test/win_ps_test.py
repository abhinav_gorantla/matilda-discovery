from matilda_discovery.connector import cmd_handler
from matilda_discovery.utils import util
from matilda_discovery.services.discovery.windows import Windows
from pprint import pprint


def win_test(req):
    try:
        connector = cmd_handler.CMDHandler(req)

        session, host_resp, winflag, win_error = connector.windows_session()

        print(f"WINDOWS ERROR {win_error}\n")
        print(f"WINDOWS VERSION {connector.version}\n")

        script = "host"

        win = Windows(connector)

        response = win.execute_powershell_script(cmd=util.get_script(name=script, version=connector.version), info=script, logger_ip=None, )[0]

        print(" FINAL RESPONSE\n")

        pprint(response)

    except Exception as e:
        print(f"EXCEPTION {e}")


win_test(req=dict(current_ip='', username='', password=''))