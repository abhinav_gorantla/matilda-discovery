from matilda_discovery.utils import util
from matilda_discovery.db import queries_helper
from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db.connection import get_engine


class GetPswd():

    def __init__(self):
        self.engine = get_engine()

    def get_pass(self, req):
        try:

            cred_sql = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section="get_crerds")

            query_response = queries_helper.execute_report_queries(cred_sql, self.engine, info="inventory_report", host_id=req.get('host_id'))

            data = query_response[0].get('query_data')[0]

            if data:

                pswd = util.simple_decrypt(data.get('password'))

                print(f"U : {data.get('username')} \nP : {pswd}")

        except Exception as e:
            print(f"EXCEPTION {e}")


a = GetPswd()

a.get_pass(req=dict(host_id=''))
