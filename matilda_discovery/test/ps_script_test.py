from matilda_discovery.connector import cmd_handler
from matilda_discovery.services.discovery.windows import Windows
from matilda_discovery.services.service_discovery.initiate_service_discovery import initiate_service_discovery
from matilda_discovery.utils import util

req = {'task_id': '1', 'cloud_id': '',
       'current_ip':'54.218.200.181',
       'host_id':'1',
       'username': 'Administrator',
       'version': '',
       'password': 'M@t!ld@32!',
       'name': 'discoveryname',
       'excution_mode': '',
       'scheduled_time': ''}


connector = cmd_handler.CMDHandler(req)
session, host_resp, winflag, win_error, e = connector.windows_session()
print(winflag)

tem = initiate_service_discovery(connector, servicetype='weblogic', path='//path/', platform='Windows', logger_ip=None)
print(tem)


discovery_object = Windows(connector)
#arguments should be the file paths('/')
response = connector.execute_power_shell(cmd=util.get_script(name='iis_version', version=connector.version,arguments=None), cmd_info='test script execution', logger_ip=req.get('current_ip'))
print(response)
result = util.powershell_to_json(response)
print(result)
