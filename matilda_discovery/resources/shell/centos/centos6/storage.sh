#!/usr/bin/env bash


for i in $(cat /etc/fstab  |grep -Ev "^#|swap|^none|pts|sys|proc" |awk '{print $2}' |sed '/^$/d')

do

if [[ $i != '' ]];then


  file_system=$(df -Ph |grep -w $i  | head -1 |awk '{print $1}')

  total=$(df -Ph |grep -w $i  | head -1 |awk '{print $2}')

  used=$(df -Ph |grep -w $i  | head -1 |awk '{print $3}')

  available=$(df -Ph |grep -w $i  | head -1 |awk '{print $4}')

  used_percent=$(df -Ph |grep -w $i  | head -1 |awk '{print $5}')

  mounted=$(df -Ph |grep -w $i  | head -1 |awk '{print $6}')



echo -e "{\"'file_system'\":\"'"$file_system"'\",\"'total'\":\"'"$total"'\",\"'used'\":\"'"$used"'\",\"'available'\":\"'"$available"'\",\"'used_percent'\":\"'"$used_percent"'\",\"'mounted'\":\"'"$mounted"'\"}"
fi
  done