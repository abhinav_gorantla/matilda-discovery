#!/usr/bin/env bash

ps aux  > /tmp/jj1
sed 1d  /tmp/jj1 >  /tmp/jj2

while IFS= read -r line;

do
USER=$(echo "$line"|awk  '{print $1}')
PID=$(echo "$line"|awk  '{print $2}')
CPU_percentage=$(echo "$line"|awk  '{print $3}')
MEM_Percentage=$(echo "$line"|awk  '{print $4}')
VSZ=$(echo "$line"|awk  '{print $5}')
RSS=$(echo "$line"|awk  '{print $6}')
TTY=$(echo "$line"|awk  '{print $7}')
STAT=$(echo "$line"|awk  '{print $8}')
START=$(echo "$line"|awk  '{print $9}')
TIME=$(echo "$line"|awk  '{print $10}')
COMMAND=$(echo "$line"|awk  '{print $11}')



    echo -e "{\"'user'\":\"'"$USER"'\",\"'process_id'\":\"'"$PID"'\",\"'cpu_percent'\":\"'"$CPU_percentage"'\",\"'memory_percent'\":\"'"$MEM_Percentage"'\",\"'vsz'\":\"'"$VSZ"'\",\"'rss'\":\"'"$RSS"'\",\"'tty'\":\"'"$TTY"'\",\"'stat'\":\"'"$STAT"'\",\"'start_time'\":\"'"$START"'\",\"'total_processor_time'\":\"'"$TIME"'\",\"'command'\":\"'"$COMMAND"'\"}"



done < /tmp/jj2