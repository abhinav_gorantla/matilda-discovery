#!/usr/bin/env bash
netstat -anlpt 2>/dev/null | grep -E "ESTABLISHED|LISTEN" |grep -v "127.0.0.1" |sed 's/*/any_port/g' |sed 's/::/0.0.0.0/g' > /tmp/cp11

awk -F':' '$1=$1' OFS="\t " /tmp/cp11 > /tmp/cp12

awk -F"/" '$1=$1' OFS="\t" /tmp/cp12 > /tmp/cp13

sed  -i '1i Recv-Q Send-Q Local Address localport Foreign Address foreignport State PID Program name' /tmp/cp13


awk '{print " "$1" "$2" "$3" "$4" "$5" "$6" "$7" "$8" "$9" "$10" "$11" "}' /tmp/cp13 > /tmp/cp14


awk 'NR==1 {    for (i=1; i<=NF; i++) {        f[$i] = i    }}{ print $(f["Proto"]), $(f["recv_queue"]), $(f["send_queue"]), $(f["local_address"]), $(f["number"]),  $(f["foreign_address"]), $(f["foreign_port"]), $(f["status"]), $(f["process_id"]), $(f["service"]) }' /tmp/cp14 > /tmp/cp15

awk '{if(NR>1)print}' /tmp/cp15 >/tmp/cp16

awk ' { printf "\"protocol\":\"" $1  "\",\"recv_queue\":\"" $2  "\",\"send_queue\":\"" $3  "\",\"local_address\":\"" $4  "\",\"number\":\"" $5  "\",\"foreign_address\":\"" $6  "\",\"foreign_port\":\"" $7  "\",\"status\":\"" $8  "\",\"process_id\":\"" $9  "\",\"service\":\"" $10  "\" ||" } ' /tmp/cp16



