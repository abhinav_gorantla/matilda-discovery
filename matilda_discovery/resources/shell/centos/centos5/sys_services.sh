#!/usr/bin/env bash

/sbin/chkconfig --list 2>/dev/null | {

while IFS= read -r line;

do
system_service_name=$(echo "$line"|awk  '{print $1}')
cpu_usage=$(ps aux |grep -w $system_service_name |grep -v grep |head -1 |awk '{print $3}')
memory_usage=$(ps aux |grep -w $system_service_name |grep -v grep| head -1 |awk '{print $4}')
port_number=`netstat -anlp |grep -w $system_service_name |grep -w "LISTEN" |grep -v grep  |head -1  |awk  '{print $4}'| awk -F ":" '{print $2}'`


echo -e "{\"'system_service_name'\":\"'"$system_service_name"'\",\"'cpu_usage'\":\"'"$cpu_usage"'\",\"'memory_usage'\":\"'"$memory_usage"'\",\"'port_number'\":\"'"$port_number"'\"}"



done

}
