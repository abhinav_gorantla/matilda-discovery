#!/usr/bin/env bash

#L/LK - password locked
#N/NP - no password
#P/PS - password set

for i  in $(cut -d: -f1,3 /etc/passwd | egrep ':[0-9]{4}$' | cut -d: -f1);do

mygroup=$(id -nG $i)


echo -e "{\"'username'\":\"'" $i  "'\",\"'group_name'\":\"'" ${mygroup#*:} "'\",\"'home_directory'\":\"'" $(grep -w $i /etc/passwd | cut -d ":" -f6)  "'\",\"'user_id'\":\"'" $(grep -w $i /etc/passwd | cut -d ":" -f3)  "'\",\"'password_expiry_date'\":\"'" $(chage -l $i |grep "Password expires" |awk '{print $4" "$5" "$6}')  "'\",\"'last_login'\":\"'" $(last $i |head -1 |awk '{print $3" "$4" "$5" "$6" "$7}')  "'\",\"'last_password_change'\":\"'" $(chage -l $i |grep "Last password change" |awk '{print $5" "$6" "$7}') "'\",\"'account_expiry_date'\":\"'" $(chage -l $i |grep "Account expires" |awk '{print $4" "$5" "$6}')  "'\",\"'min_days_for_pwd_change'\":\"'" $(chage -l $i |grep "Minimum number of days between password change" |awk '{print $9}')  "'\",\"'max_days_for_pwd_change'\":\"'" $(chage -l $i |grep "Maximum number of days between password change" |awk '{print $9}')  "'\",\"'warning_days_for_pwd_change'\":\"'" $(chage -l $i |grep "Number of days of warning before password expires" |awk '{print $10}')  "'\",\"'password_inactive'\":\"'" $(chage -l $i |grep "Password inactive" |awk '{print $4" "$5" "$6}')  "'\",\"'password_status'\":\"'" $(passwd -S $i |awk '{print $2}')  "'\"}"

done