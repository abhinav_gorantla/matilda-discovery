#!/usr/bin/env bash

yum list installed > /tmp/cp1

tail -n +3 /tmp/cp1 > /tmp/cp2

sed  -i '1i package version  repository' /tmp/cp2

awk '{print " "$1" "$2" "$3"  "}' /tmp/cp2 > /tmp/cp3

awk 'NR==1 {    for (i=1; i<=NF; i++) {        f[$i] = i    }}{ print $(f["name"]), $(f["version"]), $(f["repository"]) }' /tmp/cp3 > /tmp/cp4

awk '{if(NR>1)print}' /tmp/cp4 >/tmp/cp5


awk ' { printf "\"name\":\"" $1  "\",\"version\":\"" $2 "\",\"repository\":\"" $3 "\" || "} ' /tmp/cp5

