#!/usr/bin/env bash

for i in $(sudo ifconfig -a |grep -v IPv6 | awk  '$1>0 {print $1}' |sed "s/:$//")


do

if [ $i == lo ]; then


echo -e "{\"'name'\":\"'"$i"'\",\"'ip_address'\":'"$(sudo ifconfig lo 2>/dev/null |grep inet |grep -v inet6 |awk '{print $2}')"',\"'packet_max_size'\":'"$(sudo ifconfig lo 2>/dev/null |grep mtu | awk '{print $NF}')"',\"'ip_subnet'\":'"$(sudo ifconfig lo 2>/dev/null |grep inet |grep -v inet6 |awk '{print $4}')"',\"'dns_server'\":'"$(cat /etc/resolv.conf  2>/dev/null |grep -i '^nameserver'|head -n1|cut -d ' ' -f2)"',\"'received_packets'\":'"$(sudo ifconfig lo 2>/dev/null |grep -i RX |head -1 |awk '{print $3}')"',\"'type'\":'"$(ls -lrt /sys/class/net/  2>/dev/null |grep lo |awk ' {print $11}' |awk -F "/" ' {print $4}')"',\"'Flg'\":'"$(sudo ifconfig -s lo 2>/dev/null |grep -v Flg |awk '{print $11}')"',\"'sent_packets'\":'"$(sudo ifconfig lo 2>/dev/null |grep  TX |head -1 |awk '{print $3}')"'}"
fi

if [ $i != lo ]; then

abc=$(sudo ifconfig $i 2>/dev/null |grep inet |awk '{print $2}')

if [[ $abc != '' ]];then

test=`sudo ifconfig $i 2>/dev/null |head -1 |awk '{print $1}' |sed "s/:$//" |cut -d: -f 1`


echo -e "{\"'name'\":\"'"$i"'\",\"'ip_address'\":'"$(sudo ifconfig $i 2>/dev/null |grep inet |grep -v inet6 |awk '{print $2}')"',\"'mac_address'\":'"$(sudo ifconfig $i 2>/dev/null |grep ether |awk '{print $2}')"',\"'dhcp_enabled'\":'"$(cat  /etc/sysconfig/network-scripts/ifcfg-$i  2>/dev/null |grep BOOTPROTO |awk -F "=" '{print $2}')"',\"'packet_max_size'\":'"$(sudo ifconfig $i 2>/dev/null |grep mtu | awk '{print $NF}')"',\"'speed'\":'"$(ethtool $i 2>/dev/null |grep Speed |awk '{print $2}')"',\"'ip_subnet'\":'"$(sudo ifconfig $i 2>/dev/null |grep inet |grep -v inet6 |awk '{print $4}')"',\"'dns_server'\":'"$(cat /etc/resolv.conf  2>/dev/null |grep -i '^nameserver'|head -n1|cut -d ' ' -f2)"',\"'received_packets'\":'"$(sudo ifconfig $i 2>/dev/null |grep -i RX |head -1 |awk '{print $3}')"',\"'type'\":'"physical"',\"'Flg'\":'"$(sudo ifconfig -s $i 2>/dev/null |grep -v Flg |awk '{print $11}')"',\"'sent_packets'\":'"$(sudo ifconfig $i 2>/dev/null |grep  TX |head -1 |awk '{print $3}')"'}"


fi

fi

done
