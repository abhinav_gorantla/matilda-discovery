#!/usr/bin/env bash


var1=`find /etc/certs -name "*"  -type f ! -size 0 2>&1 |egrep -v "tmp|backup|bkp|docker|WinSxS|Permission|HISTORY|alternatives|bearer11ssl|manifest"`

for loc1 in $var1

do

test=`/usr/sfw/bin/openssl x509 -in "$loc1" -text -noout 2>/dev/null`

 if [ "$test" != "" ];then

   path=`echo "$loc1"`
   certificate_name=`echo "$loc1" |awk -F/ '{ print $NF }'`
   not_after=`/usr/sfw/bin/openssl x509 -in "$loc1"  -text -noout 2>/dev/null |grep -w "Not After :" |head -1 |awk '{print $4" "$5" "$6" "$7" "$8}'`
   not_before=`/usr/sfw/bin/openssl x509 -in "$loc1"  -text -noout 2>/dev/null |grep -w "Not Before:" | head -1 |awk '{print $3" "$4" "$5" "$6" "$7}'`
   serial_number=`/usr/sfw/bin/openssl x509 -in "$loc1"  -text -noout 2>/dev/null |awk '/'"Serial Number:"'/{l=1;count=NR;next} l>0 && NR-count < '"1"+1' {print}'`
   issuer=`/usr/sfw/bin/openssl x509 -in "$loc1"  -text -noout 2>/dev/null |grep -w "Issuer:" |sed ' s/[^:]*: *//;'`



echo  "{\"'path'\":\"'"$path"'\",\"'certificate_name'\":\"'"$certificate_name"'\",\"'not_after'\":\"'"$not_after"'\",\"'not_before'\":\"'"$not_before"'\",\"'serial_number'\":\"'"$serial_number"'\",\"'issuer'\":\"'"$issuer"'\"}"
fi
done
