#!/usr/bin/env bash
netstat -aun -P tcp |egrep "LISTEN|IDLE|ESTABLISHED" |grep -v ::1 |grep -v "*"  | {



while IFS= read -r line

  do

  local_address=$(echo "$line"|awk '{print $1}' |cut -d "." -f1-4)

  number=$(echo "$line"|awk '{print $1}' |cut -d "." -f5)

  foreign_address=$(echo "$line"|awk '{print $2}' |cut -d "." -f1-4)

  foreign_port=$(echo "$line"|awk '{print $2}' |cut -d "." -f5)

  status=$(echo "$line"|awk '{print $10}')

  process_id=$(echo "$line"|awk '{print $4}')

  recv_queue=$(echo "$line"|awk '{print $9}')

  send_queue=$(echo "$line"|awk '{print $7}')

  program=$(echo "$line"|awk '{print $5}')

  user=$(echo "$line"|awk '{print $3}')







echo -e "{\"'local_address'\":\"'"$local_address"'\",\"'number'\":\"'"$number"'\",\"'foreign_address'\":\"'"$foreign_address"'\", \"'foreign_port'\":\"'"$foreign_port"'\", \"'status'\":\"'"$status"'\", \"'process_id'\":\"'"$process_id"'\", \"'recv_queue'\":\"'"$recv_queue"'\", \"'send_queue'\":\"'"$send_queue"'\", \"'program'\":\"'"$program"'\", \"'user'\":\"'"$user"'\"}"



  done



}
