#!/usr/bin/env bash

Total=`prtconf | grep Memory |awk '{print $3}'`
Free=`echo $(($(vmstat | awk 'NR==3{print$5}') / 1024))`
Used=`echo "$(($Total-$Free))"`

used_percent=`echo "($Used*100/$Total)" | bc -l|cut -c1-5`

echo -e "{\"'total'\":\"'"$Total"'\",\"'free'\":\"'"$Free"'\",\"'used'\":\"'"$Used"'\",\"'used_percent'\":\"'"$used_percent"'\",\"'available'\":\"'"$Free"'\"}"
