#!/usr/bin/env bash
Total=`prtconf | grep Memory |awk '{print $3}'`
Free=`echo $(($(vmstat | awk 'NR==3{print$5}') / 1024))`
Used=`echo "$(($Total-$Free))"`
memory_percent=`echo "$(($Used/100))"`
cpu_percent=`iostat -c | tail -1 | awk '{print 100- $4}'`
total_storage=`df -kl |grep -v tmpfs |sed 1d | awk '{ total = total + $2 } END { print  total/1024/1024 }'`
used_Storage=`df -kl |grep -v tmpfs |sed 1d | awk '{ total = total + $3 } END { print  total/1024/1024 }'`
storage_percent=`echo "scale=2 ; $used_Storage / $total_storage * 100" | bc`


echo -e "\
            {\
                'memory_percent':'$memory_percent',\
                'cpu_percent':'$cpu_percent',\
                'storage_percent':'$storage_percent'\
            }"


