#!/usr/bin/env bash

last | egrep -v "crash|reboot" | head -50 | while  read  line;

  do

  user_name=`echo "$line"|awk '{print $1}'`

  server_ip=`echo "$line"|awk '{print $3}'`

  time=`echo "$line"|awk '{print $5" "$6" "$7}'`



echo  "{\"'user_name'\":\"'"$user_name"'\",\"'server_ip'\":\"'"$server_ip"'\",\"'time'\":\"'"$time"'\"}"

done

