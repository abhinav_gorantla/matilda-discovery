#!/usr/bin/env bash

Platform=`cat /etc/release |head -1|awk '{print  $2}'`
OperatingSystem=`cat /etc/release |head -1|awk '{print $2}'`
ReleaseVersion=`cat /etc/release |head -1| awk  '{print $3}'`
Version=`uname -r`
BootUpDate=`date '+%Y-%m-%d %H:%M:%S'`
/usr/sbin/prtconf |grep "Memory size:" |awk '{print $3}' > file.log
MemSize=`awk '{ printf "%.2f\n", $1/1024; }' file.log`
Hostname=`hostname`
#InstanceType=`/usr/sbin/prtdiag -v |grep "System Configuration" |awk  '{print $3}'`
instance1=`cat /etc/release |head -1  |awk '{print $NF}'`
instance2=`zonename`

if [ "$instance1" = X86 -o "$instance2" != global ]; then
InstanceType=Virtual
else
InstanceType=Physical
fi
ServiceProvider=`cat /etc/release |head -1|awk '{print $1}'`


Instruction_Set=`isainfo -kv 2>/dev/null |head -1 |awk '{print $1}'`
BIOS_Configuration=`/usr/sbin/prtdiag -v 2>/dev/null | grep "System Configuration" |awk '{print $3" "$4}'`
Pagefile_Size=`getconf PAGE_SIZE | awk '{$1=$1/1024; print $1,"KB";}'`

Internet1=`ping -c1 google.com |awk '{print $3}'`
if [ "$Internet1" = alive ]; then
Internet_Access=True
else
Internet_Access=False
fi

Timezone=`date +"%Z %z" |awk '{print $1}'`
Installed_On=`ls -l /var/sadm/system/logs/install_log 2>/dev/null |awk '{print $6 " " $7" " $8}'`
Uptime=`uptime |awk '{print $3" "$4}' | sed 's/.$//'`



echo  "{\"'platform'\":\"'"$Platform"'\",\"'instance_type'\":\"'"$InstanceType"'\",\"'service_provider'\":\"'"$ServiceProvider"'\",\"'operating_system'\":\"'"$OperatingSystem"'\",\"'release_version'\":\"'"$ReleaseVersion"'\",\"'version'\":\"'"$Version"'\",\"'system_date'\":\"'"$BootUpDate"'\",\"'memory'\":\"'"$MemSize"'\",\"'name'\":\"'"$Hostname"'\",\"'instruction_set'\":\"'"$Instruction_Set"'\",\"'bios_configuration'\":\"'"$BIOS_Configuration"'\",\"'pagefile_size'\":\"'"$Pagefile_Size"'\",\"'internet_access'\":\"'"$Internet_Access"'\",\"'timezone'\":\"'"$Timezone"'\",\"'installed_on'\":\"'"$Installed_On"'\",\"'uptime'\":\"'"$Uptime"'\"}"
