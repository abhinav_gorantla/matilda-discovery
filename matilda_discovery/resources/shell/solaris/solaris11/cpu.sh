#!/usr/bin/env bash

Cpu_Percentage=`iostat -c | tail -1 | awk '{print 100- $4}'`
Cpu_Count=`psrinfo -p`
Vcpu_Count=`psrinfo |wc -l`
Cpu_Cores=`kstat cpu_info|grep -i module |wc -l`
Cpu_Model_Name=`psrinfo -pv | awk 'NR==3 {$2 = $2; print $0;}'`



echo -e "{\"'utilization_percent'\":\"'"$Cpu_Percentage"'\",\"'physical_processors'\":\"'"$Cpu_Count"'\",\"'physical_cores'\":\"'"$Cpu_Cores"'\",\"'logical_processors'\":\"'"$Vcpu_Count"'\",\"'model_name'\":\"'"$Cpu_Model_Name"'\"}"
