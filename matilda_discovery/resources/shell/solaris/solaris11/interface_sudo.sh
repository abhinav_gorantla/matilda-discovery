#!/usr/bin/env bash
for i in $(sudo ifconfig -a |grep -v IPv6 | awk -F: '$1>0 {print $1}')


do

if [ $i == lo0 ]; then

echo -e "{\"'InterfaceName'\":\"'"$i"'\",\"'ip_address'\":'"$(sudo ifconfig $i |grep inet |awk '{print $2}')"',\"'description'\":\"'its a loopback address'\",\"'packet_max_size'\":'"$(sudo ifconfig $i |grep mtu | awk '{print $4}')"',\"'type'\":'"$(ipadm show-addr $i | grep -v  /v6 |tail -1 |awk '{print $2}')"'}"

fi

if [ $i != lo0 ]; then

#echo -e "{\"'InterfaceName'\":\"'"$i"'\",\"'ip_address'\":'"$(sudo ifconfig $i |grep inet |awk '{print $2}')"',\"'mac_address'\":'"$(sudo ifconfig $i |grep ether |awk '{print $2}')"',\"'description'\":\"'A broadcast address has been set for this interface.'\",\"'packet_max_size'\":'"$(sudo ifconfig $i |grep mtu | awk '{print $4}')"',\"'received_packets'\":'"$(sudo ifconfig $i |grep -i RX |head -1 |awk '{print $3}')"',\"'sent_packets'\":'"$(sudo ifconfig $i |grep  TX |head -1 |awk '{print $3}')"',\"'speed'\":'"$(sudo ethtool $i |grep Speed |awk '{print $2}')"'}"
echo -e "{\"'InterfaceName'\":\"'"$i"'\",\"'ip_address'\":'"$(sudo ifconfig $i |grep inet |awk '{print $2}')"',\"'mac_address'\":'"$(sudo ifconfig $i |grep ether |awk '{print $2}')"',\"'description'\":\"'A broadcast address has been set for this interface.'\",\"'packet_max_size'\":'"$(sudo ifconfig $i |grep mtu | awk '{print $4}')"',\"'type'\":'"$(ipadm show-addr $i | grep -v  /v6 |tail -1 |awk '{print $2}')"',\"'speed'\":'"$(dladm show-phys |grep $i |awk '{print $4}')"'}"

fi

done