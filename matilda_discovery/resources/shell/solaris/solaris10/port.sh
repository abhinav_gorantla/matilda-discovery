#!/usr/bin/env bash

netstat -an |egrep   "LISTEN|ESTABLISHED" |grep -v ::1 |grep -v tcp6 | grep -v 127.0.0.1 |sed 's/*/any_interface/g'  | {

while  read  line

  do
test=`echo "$line"|awk '{print $2}' |cut -d "." -f1`

if [ "$test" = "any_interface" ];then


  local_address=`echo "$line"|awk '{print $1}' |cut -d "." -f1`

  number=`echo "$line" |awk '{print $1}' |cut -d "." -f2`

  foreign_address=`echo "$line"|awk  '{print $2}' | cut -d "." -f1`

  foreign_port=`echo "$line"|awk  '{print $2}' | cut -d "." -f2`

  status=`echo "$line"|awk '{print $7}'`
  recv_queue=`echo "$line"|awk '{print $6}'`
  send_queue=`echo "$line"|awk '{print $4}'`


echo  "{\"'local_address'\":\"'"$local_address"'\",\"'number'\":\"'"$number"'\",\"'foreign_address'\":\"'"$foreign_address"'\", \"'foreign_port'\":\"'"any_port"'\", \"'status'\":\"'"$status"'\", \"'recv_queue'\":\"'"$recv_queue"'\", \"'send_queue'\":\"'"$send_queue"'\"}"

else

 local_address=`echo "$line"|awk  '{print $1}' |cut -d "." -f1-4`

  number=`echo "$line" |awk  '{print $1}'|cut -d "." -f5`

  foreign_address=`echo "$line" |awk  '{print $2}' | cut -d "." -f1-4`

  foreign_port=`echo "$line" |awk  '{print $2}' | cut -d "." -f5`

  status=`echo "$line"|awk '{print $7}'`
  recv_queue=`echo "$line"|awk '{print $6}'`
  send_queue=`echo "$line"|awk '{print $4}'`

echo  "{\"'local_address'\":\"'"$local_address"'\",\"'number'\":\"'"$number"'\",\"'foreign_address'\":\"'"$foreign_address"'\", \"'foreign_port'\":\"'"$foreign_port"'\", \"'status'\":\"'"$status"'\", \"'recv_queue'\":\"'"$recv_queue"'\", \"'send_queue'\":\"'"$send_queue"'\"}"

fi


  done

}
