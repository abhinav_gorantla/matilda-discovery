#!/usr/bin/env bash

Cpu_Percentage=`iostat -c 2>/dev/null  | tail -1 | awk '{print 100- $4}'`
Cpu_Count=`/usr/sbin/psrinfo -p 2>/dev/null`
Vcpu_Count=`/usr/sbin/psrinfo 2>/dev/null |wc -l`
Cpu_Cores=`kstat cpu_info|grep -i module |wc -l`
Cpu_Model_Name=`/usr/sbin/psrinfo -pv 2>/dev/null | awk 'NR==2 {$2 = $2; print $0;}' |awk '{print $1}'`


echo "{\"'utilization_percent'\":\"'"$Cpu_Percentage"'\",\"'physical_processors'\":\"'"$Cpu_Count"'\",\"'physical_cores'\":\"'"$Cpu_Cores"'\",\"'logical_processors'\":\"'"$Vcpu_Count"'\",\"'model_name'\":\"'"$Cpu_Model_Name"'\"}"

