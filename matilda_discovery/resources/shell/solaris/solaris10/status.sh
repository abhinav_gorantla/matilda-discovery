#!/usr/bin/env bash


if [ -r /usr/ucb/ps  ]
then
 nginx_pid=`ps -ef |grep nginx  |grep -v grep |head -1  |awk '{print $2}'`
 if [ "nginx_pid" != "" ];then
nginx_cpu=`/usr/ucb/ps aux 2>/dev/null |grep -w $nginx_pid |grep -v grep |awk '{print $3}'`
nginx_memory=`/usr/ucb/ps aux 2>/dev/null |grep -w $nginx_pid |grep -v grep |awk '{print $4}'`
nginx_port=`pfiles $nginx_pid 2> /dev/null | grep -i "port" > /dev/null 2>&1`
echo  "{\"'service'\":\"'"nginx"'\",\"'cpu'\":\"'"$nginx_cpu"'\",\"'memory'\":\"'"$nginx_memory"'\",\"'port'\":\"'"$nginx_port"'\"}"
fi


else

nginx_pid=`ps -ef |grep nginx  |grep -v grep |head -1  |awk '{print $2}'`
if [ "nginx_pid" != "" ];then
nginx_cpu=`ps aux 2>/dev/null |grep -w $nginx_pid |grep -v grep |awk '{print $3}'`
nginx_memory=`ps aux 2>/dev/null |grep -w $nginx_pid |grep -v grep |awk '{print $4}'`
nginx_port=`pfiles $nginx_pid 2> /dev/null | grep -i "port" > /dev/null 2>&1`
echo  "{\"'service'\":\"'"nginx"'\",\"'cpu'\":\"'"$nginx_cpu"'\",\"'memory'\":\"'"$nginx_memory"'\",\"'port'\":\"'"$nginx_port"'\"}"
fi


fi

if [ -r /usr/ucb/ps  ]
then
   mysql_pid=`ps -ef |grep mysql  |grep -v grep |head -1  |awk '{print $2}'`
if [ "$mysql_pid" != "" ];then
mysql_cpu=`/usr/ucb/ps aux 2>/dev/null |grep -w $mysql_pid |grep -v grep |awk '{print $3}'`
mysql_memory=`/usr/ucb/ps aux 2>/dev/null |grep -w $mysql_pid |grep -v grep |awk '{print $4}'`
mysql_port=`pfiles $mysql_pid 2> /dev/null | grep -i "port" > /dev/null 2>&1`
echo "{\"'service'\":\"'"mysql"'\",\"'cpu'\":\"'"$mysql_cpu"'\",\"'memory'\":\"'"$mysql_memory"'\",\"'port'\":\"'"$mysql_port"'\"}"
fi


else

mysql_pid=`ps -ef |grep mysql  |grep -v grep |head -1  |awk '{print $2}'`
if [ "$mysql_pid" != "" ];then
mysql_cpu=`ps aux 2>/dev/null |grep -w $mysql_pid |grep -v grep |awk '{print $3}'`
mysql_memory=`ps aux 2>/dev/null |grep -w $mysql_pid |grep -v grep |awk '{print $4}'`
mysql_port=`pfiles $mysql_pid 2> /dev/null | grep -i "port" > /dev/null 2>&1`
echo  "{\"'service'\":\"'"mysql"'\",\"'cpu'\":\"'"$mysql_cpu"'\",\"'memory'\":\"'"$mysql_memory"'\",\"'port'\":\"'"$mysql_port"'\"}"
fi


fi


if [ -r /usr/ucb/ps  ]
 then
   tomcat_pid=`ps -ef |grep tomcat  |grep -v grep |head -1  |awk '{print $2}'`
if [ "$tomcat_pid" != "" ];then
tomcat_cpu=`/usr/ucb/ps aux 2>/dev/null |grep -w $tomcat_pid |grep -v grep |awk '{print $3}'`
tomcat_memory=`/usr/ucb/ps aux 2>/dev/null |grep -w $tomcat_pid |grep -v grep |awk '{print $4}'`
tomcat_port=`pfiles $tomcat_pid 2> /dev/null | grep -i "port" > /dev/null 2>&1`
tomcat_home=`ps -ef |grep java |grep -w  $tomcat_pid  |grep -v grep |sed -n '/Dcatalina.home=/{s/.*Dcatalina.home=//;p;}' |awk '{print $1}' |head -1`
echo  "{\"'service'\":\"'"tomcat"'\",\"'home_path'\":\"'"$tomcat_home"'\",\"'cpu'\":\"'"$tomcat_cpu"'\",\"'memory'\":\"'"$tomcat_memory"'\",\"'port'\":\"'"$tomcat_port"'\"}"

fi


else

tomcat_pid=`ps -ef |grep tomcat  |grep -v grep |head -1  |awk '{print $2}'`
if [ "$tomcat_pid" != "" ];then
tomcat_cpu=`ps aux 2>/dev/null |grep -w $tomcat_pid |grep -v grep |awk '{print $3}'`
tomcat_memory=`ps aux 2>/dev/null |grep -w $tomcat_pid |grep -v grep |awk '{print $4}'`
tomcat_port=`pfiles $tomcat_pid 2> /dev/null | grep -i "port" > /dev/null 2>&1`
tomcat_home=`ps -ef |grep java |grep -w  $tomcat_pid  |grep -v grep |sed -n '/Dcatalina.home=/{s/.*Dcatalina.home=//;p;}' |awk '{print $1}' |head -1`
echo  "{\"'service'\":\"'"tomcat"'\",\"'home_path'\":\"'"$tomcat_home"'\",\"'cpu'\":\"'"$tomcat_cpu"'\",\"'memory'\":\"'"$tomcat_memory"'\",\"'port'\":\"'"$tomcat_port"'\"}"

fi


fi


if [ -r /usr/ucb/ps  ]
 then
   weblogic_pid=`ps -ef |grep java |grep weblogic  |grep -v grep |head -1  |awk '{print $2}'`
if [ "$weblogic_pid" != "" ];then
weblogic_cpu=`/usr/ucb/ps aux 2>/dev/null |grep -w $weblogic_pid |grep -v grep |awk '{print $3}'`
weblogic_memory=`/usr/ucb/ps aux 2>/dev/null |grep -w $weblogic_pid |grep -v grep |awk '{print $4}'`
weblogic_port=`pfiles $weblogic_pid 2> /dev/null | grep -i "port" > /dev/null 2>&1`
weblogic_home=`ps -ef |grep java |grep -w $weblogic_pid |grep -v grep |sed -n '/Dwls.home=/{s/.*Dwls.home=//;p;}' |awk '{print $1}' |head -1`
echo  "{\"'service'\":\"'"weblogic"'\",\"'cpu'\":\"'"$weblogic_cpu"'\",\"'memory'\":\"'"$weblogic_memory"'\",\"'port'\":\"'"$weblogic_port"'\",\"'home_path'\":\"'"$weblogic_home"'\"}"
fi


else

weblogic_pid=`ps -ef |grep java |grep weblogic  |grep -v grep |head -1  |awk '{print $2}'`
if [ "$weblogic_pid" != "" ];then
weblogic_cpu=`ps aux 2>/dev/null |grep -w $weblogic_pid |grep -v grep |awk '{print $3}'`
weblogic_memory=`ps aux 2>/dev/null |grep -w $weblogic_pid |grep -v grep |awk '{print $4}'`
weblogic_port=`pfiles $weblogic_pid 2> /dev/null | grep -i "port" > /dev/null 2>&1`
weblogic_home=`ps -ef |grep java |grep -w $weblogic_pid |grep -v grep |sed -n '/Dwls.home=/{s/.*Dwls.home=//;p;}' |awk '{print $1}' |head -1`
echo  "{\"'service'\":\"'"weblogic"'\",\"'cpu'\":\"'"$weblogic_cpu"'\",\"'memory'\":\"'"$weblogic_memory"'\",\"'port'\":\"'"$weblogic_port"'\",\"'home_path'\":\"'"$weblogic_home"'\"}"
fi


fi


if [ -r /usr/ucb/ps  ]
 then
jboss_pid=`ps -ef |grep jboss  |grep -v grep |head -1  |awk '{print $2}'`
if [ "$jboss_pid" != "" ];then
jboss_cpu=`/usr/ucb/ps aux 2>/dev/null |grep -w $jboss_pid |grep -v grep |awk '{print $3}'`
jboss_memory=`/usr/ucb/ps aux 2>/dev/null |grep -w $jboss_pid |grep -v grep |awk '{print $4}'`
jboss_port=`pfiles $jboss_pid 2> /dev/null | grep -i "port" > /dev/null 2>&1`
jboss_home=`ps -ef |grep java |grep -w $jboss_pid |grep -v grep |sed -n '/Djboss.home.dir=/{s/.*Djboss.home.dir=//;p;}' |awk '{print $1}' |head -1`
echo "{\"'service'\":\"'"jboss"'\",\"'cpu'\":\"'"$jboss_cpu"'\",\"'memory'\":\"'"$jboss_memory"'\",\"'port'\":\"'"$jboss_port"'\",\"'home_path'\":\"'"$jboss_home"'\"}"
fi


else

jboss_pid=`ps -ef |grep java |grep jboss  |grep -v grep |head -1  |awk '{print $2}'`
if [ "$jboss_pid" != "" ];then
jboss_cpu=`ps aux 2>/dev/null |grep -w $jboss_pid |grep -v grep |awk '{print $3}'`
jboss_memory=`ps aux 2>/dev/null |grep -w $jboss_pid |grep -v grep |awk '{print $4}'`
jboss_port=`pfiles $jboss_pid 2> /dev/null | grep -i "port" > /dev/null 2>&1`
jboss_home=`ps -ef |grep java |grep -w $jboss_pid |grep -v grep |sed -n '/Djboss.home.dir=/{s/.*Djboss.home.dir=//;p;}' |awk '{print $1}' |head -1`
echo  "{\"'service'\":\"'"jboss"'\",\"'cpu'\":\"'"$jboss_cpu"'\",\"'memory'\":\"'"$jboss_memory"'\",\"'port'\":\"'"$jboss_port"'\",\"'home_path'\":\"'"$jboss_home"'\"}"
fi


fi