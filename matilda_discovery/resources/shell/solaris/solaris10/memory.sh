#!/usr/bin/env bash

/usr/sbin/prtconf |grep "Memory size:" |awk '{print $3}' > file1.log
Total=`awk '{ printf "%.2f\n", $1/1024; }' file1.log`
vmstat | awk 'NR==3{print$5}' > file2.log
Free=`awk '{ printf "%.2f\n", $1/1024/1024; }' file2.log`

Used=`echo "$Total - $Free" |bc`
used_percent=`echo "scale=2; $Used / $Total * 100" | bc`

echo "{\"'total'\":\"'"$Total"'\",\"'free'\":\"'"$Free"'\",\"'used'\":\"'"$Used"'\",\"'used_percent'\":\"'"$used_percent"'\",\"'available'\":\"'"$Free"'\"}"

