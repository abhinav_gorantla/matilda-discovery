#!/usr/bin/env bash

var=(ARGUMENTS)
for loc1 in "${var[@]}"; do
    #abc=$(echo "$loc1" | rev | cut -c 13- | rev)
    weblogic_version=`java -cp "$loc1" weblogic.version 2>/dev/null | grep "WebLogic Server" | awk '{print $3}'`
    if [ "$weblogic_version" != "" ];then
    break
    fi
done

echo  "{\"'weblogic_version'\":\"'"$weblogic_version"'\"}"