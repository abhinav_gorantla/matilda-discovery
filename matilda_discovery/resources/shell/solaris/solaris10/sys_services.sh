#!/usr/bin/env bash

svcs -a | grep -w "online" |sed 1d  | {



while  read  line;

do

system_service_name=$(echo "$line"|awk   '{print $3}' | awk -F/ '{print $NF}' |awk -F: '{print $1}')
system_service_status=$(echo "$line"|awk  '{print $1}')
cpu_usage=$(/usr/ucb/ps aux |grep -w $system_service_name |grep -v grep |head -1 |awk '{print $3}')
memory_usage=$(/usr/ucb/ps aux |grep -w $system_service_name |grep -v grep| head -1 |awk '{print $4}')
port_number=`netstat -an |grep -w $system_service_name |grep -w "LISTEN" |grep -v grep  |head -1  |awk '{print $4}'`

echo -e "{\"'system_service_name'\":\"'"$system_service_name"'\",\"'cpu_usage'\":\"'"$cpu_usage"'\",\"'memory_usage'\":\"'"$memory_usage"'\",\"'port_number'\":\"'"$port_number"'\"}"

done

}
