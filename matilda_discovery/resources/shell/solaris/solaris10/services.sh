#!/usr/dt/bin/dtksh

typeset -A hash=( ["nginx.conf"]="nginx" ["domain-registry.xml"]="weblogic" ["domain.xml"]="jboss" ["server.xml"]="tomcat" ["httpd.conf"]="apache" ["my.cnf"]="mysql" ["tnsnames.ora"]="oracle" ["version.sh"]="tomcat_version" ["weblogic.jar"]="weblogic_version" ["domain.sh"]="jboss_version" )
#ignore_list='tmp|denied|permission|find|docker|WinSxS|Permission|HISTORY|alternatives|bearer11ssl|manifest|proc'

var1=$(find / \( -name nginx.conf -o -name domain-registry.xml -o -name domain.xml -o -name server.xml -o -name httpd.conf -o -name my.cnf -o -name version.sh -o -name weblogic.jar -o -name domain.sh \) -print 2>&1 |egrep -v "tmp|denied|permission|matilda|temp|Matilda_Refactor|find|docker|WinSxS|Permission|HISTORY|alternatives|bearer11ssl|manifest|proc")

for serviceitem in $var1
do
    file_name=$(echo "$serviceitem" |awk -F/ '{ print $NF }')
    #echo -e "{\"'"${hash["$file_name"]}"'\":\"'"$serviceitem"'\"}"
    printf "{\"'%s'\":\"'%s'\"}\n" "${hash["$file_name"]}" "$serviceitem"
done
