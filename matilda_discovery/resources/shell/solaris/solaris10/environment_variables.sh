#!/usr/bin/env bash

env |grep -v SSH | {

while  read  line;

do
   env_variable_name=`echo "$line"|nawk -F "=" '{print $1}'`

   env_variable_value=`echo "$line"|nawk -F "=" '{print $2}'`


   echo  "{\"'name'\":\"'"$env_variable_name"'\",\"'value'\":\"'"$env_variable_value"'\"}"


done

}