#!/usr/bin/env bash

(df -Ph 2>/dev/null | df -h) | cat > /tmp/cs1

awk '{print " "$1" "$2" "$3" "$4" "$5" "$6" "}' /tmp/cs1 > /tmp/cs2

awk '{if(NR>1)print}' /tmp/cs2 >/tmp/cs3
while  read  line

  do

  file_system=`echo "$line"|awk '{print $1}'`

  total=`echo "$line"|awk '{print $2}'`

  used=`echo "$line"|awk '{print $3}'`

  available=`echo "$line"|awk '{print $4}'`

  used_percent=`echo "$line"|awk '{print $5}'`

  mounted=`echo "$line"|awk '{print $6}'`



echo  "{\"'file_system'\":\"'"$file_system"'\",\"'total'\":\"'"$total"'\",\"'used'\":\"'"$used"'\",\"'available'\":\"'"$available"'\",\"'used_percent'\":\"'"$used_percent"'\",\"'mounted'\":\"'"$mounted"'\"}"

  done < /tmp/cs3

