#!/usr/bin/env bash

pkginfo -l | egrep '(PKGINST|CATEGORY|ARCH|VERSION|BASEDIR|VENDOR|INSTDATE|STATUS)' | awk '{print}' > /tmp/cp1

/usr/bin/nawk -F: '
                {for (i=1; i<=NF; i++)  {gsub (/^ *| *$/, "", $i)
                                         $i = "\"" $i "\""
                                        }
                }
/PKGINST/          {print "{" $0 ","}
/CATEGORY/       {print     $0 ","}
/ARCH/       {print     $0 ","}
/VERSION/       {print     $0 ","}
/BASEDIR/       {print     $0 ","}
/VENDOR/       {print     $0 ","}
/INSTDATE/       {print     $0 ","}
/STATUS/       {print     $0 " ||}" }
END {printf "\n"}
' OFS=":" /tmp/cp1 > /tmp/cp22

sed 's/PKGINST/name/g' /tmp/cp22 > /tmp/cp23
sed 's/VERSION/version/g'  /tmp/cp23 > /tmp/cp24
sed 's/BASEDIR/basedir/g'  /tmp/cp24 > /tmp/cp25
sed 's/CATEGORY/category/g'  /tmp/cp25 > /tmp/cp26
sed 's/ARCH/arch/g' /tmp/cp26 > /tmp/cp27
sed 's/VENDOR/vendor/g'  /tmp/cp27 > /tmp/cp28
sed 's/INSTDATE/instdate/g'  /tmp/cp28 > /tmp/cp29
sed 's/STATUS/status/g'  /tmp/cp29
