#!/usr/bin/env bash

nginx_version=`nginx -v 2>/dev/null |awk '{print $3}' |awk -F '/' '{print $2}'`

echo  "{\"'nginx_version'\":\"'"$nginx_version"'\"}"
