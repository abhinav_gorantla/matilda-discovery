#!/usr/bin/env bash

for i  in `cat /etc/passwd |awk -F: '$3 > 1000 {print $1}'`;do


echo  "{\"'username'\":\"'" $i  "'\",\"'group_name'\":\"'" `groups $i` "'\",\"'home_directory'\":\"'" `grep -w $i /etc/passwd | cut -d ":" -f6`  "'\",\"'user_id'\":\"'" `grep -w $i /etc/passwd | cut -d ":" -f3`  "'\",\"'password_expiry_date'\":\"'" `grep -w $i /etc/shadow 2>/dev/null | cut -d ":" -f8`  "'\",\"'last_login'\":\"'" `last $i 2>/dev/null |head -1 |awk '{print $3" "$4" "$5" "$6" "$7}'`  "'\",\"'last_password_change'\":\"'" `grep -w $i /etc/shadow 2>/dev/null | cut -d ":" -f3` "'\",\"'account_expiry_date'\":\"'" `grep -w $i /etc/shadow 2>/dev/null | cut -d ":" -f8`  "'\",\"'min_days_for_pwd_change'\":\"'" `grep -w $i /etc/shadow 2>/dev/null | cut -d ":" -f4`  "'\",\"'max_days_for_pwd_change'\":\"'" `grep -w $i /etc/shadow 2>/dev/null | cut -d ":" -f5`  "'\",\"'warning_days_for_pwd_change'\":\"'" `grep -w $i /etc/shadow 2>/dev/null | cut -d ":" -f6`  "'\",\"'password_inactive'\":\"'" `grep -w $i /etc/shadow 2>/dev/null | cut -d ":" -f7`  "'\",\"'password_status'\":\"'" `passwd -s $i 2>/dev/null |awk '{print $2}'`  "'\"}"



done

