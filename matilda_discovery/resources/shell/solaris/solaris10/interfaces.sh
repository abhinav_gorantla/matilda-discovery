#!/usr/bin/env bash

netstat -i |grep -v Mtu | grep -v :: |grep -v loopback | {

while  read  line

do
Iface=`echo "$line"|awk  '{print $1}'`

MTU=`echo "$line"|awk  '{print $2}'`
Net=`echo "$line"|awk  '{print $3}'`
IP=`echo "$line"|awk  '{print $4}'`
Ipkts=`echo "$line"|awk  '{print $5}'`
Ierrs=`echo "$line"|awk  '{print $6}'`
Opkts=`echo "$line"|awk  '{print $7}'`
Oerrs=`echo "$line"|awk  '{print $8}'`
Collis=`echo "$line"|awk  '{print $9}'`
Queue=`echo "$line"|awk  '{print $10}'`

echo  "{\"'Iface'\":\"'"$Iface"'\",\"'MTU'\":\"'"$MTU"'\",\"'Net'\":\"'"$Net"'\",\"'IP'\":\"'"$IP"'\",\"'Ipkts'\":\"'"$Ipkts"'\",\"'Ierrs'\":\"'"$Ierrs"'\",\"'Opkts'\":\"'"$Opkts"'\",\"'Oerrs'\":\"'"$Oerrs"'\",\"'TX-DRP'\":\"'"$TX_DRP"'\",\"'Collis'\":\"'"$Collis"'\",\"'Queue'\":\"'"$Queue"'\"}"



done

}