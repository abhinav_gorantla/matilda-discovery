#!/usr/bin/env bash
/usr/sbin/prtconf |grep "Memory size:" |awk '{print $3}' > file1.log
Total=`awk '{ printf "%.2f\n", $1/1024; }' file1.log`
vmstat | awk 'NR==3{print$5}' > file2.log
Free=`awk '{ printf "%.2f\n", $1/1024; }' file2.log`
Used=`echo "$Total - $Free" |bc`
memory_percent=`echo "scale=2; $Used / $Total * 100" | bc`
cpu_percent=`iostat -c 2>/dev/null  | tail -1 | awk '{print 100- $4}'`
total_storage=`df -kl |grep -v tmpfs |sed 1d | awk '{ total = total + $2 } END { print  total/1024/1024 }'`
used_Storage=`df -kl |grep -v tmpfs |sed 1d | awk '{ total = total + $3 } END { print  total/1024/1024 }'`
storage_percent=`echo "scale=2 ; $used_Storage / $total_storage * 100" | bc`


echo  "\
            {\
                'memory_percent':'$memory_percent',\
                'cpu_percent':'$cpu_percent',\
                'storage_percent':'$storage_percent'\
            }"


