#!/usr/bin/env bash

pkginfo -l | egrep '(BASEDIR|NAME|VERSION)' | awk '{print}' > /tmp/cp1



/usr/bin/nawk -F: '
                {for (i=1; i<=NF; i++)  {gsub (/^ *| *$/, "", $i)
                                         $i = "\"" $i "\""
                                        }
                }
/NAME/          {printf "%s,", $0}
/VERSION/       {printf "%s,", $0}
/BASEDIR/       {printf "%s || ", $0 }
END {printf "\n"}
' OFS=":" /tmp/cp1 > /tmp/cp2



sed  's/NAME/name/g' /tmp/cp2 > /tmp/cp3
sed  's/VERSION/version/g' /tmp/cp3 > /tmp/cp4
sed  's/BASEDIR/basedir/g' /tmp/cp4