#!/usr/bin/env bash
#for i in $(ip link show | awk -F: '$1>0 {print $2}')
for i in $(sudo ifconfig -a 2>/dev/null |grep -v IPv6 | awk  '$1>0 {print $1}' |sed "s/:$//")
do

if [ $i == lo ]; then


echo -e "{\"'name'\":\"'"$i"'\",\"'ip_address'\":'"$(sudo ifconfig $i |grep inet |grep -v inet6 |awk '{print $2}'|awk -F ":" '{print $2}')"',\"'packet_max_size'\":'"$(sudo ifconfig $i |grep -i mtu |awk '{print $4}' |awk -F ":" '{print $2}')"',\"'ip_subnet'\":'"$(sudo ifconfig $i |grep -i Mask |awk '{print $3}' |awk -F ":" '{print $2}')"',\"'dns_server'\":'"$(cat /etc/resolv.conf  2>/dev/null |grep -i '^nameserver'|head -n1|cut -d ' ' -f2)"',\"'received_packets'\":'"$(sudo ifconfig $i |grep -i RX |head -1 |awk '{print $2}' |awk -F ":" '{print $2}')"',\"'type'\":'"$(ls -lrt /sys/class/net/  2>/dev/null |grep $i |awk ' {print $11}' |awk -F "/" ' {print $4}')"',\"'Flg'\":'"$(sudo ifconfig -s $i |grep -v Flg |awk '{print $12}')"',\"'sent_packets'\":'"$(sudo ifconfig $i |grep  TX |head -1 |awk '{print $2}' |awk -F ":" '{print $2}')"'}"
fi
if [ $i != lo ]; then

speed_test=`ethtool $i 2>/dev/null |grep Speed |awk '{print $2}'`

if [[ "$speed_test" == *"Unknown!"*  ]]; then

echo -e "{\"'name'\":\"'"$i"'\",\"'ip_address'\":'"$(sudo ifconfig $i |grep inet |grep -v inet6 |awk '{print $2}'|awk -F ":" '{print $2}')"',\"'mac_address'\":'"$(sudo ifconfig $i |grep -i HWaddr |awk '{print $5}')"',\"'dhcp_enabled'\":'"$(cat /etc/network/interfaces 2>/dev/null |grep $i |egrep "dhcp|static" |awk '{print $4}')"',\"'packet_max_size'\":'"$(sudo ifconfig $i |grep -i mtu |awk '{print $5}' |awk -F ":" '{print $2}')"',\"'speed'\":'"Unknown"',\"'ip_subnet'\":'"$(sudo ifconfig $i |grep -i mask |awk '{print $4}' |awk -F ":" '{print $2}')"',\"'dns_server'\":'"$(cat /etc/resolv.conf  2>/dev/null |grep -i '^nameserver'|head -n1|cut -d ' ' -f2)"',\"'received_packets'\":'"$(sudo ifconfig $i |grep -i RX |head -1 |awk '{print $2}' |awk -F ":" '{print $2}')"',\"'type'\":'"physical"',\"'Flg'\":'"$(sudo ifconfig -s $i |grep -v Flg |awk '{print $12}')"',\"'sent_packets'\":'"$(sudo ifconfig $i |grep  TX |head -1 |awk '{print $2}' |awk -F ":" '{print $2}')"'}"

elif [[ "$speed_test" != *"Unknown!"*  ]]; then

echo -e "{\"'name'\":\"'"$i"'\",\"'ip_address'\":'"$(sudo ifconfig $i |grep inet |grep -v inet6 |awk '{print $2}'|awk -F ":" '{print $2}')"',\"'mac_address'\":'"$(sudo ifconfig $i |grep -i HWaddr |awk '{print $5}')"',\"'dhcp_enabled'\":'"$(cat /etc/network/interfaces 2>/dev/null |grep $i |egrep "dhcp|static" |awk '{print $4}')"',\"'packet_max_size'\":'"$(sudo ifconfig $i |grep -i mtu |awk '{print $5}' |awk -F ":" '{print $2}')"',\"'speed'\":'"$(ethtool $i 2>/dev/null |grep Speed |awk '{print $2}')"',\"'ip_subnet'\":'"$(sudo ifconfig $i |grep -i mask |awk '{print $4}' |awk -F ":" '{print $2}')"',\"'dns_server'\":'"$(cat /etc/resolv.conf  2>/dev/null |grep -i '^nameserver'|head -n1|cut -d ' ' -f2)"',\"'received_packets'\":'"$(sudo ifconfig $i |grep -i RX |head -1 |awk '{print $2}' |awk -F ":" '{print $2}')"',\"'type'\":'"physical"',\"'Flg'\":'"$(sudo ifconfig -s $i |grep -v Flg |awk '{print $12}')"',\"'sent_packets'\":'"$(sudo ifconfig $i |grep  TX |head -1 |awk '{print $2}' |awk -F ":" '{print $2}')"'}"

fi

fi

done
