#!/usr/bin/env bash

Cpu_Percentage=`grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage}'|cut -c1-5`
Cpu_Count=`lscpu | grep 'CPU(s)' | awk 'NR==1{print $2}'`
Vcpu_Count=`grep -c ^processor /proc/cpuinfo`
Cpu_Cores=`nproc`
Cpu_Model_Name=`lscpu | grep 'Model name' |awk '{print $3 " " $4" "$5" "$6" "$7 " "$8" "$9}'`


echo -e "\
    {\
        'utilization_percent':'$Cpu_Percentage',\
        'physical_processors':'$Cpu_Count',\
        'physical_cores':'$Cpu_Cores',\
        'logical_processors':'$Vcpu_Count',\
        'model_name':'$Cpu_Model_Name'\
    }"

