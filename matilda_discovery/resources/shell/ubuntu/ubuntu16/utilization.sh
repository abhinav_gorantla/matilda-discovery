#!/usr/bin/env bash

memory_percent=`free | grep Mem | awk '{print $3/$2 * 100.0}'|cut -c1-5`
cpu_percent=`grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage}'|cut -c1-5`
storage_percent=`df -h --total |tail -1 |awk '{print $5}' | sed 's/.$//'`


echo -e "\
            {\
                'memory_percent':'$memory_percent',\
                'cpu_percent':'$cpu_percent',\
                'storage_percent':'$storage_percent'\
            }"


