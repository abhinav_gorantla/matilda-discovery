dpkg-query -l > /tmp/cp1
tail -n +6  /tmp/cp1 > /tmp/cp2
awk '{print $2" "$3" "$4}' /tmp/cp2 > /tmp/cp3

sed  -i '1i Name Version Architecture ' /tmp/cp3

awk '{print " "$1" "$2" "$3"   "}' /tmp/cp3 > /tmp/cp4

awk 'NR==1 {    for (i=1; i<=NF; i++) {        f[$i] = i    }}{ print $(f["name"]), $(f["version"]), $(f["Architecture"]) }' /tmp/cp4 > /tmp/cp5

awk '{if(NR>1)print}' /tmp/cp5 >/tmp/cp6

awk ' { printf "\"name\":\"" $1  "\",\"version\":\"" $2 "\",\"Architecture\":\"" $3"\" || "} ' /tmp/cp6