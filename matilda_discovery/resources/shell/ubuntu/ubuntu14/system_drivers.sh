#!/usr/bin/env bash

 lsmod > /tmp/sd

sed 1d /tmp/sd > /tmp/sd1

while IFS= read -r line;

do
module=$(echo "$line"|awk  '{print $1}')

size=$(echo "$line"|awk  '{print $2}')
used_by=$(echo "$line"|awk  '{print $3}')


echo -e "{\"'module'\":\"'"$module"'\",\"'size'\":\"'"$size"'\",\"'used_by'\":\"'"$used_by"'\"}"



done < /tmp/sd1