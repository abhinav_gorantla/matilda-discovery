#!/usr/bin/env bash

memory_percent=`free | grep Mem | awk '{print $3/$2 * 100.0}'|cut -c1-5`
cpu_percent=`grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage}'|cut -c1-5`
total_storage=`df -kl |grep -v tmpfs |sed 1d | awk '{ total = total + $2 } END { print  total/1024/1024 }'`
used_Storage=`df -kl |grep -v tmpfs |sed 1d | awk '{ total = total + $3 } END { print  total/1024/1024 }'`
storage_percent=`echo "scale=2 ; $used_Storage / $total_storage * 100" | bc`


echo -e "\
            {\
                'memory_percent':'$memory_percent',\
                'cpu_percent':'$cpu_percent',\
                'storage_percent':'$storage_percent'\
            }"


