#!/usr/bin/env bash


var1=$(find /etc/ssl/certs -name "*.pem" ! -size 0 2>&1 |egrep -v "tmp|docker|WinSxS|Permission|HISTORY|alternatives|bearer11ssl|manifest")

for loc1 in $var1

do

test=$(openssl x509 -in "$loc1" -text -noout)

 if [ "$test" != "" ];then

   path=`echo "$loc1"`
   certificate_name=$(echo "$loc1" |awk -F/ '{ print $NF }')
   not_after=$(openssl x509 -in "$loc1"  -text -noout |grep -w "Not After :" |head -1 |awk '{print $4" "$5" "$6" "$7" "$8}')
   not_before=$(openssl x509 -in "$loc1"  -text -noout |grep -w "Not Before:" | head -1 |awk '{print $3" "$4" "$5" "$6" "$7}')
   files=($(openssl x509 -in "$loc1"  -text -noout |grep -A1 "Serial Number:" ))
   serial_number=${files[2]}
   issuer=$(openssl x509 -in "$loc1"  -text -noout |grep -w "Issuer:" |sed ' s/[^:]*: *//;')



echo -e "{\"'path'\":\"'"$path"'\",\"'certificate_name'\":\"'"$certificate_name"'\",\"'not_after'\":\"'"$not_after"'\",\"'not_before'\":\"'"$not_before"'\",\"'serial_number'\":\"'"$serial_number"'\",\"'issuer'\":\"'"$issuer"'\"}"
fi


done


var2=$(find /etc/ssl/certs -name "*.crt" ! -size 0 2>&1 |egrep -v "tmp|docker|WinSxS|Permission|HISTORY|alternatives|bearer11ssl|manifest")

for loc2 in $var2

do

test1=$(openssl x509 -in "$loc2" -text -noout)

 if [ "$test1" != "" ];then

   path1=`echo "$loc2"`
   certificate_name1=$(echo "$loc2" |awk -F/ '{ print $NF }')
   not_after1=$(openssl x509 -in "$loc2"  -text -noout |grep -w "Not After :" |head -1 |awk '{print $4" "$5" "$6" "$7" "$8}')
   not_before1=$(openssl x509 -in "$loc2"  -text -noout |grep -w "Not Before:" | head -1 |awk '{print $3" "$4" "$5" "$6" "$7}')
   files1=($(openssl x509 -in "$loc2"  -text -noout |grep -A1 "Serial Number:" ))
   serial_number1=${files[2]}
   issuer1=$(openssl x509 -in "$loc2"  -text -noout |grep -w "Issuer:" |sed ' s/[^:]*: *//;')



echo -e "{\"'path'\":\"'"$path1"'\",\"'certificate_name'\":\"'"$certificate_name1"'\",\"'not_after'\":\"'"$not_after1"'\",\"'not_before'\":\"'"$not_before1"'\",\"'serial_number'\":\"'"$serial_number1"'\",\"'issuer'\":\"'"$issuer1"'\"}"
fi


done
