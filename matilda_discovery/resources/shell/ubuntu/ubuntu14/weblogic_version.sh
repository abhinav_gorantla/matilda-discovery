#!/usr/bin/env bash

var=(ARGUMENTS)

for loc1 in "${var[@]}"; do
    abc=$(echo "$loc1" | rev | cut -c 13- | rev)
    if [[ $(which java 2>/dev/null) ]];then
    weblogic_version=$(cd "$abc";java -cp weblogic.jar weblogic.version | grep "WebLogic Server" | awk '{print $3}')
    if [ "$weblogic_version" != "" ];then
    break
    fi
    else
    xyz=$(ps -ef |grep java |grep weblogic |grep -v grep |head -1 |awk '{print $8}')
    weblogic_version=$(cd "$abc";echo "$xyz" -cp weblogic.jar weblogic.version 2>/dev/null | grep "WebLogic Server" | awk '{print $3}')
    if [ "$weblogic_version" != "" ];then
    break
    fi
    fi
done

echo -e "{\"'weblogic_version'\":\"'"$weblogic_version"'\"}"