#!/usr/bin/env bash

mysql_version=$(mysql --version 2>/dev/null |awk '{print $5}' |awk -F "." '{print $1"." $2}')

echo -e "{\"'mysql_version'\":\"'"$mysql_version"'\"}"