#!/usr/bin/env bash

ifconfig -s  > /tmp/rr

sed 1d /tmp/rr > /tmp/rr1

while IFS= read -r line;

do
Iface=$(echo "$line"|awk  '{print $1}')

MTU=$(echo "$line"|awk  '{print $2}')
RX_OK=$(echo "$line"|awk  '{print $3}')
RX_ERR=$(echo "$line"|awk  '{print $4}')
RX_DRP=$(echo "$line"|awk  '{print $5}')
RX_OVR=$(echo "$line"|awk  '{print $6}')
TX_OK=$(echo "$line"|awk  '{print $7}')
TX_ERR=$(echo "$line"|awk  '{print $8}')
TX_DRP=$(echo "$line"|awk  '{print $9}')
TX_OVR=$(echo "$line"|awk  '{print $10}')
Flg=$(echo "$line"|awk  '{print $11}')


echo -e "{\"'Iface'\":\"'"$Iface"'\",\"'MTU'\":\"'"$MTU"'\",\"'RX-OK'\":\"'"$RX_OK"'\",\"'RX-ERR'\":\"'"$RX_ERR"'\",\"'RX-DRP'\":\"'"$RX_DRP"'\",\"'RX-OVR'\":\"'"$RX_OVR"'\",\"'TX-OK'\":\"'"$TX_OK"'\",\"'TX-ERR'\":\"'"$TX_ERR"'\",\"'TX-DRP'\":\"'"$TX_DRP"'\",\"'TX-OVR'\":\"'"$TX_OVR"'\",\"'Flg'\":\"'"$Flg"'\"}"



done < /tmp/rr1