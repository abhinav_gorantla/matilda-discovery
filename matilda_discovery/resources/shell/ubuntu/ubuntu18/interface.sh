#!/usr/bin/env bash

if [[ $(which ifconfig) ]]; then
   for i in $(ifconfig -a 2>/dev/null |grep -v IPv6 | awk  '$1>0 {print $1}' |sed "s/:$//")

do

if [ $i == lo ]; then


echo -e "{\"'name'\":\"'"$i"'\",\"'ip_address'\":'"$(ifconfig $i |grep inet |grep -v inet6 |awk '{print $2}')"',\"'packet_max_size'\":'"$(ifconfig $i |grep mtu | awk '{print $NF}')"',\"'ip_subnet'\":'"$(ifconfig $i |grep inet |grep -v inet6 |awk '{print $4}')"',\"'dns_server'\":'"$(cat /etc/resolv.conf  2>/dev/null |grep -i '^nameserver'|head -n1|cut -d ' ' -f2)"',\"'received_packets'\":'"$(ifconfig $i |grep -i RX |head -1 |awk '{print $3}')"',\"'type'\":'"$(ls -lrt /sys/class/net/  2>/dev/null |grep $i |awk ' {print $11}' |awk -F "/" ' {print $4}')"',\"'Flg'\":'"$(ifconfig -s $i |grep -v Flg |awk '{print $11}')"',\"'sent_packets'\":'"$(ifconfig $i |grep  TX |head -1 |awk '{print $3}')"'}"
fi
if [ $i != lo ]; then

speed_test=`ethtool $i 2>/dev/null |grep Speed |awk '{print $2}'`

if [[ "$speed_test" == *"Unknown!"*  ]]; then

echo -e "{\"'name'\":\"'"$i"'\",\"'ip_address'\":'"$(ifconfig $i |grep inet |grep -v inet6 |awk '{print $2}')"',\"'mac_address'\":'"$(ifconfig $i |grep ether |awk '{print $2}')"',\"'dhcp_enabled'\":'"$(cat  /etc/network/interfaces  2>/dev/null |grep $i |egrep "dhcp|static" |awk '{print $4}')"',\"'packet_max_size'\":'"$(ifconfig $i |grep mtu | awk '{print $NF}')"',\"'speed'\":'"Unknown"',\"'ip_subnet'\":'"$(ifconfig $i |grep inet |grep -v inet6 |awk '{print $4}')"',\"'dns_server'\":'"$(cat /etc/resolv.conf  2>/dev/null |grep -i '^nameserver'|head -n1|cut -d ' ' -f2)"',\"'received_packets'\":'"$(ifconfig $i |grep -i RX |head -1 |awk '{print $3}')"',\"'type'\":'"physical"',\"'Flg'\":'"$(ifconfig -s $i |grep -v Flg |awk '{print $11}')"',\"'sent_packets'\":'"$(ifconfig $i |grep  TX |head -1 |awk '{print $3}')"'}"

elif [[ "$speed_test" != *"Unknown!"*  ]]; then

echo -e "{\"'name'\":\"'"$i"'\",\"'ip_address'\":'"$(ifconfig $i |grep inet |grep -v inet6 |awk '{print $2}')"',\"'mac_address'\":'"$(ifconfig $i |grep ether |awk '{print $2}')"',\"'dhcp_enabled'\":'"$(cat  /etc/network/interfaces  2>/dev/null |grep $i |egrep "dhcp|static" |awk '{print $4}')"',\"'packet_max_size'\":'"$(ifconfig $i |grep mtu | awk '{print $NF}')"',\"'speed'\":'"$(ethtool $i 2>/dev/null |grep Speed |awk '{print $2}')"',\"'ip_subnet'\":'"$(ifconfig $i |grep inet |grep -v inet6 |awk '{print $4}')"',\"'dns_server'\":'"$(cat /etc/resolv.conf  2>/dev/null |grep -i '^nameserver'|head -n1|cut -d ' ' -f2)"',\"'received_packets'\":'"$(ifconfig $i |grep -i RX |head -1 |awk '{print $3}')"',\"'type'\":'"physical"',\"'Flg'\":'"$(ifconfig -s $i |grep -v Flg |awk '{print $11}')"',\"'sent_packets'\":'"$(ifconfig $i |grep  TX |head -1 |awk '{print $3}')"'}"


fi

fi

done


  else
    for i in $(ip link show | awk -F: '$1>0 {print $2}')

do

if [ $i == lo ]; then


echo -e "{\"'name'\":\"'"$i"'\",\"'ip_address'\":'"$(ip addr show $i |grep inet |grep -v inet6 |awk '{print $2}')"',\"'packet_max_size'\":'"$(ip addr show $i |grep mtu |sed 's/.*mtu //' | cut -d " " -f 1)"',\"'ip_subnet'\":'"$(ifconfig $i |grep inet |grep -v inet6 |awk '{print $4}')"',\"'dns_server'\":'"$(cat /etc/resolv.conf  2>/dev/null |grep -i '^nameserver'|head -n1|cut -d ' ' -f2)"',\"'received_packets'\":'"$(netstat -i |grep $i | awk '{print $3}')"',\"'type'\":'"$(ls -lrt /sys/class/net/  2>/dev/null |grep $i |awk ' {print $11}' |awk -F "/" ' {print $4}')"',\"'Flg'\":'"$(netstat -i |grep $i | awk '{print $11}')"',\"'sent_packets'\":'"$(netstat -i |grep $i | awk '{print $7}')"'}"
fi
if [ $i != lo ]; then

speed_test=`ethtool $i 2>/dev/null |grep Speed |awk '{print $2}'`

if [[ "$speed_test" == *"Unknown!"*  ]]; then

echo -e "{\"'name'\":\"'"$i"'\",\"'ip_address'\":'"$(ip addr show $i |grep inet |grep -v inet6 |awk '{print $2}')"',\"'mac_address'\":'"$(ip addr show $i |grep ether  |awk '{print $2}')"',\"'dhcp_enabled'\":'"$(cat  /etc/network/interfaces  2>/dev/null |grep $i |egrep "dhcp|static" |awk '{print $4}')"',\"'packet_max_size'\":'"$(netstat -i |grep $i  |awk '{print $2}')"',\"'speed'\":'"Unknown"',\"'ip_subnet'\":'"$(ifconfig $i |grep inet |grep -v inet6 |awk '{print $4}')"',\"'dns_server'\":'"$(cat /etc/resolv.conf  2>/dev/null |grep -i '^nameserver'|head -n1|cut -d ' ' -f2)"',\"'received_packets'\":'"$(netstat -i  |grep $i | awk '{print $3}')"',\"'type'\":'"physical"',\"'Flg'\":'"$(netstat -i  |grep $i | awk '{print $11}')"',\"'sent_packets'\":'"$(netstat -i  |grep $i | awk '{print $7}')"'}"

elif [[ "$speed_test" != *"Unknown!"*  ]]; then

echo -e "{\"'name'\":\"'"$i"'\",\"'ip_address'\":'"$(ip addr show $i |grep inet |grep -v inet6 |awk '{print $2}')"',\"'mac_address'\":'"$(ip addr show $i |grep ether  |awk '{print $2}')"',\"'dhcp_enabled'\":'"$(cat  /etc/network/interfaces  2>/dev/null |grep $i |egrep "dhcp|static" |awk '{print $4}')"',\"'packet_max_size'\":'"$(netstat -i |grep $i  |awk '{print $2}')"',\"'speed'\":'"$(ethtool $i 2>/dev/null |grep Speed |awk '{print $2}')"',\"'ip_subnet'\":'"$(ifconfig $i |grep inet |grep -v inet6 |awk '{print $4}')"',\"'dns_server'\":'"$(cat /etc/resolv.conf  2>/dev/null |grep -i '^nameserver'|head -n1|cut -d ' ' -f2)"',\"'received_packets'\":'"$(netstat -i  |grep $i | awk '{print $3}')"',\"'type'\":'"physical"',\"'Flg'\":'"$(netstat -i  |grep $i | awk '{print $11}')"',\"'sent_packets'\":'"$(netstat -i  |grep $i | awk '{print $7}')"'}"


fi

fi

done
fi