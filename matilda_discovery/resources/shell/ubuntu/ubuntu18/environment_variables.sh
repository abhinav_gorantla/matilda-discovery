#!/usr/bin/env bash

printenv > /tmp/qq

while IFS= read -r line;

do
   env_variable_name=$(echo "$line"|awk -F "=" '{print $1}')

   env_variable_value=$(echo "$line"|awk -F "=" '{print $2}')

  if [ "$env_variable_value" != "" ];then

   echo -e "{\"'name'\":\"'"$env_variable_name"'\",\"'value'\":\"'"$env_variable_value"'\"}"

    fi



done < /tmp/qq