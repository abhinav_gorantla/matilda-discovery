#!/usr/bin/env bash

Total=`free -m | grep Mem | awk '{$2=$2/1024; printf "%0.2f\n", $2}'`
Used_Memory=`free -m | grep Mem | awk '{$3=$3/1024; printf "%0.2f\n", $3}'`
Free=`free -m | grep Mem | awk '{$4=$4/1024; printf "%0.2f\n", $4}'`
Used=`free | grep Mem | awk '{print $3/$2 * 100.0}'|cut -c1-5`


echo -e "\
    {\
        'used':'$Used_Memory',\
        'free':'$Free',\
        'total':'$Total',\
        'available':'$Free',\
        'used_percent':'$Used'\
    }"



