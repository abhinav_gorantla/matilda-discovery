#!/usr/bin/env bash

os=`cat /etc/os-release 2>&1 |grep -i PRETTY_NAME |awk -F "=" '{print $2}' 2>&1 |cut -c2-13`
os1=`cat /etc/redhat-release 2>&1|awk '{print $1i" " $3}'|cut -c1-8`
os2=`cat /etc/redhat-release 2>&1|awk '{print $1i" " $4}'|cut -c1-8`
os3=`cat /etc/redhat-release 2>&1  |awk '{print $1 $2$3$4$5$6$7}' |cut -c1-35`
os4=`uname -a 2>&1 |awk '{print $3}'`
os5=`uname -v 2>&1`
os6=`cat /etc/enterprise-release 2>&1  |awk '{print $1 $2$3$4$5$6$7}' |awk -F "." '{print $1}'`


if [ "${os1}" = "CentOS 5" ] ; then

    echo "centos5"

elif [ "${os1}" = "CentOS 6" ] ; then

    echo "centos6"

elif [ "${os2}" = "CentOS 7" ] ; then

echo "centos7"



elif [ "${os3}" = "RedHatEnterpriseLinuxServerrelease5" ] ; then

    echo "rhel5"

elif [ "${os3}" = "RedHatEnterpriseLinuxServerrelease6" ] ; then

    echo "rhel6"

elif [ "${os3}" = "RedHatEnterpriseLinuxServerrelease7" ] ; then

echo "rhel7"


elif [ "${os}" = "Ubuntu 18.04" ] ; then

    echo "ubuntu18"

elif [ "${os}" = "Ubuntu 16.04" ] ; then

    echo "ubuntu16"

elif [ "${os}" = "Ubuntu 14.04" ] ; then

echo "ubuntu14"

elif [ "${os4}" = "5.10" ] ; then

    echo "solaris10"

elif [ "${os4}" = "5.11" ] ; then

echo "solaris11"

elif [ "${os5}" = "7" ] ; then

echo "aix7"

elif [ "${os5}" = "6" ] ; then

echo "aix6"

elif [ "${os6}" = "EnterpriseLinuxEnterpriseLinuxServerrelease5" ] ; then

echo "oel5"

fi
