#!/usr/bin/env bash

var=(ARGUMENTS)

for loc1 in "${var[@]}"; do
    abc=$(echo "$loc1" | rev | cut -c 11- | rev)
    tomcat_version=$(cd "$abc";./version.sh | grep "Server number:" | awk '{print $3}')
    if [ "$tomcat_version" != "" ];then
    break
    fi
done

echo -e "{\"'tomcat_version'\":\"'"$tomcat_version"'\"}"