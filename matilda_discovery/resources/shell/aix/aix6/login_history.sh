#!/usr/bin/env bash

last | egrep -v "crash|reboot" | head -50 | while IFS= read -r line;

  do

  user_name=$(echo "$line"|awk '{print $1}')

  server_ip=$(echo "$line"|awk '{print $3}')

  time=$(echo "$line"|awk '{print $4" "$5" "$6}')

if [[ "$server_ip" =~ [a-z] ]]; then

echo -e "{\"'user_name'\":\"'"$user_name"'\",\"'server_ip'\":\"'"$server_ip"'\",\"'time'\":\"'"$time"'\"}"
fi
done
