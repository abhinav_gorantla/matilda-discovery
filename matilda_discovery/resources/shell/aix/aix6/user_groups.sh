#!/usr/bin/env bash

for i  in $(cat /etc/passwd | awk -F: '$3>99 {print $1}');do

p1=$(pwdadm -q $i 2>/dev/null |grep lastupdate |awk '{print $3}')


echo -e "{\"'username'\":\"'" $i  "'\",\"'group_name'\":\"'" $(id -gn $i) "'\",\"'home_directory'\":\"'" $(grep -w $i /etc/passwd | cut -d ":" -f6)  "'\",\"'user_id'\":\"'" $(grep -w $i /etc/passwd | cut -d ":" -f3)  "'\",\"'password_expiry_date'\":\"'" $(grep -w $i /etc/shadow 2>/dev/null | cut -d ":" -f8)  "'\",\"'last_login'\":\"'" $(last $i |head -1 |awk '{print $4" "$5" "$6}')  "'\",\"'last_password_change'\":\"'" $(perl -le 'print scalar localtime $p1' 2>/dev/null) "'\",\"'account_expiry_date'\":\"'" $(lsuser -f $i 2>/dev/null| grep expires |cut -d = -f 2)  "'\",\"'min_days_for_pwd_change'\":\"'" $(lsuser -f $i 2>/dev/null | grep minage |cut -d= -f 2)  "'\",\"'max_days_for_pwd_change'\":\"'" $(lsuser -f $i 2>/dev/null | grep maxage |cut -d= -f 2)  "'\",\"'warning_days_for_pwd_change'\":\"'" $(lsuser -f $i 2>/dev/null | grep pwdwarntime |cut -d= -f 2)  "'\",\"'password_inactive'\":\"'" $(lsuser -f $i 2>/dev/null | grep maxexpired |cut -d= -f 2)  "'\",\"'password_status'\":\"'" $(lsuser -f $i 2>/dev/null | grep -w account_locked |cut -d= -f 2)  "'\"}"



done

