#!/usr/bin/env bash


netstat -an |egrep  -w "LISTEN|ESTABLISHED" |grep -v ::1 |grep -v tcp6 |grep -v "127.0.0.1" |sed 's/*/all_interface/g'  | {


while IFS= read -r line

  do

  protocol=$(echo "$line"|awk '{print $1}')
  local_address=$(echo "$line"|awk '{print $4}' |awk -F "." '{print $1}')

  number=$(echo "$line"|awk '{print $4}' |awk -F "." '{print $2}')

  foreign_address=$(echo "$line"|awk '{print $5}' |awk -F "." '{print $1}')

  foreign_port=$(echo "$line"|awk '{print $5}' |awk -F "." '{print $2}')

  status=$(echo "$line"|awk '{print $6}')
#process_id=$(echo "$line"|awk '{print $4}')

  recv_queue=$(echo "$line"|awk '{print $2}')

  send_queue=$(echo "$line"|awk '{print $3}')

#  program=$(echo "$line"|awk '{print $5}')
#  user=$(echo "$line"|awk '{print $3}')

foreign_port1=`foreign_port=$(echo "$line"|awk '{print $5}' |cut -d "." -f5)`
if [[ "$foreign_port" == *"all_interface"* ]];then

echo -e "{\"'local_address'\":\"'"$local_address"'\",\"'number'\":\"'"$number"'\",\"'foreign_address'\":\"'"$foreign_address"'\", \"'foreign_port'\":\"'"any_port"'\", \"'status'\":\"'"$status"'\"}"
else

echo -e "{\"'local_address'\":\"'"$local_address"'\",\"'number'\":\"'"$number"'\",\"'foreign_address'\":\"'"$foreign_address"'\", \"'foreign_port'\":\"'"$foreign_port"'\", \"'status'\":\"'"$status"'\"}"

fi





  done



}
