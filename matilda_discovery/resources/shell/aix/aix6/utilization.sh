#!/usr/bin/env bash
Total=`TOTAL=0; for DISK in $(lspv | awk '{ print $1 }');do SIZE=$(bootinfo -s $DISK); TOTAL=$(echo "$TOTAL + $SIZE"|bc); done; echo "$TOTAL / 1024" | bc`
df -Pg |grep -v "-" |tail +2 |awk '{print $3}' > /tmp/disk_used
Used=`awk '{count=count+$1}END{print count}' /tmp/disk_used`
memory_percent=`svmon -G -O unit=GB |grep memory |awk '{print $3/$2 * 100.0}' |cut -c1-5`
cpu_percent=`vmstat |tail -1 | awk '{print $14 + $15 }'`
#total_storage=`df -kl |grep -v tmpfs |sed 1d | awk '{ total = total + $2 } END { print  total/1024/1024 }'`
#used_Storage=`df -kl |grep -v tmpfs |sed 1d | awk '{ total = total + $3 } END { print  total/1024/1024 }'`
storage_percent=`echo "scale=2 ; $Used / $Total * 100" | bc`


echo -e "\
            {\
                'memory_percent':'$memory_percent',\
                'cpu_percent':'$cpu_percent',\
                'storage_percent':'$storage_percent'\
            }"


