#!/usr/bin/env bash

for i in $(sudo ifconfig -a 2>/dev/null |grep -v IPv6 | awk  '$1>0 {print $1}' |sed "s/:$//")

do

if [ $i == lo0 ]; then

echo -e "{\"'name'\":\"'"$i"'\",\"'ip_address'\":'"$(sudo ifconfig lo0 2>/dev/null |grep inet |head -1 |awk '{print $2}')"',\"'received_packets'\":'"$(netstat -i 2>/dev/null |grep lo0 |head -1 |awk '{print $4}')"',\"'sent_packets'\":'"$(netstat -i 2>/dev/null |grep lo0 |head -1 |awk '{print $6}')"',\"'packet_max_size'\":'"$(netstat -i 2>/dev/null |grep lo0 |head -1 |awk '{print $2}')"',\"'type'\":'"None"'}"

fi

if [ $i != lo0 ]; then

abc=$(sudo ifconfig $i 2>/dev/null |grep inet |awk '{print $2}')

if [[ $abc != '' ]];then

test=`sudo ifconfig $i 2>/dev/null |head -1 |awk '{print $1}' |sed "s/:$//" |cut -d: -f 1`

echo -e "{\"'name'\":\"'"$i"'\",\"'ip_address'\":'"$(sudo ifconfig $i 2>/dev/null |grep inet |head -1 |awk '{print $2}')"',\"'mac_address'\":'"$(netstat -ia 2>/dev/null |grep $test |head -1 |awk '{print $4}')"',\"'received_packets'\":'"$(netstat -i 2>/dev/null |grep $test |head -1 |awk '{print $5}')"',\"'sent_packets'\":'"$(netstat -i 2>/dev/null |grep $test |head -1 |awk '{print $7}')"',\"'packet_max_size'\":'"$(netstat -i 2>/dev/null |grep $test |head -1 |awk '{print $2}')"',\"'speed'\":'"$(dladm show-dev 2>/dev/null |grep $i |awk '{print $5 " "$6}')"'}"

fi
fi
done
