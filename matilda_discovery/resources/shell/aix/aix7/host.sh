#!/usr/bin/env bash

Platform=`uname -s`
OperatingSystem=`uname -s`
ReleaseVersion=`uname -v`
Version=`oslevel`
BootUpDate=`date`
CoresCount=`prtconf |grep -w "Number Of Processors:" |awk -F: '{print $2}'`
Vcpu_Count=`lparstat -i | grep -w "Online Virtual CPUs" |awk -F: '{print $2}'`
MemSize=`svmon -G -O unit=GB |grep memory |awk '{print $2}'`
InstanceType=`uname -L 2>/dev/null |awk '{print $2}'`
ServiceProvider=`prtconf |grep -w "System Model" |awk '{print $3}'`
Hostname=`hostname`
Instruction_Set=`prtconf |grep -w "Kernel Type" |awk '{print $3}'`
BIOS_Configuration=`prtconf |grep -w "Platform Firmware level" |awk '{print $4}'`
Pagefile_Size=`getconf PAGE_SIZE | awk '{$1=$1/1024; print $1,"KB";}'`
Internet_Access=`ping -q -w1 -c1 google.com &>/dev/null && echo True || echo False`
Timezone=`date +"%Z %z" |awk '{print $1}'`
Installed_On=`getlvcb -AT hd4 2>/dev/null |grep "time created" |awk '{print $4" "$5" "$6" "$7" "$8}'`
Uptime=`uptime | awk -F, '{sub(".*up ",x,$1);print $1,$2}'`
TotalListeningPorts=`netstat -an |grep -w "LISTEN" |grep -v "127.0.0.1" |wc -l`
gateway=`netstat -rn |grep "default" |awk '{print $2}'`
gateway_ip=''
flag=False
for item in "${gateway[@]}"; do
  if [ "$gateway_ip" != "" ];then
        gateway_ip=$gateway_ip,$item
        else gateway_ip=$item
    fi
done


test1=`echo $gateway_ip | sed 's/"//g' | tr -d \'`
test2=`echo ${test1// /,}`

if [ "$InstanceType" != *"NULL"* ];then

echo -e "\
            {\
                'platform':'$Platform',\
                'operating_system':'$OperatingSystem',\
                'release_version':'$ReleaseVersion',\
                'version':'$Version',\
                'system_date':'$BootUpDate',\
                'memory':'$MemSize',\
                'instance_type':'LPAR',\
                'service_provider':'$ServiceProvider',\
                'gateway_ip':'$test2',\
                'bios_configuration':'$BIOS_Configuration',\
                'name':'$Hostname',\
                'instruction_set':'$Instruction_Set',\
                'pagefile_size':'$Pagefile_Size',\
                'internet_access':'$Internet_Access',\
                'timezone':'$Timezone',\
                'installed_on':'$Installed_On',\
                'uptime':'$Uptime'\
            }"

else

echo -e "\
            {\
                'platform':'$Platform',\
                'operating_system':'$OperatingSystem',\
                'release_version':'$ReleaseVersion',\
                'version':'$Version',\
                'system_date':'$BootUpDate',\
                'memory':'$MemSize',\
                'instance_type':'Physical Server',\
                'service_provider':'$ServiceProvider',\
                'gateway_ip':'$test2',\
                'bios_configuration':'$BIOS_Configuration',\
                'name':'$Hostname',\
                'instruction_set':'$Instruction_Set',\
                'pagefile_size':'$Pagefile_Size',\
                'internet_access':'$Internet_Access',\
                'timezone':'$Timezone',\
                'installed_on':'$Installed_On',\
                'uptime':'$Uptime'\
            }"

fi
