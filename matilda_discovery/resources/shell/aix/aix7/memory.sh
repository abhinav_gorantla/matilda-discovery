#!/usr/bin/env bash

Total=`svmon -G -O unit=GB |grep memory |awk '{print $2}'`
Used_Memory=`svmon -G -O unit=GB |grep memory |awk '{print $3}'`
Free=`svmon -G -O unit=GB |grep memory |awk '{print $4}'`
Used=`svmon -G -O unit=GB |grep memory |awk '{print $3/$2 * 100.0}' |cut -c1-5`


echo -e "\
    {\
        'used':'$Used_Memory',\
        'free':'$Free',\
        'total':'$Total',\
        'available':'$Free',\
        'used_percent':'$Used'\
    }"

