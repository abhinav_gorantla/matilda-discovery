#!/usr/bin/env bash

lslpp -Lc |tail +2  2>/dev/null > /tmp/as1

while IFS= read -r line;
  do

  name=$(echo "$line"|awk -F: '{print $1}')

  fileset=$(echo "$line"|awk -F: '{print $2}')
  version=$(echo "$line"|awk -F: '{print $3}')
  state=$(echo "$line"|awk -F: '{print $4}')
  type=$(echo "$line"|awk -F: '{print $5}')
  description=$(echo "$line"|awk -F: '{print $6}')
  destination_dir=$(echo "$line"|awk -F: '{print $7}')
  uninstaller=$(echo "$line"|awk -F: '{print $8}')
  message_catalog=$(echo "$line"|awk -F: '{print $9}')
  message_set=$(echo "$line"|awk -F: '{print $10}')
  message_number=$(echo "$line"|awk -F: '{print $11}')
  parent=$(echo "$line"|awk -F: '{print $12}')
  automatic=$(echo "$line"|awk -F: '{print $13}')
  efix_locked=$(echo "$line"|awk -F: '{print $14}')
  install_path=$(echo "$line"|awk -F: '{print $15}')
  build_date=$(echo "$line"|awk -F: '{print $16}')



echo -e "{\"'name'\":\"'"$name"'\",\"'version'\":\"'"$version"'\",\"'fileset'\":\"'"$fileset"'\",\"'state'\":\"'"$state"'\",\"'type'\":\"'"$type"'\",\"'description'\":\"'"$description"'\",\"'destination_dir'\":\"'"$destination_dir"'\",\"'uninstaller'\":\"'"$uninstaller"'\",\"'message_catalog'\":\"'"$message_catalog"'\",\"'message_set'\":\"'"$message_set"'\",\"'message_number'\":\"'"$message_number"'\",\"'parent'\":\"'"$parent"'\",\"'automatic'\":\"'"$automatic"'\",\"'efix_locked'\":\"'"$efix_locked"'\",\"'install_path'\":\"'"$install_path"'\",\"'build_date'\":\"'"$build_date"'\"}"

  done < /tmp/as1
