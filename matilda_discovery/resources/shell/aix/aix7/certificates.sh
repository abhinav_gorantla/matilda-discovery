#!/usr/bin/env bash


var1=$(find /etc/opt/ibm/powervp/certs -name "*"  -type f ! -size 0 2>&1 |egrep -v "tmp|docker|WinSxS|Permission|HISTORY|alternatives|bearer11ssl|manifest")

for loc1 in $var1

do

test=$(openssl x509 -in "$loc1" -text -noout 2>/dev/null)

 if [ "$test" != "" ];then

   path=`echo "$loc1"`
   certificate_name=$(echo "$loc1" |awk -F/ '{ print $NF }')
   not_after=$(openssl x509 -in "$loc1"  -text -noout 2>/dev/null |grep -w "Not After :" |head -1 |awk '{print $4" "$5" "$6" "$7" "$8}')
   not_before=$(openssl x509 -in "$loc1"  -text -noout 2>/dev/null |grep -w "Not Before:" | head -1 |awk '{print $3" "$4" "$5" "$6" "$7" "$8}')
   serial_number=$(openssl x509 -in "$loc1"  -text -noout 2>/dev/null |awk '/'"Serial Number:"'/{l=1;count=NR;next} l>0 && NR-count < '"1"+1' {print}')
   issuer=$(openssl x509 -in "$loc1"  -text -noout 2>/dev/null |grep -w "Issuer:" |sed ' s/[^:]*: *//;')



echo -e "{\"'path'\":\"'"$path"'\",\"'certificate_name'\":\"'"$certificate_name"'\",\"'not_after'\":\"'"$not_after"'\",\"'not_before'\":\"'"$not_before"'\",\"'serial_number'\":\"'"$serial_number"'\",\"'issuer'\":\"'"$issuer"'\"}"
fi
done
