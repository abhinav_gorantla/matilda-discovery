#!/usr/bin/env bash
#CPU Details
Cpu_Percentage=`vmstat |tail -1 | awk '{print $14 + $15 }'`
Cpu_Count=`lsdev -C -c processor |wc -l`
Vcpu_Count=`lparstat -i | grep -w "Online Virtual CPUs" |awk -F: '{print $2}'`
Cpu_Cores=`prtconf |grep -w "Number Of Processors:" |awk -F: '{print $2}'`
Cpu_Model_Name=`prtconf |grep -w "Processor Type:" |awk '{print $3}'`


echo -e "\
    {\
        'utilization_percent':'$Cpu_Percentage',\
        'physical_processors':'$Cpu_Count',\
        'physical_cores':'$Cpu_Cores',\
        'logical_processors':'$Vcpu_Count',\
        'model_name':'$Cpu_Model_Name'\
    }"


