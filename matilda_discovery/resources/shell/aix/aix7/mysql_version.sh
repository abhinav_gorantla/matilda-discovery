#!/usr/bin/env bash

mysql_version=$(mysql --version 2>/dev/null | awk '{print $3}')

echo -e "{\"'mysql_version'\":\"'"$mysql_version"'\"}"