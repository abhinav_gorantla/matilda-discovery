#!/usr/bin/env bash

for i in $(ifconfig -a 2>/dev/null |grep -v IPv6 | awk  '$1>0 {print $1}' |sed "s/:$//")

do

if [ $i == lo0 ]; then

echo -e "{\"'name'\":\"'"$i"'\",\"'ip_address'\":'"$(ifconfig lo0 2>/dev/null |grep inet |head -1 |awk '{print $2}')"',\"'received_packets'\":'"$(netstat -i 2>/dev/null |grep lo0 |head -1 |awk '{print $4}')"',\"'sent_packets'\":'"$(netstat -i 2>/dev/null |grep lo0 |head -1 |awk '{print $6}')"',\"'packet_max_size'\":'"$(netstat -i 2>/dev/null |grep lo0 |head -1 |awk '{print $2}')"',\"'type'\":'"None"'}"

fi

if [ $i != lo0 ]; then

abc=$(ifconfig $i 2>/dev/null |grep inet |awk '{print $2}')

if [[ $abc != '' ]];then

test=`ifconfig $i 2>/dev/null |head -1 |awk '{print $1}' |sed "s/:$//" |cut -d: -f 1`

echo -e "{\"'name'\":\"'"$i"'\",\"'ip_address'\":'"$(ifconfig $i 2>/dev/null |grep inet |head -1 |awk '{print $2}')"',\"'mac_address'\":'"$(netstat -ia 2>/dev/null |grep $test |head -1 |awk '{print $4}')"',\"'received_packets'\":'"$(netstat -i 2>/dev/null |grep $test |head -1 |awk '{print $5}')"',\"'sent_packets'\":'"$(netstat -i 2>/dev/null |grep $test |head -1 |awk '{print $7}')"',\"'packet_max_size'\":'"$(netstat -i 2>/dev/null |grep $test |head -1 |awk '{print $2}')"',\"'speed'\":'"$(entstat -d $i 2>/dev/null | grep -i speed | awk '{if(NR==2) print}' |awk '{print $4" "$5}')"',\"'ip_subnet'\":'"$(lsconf 2>/dev/null |grep -i "Sub Netmask" |awk '{print $3}')"',\"'dns_server'\":'"$(cat /etc/resolv.conf 2>/dev/null |grep nameserver |grep -vE "^#" |head -1  |awk '{print $2}')"',\"'gateway'\":'"$(lsconf 2>/dev/null | grep -i gateway |awk '{print $2}')"',\"'status'\":'"$(netstat -rn |grep -w "^146.1.248.5" |awk '{print $3}')"'}"

fi
fi
done
