#!/usr/bin/env bash

#L/LK - password locked
#N/NP - no password
#P/PS - password set

for i  in $(cat /etc/passwd |awk -F: '$3 > 1000 {print $1}');do

mygroup=$(id -nG $i)


echo -e "{\"'username'\":\"'" $i  "'\",\"'group_name'\":\"'" ${mygroup#*:} "'\",\"'home_directory'\":\"'" $(grep -w $i /etc/passwd | cut -d ":" -f6)  "'\",\"'user_id'\":\"'" $(grep -w $i /etc/passwd | cut -d ":" -f3)  "'\",\"'password_expiry_date'\":\"'" $(chage -l $i 2>/dev/null |grep "Password expires" |awk '{print $4" "$5" "$6}')  "'\",\"'last_login'\":\"'" $(last $i 2>/dev/null |head -1 |awk '{print $3" "$4" "$5" "$6" "$7}')  "'\",\"'last_password_change'\":\"'" $(chage -l $i 2>/dev/null |grep "Last password change" |awk '{print $5" "$6" "$7}') "'\",\"'account_expiry_date'\":\"'" $(chage -l $i 2>/dev/null |grep "Account expires" |awk '{print $4" "$5" "$6}')  "'\",\"'min_days_for_pwd_change'\":\"'" $(chage -l $i 2>/dev/null |grep "Minimum number of days between password change" |awk '{print $9}')  "'\",\"'max_days_for_pwd_change'\":\"'" $(chage -l $i 2>/dev/null |grep "Maximum number of days between password change" |awk '{print $9}')  "'\",\"'warning_days_for_pwd_change'\":\"'" $(chage -l $i 2>/dev/null |grep "Number of days of warning before password expires" |awk '{print $10}')  "'\",\"'password_inactive'\":\"'" $(chage -l $i 2>/dev/null |grep "Password inactive" |awk '{print $4" "$5" "$6}')  "'\",\"'password_status'\":\"'" $(passwd -S $i 2>/dev/null |awk '{print $2}')  "'\"}"

done

