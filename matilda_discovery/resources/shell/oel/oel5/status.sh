#!/usr/bin/env bash

nginx_status=`systemctl --type=service --state=running |grep nginx |grep -v grep |awk '{print $1}' |awk -F "." '{print $1}'`

mysql_status=`systemctl --type=service --state=running |grep mysqld |grep -v grep |awk '{print $1}' |awk -F "." '{print $1}'`


if [[ "$nginx_status" == *"nginx"* ]]; then
   nginx_pid=`netstat -ltup | grep -w nginx |grep -v grep |head -1 |awk '{print $7}' |awk -F "/" '{print $1}'`
   nginx_port=`netstat -anlp |grep $nginx_pid |grep -w "LISTEN" |grep -v grep |head -1 |awk '{print $4}'`
   nginx_cpu=`ps aux |grep -w $nginx_pid |grep -v grep |awk '{print $3}'`
   nginx_memory=`ps aux |grep -w $nginx_pid |grep -v grep |awk '{print $4}'`
   echo -e "{\"'service'\":\"'"nginx"'\",\"'cpu'\":\"'"$nginx_cpu"'\",\"'memory'\":\"'"$nginx_memory"'\",\"'port'\":\"'"${nginx_port##*:}"'\"}"

    fi


if [[ "$mysql_status" == *"mysql"* ]]; then
   mysql_pid=`netstat -ltup | grep mysqld |grep -v grep |head -1 |awk '{print $7}' |awk -F "/" '{print $1}'`
   mysql_cpu=`ps aux |grep -w $mysql_pid |grep -v grep |awk '{print $3}'`
   mysql_memory=`ps aux |grep -w $mysql_pid |grep -v grep |awk '{print $4}'`
   mysql_port=`netstat -anlp |grep -w $mysql_pid |grep -w "LISTEN" |grep -v grep |head -1  |awk '{print $4}'`
   echo -e "{\"'service'\":\"'"mysql"'\",\"'cpu'\":\"'"$mysql_cpu"'\",\"'memory'\":\"'"$mysql_memory"'\",\"'port'\":\"'"${mysql_port##*:}"'\"}"


    fi



tomcat_pid=`ps -ef |grep tomcat |grep -v jboss|grep -v newrelic |grep -v grep |head -1 |awk '{print $2}'`
if [[ "$tomcat_pid" != "" ]]; then
tomcat_port=$(netstat -anlp |grep -w $tomcat_pid |grep -w "LISTEN" |grep -v 127.0.0.1 |grep -v grep |head -1 |awk '{print $4}')
tomcat_cpu=$(ps aux |grep -w $tomcat_pid |grep -v grep |awk '{print $3}')
tomcat_memory=$(ps aux |grep -w  $tomcat_pid |grep -v grep |awk '{print $4}')
tomcat_home=`ps -ef |grep java |grep -w  $tomcat_pid  |grep -v grep |sed -n '/Dcatalina.home=/{s/.*Dcatalina.home=//;p;}' |awk '{print $1}' |head -1`
echo -e "{\"'service'\":\"'"tomcat"'\",\"'home_path'\":\"'"$tomcat_home"'\",\"'cpu'\":\"'"$tomcat_cpu"'\",\"'memory'\":\"'"$tomcat_memory"'\",\"'port'\":\"'"${tomcat_port##*:}"'\"}"
fi


weblogic_pid=`ps -ef |grep java |grep weblogic |grep "AdminServer" |grep -v grep |head -1 |awk '{print $2}'`
if [ "$weblogic_pid" != "" ]; then
weblogic_cpu=$(ps aux |grep -w $weblogic_pid |grep -v grep |awk '{print $3}')
weblogic_memory=$(ps aux |grep -w $weblogic_pid |grep -v grep |awk '{print $4}')
weblogic_port=$(ps -ef |grep java |grep -w $weblogic_pid |grep "AdminServer" |sed -n '/appserver_port=/{s/.*appserver_port=//;p;}' |awk '{print $1}')
weblogic_home=`ps -ef |grep java |grep -w $weblogic_pid |grep -v grep |sed -n '/Dwls.home=/{s/.*Dwls.home=//;p;}' |awk '{print $1}' |head -1`

if [ "$weblogic_port" != "" ];then
echo -e "{\"'service'\":\"'"weblogic"'\",\"'cpu'\":\"'"$weblogic_cpu"'\",\"'home_path'\":\"'"$weblogic_home"'\",\"'memory'\":\"'"$weblogic_memory"'\",\"'port'\":\"'"$weblogic_port"'\"}"
else
weblogic_port=`netstat -anlp |grep -w $weblogic_pid |grep -w "LISTEN" |head -1  |awk '{print $4}'`
echo -e "{\"'service'\":\"'"weblogic"'\",\"'cpu'\":\"'"$weblogic_cpu"'\",\"'home_path'\":\"'"$weblogic_home"'\",\"'memory'\":\"'"$weblogic_memory"'\",\"'port'\":\"'"${weblogic_port##*:}"'\"}"
fi
fi


jboss_pid=`ps -ef |grep java |grep jboss |grep -v grep |head -1 |awk '{print $2}'`
if [ "$jboss_pid" != "" ];then
jboss_cpu=`ps aux |grep -w $jboss_pid |grep -v grep |awk '{print $3}'`
jboss_memory=`ps aux |grep -w $jboss_pid |grep -v grep |awk '{print $4}'`
jboss_home=`ps -ef |grep java |grep -w $jboss_pid |grep -v grep |sed -n '/Djboss.home.dir=/{s/.*Djboss.home.dir=//;p;}' |awk '{print $1}' |head -1`
jboss_port=`ps -ef |grep java |grep -w $jboss_pid |grep -v grep |sed -n '/Djboss.https.port=/{s/.*Djboss.https.port=//;p;}' |awk '{print $1}' |head -1`

if [ "$jboss_port" != "" ];then
echo -e "{\"'service'\":\"'"jboss"'\",\"'cpu'\":\"'"$jboss_cpu"'\",\"'home_path'\":\"'"$jboss_home"'\",\"'memory'\":\"'"$jboss_memory"'\",\"'port'\":\"'"$jboss_port"'\"}"
else
jboss_port=`netstat -anlp |grep -w $jboss_pid |grep -w "LISTEN" |grep -v grep |head -1  |awk '{print $4}'`
echo -e "{\"'service'\":\"'"jboss"'\",\"'cpu'\":\"'"$jboss_cpu"'\",\"'home_path'\":\"'"$jboss_home"'\",\"'memory'\":\"'"$jboss_memory"'\",\"'port'\":\"'"${jboss_port##*:}"'\"}"
fi
fi
