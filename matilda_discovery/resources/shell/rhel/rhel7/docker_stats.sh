#!/usr/bin/env bash

if abc=$(docker stats --nostream --all 2>/dev/null);

then

  echo "True"

docker stats --no-trunc --no-stream --all 2>/dev/null | tail +2 | while IFS= read -r line;

  do

  container_id=$(echo "$line"|awk '{print $1}')

  name=$(echo "$line"|awk '{print $2}')

  cpu_percentage=$(echo "$line"|awk '{print $3}')

  memory_usage=$(echo "$line"|awk '{print $4}')

  memory_limit=$(echo "$line"|awk '{print $6}')

  memory_percentage=$(echo "$line"|awk '{print $7}')

  net_io=$(echo "$line"|awk '{print $8}')

  net_out=$(echo "$line"|awk '{print $10}')

  block_io=$(echo "$line"|awk '{print $11}')

  block_out=$(echo "$line"|awk '{print $13}')

  pid=$(echo "$line"|awk '{print $14}')


for i in $container_id ;do

docker container top $i 2>/dev/null |tail +2 | while IFS= read -r line;do

  user_id=$(echo "$line"|awk '{print $1}')

  process_id=$(echo "$line"|awk '{print $2}')

  cpu_percent=$(echo "$line"|awk '{print $4}')

  cmd=$(echo "$line"|awk '{print $8" "$9" "$10" "$11" "$12}')


echo -e "{\"'container_id'\":\"'"$container_id"'\",\"'name'\":\"'"$name"'\",\"'cpu_percentage'\":\"'"$cpu_percentage"'\",\"'memory_usage'\":\"'"$memory_usage"'\",\"'memory_limit'\":\"'"$memory_limit"'\",\"'memory_percentage'\":\"'"$memory_percentage"'\",\"'net_in'\":\"'"$net_io"'\",\"'net_out'\":\"'"$net_out"'\",\"'block_in'\":\"'"$block_io"'\",\"'block_out'\":\"'"$block_out"'\",\"'pid'\":\"'"$pid"'\",\"'user_id'\":\"'"$user_id"'\",\"'process_id'\":\"'"$process_id"'\",\"'cpu_percent'\":\"'"$cpu_percent"'\",\"'command'\":\"'"$cmd"'\"}"


done
done
done

else

docker stats --no-trunc --no-stream --all 2>/dev/null | tail +2 | while IFS= read -r line;

  do

  container_id=$(echo "$line"|awk '{print $1}')

  name=$(echo "$line"|awk '{print $2}')

  cpu_percentage=$(echo "$line"|awk '{print $3}')

  memory_usage=$(echo "$line"|awk '{print $4}')

  memory_limit=$(echo "$line"|awk '{print $6}')

  memory_percentage=$(echo "$line"|awk '{print $7}')

  net_io=$(echo "$line"|awk '{print $8}')

  net_out=$(echo "$line"|awk '{print $10}')

  block_io=$(echo "$line"|awk '{print $11}')

  block_out=$(echo "$line"|awk '{print $13}')

  pid=$(echo "$line"|awk '{print $14}')


for i in $container_id ;do

docker container top $i 2>/dev/null |tail +2 | while IFS= read -r line;do

  user_id=$(echo "$line"|awk '{print $1}')

  process_id=$(echo "$line"|awk '{print $2}')

  cpu_percent=$(echo "$line"|awk '{print $4}')

  cmd=$(echo "$line"|awk '{print $8" "$9" "$10" "$11" "$12}')

echo -e "{\"'container_id'\":\"'"$container_id"'\",\"'name'\":\"'"$name"'\",\"'cpu_percentage'\":\"'"$cpu_percentage"'\",\"'memory_usage'\":\"'"$memory_usage"'\",\"'memory_limit'\":\"'"$memory_limit"'\",\"'memory_percentage'\":\"'"$memory_percentage"'\",\"'net_in'\":\"'"$net_io"'\",\"'net_out'\":\"'"$net_out"'\",\"'block_in'\":\"'"$block_io"'\",\"'block_out'\":\"'"$block_out"'\",\"'pid'\":\"'"$pid"'\",\"'user_id'\":\"'"$user_id"'\",\"'process_id'\":\"'"$process_id"'\",\"'cpu_percent'\":\"'"$cpu_percent"'\",\"'command'\":\"'"$cmd"'\"}"

done
done
done
fi
