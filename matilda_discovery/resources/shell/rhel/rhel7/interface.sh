#!/usr/bin/env bash

for i in $(/sbin/ifconfig -a |grep -v IPv6 | awk  '$1>0 {print $1}' |sed "s/:$//")


do

if [ $i == lo ]; then


echo -e "{\"'name'\":\"'"$i"'\",\"'ip_address'\":'"$(/sbin/ifconfig lo 2>/dev/null |grep inet |grep -v inet6 |head -1 |awk '{print $2}')"',\"'packet_max_size'\":'"$(/sbin/ifconfig lo 2>/dev/null |grep mtu |head -1 | awk '{print $NF}')"',\"'ip_subnet'\":'"$(/sbin/ifconfig lo 2>/dev/null |grep inet |grep -v inet6 |head -1 |awk '{print $4}')"',\"'dns_server'\":'"$(cat /etc/resolv.conf | grep -v "#" | head -1 |cut -d ' ' -f2)"',\"'received_packets'\":'"$(/sbin/ifconfig lo 2>/dev/null |grep -i RX |head -1 |awk '{print $3}')"',\"'type'\":'"$(ls -lrt /sys/class/net/  2>/dev/null |grep lo |awk ' {print $11}' |head -1 |awk -F "/" ' {print $4}')"',\"'Flg'\":'"$(/sbin/ifconfig -s lo 2>/dev/null |grep -v Flg |head -1 |awk '{print $11}')"',\"'sent_packets'\":'"$(/sbin/ifconfig lo 2>/dev/null |grep  TX |head -1 |awk '{print $3}')"'}"
fi

if [ $i != lo ]; then

abc=$(/sbin/ifconfig $i 2>/dev/null |grep inet |awk '{print $2}')

if [[ $abc != '' ]];then

test=`/sbin/ifconfig $i 2>/dev/null |head -1 |awk '{print $1}' |sed "s/:$//" |cut -d: -f 1`


echo -e "{\"'name'\":\"'"$i"'\",\"'ip_address'\":'"$(/sbin/ifconfig $i 2>/dev/null |grep inet |grep -v inet6 | head -1 |awk '{print $2}')"',\"'mac_address'\":'"$(/sbin/ifconfig $i 2>/dev/null |grep ether |head -1 |awk '{print $2}')"',\"'dhcp_enabled'\":'"$(cat  /etc/sysconfig/network-scripts/ifcfg-$i  2>/dev/null |grep BOOTPROTO |head -1 |awk -F "=" '{print $2}')"',\"'packet_max_size'\":'"$(/sbin/ifconfig $i 2>/dev/null |grep mtu |head -1 | awk '{print $NF}')"',\"'speed'\":'"$(ethtool $i 2>/dev/null |grep Speed |awk '{print $2}')"',\"'ip_subnet'\":'"$(/sbin/ifconfig $i 2>/dev/null |grep inet  |grep -v inet6 |head -1 |awk '{print $4}')"',\"'dns_server'\":'"$(cat /etc/resolv.conf | grep -v "#" | head -1 |cut -d ' ' -f2)"',\"'received_packets'\":'"$(/sbin/ifconfig $i 2>/dev/null |grep -i RX |head -1 |awk '{print $3}')"',\"'type'\":'"physical"',\"'Flg'\":'"$(/sbin/ifconfig -s $i 2>/dev/null |grep -v Flg |head -1 |awk '{print $11}')"',\"'sent_packets'\":'"$(/sbin/ifconfig $i 2>/dev/null |grep  TX |head -1 |awk '{print $3}')"'}"


fi

fi

done
