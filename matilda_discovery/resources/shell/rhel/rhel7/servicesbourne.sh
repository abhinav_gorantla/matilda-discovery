#!/usr/bin/env bash


var1=`find / \( -name nginx.conf -o -name domain-registry.xml -o -name domain.xml -o -name server.xml -o -name httpd.conf -o -name my.cnf -o -name version.sh -o -name weblogic.jar -o -name domain.sh \) -print 2>&1 |egrep -v "tmp|denied|backup|bkp|snpashot|template|permission|matilda|temp|Matilda_Refactor|find|docker|WinSxS|Permission|HISTORY|alternatives|bearer11ssl|manifest|proc"`

for serviceitem in $var1
do
    file_name=`echo "$serviceitem" |awk -F/ '{ print $NF }'`
    #echo -e "{\"'""$file_name"'\":\"'"$serviceitem"'\"}"
    printf "{\"'%s'\":\"'%s'\"}\n" "$file_name" "$serviceitem"
done
