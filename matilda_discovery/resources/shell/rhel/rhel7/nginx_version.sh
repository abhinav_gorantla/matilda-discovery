#!/usr/bin/env bash

nginx_version=$(nginx -v 2>&1 |awk '{print $3}' |awk -F '/' '{print $2}')

echo -e "{\"'nginx_version'\":\"'"$nginx_version"'\"}"
