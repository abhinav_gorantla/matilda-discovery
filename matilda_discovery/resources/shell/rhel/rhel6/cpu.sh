#!/usr/bin/env bash

Cpu_Percentage=`grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage}'|cut -c1-5`
Cpu_Count=`cat /proc/cpuinfo | grep "physical id" | sort | uniq | wc -l`
Vcpu_Count=`grep -c ^processor /proc/cpuinfo`
Cpu_Cores=`cat /proc/cpuinfo |grep cpu\ cores |uniq |awk '{print $4}'`
Cpu_Model_Name=`cat /proc/cpuinfo  |grep "model name" |uniq |awk '{print $4 " " $5 " " $6 " " $7" " $8" " $9" " $10}'`


echo -e "\
    {\
        'utilization_percent':'$Cpu_Percentage',\
        'physical_processors':'$Cpu_Count',\
        'physical_cores':'$Cpu_Cores',\
        'logical_processors':'$Vcpu_Count',\
        'model_name':'$Cpu_Model_Name'\
    }"

