#!/usr/bin/env bash

last -F | head -n-4 |egrep -v "crash|reboot" | while IFS= read -r line;

  do

  user_name=$(echo "$line"|awk '{print $1}')

  server_ip=$(echo "$line"|awk '{print $3}')

  time=$(echo "$line"|awk '{print $5" "$6" "$7" "$8}')

if [[ "$server_ip" =~ [0-9] ]]; then


echo -e "{\"'user_name'\":\"'"$user_name"'\",\"'server_ip'\":\"'"$server_ip"'\",\"'time'\":\"'"$time"'\"}"

fi
done


