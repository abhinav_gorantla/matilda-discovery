#!/usr/bin/env bash

/sbin/chkconfig --list 2>/dev/null | {

while IFS= read -r line;

do
system_service_name=$(echo "$line"|awk  '{print $1}')
system_runelevel1_status=$(echo "$line"|awk  '{print $2}' | awk -F ":" '{print $2}' )
system_runelevel2_status=$(echo "$line"|awk  '{print $3}' | awk -F ":" '{print $2}')
system_runelevel3_status=$(echo "$line"|awk  '{print $4}' | awk -F ":" '{print $2}')
system_runelevel4_status=$(echo "$line"|awk  '{print $5}' | awk -F ":" '{print $2}')
system_runelevel5_status=$(echo "$line"|awk  '{print $6}' | awk -F ":" '{print $2}')
system_runelevel6_status=$(echo "$line"|awk  '{print $7}' | awk -F ":" '{print $2}')



   echo -e "{\"'system_service_name'\":\"'"$system_service_name"'\",\"'system_runelevel1_status'\":\"'"$system_runelevel1_status"'\",\"'system_runelevel2_status'\":\"'"$system_runelevel2_status"'\",\"'system_runelevel3_status'\":\"'"$system_runelevel3_status"'\",\"'system_runelevel4_status'\":\"'"$system_runelevel4_status"'\",\"'system_runelevel5_status'\":\"'"$system_runelevel5_status"'\",\"'system_runelevel6_status'\":\"'"$system_runelevel6_status"'\"}"



done

}