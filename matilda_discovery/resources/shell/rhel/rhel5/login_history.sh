#!/usr/bin/env bash

last 2>/dev/null | egrep -v "crash|reboot" | while IFS= read -r line;

  do

  user_name=$(echo "$line"|awk '{print $1}')

  server_ip=$(echo "$line"|awk '{print $3}')

  time=$(echo "$line"|awk '{print $4" "$5" "$6" "$7}')

if [[ "$server_ip" =~ [a-z] ]]; then

echo -e "{\"'user_name'\":\"'"$user_name"'\",\"'server_ip'\":\"'"$server_ip"'\",\"'time'\":\"'"$time"'\"}"
fi
done
