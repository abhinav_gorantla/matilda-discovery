#!/usr/bin/env bash

Platform=`uname -a |awk '{print $1}'`
OperatingSystem=`cat /etc/redhat-release | awk '{print $1" "$2" "$3" "$4" "$5}'`
ReleaseVersion=`cat /etc/redhat-release |awk '{print $7}' |cut -c1`
Version=`cat /etc/redhat-release |awk '{print $7}'`
BootUpDate=`date`
CoresCount=`cat /proc/cpuinfo |grep cpu\ cores |uniq |awk '{print $4}'`
Vcpu_Count=`grep -c ^processor /proc/cpuinfo`
MemSize=`free -g | grep Mem | awk '{print $2}'`
InstanceType=`dmesg | grep DMI &>/dev/null&& echo Virtual || echo Physical`
ServiceProvider=`dmidecode -s system-manufacturer`
Hostname=`hostname`
Instruction_Set=`getconf LONG_BIT`
BIOS_Configuration=`dmidecode | grep "BIOS Information" -A10 | grep -e "Version:" -e "Vendor:" -e "Release Date:" |awk -F ":" '{print $2}'|xargs | sed 's/\n/,/g'`
Pagefile_Size=`getconf PAGE_SIZE | awk '{$1=$1/1024; print $1,"KB";}'`
Internet_Access=`ping -q -w1 -c1 google.com &>/dev/null && echo True || echo False`
Timezone=`date +"%Z %z" |awk '{print $1}'`
Installed_On=`rpm -qi basesystem | grep Install |awk '{print $4 " "  $5 " "  $6 " " }'`
Uptime=`uptime |awk '{print $3}'|sed 's/.$//' |awk -F ":" '{print $1 " " "Hours" " " $2 " " "Minutes"}'`
gateway=`route -n | grep 'UG[ \t]' | head -1| awk '{print $2}'`

gateway_ip=''
flag=False
for item in "${gateway[@]}"; do
  if [ "$gateway_ip" != "" ];then
        gateway_ip=$gateway_ip,$item
        else gateway_ip=$item
    fi
done


test1=`echo $gateway_ip | sed 's/"//g' | tr -d \'`
test2=`echo ${test1// /,}`



echo -e "\
            {\
                'platform':'$Platform',\
                'operating_system':'$OperatingSystem',\
                'release_version':'$ReleaseVersion',\
                'version':'$Version',\
                'system_date':'$BootUpDate',\
                'memory':'$MemSize',\
                'instance_type':'$InstanceType',\
                'service_provider':'$ServiceProvider',\
                'gateway_ip':'$test2',\
                'bios_configuration':'$BIOS_Configuration',\
                'name':'$Hostname',\
                'instruction_set':'$Instruction_Set',\
                'pagefile_size':'$Pagefile_Size',\
                'internet_access':'$Internet_Access',\
                'timezone':'$Timezone',\
                'installed_on':'$Installed_On',\
                'uptime':'$Uptime'\
            }"

