#!/usr/bin/env bash

for i in $(sudo ifconfig -a 2>/dev/null |grep -v IPv6 | awk  '$1>0 {print $1}' |sed "s/:$//")

do

if [ $i == lo ]; then


echo -e "{\"'name'\":\"'"$i"'\",\"'ip_address'\":'"$(sudo ifconfig $i 2>/dev/null |grep inet |grep -v inet6 |awk '{print $2}'|awk -F ":" '{print $2}')"',\"'packet_max_size'\":'"$(sudo ifconfig $i 2>/dev/null |grep -i mtu |awk '{print $4}' |awk -F ":" '{print $2}')"',\"'ip_subnet'\":'"$(sudo ifconfig $i 2>/dev/null |grep inet |grep -v inet6 |awk -F ":" '{print $3}')"',\"'dns_server'\":'"$(cat /etc/resolv.conf  2>/dev/null |grep -i '^nameserver'|head -n1|cut -d ' ' -f2)"',\"'received_packets'\":'"$(sudo ifconfig $i 2>/dev/null |grep  RX |head -1 |awk '{print $2}'| awk -F ":" '{print $2}')"',\"'type'\":'"$(ls -lrt /sys/class/net/  2>/dev/null |grep $i |awk ' {print $11}' |awk -F "/" ' {print $4}')"',\"'Flg'\":'"$(sudo ifconfig -s $i 2>/dev/null |grep -v Flg |awk '{print $NF}')"',\"'sent_packets'\":'"$(sudo ifconfig $i 2>/dev/null |grep  TX |head -1 |awk '{print $2}'| awk -F ":" '{print $2}')"'}"
fi
if [ $i != lo ]; then

test=$(sudo ifconfig $i 2>/dev/null |grep inet |grep -v inet6 |awk '{print $2}'|awk -F ":" '{print $2}')

if [[ $test != '' ]];then

echo -e "{\"'name'\":\"'"$i"'\",\"'ip_address'\":'"$(sudo ifconfig $i 2>/dev/null |grep inet |grep -v inet6 |awk '{print $2}'|awk -F ":" '{print $2}')"',\"'mac_address'\":'"$(sudo ifconfig $i 2>/dev/null|grep HWaddr |awk '{print $5}')"',\"'dhcp_enabled'\":'"$(cat  /etc/sysconfig/network-scripts/ifcfg-$i  2>/dev/null |grep BOOTPROTO |awk -F "=" '{print $2}')"',\"'packet_max_size'\":'"$(sudo ifconfig $i 2>/dev/null |grep -i mtu |awk '{print $5}' |awk -F ":" '{print $2}')"',\"'speed'\":'"$(ethtool $i 2>/dev/null |grep Speed |awk '{print $2}')"',\"'ip_subnet'\":'"$(sudo ifconfig $i 2>/dev/null |grep inet |grep -v inet6 |awk '{print $4}' |awk -F ":" '{print $2}')"',\"'dns_server'\":'"$(cat /etc/resolv.conf  2>/dev/null |grep -i '^nameserver'|head -n1|cut -d ' ' -f2)"',\"'received_packets'\":'"$(sudo ifconfig $i 2>/dev/null |grep  RX |head -1 |awk '{print $2}'| awk -F ":" '{print $2}')"',\"'type'\":'"physical"',\"'Flg'\":'"$(sudo ifconfig -s $i 2>/dev/null |grep -v Flg |awk '{print $NF}')"',\"'sent_packets'\":'"$(sudo ifconfig $i 2>/dev/null |grep  TX |head -1 |awk '{print $2}'| awk -F ":" '{print $2}')"'}"

fi
fi

done
