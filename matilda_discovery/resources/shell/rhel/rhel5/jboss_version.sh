#!/usr/bin/env bash

var=(ARGUMENTS)

for loc1 in "${var[@]}"; do
    abc=$(echo "$loc1" | rev | cut -c 10- | rev)
    jboss_version=$(cd "$abc";./domain.sh --version |tail -1 |awk '{print $3}')
    if [ "$jboss_version" != "" ];then
    break
    fi
done

echo -e "{\"'jboss_version'\":\"'"$jboss_version"'\"}"