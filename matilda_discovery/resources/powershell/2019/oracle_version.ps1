﻿$ErrorActionPreference = "SilentlyContinue"
[string[]]$pathlist=@(ARGUMENTS)
$oracle_version = $null
foreach($item in $pathlist){
       if($null -eq $oracle_version){
            $oracle_result = gci $item -ErrorAction SilentlyContinue | %{ $_.VersionInfo } | Select-Object -Property FileVersion

            if($oracle_result){
                $oracle_version=$oracle_result.FileVersion}
                                    }
                         }

$main_data=New-Object Psobject
$main_data | Add-Member oracle_version ($oracle_version)
$main_data | format-list