﻿$ErrorActionPreference = "SilentlyContinue"
[string[]]$pathlist=@(ARGUMENTS)
$tomcat_version = $null
foreach($item in $pathlist){
       if($tomcat_version -eq $null){
            $tomcat_cmd='cd ' + "'" + $item + "'" + '; .\\version.bat --version'
            $tomcat_result=Invoke-Expression $tomcat_cmd
            if($tomcat_result){
                 $tomcat_version=$tomcat_result[5].Split('/')[1]
                 break}

       }
}
$main_data=New-Object Psobject
$main_data | Add-Member tomcat_version ($tomcat_version)
$main_data | format-list


