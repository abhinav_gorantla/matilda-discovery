$ErrorActionPreference = "SilentlyContinue"
$ComputerName = $env:ComputerName
$header = 'Hostname','OSName','OSVersion','OSManufacturer','OSConfig','Buildtype', 'RegisteredOwner','RegisteredOrganization','ProductID','InstallDate', 'StartTime','Manufacturer','Model','Type','Processor','BIOSVersion', 'WindowsFolder' ,'SystemFolder','StartDevice','Culture', 'UICulture', 'TimeZone','PhysicalMemory', 'AvailablePhysicalMemory' , 'MaxVirtualMemory', 'AvailableVirtualMemory','UsedVirtualMemory','PagingFile','Domain' ,'LogonServer'
$host_info=systeminfo.exe /FO CSV /S $ComputerName |Select-Object -Skip 1 |ConvertFrom-CSV -Header $header

$BaseSize=Get-WmiObject Win32_PageFileusage | select AllocatedBaseSize
if($BaseSize.AllocatedBaseSize -lt 500){$page_file_size = $BaseSize.AllocatedBaseSize.ToString() + " MB" } else { $page_file_size = [Math]::Round($BaseSize.AllocatedBaseSize/1024, 2).ToString() + " GB"}
$sys_date=get-date | select DateTime -ErrorAction Stop
$sku_number=Get-WmiObject Win32_OperatingSystem | select OperatingSystemSKU
$virtual_instances = "HVM domU", 'VMware Virtual Platform', 'OpenStack Nova', 'VMware7,1'
if($virtual_instances -contains $host_info.Model){$Instance_Type="Virtual"} else{$Instance_Type="Physical"}
$InternetAccess=Test-Connection -ComputerName google.com -Quiet -Count 1
$mem=[math]::Round([int]($host_info.PhysicalMemory -replace "[^0-9.]")/1024,2)
$version=$host_info.OSName -split "Windows" |Select -Skip 1 -First 1

$resp=New-Object -TypeName psobject
$resp|Add-Member -MemberType NoteProperty -Name release_version -Value $host_info.OSVersion
$resp|Add-Member -MemberType NoteProperty -Name bios_configuration -Value $host_info.BIOSVersion
$resp|Add-Member -MemberType NoteProperty -Name name -Value $host_info.Hostname
$resp|Add-Member -MemberType NoteProperty -Name installed_on -Value $host_info.InstallDate
$resp|Add-Member -MemberType NoteProperty -Name uptime -Value $host_info.StartTime
$resp|Add-Member -MemberType NoteProperty -Name instruction_set -Value $host_info.Type
$resp|Add-Member -MemberType NoteProperty -Name timezone -Value $host_info.TimeZone
$resp|Add-Member -MemberType NoteProperty -Name service_provider -Value $host_info.OSManufacturer
$resp|Add-Member -MemberType NoteProperty -Name version -Value $version
$resp|Add-Member -MemberType NoteProperty -Name memory -Value $mem
$resp|Add-Member -MemberType NoteProperty -Name internet_access -Value $InternetAccess
$resp|Add-Member -MemberType NoteProperty -Name pagefile_size -Value $page_file_size
$resp|Add-Member -MemberType NoteProperty -Name system_date -Value $sys_date.DateTime
$resp|Add-Member -MemberType NoteProperty -Name sku -Value $sku_number.OperatingSystemSKU
$resp|Add-Member -MemberType NoteProperty -Name instance_type -Value $Instance_Type
$resp|Add-Member -MemberType NoteProperty -Name physical_processors -Value $host_info.Processor.split()[0]
$resp|Add-Member -MemberType NoteProperty -Name domain -Value $host_info.Domain
$resp