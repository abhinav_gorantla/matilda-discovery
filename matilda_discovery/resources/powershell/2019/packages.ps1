$ErrorActionPreference = "SilentlyContinue"

$packages = Get-ItemProperty HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\* | Select-Object DisplayName,DisplayVersion,Publisher 
$packageList = @()
foreach ($package in $packages){
if($package.DisplayName -ne $null)
    {
		$pObj = New-Object PSobject
		$pObj | Add-Member -MemberType NoteProperty -Name "name" -Value $package.DisplayName
		$pObj | Add-Member -MemberType NoteProperty -Name "version" -Value $package.DisplayVersion
		$pObj | Add-Member -MemberType NoteProperty -Name "repository" -Value $package.Publisher
		$packageList += $pObj
	}}
$packageList | Format-List

