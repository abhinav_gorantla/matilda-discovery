$ErrorActionPreference = "SilentlyContinue"

$upservices = Get-CimInstance Win32_StartupCommand

$upservicesList = @()

foreach ($service in $upservices){
if($service.Command -ne $null)
    {
		$pObj = New-Object PSobject
		$pObj | Add-Member -MemberType NoteProperty -Name "command" -Value $service.Command
        $pObj | Add-Member -MemberType NoteProperty -Name "user" -Value $service.User
        $pObj | Add-Member -MemberType NoteProperty -Name "caption" -Value $service.Caption


		$upservicesList += $pObj
	}}
$upservicesList | Format-List