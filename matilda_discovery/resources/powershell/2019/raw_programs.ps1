$ErrorActionPreference = "SilentlyContinue"

$programs = Get-ItemProperty HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\* | Select-Object DisplayName, Publisher, InstallLocation, Version, DisplayVersion, VersionMajor, VersionMinor, InstallDate, InstallSource

$programsList = @()

foreach ($program in $programs){
if($program.DisplayName -ne $null)
    {
		$pObj = New-Object PSobject
		$pObj | Add-Member -MemberType NoteProperty -Name "name" -Value $program.DisplayName
        $pObj | Add-Member -MemberType NoteProperty -Name "publisher" -Value $program.Publisher
        $pObj | Add-Member -MemberType NoteProperty -Name "install_location" -Value $program.InstallLocation
        $pObj | Add-Member -MemberType NoteProperty -Name "version" -Value $program.Version
        $pObj | Add-Member -MemberType NoteProperty -Name "display_version" -Value $program.DisplayVersion
        $pObj | Add-Member -MemberType NoteProperty -Name "version_major" -Value $program.VersionMajor
        $pObj | Add-Member -MemberType NoteProperty -Name "version_minor" -Value $program.VersionMinor
        $pObj | Add-Member -MemberType NoteProperty -Name "install_date" -Value $program.InstallDate
        $pObj | Add-Member -MemberType NoteProperty -Name "install_source" -Value $program.InstallSource



		$programsList += $pObj
	}}
$programsList | Format-List
