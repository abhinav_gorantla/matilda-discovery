$ErrorActionPreference = "SilentlyContinue"
$CpuCores = (Get-WMIObject Win32_ComputerSystem).NumberOfLogicalProcessors
$ComputerName = $env:ComputerName
$header = 'Hostname','OSName','OSVersion','OSManufacturer','OSConfig','Buildtype', 'RegisteredOwner','RegisteredOrganization','ProductID','InstallDate', 'StartTime','Manufacturer','Model','Type','Processor','BIOSVersion', 'WindowsFolder' ,'SystemFolder','StartDevice','Culture', 'UICulture', 'TimeZone','PhysicalMemory', 'AvailablePhysicalMemory' , 'MaxVirtualMemory', 'AvailableVirtualMemory','UsedVirtualMemory','PagingFile','Domain' ,'LogonServer'
$host_info=systeminfo.exe /FO CSV /S $ComputerName |Select-Object -Skip 1 |ConvertFrom-CSV -Header $header
$total_mem_Mb=[math]::Round([int]($host_info.PhysicalMemory -replace "[^0-9.]"),2)


function Get-NetworkStatistics
{
 $properties = 'protocol','local_address','number', 'Memory_percent','Cpu_percent'
 $properties += 'foreign_address','foreign_port','status','service','process_id','cpu_time'
 netstat -anop TCP |Select-String -Pattern '\s+(TCP|UDP)' | ForEach-Object {
 $item = $_.line.split(" ",[System.StringSplitOptions]::RemoveEmptyEntries)



 if($item[1] -notmatch '^\[::')
 {
 if (($la = $item[1] -as [ipaddress]).AddressFamily -eq 'InterNetworkV6')
 {
 $localAddress = $la.IPAddressToString
 $localPort = $item[1].split('\]:')[-1]
 }
 else
 {
 $localAddress = $item[1].split(':')[0]
 $localPort = $item[1].split(':')[-1]
 }



 if (($ra = $item[2] -as [ipaddress]).AddressFamily -eq 'InterNetworkV6')
 {
 $remoteAddress = $ra.IPAddressToString
 $remotePort = $item[2].split('\]:')[-1]
 }
 else
 {
 $remoteAddress = $item[2].split(':')[0]
 $remotePort = $item[2].split(':')[-1]
 }



  $processes = Get-Process | Where-Object {$_.Id -eq $item[-1]} | Select Name, CPU
  $cpu = $processes.CPU
  $service = $processes.Name
  $memoryCmd = 'Get-WmiObject -class Win32_PerfFormattedData_PerfProc_Process -filter {name = "'  + $service +   '"} | Select-Object workingSetPrivate'
  $WorkingSetPrivate = Invoke-Expression $memoryCmd
  $Memory_Mb=[Math]::Round(($WorkingSetPrivate.workingSetPrivate/1mb),2)
  $Memory_percent=[Math]::Round(($Memory_Mb*100)/$total_mem_Mb,2)
  $CpuCmd = (Get-Counter '\Process($service*)\% Processor Time').CounterSamples | Select-Object CookedValue
  $Cpu_percent=[Math]::Round(($CpuCmd.CookedValue)/$CpuCores,2)



New-Object PSObject -Property @{
 process_id = $item[-1]
 #service = (Get-Process -Id $item[-1] -ErrorAction SilentlyContinue).Name
 #cpu = (Get-Process -Id $item[-1] -ErrorAction SilentlyContinue).CPU
 service = $service
 protocol = $item[0]
 local_address = $localAddress
 number = $localPort
 foreign_address =$remoteAddress
 foreign_port = $remotePort
 cpu_time = $cpu
 memory_percent = $Memory_percent
 cpu_percent = $Cpu_percent
 status = if($item[0] -eq 'tcp') {$item[3]} else {$null}
 } |Select-Object -Property $properties
 }
 }
}




Get-NetworkStatistics |Format-List