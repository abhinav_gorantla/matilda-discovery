    $events = Get-EventLog -LogName security -After (Get-Date).AddDays(-30) | Where-Object {($_.instanceId -eq 682)}             
	$loginList = @()
    Foreach($i in $events) {                        

		$messages = $i.Message -split "`n" 
		foreach($message in $messages){
			$domainString = $message | Select-String -Pattern "Domain:" | Out-String
			$domain_pos = $domainString.Trim().IndexOf("Domain:") 
			
			if($domainString -and $domain_pos -eq 0){
				$domain_val = $domainString.Substring($domainString.IndexOf(':')+1) 
				if($domain_val){
					$domain_val = $domain_val.Trim()
				}
			}
			
			$userString = $message | Select-String -Pattern "User Name:" | Out-String
			$user_pos = $userString.Trim().IndexOf("User Name:") 
			if($userString -and $user_pos -eq 0){
				$user_val = $userString.Substring($userString.IndexOf(':')+1) 
				if($user_val){
					$user_val = $user_val.Trim()
				}
			}
			
			
			$clientString = $message | Select-String -Pattern "Client Address:" | Out-String
			$client_pos = $clientString.Trim().IndexOf("Client Address:") 
			if($clientString -and $client_pos -eq 0){
				$clientAddress = $clientString.Substring($clientString.IndexOf(':')+1) 
				if($clientAddress){
					$clientAddress = $clientAddress.Trim()
				}
			}
		}

	   $DateTime = $i.TimeWritten	   
	   $domain = $domain_val
	   $user_val = $domain_val+ '\' + $user_val
	   $clientAddress = $clientAddress
	   $action = 'LogOn'
	   
	    $login = New-Object Psobject
		$login | Add-Member noteproperty User ($user_val)
		$login | Add-Member noteproperty Domain ($domain)
		$login | Add-Member noteproperty Action ($action)
		$login | Add-Member noteproperty ClientAddress ($clientAddress)
		$login | Add-Member noteproperty DateTime ($DateTime)
		
		$loginList += $login
		
	}
	  
$loginList                       

