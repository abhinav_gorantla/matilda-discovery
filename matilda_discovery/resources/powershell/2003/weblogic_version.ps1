﻿$ErrorActionPreference = "SilentlyContinue"
[string[]]$pathlist=@(ARGUMENTS)
$weblogic_version = $null
foreach($item in $pathlist){
       if($weblogic_version -eq $null){
            $result_weblogic='cd '+$item +" | java -cp weblogic.jar weblogic.version"
            $result_weblogic=Invoke-Expression $result_weblogic
            if($result_weblogic){
                 $weblogic_version=$result_weblogic[1].Split(" ")[2]
                 break}

       }
}
$main_data=New-Object Psobject
$main_data | Add-Member weblogic_version ($weblogic_version)
$main_data | format-list


