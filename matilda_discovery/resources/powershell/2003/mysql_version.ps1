﻿$ErrorActionPreference = "SilentlyContinue"
[string[]]$pathlist=@(ARGUMENTS)
$sql_version = $null
foreach($item in $pathlist){
       if($sql_version -eq $null){
            $sql_cmd='cd ' + "'" + $item + "'" + '; .\mysql.exe --version'
            $sql_result=Invoke-Expression $sql_cmd
            if($sql_result){
                 $tempitem = $item.Replace("/","\")
                 $sql_version=$sql_result.Replace($tempitem + '\mysql.exe  Ver ',"").split("")[0]
                 break}

       }
}
$main_data=New-Object Psobject
$main_data | Add-Member mysql_version ($sql_version)
$main_data | format-list
