﻿$ErrorActionPreference = "SilentlyContinue"
[string[]]$pathlist=@(ARGUMENTS)
$jboss_version = $null
foreach($item in $pathlist){
       if($jboss_version -eq $null){
            $jboss_result = $item.Substring(0,$item.IndexOf('/bin')+1) +'modules\system\layers\base\org\jboss\as\product\wildfly-web\dir\META-INF\MANIFEST.MF'
            if($jboss_result){
                 $jboss_version=((Get-Content -Path $jboss_result).Split("")[4])
                 break}

       }
}
$main_data=New-Object Psobject
$main_data | Add-Member jboss_version ($jboss_version)
$main_data | format-list
