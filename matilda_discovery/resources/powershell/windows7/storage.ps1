$ErrorActionPreference = "SilentlyContinue"
$result=@()
    $disks=gwmi -query "SELECT * FROM Win32_DiskDrive"
    ForEach($disk in $disks){
        $partitions=gwmi -query "ASSOCIATORS OF {Win32_DiskDrive.DeviceID='$($disk.DeviceID)'} WHERE AssocClass = Win32_DiskDriveToDiskPartition"
        ForEach($partition in $partitions){
            $logicaldisks=gwmi -query "ASSOCIATORS OF {Win32_DiskPartition.DeviceID='$($partition.DeviceID)'} WHERE AssocClass = Win32_LogicalDiskToPartition"
            ForEach($logicaldisk in $logicaldisks){
                #if($logicaldisk){
                    $result+=New-Object PSObject -Property @{
					'disk' = "Disk" + $disk.index
					'model' = $disk.Model
					'firmware' = $disk.FirmwareRevision
					'disk_serial_number' = $disk.SerialNumber
					'disk_size' = [math]::Round(($disk.Size / 1GB ), 2)
					'partitions_count' = $disk.Partitions
					'partition_index' = $partition.index
					'boot_partition' = $partition.BootPartition
					'partition_size' = [math]::Round(($partition.Size / 1GB ), 2)
					'blocks_count' = $partition.NumberOfBlocks
					'block_size' = $partition.BlockSize
					'volume_serial_number' = $logicaldisk.SerialNumber
					'compressed' = $logicaldisk.Compressed
					'logical_disk' = $logicaldisk.VolumeName
					'file_system' = $logicaldisk.FileSystem
					'total' =  [math]::Round(($logicaldisk.Size / 1GB ), 2)
					'available' =  [math]::Round(($logicaldisk.FreeSpace / 1GB ), 2)
					'used' =  [math]::Round($logicaldisk.Size / 1GB -$logicaldisk.FreeSpace / 1GB,2)
                    'used_percent' =  [math]::Round([math]::Round($logicaldisk.Size / 1GB -$logicaldisk.FreeSpace / 1GB,2)/[math]::Round(($logicaldisk.Size / 1GB ), 2)*100,2)

                    }
                #}
            }
        }
    }
$result