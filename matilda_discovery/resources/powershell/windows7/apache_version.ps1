﻿$ErrorActionPreference = "SilentlyContinue"
[string[]]$pathlist=@("C:\Users\matilda\AppData\Roaming\Apache24\bin")
$apache_version = $null
foreach($item in $pathlist){
       if($apache_version -eq $null){
            $apache_cmd=$item + '\httpd.exe -v'
            $apache_result=Invoke-Expression $apache_cmd
            $apache_version=$apache_result[0].Split('')[2].split('/')[1]

       }
}
$main_data=New-Object Psobject
$main_data | Add-Member apache_version ($apache_version)
$main_data | format-list
