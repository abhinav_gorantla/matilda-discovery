﻿$ErrorActionPreference = "SilentlyContinue"

#MEMORY
$ComputerName = $env:ComputerName

#creatign an object and fetching all the info required using header
$header = 'Hostname','OSName','OSVersion','OSManufacturer','OSConfig','Buildtype', 'RegisteredOwner','RegisteredOrganization','ProductID','InstallDate', 'StartTime','Manufacturer','Model','Type','Processor','BIOSVersion', 'WindowsFolder' ,'SystemFolder','StartDevice','Culture', 'UICulture', 'TimeZone','PhysicalMemory', 'AvailablePhysicalMemory' , 'MaxVirtualMemory', 'AvailableVirtualMemory','UsedVirtualMemory','PagingFile','Domain' ,'LogonServer'
$header1 = 'Hostname'
$host_info=systeminfo.exe /FO CSV /S $ComputerName |Select-Object -Skip 1 |ConvertFrom-CSV -Header $header

#adding addtitonal features required for the response body and formating the details as per the response required

$total_memory=[math]::Round([int]($host_info.PhysicalMemory -replace "[^0-9.]")/1024,2)
$available_memory=[math]::Round([int]($host_info.AvailablePhysicalMemory -replace "[^0-9.]")/1024,2)
$used_memory=$total_memory-$available_memory
$used_percentage=[math]::Round($used_memory/$total_memory*100,2)


#CPU
$cpu_average=Get-WmiObject win32_processor | Measure-Object -property LoadPercentage -Average | select Average

#STORAGE
$totalstorage = 0
$availablestorage =0
$storage_used_percentage = 0
$disks=gwmi -query "SELECT * FROM Win32_DiskDrive"
    ForEach($disk in $disks){
        $partitions=gwmi -query "ASSOCIATORS OF {Win32_DiskDrive.DeviceID='$($disk.DeviceID)'} WHERE AssocClass = Win32_DiskDriveToDiskPartition"
        ForEach($partition in $partitions){
            $logicaldisks=gwmi -query "ASSOCIATORS OF {Win32_DiskPartition.DeviceID='$($partition.DeviceID)'} WHERE AssocClass = Win32_LogicalDiskToPartition"
            ForEach($logicaldisk in $logicaldisks){
                    $totalstorage+=[math]::Round(($logicaldisk.Size / 1GB ), 2)
                    $availablestorage+=[math]::Round(($logicaldisk.FreeSpace / 1GB ), 2)

            }
        }
    }
    $storage_used_percentage = [math]::Round((($totalstorage-$availablestorage)*100/$totalstorage), 2)




$resp=New-Object -TypeName psobject
$resp |Add-Member -MemberType NoteProperty -Name cpu_percent -value $cpu_average.Average
$resp|Add-Member -MemberType NoteProperty -Name memory_percent -value $used_percentage
$resp|Add-Member -MemberType NoteProperty -Name storage_percent -value $storage_used_percentage


$resp|format-list