$main_data=$null
$ErrorActionPreference = "SilentlyContinue"
$certificates = Get-ChildItem -Path Cert:\LocalMachine -recurse
$main_data=@()
foreach($certificate in $certificates){
If ($certificate.PSISContainer -eq $false) {
    $pObj = New-Object PSobject
    $pObj | Add-Member -MemberType NoteProperty -Name path -Value $certificate.PSPath
    $pObj | Add-Member -MemberType NoteProperty -Name certificate_name -Value $certificate.FriendlyName
    $pObj | Add-Member -MemberType NoteProperty -Name issuer -Value $certificate.Issuer
    $pObj | Add-Member -MemberType NoteProperty -Name not_after -Value $certificate.NotAfter
    $pObj | Add-Member -MemberType NoteProperty -Name not_before -Value $certificate.NotBefore
    $pObj | Add-Member -MemberType NoteProperty -Name serial_number -Value $certificate.SerialNumber
    $pObj | Add-Member -MemberType NoteProperty -Name dns_name_list -Value $certificate.DnsNameList
    $pObj | Add-Member -MemberType NoteProperty -Name version -Value $certificate.Version
    $pObj | Add-Member -MemberType NoteProperty -Name subject -Value $certificate.Subject
    $pObj | Add-Member -MemberType NoteProperty -Name ps_provider -Value $certificate.PSProvider
    $pObj | Add-Member -MemberType NoteProperty -Name thumb_print -Value $certificate.Thumbprint

    $main_data += $pObj
    }
}
$main_data | Format-List | Out-String -Width 4096