$ErrorActionPreference = "SilentlyContinue"

$packages = Get-Process | Select-Object Name, Description, ProcessName, Product, Path, CPU, FileVersion, ProductVersion, Id, TotalProcessorTime, StartTime
$packageList = @()

foreach ($package in $packages){
if($package.Name -ne $null)
    {
		$pObj = New-Object PSobject
		$pObj | Add-Member -MemberType NoteProperty -Name "name" -Value $package.Name
        $pObj | Add-Member -MemberType NoteProperty -Name "description" -Value $package.Description
        $pObj | Add-Member -MemberType NoteProperty -Name "process_name" -Value $package.ProcessName
        $pObj | Add-Member -MemberType NoteProperty -Name "product" -Value $package.Product
        $pObj | Add-Member -MemberType NoteProperty -Name "path" -Value $package.Path
        $pObj | Add-Member -MemberType NoteProperty -Name "cpu_percent" -Value $package.CPU
        $pObj | Add-Member -MemberType NoteProperty -Name "file_version" -Value $package.FileVersion
        $pObj | Add-Member -MemberType NoteProperty -Name "product_version" -Value $package.ProductVersion
        $pObj | Add-Member -MemberType NoteProperty -Name "process_id" -Value $package.Id
        $pObj | Add-Member -MemberType NoteProperty -Name "total_processor_time" -Value $package.TotalProcessorTime
        $pObj | Add-Member -MemberType NoteProperty -Name "start_time" -Value $package.StartTime



		$packageList += $pObj
	}}
$packageList | Format-List
