$resultList = @()

$NonDefaultServices = Get-wmiobject win32_service | where { $_.Caption -notmatch "Windows" -and $_.PathName -notmatch "Windows" -and $_.PathName -notmatch "policyhost.exe" -and $_.Name -ne "LSM" -and $_.PathName -notmatch "OSE.EXE" -and $_.PathName -notmatch "OSPPSVC.EXE" -and $_.PathName -notmatch "Microsoft Security Client"}
$memory_data
$cpuCores = (Get-WMIObject Win32_ComputerSystem).NumberOfLogicalProcessors
if ($memory = Get-WmiObject -Class Win32_OperatingSystem | Select-Object TotalVisibleMemorySize) {
        $memory | Foreach {
            $TotalRAM    =  $_.TotalVisibleMemorySize/1024
        }
        $memory_data = $TotalRAM
    }

foreach ($service in $NonDefaultServices)
	{

		if($service.PathName){
			$processId = $service.ProcessId
			$perform = Get-WmiObject -class Win32_PerfFormattedData_PerfProc_Process | where {$_.IDProcess -eq $processId}
			$workingSetPrivate=0
			$percentProcessorTime=0
			$processCount=0
			foreach ($performobj in $perform){

				$workingSetPrivate = $workingSetPrivate + $performobj.workingSetPrivate
				$percentProcessorTime = $percentProcessorTime + $performobj.PercentProcessorTime
			}
				$memory = [Math]::Round(($workingSetPrivate/1024/1024),2)

				$memory_percent=[Math]::Round(($memory*100)/$memory_data,2)
				$cpu = $percentProcessorTime


			$path = $service.PathName.split('"')[1]

			$pObj = New-Object PSobject
			$pObj | Add-Member -MemberType NoteProperty -Name "Name" -Value $service.DisplayName
			$pObj | Add-Member -MemberType NoteProperty -Name "Path" -Value $path
			$pObj | Add-Member -MemberType NoteProperty -Name "StartMode" -Value $service.StartMode
			$pObj | Add-Member -MemberType NoteProperty -Name "State" -Value $service.State
			$pObj | Add-Member -MemberType NoteProperty -Name "Status" -Value $service.Status
			$pObj | Add-Member -MemberType NoteProperty -Name "Started" -Value $service.Started
			$pObj | Add-Member -MemberType NoteProperty -Name "memory(mb)" -Value $memory
			$pObj | Add-Member -MemberType NoteProperty -Name "memory(%)" -Value $memory_percent
			$pObj | Add-Member -MemberType NoteProperty -Name "cpu" -Value $cpu



			$resultList += $pObj
		}

	}
$resultList | Out-String -Width 4096

