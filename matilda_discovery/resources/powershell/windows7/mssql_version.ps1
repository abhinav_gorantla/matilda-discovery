﻿$ErrorActionPreference = "SilentlyContinue"
$mssql_version = $null
$instance = (get-itemproperty 'HKLM:\SOFTWARE\Microsoft\Microsoft SQL Server').InstalledInstances
foreach ($item in $instance)
{
   $path = (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Microsoft SQL Server\Instance Names\SQL').$item
   $mssql_version = (Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Microsoft SQL Server\$path\Setup").Version
}

$main_data=New-Object Psobject
$main_data | Add-Member mssql_version ($mssql_version)
$main_data | format-list
