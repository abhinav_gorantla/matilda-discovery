$ErrorActionPreference = "SilentlyContinue"

$userList = @()
    $AllLocalAccounts = Get-WmiObject -Class Win32_UserAccount -Namespace "root\cimv2" -Filter "LocalAccount='$True'" -ErrorAction Stop

    Foreach($LocalAccount in $AllLocalAccounts)
    {
        $loginexp = "WinNT://$env:COMPUTERNAME/"
        $loginexp += $LocalAccount.Name
        $loginexp += ",user"
        $loginobj = [ADSI]($loginexp)

        $userName = '*' + $LocalAccount.Name + '*'
    $Object = New-Object -TypeName PSObject

        $Object|Add-Member -MemberType NoteProperty -Name "username" -Value $LocalAccount.Name
        $Object|Add-Member -MemberType NoteProperty -Name "full_name" -Value $LocalAccount.FullName
        $Object|Add-Member -MemberType NoteProperty -Name "caption" -Value $LocalAccount.Caption
        $Object|Add-Member -MemberType NoteProperty -Name "disabled" -Value $LocalAccount.Disabled
        $Object|Add-Member -MemberType NoteProperty -Name "status" -Value $LocalAccount.Status
        $Object|Add-Member -MemberType NoteProperty -Name "lockout" -Value $LocalAccount.LockOut
        $Object|Add-Member -MemberType NoteProperty -Name "password_changeable" -Value $LocalAccount.PasswordChangeable
        $Object|Add-Member -MemberType NoteProperty -Name "password_expires" -Value $LocalAccount.PasswordExpires
        $Object|Add-Member -MemberType NoteProperty -Name "password_required" -Value $LocalAccount.PasswordRequired
        $Object|Add-Member -MemberType NoteProperty -Name "SID" -Value $LocalAccount.SID
        $Object|Add-Member -MemberType NoteProperty -Name "user_type" -Value $LocalAccount.SIDType
        $Object|Add-Member -MemberType NoteProperty -Name "account_type" -Value $LocalAccount.AccountType
        $Object|Add-Member -MemberType NoteProperty -Name "domain" -Value $LocalAccount.Domain
        $Object|Add-Member -MemberType NoteProperty -Name "group_description" -Value $LocalAccount.Description
        $Object|Add-Member -MemberType NoteProperty -Name "last_login" -Value $loginobj.lastlogin

        $userList+=$Object
    }$userList