$ErrorActionPreference = "SilentlyContinue"

$envs = Get-ChildItem Env:

$envsList = @()

foreach ($env in $envs){
if($env.Name -ne $null)
    {
		$pObj = New-Object PSobject
		$pObj | Add-Member -MemberType NoteProperty -Name "name" -Value $env.Name
        $pObj | Add-Member -MemberType NoteProperty -Name "value" -Value $env.Value


		$envsList += $pObj
	}}
$envsList | Format-List
