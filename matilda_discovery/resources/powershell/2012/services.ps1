Param
(
  [System.Collections.ArrayList]$includelist=("applicationhost.config","domain-registry.xml","server.xml","httpd.exe","nginx.conf","tnsnames.ora","mysql.exe","standalone.bat","weblogic.jar","version.bat","oraclient*.dll"),
  [string[]]$excludelist=("*\tmp\*","*\docker\*","*\history\*","*\HISTORY\*","*\WinSxS\*","*\alternatives\*","*\bearer11ssl\*","*\manifest\*")
)
$hash = @{"applicationhost.config"="iis";"domain-registry.xml"="weblogic";"server.xml"="tomcat";"httpd.exe"="apache";"nginx.conf"="nginx";"tnsnames.ora"="oracle";"mysql.exe"="mysql_version";"standalone.bat"="jboss_version";"weblogic.jar"="weblogic_version";"version.bat"="tomcat_version";"oraclient"="oracle_version"}
$drives = Get-PSDrive -PSProvider 'FileSystem'
[System.Collections.ArrayList]$resultList = @()
$main_data=@()
foreach ($drive in $drives)
               {
               if($includelist -ne $null){
                    $includestr=$null
                    foreach($includeitem in $includelist){$includestr +=$includeitem + ','}
                              $searchCommand = 'Get-ChildItem -Path ' + $drive.Root + ' -Include ' + $includestr.TrimEnd(',') + ' -Recurse -OutBuffer 1000 -ErrorAction SilentlyContinue | Where {'
                              foreach($exclude in $excludelist){
                                             if ($exclude -eq $excludelist[-1]){
                                                            $searchCommand += '$_.FullName -notlike "' + $exclude +'"'
                                             }else{
                                                            $searchCommand += '$_.FullName -notlike "' + $exclude +'" -and '
                                             }}
                              $searchCommand +=      '} | Select DirectoryName,Name'
                              $result = Invoke-Expression $searchCommand
                              foreach($item in $result){
                                        $includelist.Remove($item.Name)
                                        if($item.Name.Contains("oraclient")){$item.Name="oraclient"}
                                        $pObj = New-Object PSobject
                                        $pObj | Add-Member -MemberType NoteProperty -Name $hash[$item.Name] -Value $item.DirectoryName
                                        $main_data += $pObj
                                        if($item.Name -eq "standalone.bat"){$obj = New-Object PSobject
                                            $obj | Add-Member -MemberType NoteProperty -Name "jboss" -Value $item.DirectoryName
                                            $main_data += $obj}
                                                        }
                              }else{
                              break
                              }}
$main_data | Format-List | Out-String -Width 4096
