$ErrorActionPreference = "SilentlyContinue"
#Finding name of the computer
$ComputerName = $env:ComputerName

#creatign an object and fetching all the info required using header
$header = 'Hostname','OSName','OSVersion','OSManufacturer','OSConfig','Buildtype', 'RegisteredOwner','RegisteredOrganization','ProductID','InstallDate', 'StartTime','Manufacturer','Model','Type','Processor','BIOSVersion', 'WindowsFolder' ,'SystemFolder','StartDevice','Culture', 'UICulture', 'TimeZone','PhysicalMemory', 'AvailablePhysicalMemory' , 'MaxVirtualMemory', 'AvailableVirtualMemory','UsedVirtualMemory','PagingFile','Domain' ,'LogonServer'
$header1 = 'Hostname'
$host_info=systeminfo.exe /FO CSV /S $ComputerName |Select-Object -Skip 1 |ConvertFrom-CSV -Header $header

#adding addtitonal features required for the response body and formating the details as per the response required
$win_memory_type_speed=Get-WmiObject win32_physicalmemory | select MemoryType, Speed
$total_memory=[math]::Round([int]($host_info.PhysicalMemory -replace "[^0-9.]")/1024,2)
$available_memory=[math]::Round([int]($host_info.AvailablePhysicalMemory -replace "[^0-9.]")/1024,2)
$used_memory=$total_memory-$available_memory
$used_percentage=[math]::Round($used_memory/$total_memory*100,2)

#Creating the response object and adding the results to the response object
$resp=New-Object -TypeName psobject
$resp|Add-Member -MemberType NoteProperty -Name total -value $total_memory
$resp|Add-Member -MemberType NoteProperty -Name available -value $available_memory
$resp|Add-Member -MemberType NoteProperty -Name used -value $used_memory
$resp|Add-Member -MemberType NoteProperty -Name used_percent -value $used_percentage
$resp|Add-Member -MemberType NoteProperty -Name type -value $win_memory_type_speed.MemoryType
$resp|Add-Member -MemberType NoteProperty -Name speed -value $win_memory_type_speed.Speed


$resp