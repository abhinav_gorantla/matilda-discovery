$ErrorActionPreference = "SilentlyContinue"

$services = Get-Service | Select-Object Name, DisplayName, Status, DependentServices, ServicesDependedOn, ServiceType

$servicesList = @()

foreach ($service in $services){
if($service.Name -ne $null)
    {
		$pObj = New-Object PSobject
		$pObj | Add-Member -MemberType NoteProperty -Name "name" -Value $service.Name
        $pObj | Add-Member -MemberType NoteProperty -Name "display_name" -Value $service.DisplayName
        $pObj | Add-Member -MemberType NoteProperty -Name "status" -Value $service.Status
        $pObj | Add-Member -MemberType NoteProperty -Name "dependent_services" -Value $service.DependentServices
        $pObj | Add-Member -MemberType NoteProperty -Name "services_depended_on" -Value $service.ServicesDependedOn
        $pObj | Add-Member -MemberType NoteProperty -Name "service_type" -Value $service.ServiceType

		$servicesList += $pObj
	}}
$servicesList | Format-List
