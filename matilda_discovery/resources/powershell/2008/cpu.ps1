$ErrorActionPreference = "SilentlyContinue"

$cpu_details=Get-WmiObject  Win32_processor | select Name, NumberOfCores, NumberOfLogicalProcessors
$cpu_average=Get-WmiObject win32_processor | Measure-Object -property LoadPercentage -Average | select Average

$names_count=$cpu_details.Name|Measure-Object -sum

if($names_count.Count -gt 1){
$cpu_name=$cpu_details.Name[0]
}
else{
$cpu_name=$cpu_details.Name
}

$resp=New-Object -TypeName psobject
$resp |Add-Member -MemberType NoteProperty -Name utilization_percent -value $cpu_average.Average
$resp |Add-Member -MemberType NoteProperty -Name model_name -value $cpu_name
$resp |Add-Member -MemberType NoteProperty -Name physical_cores -value  ($cpu_details.NumberOfCores|Measure-Object -Sum).sum
$resp |Add-Member -MemberType NoteProperty -Name logical_processors -value ($cpu_details.NumberofLogicalProcessors|Measure-Object -Sum).sum

$resp|format-list