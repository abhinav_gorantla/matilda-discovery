   $ErrorActionPreference = "SilentlyContinue"
   $interface_main_data = $null
   $interface_main_data_list = @()
   $Networks = Get-WmiObject Win32_NetworkAdapter -filter "netconnectionstatus = 2"| Select-Object MacAddress, netconnectionstatus, MaxSpeed,  Name, Speed, InterfaceIndex, PhysicalAdapter,NetConnectionID
   $packets= [System.Net.NetworkInformation.Networkinterface]::GetAllNetworkInterfaces() | where {($_.Name -eq $Networks.NetConnectionID) -and ($_.operationalstatus -eq "up")}
   foreach ($Network in $Networks) {
	            $Virtual = 'true'
	            if($Network.PhysicalAdapter){$Virtual = 'false'}
	            $speed=[math]::Round(($Network.Speed)/(1024*1024))
	                $interface_main_data = New-Object Psobject
	                $interface_main_data | Add-Member noteproperty name ($Network.Name)
	                $interface_main_data | Add-Member noteproperty description ($Network.Name)
	                $interface_main_data | Add-Member noteproperty type "None"
	                $interface_main_data | Add-Member noteproperty status 'Up'
	                $interface_main_data | Add-Member noteproperty speed  $speed
	                $interface_main_data | Add-Member noteproperty packet_max_size (netsh interface ipv4 show interface | Select-String $Network.NetConnectionID).ToString().split(" ",[System.StringSplitOptions]::RemoveEmptyEntries)[2]
	                $interface_main_data | Add-Member noteproperty virtual ($Virtual)
	                $interface_main_data | Add-Member noteproperty mac_address ($Network.MacAddress)
    	            $interface_main_data | Add-Member noteproperty interface_index ($Network.InterfaceIndex)
    	            $interface_main_data | Add-Member noteproperty gateway ($Network.DefaultIPGateway)
                    $stats = $packets.GetIPv4Statistics()
                    $interface_main_data | Add-Member noteproperty received_packets $stats.BytesReceived
                    $interface_main_data | Add-Member noteproperty sent_packets $stats.BytesSent
                $netadd = Get-WmiObject Win32_NetworkAdapterConfiguration | where {$_.InterfaceIndex -eq $Network.InterfaceIndex}
                If($netadd)
                {
                    $interface_main_data|Add-Member noteproperty ip_address ($netadd.IpAddress[0])
                    $interface_main_data|Add-Member noteproperty ip_subnet ($netadd.IPSubnet[0])
                    $interface_main_data|Add-Member noteproperty additional_ip ($netadd.IpAddress[1])
                $IsDHCPEnabled = 'n'
                if($netadd.DHCPEnabled) {
                 $IsDHCPEnabled = 'y'
                }
                    $interface_main_data|Add-Member noteproperty dhcp_enabled ($IsDHCPEnabled)
                    $interface_main_data|Add-Member noteproperty dns_server ($netadd.DNSServerSearchOrder[0])
                }
                $interface_main_data_list += $interface_main_data
   }
  $interface_main_data_list


