import json
import re
import time

import paramiko
import winrm
from requests.exceptions import ConnectionError
from winrm.exceptions import InvalidCredentialsError

from matilda_discovery.constant.matilda_enum import CreateInfra

from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()

from matilda_discovery.utils import util


class CMDHandler(object):

    def __init__(self, req_data):
        self.request_data = req_data
        self.ip = self.request_data.get('current_ip')
        self.servicdiscoveryusername = self.request_data.get('username')
        self.username = self.request_data.get('username')
        self.password = self.request_data.get('password')
        self.discoveryname = self.request_data.get('name')
        self.ssh = None
        self.session = None
        self.task_id = self.request_data.get('task_id')
        self.host_id = self.request_data.get('host_id')
        self.operatingsystem = self.request_data.get('operatingsystem')
        self.version = self.request_data.get('version')
        self.utilization = self.request_data.get('utilization')
        self.interval = self.request_data.get('interval')
        self.period = self.request_data.get('period')
        self.discovery_type = self.request_data.get('discovery_type')




    # Flag indicates the sucess of the ssh connect
    def ssh_connect(self, logger_ip=None): # done
        flag = False
        error_msg = None
        out = []
        ssh = None
        try:
            logger.debug(logger_ip=logger_ip, message=f"Establishing SSH connection of the Host: {self.ip}")
            ssh = paramiko.SSHClient()
            ssh.load_system_host_keys()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(self.ip, username=self.username, password=self.password, allow_agent=False)
            self.ssh = ssh
            out, err = self.execute_shell(util.get_script(), cmd_info="os check", logger_ip=logger_ip)
            if any("Unable to get valid context" in s for s in out) or not out:
                logger.critical(logger_ip=logger_ip, message=f"Unable to connect to Host: {self.ip}")
                error_msg = "Unable to reach"
                return ssh, out, flag, error_msg
            self.operatingsystem = util.linux_os(out[0])
            # self.operatingsystem = out[0]
            flag = True
            logger.debug(logger_ip=logger_ip, message=f" Remote login successfully established for the host: {self.ip}")

        except paramiko.ssh_exception.AuthenticationException as e:
            logger.critical(logger_ip=logger_ip, message=f"Authentication failure for the Host: {self.ip}  EXCEPTION {e} ")
            error_msg = "Authentication failure"
        except TimeoutError as e:
            logger.critical(logger_ip=logger_ip, message=f"Unable to connect to Host: {self.ip}  EXCEPTION: {e}")
            error_msg = "Unable to reach"
        except paramiko.ssh_exception.NoValidConnectionsError as e:
            logger.critical(logger_ip=logger_ip, message=f"Unable to connect to Port 22 of the Host {self.ip}  EXCEPTION {e}")
            error_msg = "Unable to reach"
        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Unable to connect to the Host: {self.ip} Exception {e}")
            error_msg = "Unable to reach"
        return ssh, out, flag, error_msg


    def ssh_close(self, logger_ip = None):
        logger.debug(logger_ip=logger_ip, message=f"Closed SSH Connection of the Host: {self.ip}")
        self.ssh.close()


    def execute_shell(self, cmd, cmd_info, logger_ip=None): # done
        output = []
        try:
            logger.debug(logger_ip=logger_ip, message=f"Executing {cmd_info} shell script: ")
            (ssh_stdin, ssh_stdout, ssh_stderr) = self.ssh.exec_command(cmd)
            for line in ssh_stdout.readlines():
                ansi_escape = re.compile(r'(\x9B|\x1B\[)[0-?]*[ -/]*[@-~]')
                output.append(re.sub('[\n"]', '', line).strip())
            logger.debug(logger_ip=logger_ip, message=f"Successfully executed {cmd_info} shell script ")
            return output, None
        except:
            logger.error(logger_ip=logger_ip, message=f" Failed to Execute {cmd_info} shell script '")
        return output, None

    def execute_shell_docker(self, cmd, cmd_info, logger_ip=None): # done
        line = None
        try:
            logger.debug(logger_ip=logger_ip, message=f"Executing {cmd_info} shell script: ")
            (ssh_stdin, ssh_stdout, ssh_stderr) = self.ssh.exec_command(cmd)
            for line in ssh_stdout.readlines():
                line = json.loads(line)
                return line
        except:
            logger.error(logger_ip=logger_ip, message=f" Failed to Execute {cmd_info} shell script '")
        return line

    def execute_shell_raw(self, cmd, cmd_info, logger_ip=None): # done
        try:
            logger.debug(logger_ip=logger_ip, message=f"Executing {cmd_info} shell script: ")
            (ssh_stdin, ssh_stdout, ssh_stderr) = self.ssh.exec_command(cmd)
            return ssh_stdout
        except:
            logger.error(logger_ip=logger_ip, message=f" Failed to Execute {cmd_info} shell script '")
        return None

    def execute_power_shell(self, cmd, cmd_info, logger_ip=None): # done
        response = None
        try:
            start_time = time.time()
            logger.debug(logger_ip=logger_ip, message=f"Executing script to retrieve: {cmd_info} details")
            response = self.session.run_ps(cmd)
            if response is None:
                raise Exception(f"empty response obtained. response = {response}")
            logger.debug(logger_ip=logger_ip, message=f"Successfully executed the script, have to decode next. response = {response}")

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f" Failed to Execute script '{cmd_info}'. Error: {e}")
            return None

        try:
            resp = response.std_out.decode('utf-8')
            if resp:
                logger.debug(logger_ip=logger_ip, message=f"Successfully decoded the response. resp = {resp}")
                logger.info(logger_ip=logger_ip, message=f"Time taken to successfully run the {cmd_info} powershell: {round((time.time() - start_time), 2)} seconds")
                return resp
            else:
                raise Exception(f"Failed to decode the response using .decode('utf-8')")
        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f" Failed to decode response. Error: {e}")

        try:
            resp = response.std_out.decode('cp1252')
            if resp:
                logger.debug(logger_ip=logger_ip, message=f"Successfully decoded the response. resp = {resp}")
                logger.info(logger_ip=logger_ip, message=f"Time taken to successfully run the {cmd_info} powershell: {round((time.time() - start_time), 2)} seconds")
                return resp
            else:
                raise Exception(f"Failed to decode the response using .decode('cp1252')")

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f" Failed to decode response. Error: {e}")

        try:
            resp = response.std_out.decode('unicode_escape')
            if resp:
                logger.debug(logger_ip=logger_ip, message=f"Successfully decoded the response. resp = {resp}")
                logger.info(logger_ip=logger_ip, message=f"Time taken to successfully run the {cmd_info} powershell: {round((time.time() - start_time), 2)} seconds")
                return resp
            else:
                raise Exception(f"Failed to decode the response using .decode('unicode_escape')")

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f" Failed to decode response'. Error: {e}")

        return None

    def windows_session(self, logger_ip=None, winport_flag= True): # done
        flag = False
        error_msg = None
        session = None
        host_resp = ""
        try:
            logger.debug(logger_ip=logger_ip, message=f"Establishing Remote Login of the Host: {self.ip}")
            if winport_flag:
                winport_flag = False
                winrmproperties = util.get_properties(property_file_path=CreateInfra.etc_properties_path.value, section='winrmprop_5985')
                url = winrmproperties['http'] + self.ip + ':' + winrmproperties['port'] + winrmproperties['ws']
                session = winrm.Session(url, auth=(self.username, self.password),
                                        server_cert_validation='ignore',transport='ntlm')
            else:
                winport_flag = True
                winrmproperties = util.get_properties(property_file_path=CreateInfra.etc_properties_path.value, section='winrmprop_5986')
                url = winrmproperties['http'] + self.ip + ':' + winrmproperties['port'] + winrmproperties['ws']

                session = winrm.Session(url, auth=(self.username, self.password), server_cert_validation='ignore')
            self.session = session

            resp = session.run_ps(util.get_properties(property_file_path = CreateInfra.resource_properties_path.value, section ='Windows', property_name ='win_host_check'))
            host_info = resp.std_out.decode('utf-8')
            host_resp = util.powershell_to_json(host_info)

            self.operatingsystem = "Windows"
            self.version = util.windows_version([item.get('Caption') for item in host_resp][0])
            flag = True
            error_msg=None
            logger.debug(logger_ip=logger_ip, message=f"Successfully established remote login of the Host: {self.ip}")
            return session, host_resp, flag, error_msg, None

        except winrm.exceptions.InvalidCredentialsError as e:
            logger.critical(logger_ip=logger_ip, message=f"Invalid Credentials of the Host: {self.ip} EXCEPTION {e} ")
            if error_msg == "Authentication failure":
                return session, host_resp, flag, error_msg, e
            error_msg, e = "Authentication failure", e
        except ConnectionError as e:
            logger.critical(logger_ip=logger_ip, message=f"Unable to connect to the Host: {self.ip} EXCEPTION {e} ")
            error_msg, e = "Unable to reach", e
        except winrm.exceptions.WinRMTransportError as e:
            logger.critical(logger_ip=logger_ip, message=f"Windows Bad HTTP Response returned from the Host: {self.ip}  EXCEPTION {e} ")
            error_msg, e = "Unable to reach", e
        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Unable to connect to the Host: {self.ip} Exception {e}")
            error_msg, e = "Unable to reach", e
        if winport_flag is False:
            session, host_resp, flag, error_msg, e = self.windows_session(logger_ip=None, winport_flag=winport_flag)

        return session, host_resp, flag, error_msg, None


    def clearall(self):
        self.ip = None
        self.username = None
        self.password = None
        self.discoveryname = None
        self.session = None
        self.task_id = None
        self.cloudid = None
        self.host_id = None
        self.os = None
        self.version = None
        if self.ssh:
            self.ssh_close()


