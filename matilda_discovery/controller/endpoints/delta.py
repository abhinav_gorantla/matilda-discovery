from flask import request
from flask_restplus import Resource

from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.restplus import api
from matilda_discovery.services.delta import HostDetailsDelta
from matilda_discovery.utils.util import json_formatter

logger = logger_py_file.Logger()


assets_ns = api.namespace('delta', description='namespace for delta endpoint')

@assets_ns.route('/matilda/hostdiscovery/delta/host/details')
class HostDetailsDelta(Resource):

    def get(self):
        """
        Endpoint that accepts 2 host id's and returns delta of host details between them
        """
        try:
            logger.debug("Initiating the process of fetching delta of host details ")

            if request.args.get('current_id') is not None:
                host_details_delta_service_object = HostDetailsDelta()
                host_details_delta_response = host_details_delta_service_object.host_details_delta(current_id=request.args.get('current_id'), previous_id=request.args.get('previous_id'))
                json_formatter(host_details_delta_response)

                logger.debug("Successfully fetched delta of host details")
                return host_details_delta_response
            else:
                return {"response": "current id and/or previous id not provided"}

        except Exception as e:
            logger.error(f"Exception occurred while initializing the process of fetching delta of host details. Exception : {e}")
            return None
