import json

from flask import request
from flask_restplus import Resource
from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()
from matilda_discovery.restplus import api
from matilda_discovery.services.host import HostDetails as HostDetailsService, HostIPStatus, IpLevelLogService, ErrorsList
from matilda_discovery.services.host import HostList as HostListService
from matilda_discovery.services.host import HostReInitiate as HostReInitiateService
from matilda_discovery.services.host import CoreServiceUpdate
from matilda_discovery.utils.util import json_formatter
from matilda_discovery.services.scheduler import Scheduler

host_ns = api.namespace('host', description='namespace for host details endpoint')


@host_ns.route('/matilda/hostdiscovery/host/list')
class HostList(Resource):

    #@api.marshal_with(host_list_model)
    def get(self):
        """
        Accepts  task_id and returns list of host list
        """
        logger.debug(f"Calling task service through host endpoint")

        try:
            host_service = HostListService()
            if request.args.get('task_id') is not None:
                host_list_resp = host_service.host_list(int(request.args.get('task_id')))
            else:
                logger.error(f"Error: task_id not provided")
                return {'ERROR': 'task_id not provided'}
            if host_list_resp is not None:
                logger.debug(f"Successfully retrieved list of host details from host service ")
                json_formatter(host_list_resp)
                return host_list_resp
            else:
                logger.debug(f"Empty host list returned for task_id: {request.args.get('task_id')}")
                return None

        except Exception as e:
            logger.error(f"Failed retrieving list of host details from host service. Error: {e}")
            return None

#NEW UI
#HOST LIST
@host_ns.route('/matilda/new_hostdiscovery/host/list')
class new_HostList(Resource):

    #@api.marshal_with(host_list_model)
    def get(self):
        """
        Accepts  task_id and returns list of host list
        """
        logger.debug(f"Calling task service through host endpoint")

        try:
            host_service = HostListService()
            if request.args.get('task_id') is not None:
                host_list_resp = host_service.new_host_list(int(request.args.get('task_id')))
            else:
                logger.error(f"Error: task_id not provided")
                return {'ERROR': 'task_id not provided'}
            if host_list_resp is not None:
                logger.debug(f"Successfully retrieved list of host details from host service ")
                json_formatter(host_list_resp)
                return host_list_resp
            else:
                logger.debug(f"Empty host list returned for task_id: {request.args.get('task_id')}")
                return None

        except Exception as e:
            logger.error(f"Failed retrieving list of host details from host service. Error: {e}")
            return None


@host_ns.route('/matilda/hostdiscovery/host')
class HostDetails(Resource):
    #@api.marshal_with(host_details_model)
    def get(self):
        """
        Accepts account id or task or or host id and returns corresponding details
        """
        logger.debug(f"Calling task service through host endpoint")
        try:
            host_service = HostDetailsService()

            if request.args.get('host_id') is not None:
                logger.debug(f"fetching host details of host id: {request.args.get('host_id')}")
                resp = host_service.host_details(int(request.args.get('host_id')))
            else:
                logger.error(f"Error: Host ID not provided")
                return {'ERROR':'Host ID not provided'}

            if resp is not None:
                logger.debug(f"Successfully called host service through host endpoint")
                json_formatter(resp)
                return resp
            else:
                logger.debug(f"Empty data returned for host_id: {resp['host_id']}")
                return None

        except Exception as e:
            logger.error(f"Failed to call host service through host endpoint. Error: {e}")
            return None

@host_ns.route('/matilda/hostdiscovery/host')
class HostDetails(Resource):
    #@api.marshal_with(host_details_model)
    def get(self):
        """
        Accepts account id or task or or host id and returns corresponding details
        """
        logger.debug(f"Calling task service through host endpoint")
        try:
            host_service = HostDetailsService()

            if request.args.get('host_id') is not None:
                logger.debug(f"fetching host details of host id: {request.args.get('host_id')}")
                resp = host_service.host_details(int(request.args.get('host_id')))
            else:
                logger.error(f"Error: Host ID not provided")
                return {'ERROR':'Host ID not provided'}

            if resp is not None:
                logger.debug(f"Successfully called host service through host endpoint")
                json_formatter(resp)
                return resp
            else:
                logger.debug(f"Empty data returned for host_id: {resp['host_id']}")
                return None

        except Exception as e:
            logger.error(f"Failed to call host service through host endpoint. Error: {e}")
            return None


#NEW
@host_ns.route('/matilda/new_hostdiscovery/host')
class new_HostDetails(Resource):
    #@api.marshal_with(host_details_model)
    def get(self):
        """
        Accepts account id or task or or host id and returns corresponding details
        """
        logger.debug(f"Calling task service through host endpoint")
        try:
            host_service = HostDetailsService()

            if request.args.get('host_id') is not None:
                logger.debug(f"fetching host details of host id: {request.args.get('host_id')}")
                resp = host_service.new_host_details(host_id=int(request.args.get('host_id')), info=request.args.get('info'))
            else:
                logger.error(f"Error: Host ID not provided")
                return {'ERROR':'Host ID not provided'}

            if resp is not None:
                logger.debug(f"Successfully called host service through host endpoint")
                json_formatter(resp)
                return resp
            else:
                logger.debug(f"Empty data returned for host_id: {resp['host_id']}")
                return None

        except Exception as e:
            logger.error(f"Failed to call host service through host endpoint. Error: {e}")
            return None

@host_ns.route('/matilda/hostdiscovery/host/reinitiate')
class HostReinitiate(Resource):
    #@api.marshal_with(host_details_model)
    def post(self):
        """
        Accepts host id and host credentials if provided to reinitiate the discovery
        """
        try:
            logger.debug(f"Calling reinitiate service through host endpoint")

            payload = json.loads(request.data)
            request_data = payload['request']

            reinitiate_handler = HostReInitiateService()
            if request_data.get('host_id') is not None:

                if (request_data.get("username") is not None and request_data.get("password") is None) or (request_data.get("password") is not None and request_data.get("username") is None):
                    return {"response": "Failed", "error": "missing username or password"}

                job_host_mapping_data = reinitiate_handler.host_reinitiate(request_data, logger_ip=request_data.get('host_ip'))
                if job_host_mapping_data not in [None, []]:
                    job_details = job_host_mapping_data[0]
                    scheduler_obj = Scheduler()
                    scheduler_obj.job_pause_resume_delete(job_id=job_details.get('job_id'), status='delete')

                return {"response": "Reinitiated discovery for host id: " + str(request_data.get('host_id'))}

            else:
                return {"response": "host_id not provided", "error": "missing host id"}

        except Exception as e:
            logger.error(f"Failed to reinitiate. Error: {e}")
            return {"response": "Failed to reinitiate", "Error": e}


@host_ns.route('/matilda/hostdiscovery/hostip/details')
class HostStatus(Resource):
    # @api.marshal_with(host_details_model)
    def get(self):
        """
        accepts host_ip and returns the status and host_id of hte most recent discovery happened on that IP.
        :return: dictionary,most recent successfully discovered ip's host_id and details
        """
        logger.debug(f"Calling host status details for the ip: {request.args.get('host_ip')}")
        try:
            if request.args.get('host_ip') is not None:
                host_service_object = HostIPStatus()
                host_ip_status_response=host_service_object.recent_host_ip_details(request.args.get('host_ip'))
                json_formatter(host_ip_status_response)
                return host_ip_status_response
            else:
                return {"response":"Host_ip is not provided"}

        except Exception as e:
            logger.error(f"Failed to fetch recent host_ip details. Error: {e}")
            return None

@host_ns.route('/matilda/hostdiscovery/ip/log')
class IpLevelLogLocation(Resource):
    def get(self):
        """
        accepts host_id and returns the log file content
        :return: file, log file content for a particular ip.
        """
        logger.debug(f"Calling log file content from the location of the ip_level_logging for the host: {request.args.get('host_ip')}")
        try:
            if request.args.get('host_ip') is not None:
                ip_level_log_service_object = IpLevelLogService()
                log_content =ip_level_log_service_object.fetch_log_file_content(host_ip=request.args.get('host_ip'),logger_ip=request.args.get('host_ip'))
                return log_content
            else:
                return {"response":"Host_ip is not provided"}

        except Exception as e:
            logger.error(f"Failed to fetch recent host_ip details. Error: {e}")
            return None


@host_ns.route('/matilda/hostdiscovery/infrastructure_recommendation/reinitiate')
class ReinitiateHostRecommendation(Resource):

    def post(self):
        """
        Removes existing recommendations and restarts it for all successful recommendations. If host id is passed, recommendation will be reset only for that id
        """
        try:
            logger.debug(f"Reinitiaing recommendations")

            payload = json.loads(request.data)
            request_data = payload['request']

            sc = Scheduler()
            if request_data.get('host_id') is not None:
                sc.create_recommendation_job(host_id=request_data.get('host_id'))
            else:
                sc.create_recommendation_job()

            logger.debug(f"Successfully reinitiated recommendations")
            return {"response": "Added a job"}

        except Exception as e:
            logger.error(f"Failed to reinitiate recommendations. Error: {e}")
            return {"response": "Failed to add job",
                    "reason": e}



@host_ns.route('/matilda/hostdiscovery/sample/testing_endpoint')
class IpLevelLogLocation(Resource):
    def get(self):
        host_list = [890,776,754,750,719,704,700,699,634,612,520,457,456,453,325,264,220]

        for each in host_list:
            sc = Scheduler()
            print("for host", each)
            sc.create_recommendation_job(host_id=each)
        print("done !!")

@host_ns.route('/matilda/hostdiscovery/updatecoreservice')
class CoreServiceDetails(Resource):
    #@api.marshal_with(host_details_model)
    def get(self):
        """
        """
        logger.debug(f"Updating core service")
        try:
            core_service = CoreServiceUpdate()

            if request.args.get('service') is not None:
                logger.debug(f"Register core service : {request.args.get('service')}")
                resp = core_service.core_service_update(service=request.args.get('service'))
            else:
                logger.error(f"Error in updating core service. Service name is empty")
                return {'response':'Failed to update. Service name is empty'}

            return resp

        except Exception as e:
            logger.error(f"Failed to call host service through host endpoint. Error: {e}")
            return None

@host_ns.route('/matilda/hostdiscovery/alerts')
class OnpremActionsList(Resource):

    def get(self):
        """
        """
        try:
            logger.debug("Fetching errors list")
            if request.args.get('account_id') is not None:
                errors_list_object = ErrorsList()
                errors_list_object_response = errors_list_object.get_errors_list(account_id=request.args.get('account_id'))
                json_formatter(errors_list_object_response)

                logger.debug("Successfully fetched errors list")
                return errors_list_object_response
            else:
                return {"response": "Account_id not provided"}

        except Exception as e:
            logger.error(f"Exception occurred while fetching errors list for account_id : {request.args.get('account_id')}. Exception : {e}")
            return None