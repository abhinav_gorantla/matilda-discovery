from flask import request
from flask_restplus import Resource

from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.restplus import api
from matilda_discovery.services.utilizations.utilization import utilization
from matilda_discovery.utils.util import json_formatter
from matilda_discovery.services.scheduler import Scheduler

logger = logger_py_file.Logger()


utilization_ns = api.namespace('utilization', description='namespace for dependency endpoint')


@utilization_ns.route('/matilda/hostdiscovery/utilization')
class Utilizations(Resource):


    def get(self):
        """
        """
        try:
            logger.debug(f"Calling utilizations status update endpoint")

            request_data = request.args
            utilization_object = utilization(request_data)
            job_info = utilization_object.get_job_id()
            scheduler_obj = Scheduler()
            jobactionstatus, message = scheduler_obj.job_pause_resume_delete(job_id=job_info.get('job_id'),status=job_info.get('status'))
            if jobactionstatus:
                utilization_object.utilization_job_host_mapping_update(host_id=request_data.get('host_id'),job_id=job_info.get('job_id'), job_status=job_info.get('status'))
                logger.debug(f"Successfully called utilizations status update ")
                response = {'response': message.get('message')}
                json_formatter(response)
                return response
            else:
                response = {'response': 'Job update failed'}
                json_formatter(response)
                return response

        except Exception as e:
            logger.error(f"Failed at utilizations status update. Error: {e}")
            return {}
