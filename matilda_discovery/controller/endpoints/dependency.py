from flask_restplus import Resource
from matilda_discovery.restplus import api
from flask import request
from matilda_discovery.services import topology as topology_service
from matilda_discovery.utils.util import json_formatter
from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()


dependency_ns = api.namespace('dependency', description='namespace for dependency endpoint')


@dependency_ns.route('/matilda/hostdiscovery/network')
class DashboardDetails(Resource):

    #@api.marshal_with(dashboard_response_model)
    def get(self):
        """
        Accepts account_id and optional task_id. Calls dashboard service to get details about utilization, stage and task status.
        """
        try:
            logger.debug(f"Calling dependency service through dashboard endpoint")

            topology_service_object = topology_service.NetworkTopology()

            if request.args.get('ipaddress') is not None:
                ip_range = request.args.get('ipaddress')
                response = topology_service_object.get_network_dependency(ip_range=ip_range)

            else:
                raise Exception("Missing ip range and account id. Requires both of them to build a dependency map.")

            logger.debug(f"Successfully called topology service through dependency endpoint")
            json_formatter(response)
            return response

        except Exception as e:
            logger.error(f"Failed to fetch network level topology. Error: {e}")
            return {}
