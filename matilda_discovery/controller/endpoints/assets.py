from flask import request
from flask_restplus import Resource

from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.services.application import ApplicationList
from matilda_discovery.services.recommendation.assets import Infrastructure

logger = logger_py_file.Logger()

from matilda_discovery.restplus import api
from matilda_discovery.services.host import HostList
from matilda_discovery.utils.util import json_formatter

assets_ns = api.namespace('assets', description='namespace for assets endpoint')

@assets_ns.route('/matilda/hostdiscovery/assets/infrastructure/overview')
class AssetsInfrastructureList(Resource):
    # @api.marshal_with(task_list_model)
    def get(self):
        """
        fetches the successfully discovered ip's list and respective recommendation details.
        :return:
        """
        try:
            logger.debug("Initiating the process of fetching assets list ")

            if request.args.get('account_id') is not None and request.args.get('cloud_provider') is not None:
                infrastructure_assets_service_object = Infrastructure()
                infrastructure_overview_response = infrastructure_assets_service_object.fetch_infrastructure_overview_details(
                    account_id=request.args.get('account_id'), cloud_provider=request.args.get('cloud_provider'))
                json_formatter(infrastructure_overview_response)

                logger.debug("Successfully fetched assets overview information")
                return infrastructure_overview_response
            else:
                return {"response" : "Account_id or cloud_provider not provided"}

        except Exception as e:
            logger.error(f"Exception occurred while initializing the process of fetching infrastructure overview for account_id :"
                         f" {request.args.get('account_id')}. Exception : {e}")
            return None


############################################################################################################################################################


#NEW UI
#Instance List
@assets_ns.route('/matilda/new_hostdiscovery/assets/infrastructure/overview')
class new_AssetsInfrastructureList(Resource):
    # @api.marshal_with(task_list_model)
    def get(self):
        """
        fetches the successfully discovered ip's list and respective recommendation details.
        :return:
        """
        try:
            logger.debug("Initiating the process of fetching assets list ")
            x = request.args
            if request.args.get('account_id') is not None and request.args.get('cloud_provider') is not None:
                infrastructure_assets_service_object = Infrastructure()
                infrastructure_overview_response = infrastructure_assets_service_object.new_fetch_infrastructure_overview_details(
                    account_id=request.args.get('account_id'), cloud_provider=request.args.get('cloud_provider'))
                json_formatter(infrastructure_overview_response)

                logger.debug("Successfully fetched assets overview information")
                return infrastructure_overview_response
            else:
                return {"response" : "Account_id or cloud_provider not provided"}

        except Exception as e:
            logger.error(f"Exception occurred while initializing the process of fetching infrastructure overview for account_id :"
                         f" {request.args.get('account_id')}. Exception : {e}")
            return None

@assets_ns.route('/matilda/hostdiscovery/assets/infrastructure/migration')
class AssetsInfrastructureMigrationView(Resource):
    # @api.marshal_with(task_list_model)
    def get(self):
        """
        fetches the successfully discovered ip list migration view.
        :return:
        """
        try:
            logger.debug("Initiating the process of fetching infrastructure migration overview")
            if request.args.get('account_id') is not None and request.args.get('cloud_provider') is not None:
                infrastructure_assets_service_object = Infrastructure()
                infrastructure_overview_response = infrastructure_assets_service_object.fetch_infrastructure_migration_details(account_id = request.args.get('account_id'),cloud_provider = request.args.get('cloud_provider'))
                json_formatter(infrastructure_overview_response)

                logger.debug("Successfully fetched assets information")
                return infrastructure_overview_response
            else:
                return {"response" : "Account_id not provided"}

        except Exception as e:
            logger.error(f"Exception occurred while initializing the process of fetching assets for account_id : {request.args.get('account_id')}. Exception : {e}")
            return None


@assets_ns.route('/matilda/hostdiscovery/assets/services')
class AssetsServicesList(Resource):

    def get(self):
        """
        fetches the services details list of all the succesfullly discovered hosts.
        :return: list, discovered services and respective recommendations.
        """
        try:
            if request.args.get('account_id') is not None:
                logger.debug("Initiating the process to fetch services level assets list")
                host_service_object = HostList()
                response = host_service_object.services_assets_list(account_id=request.args.get('account_id'))
                json_formatter(response)
                return response
            else:
                return {"response" : "Account_id not provided"}

        except Exception as e:
            logger.error(f"Exception occurred while Initiating the process to fetch services level assets list.Exception: {e} ")
            return {"response":"Exception occured"}


@assets_ns.route('/matilda/hostdiscovery/assets/infrastructure')
class InfrastructureAssessmentDashboard(Resource):

    def get(self):
        """

        """
        try:
            logger.debug("Initiating the process of fetching assets dashboard details")
            if request.args.get('account_id') is not None:
                infrastructure_assets_service_object = Infrastructure()
                infrastructure_overview_response = infrastructure_assets_service_object.infrastructure_assets_dashboard(cloud_provider=request.args.get('cloud_provider'))
                json_formatter(infrastructure_overview_response)

                logger.debug("Successfully fetched assets overview information")
                return infrastructure_overview_response
            else:
                return {"response" : "Account_id not provided"}

        except Exception as e:
            logger.error(f"Exception occurred while initializing the process of fetching infrastrcuture overview for account_id : {request.args.get('account_id')}. Exception : {e}")
            return None


#NEW UI
@assets_ns.route('/matilda/new_hostdiscovery/assets/infrastructure')
class new_InfrastructureAssessmentDashboard(Resource):

    def get(self):
        """

        """
        try:
            logger.debug("Initiating the process of fetching assets dashboard details")
            if request.args.get('account_id') is not None:
                new_infrastructure_assets_service_object = Infrastructure()
                infrastructure_overview_response = new_infrastructure_assets_service_object.new_infrastructure_assets_dashboard(cloud_provider=request.args.get('cloud_provider'))
                json_formatter(infrastructure_overview_response)

                logger.debug("Successfully fetched assets overview information")
                return infrastructure_overview_response
            else:
                return {"response" : "Account_id not provided"}

        except Exception as e:
            logger.error(f"Exception occurred while initializing the process of fetching infrastrcuture overview for account_id : {request.args.get('account_id')}. Exception : {e}")
            return None
