
from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()

from flask_restplus import Resource
from matilda_discovery.restplus import api
from flask import request
from matilda_discovery.db.handler.report import ReportDetails
from matilda_discovery.services.reports.reports_generations import ReportsListGeneration
import json
from matilda_discovery.services.scheduler import Scheduler

report_ns = api.namespace('reports', description='namespace for report endpoint')


@report_ns.route('/matilda/hostdiscovery/reports/create')
class HostList(Resource):

    def post(self):
        """
        triggers the reports generation and generates reports into a specified path
        :return:
        """
        try:
            logger.debug("Triggered report generation end point")
            payload = json.loads(request.data)
            request_data = payload.get('request')
            if request_data.get('account_id') is not None:
                reports_object = ReportDetails()
                req_data = dict(name=request_data.get('name'), account_id=request_data.get('account_id'), cloud_provider=request_data.get('cloud'), type=request_data.get('type'), status="Request Submitted", job_trigger_type='report')
                report_id = reports_object.create_new_report_requests(req_data)
                req_data['id'] = report_id
                scheduler_obj = Scheduler()
                scheduler_obj.create_job(request=req_data,job_type='report')
                logger.debug("Successfully called reports generations")
                return {"Status": "Request for Summary Report Submitted"}
            else:
                return {"Error": "Account ID not Provided"}
        except Exception as e:
            logger.error(f"Exception occurred while reports generation endpoint called. Exception: {e}")


@report_ns.route('/matilda/hostdiscovery/reports/list')
class ReportsList(Resource):

    def get(self):
        """
        Downloads report for a requested is from the local location
        :return:
        """
        try:
            logger.debug("Download report call triggered")
            if request.args.get('account_id') is not None:
                account_id = request.args.get('account_id')
                req_data = dict(account_id=account_id)
                reports_service_object = ReportsListGeneration()
                reports_list_resp = reports_service_object.get_reports_list(req_data)
                return reports_list_resp
        except Exception as e:
            logger.debug(f"Exception occurred while fetching reports list at the endpoint. Error: {e}")
            return None


@report_ns.route('/matilda/hostdiscovery/reports/download')
class DownloadReport(Resource):

    def get(self):
        """

        :return:
        """
        try:
            logger.debug("Reports List endpoint called")
            if request.args.get('account_id') is not None:
                account_id = request.args.get('account_id')
                id = request.args.get('id')
                req_data = dict(account_id=account_id, id=id)
                reports_service_object = ReportsListGeneration()
                reports_list_resp = reports_service_object.download_report(req_data)
                return reports_list_resp
        except Exception as e:
            logger.debug(f"Exception occurred while fetching reports list at the endpoint. Error: {e}")
            return None
