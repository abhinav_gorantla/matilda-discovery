import pandas as pd
from flask import request, send_file
from flask_restplus import Resource
from matilda_discovery.db.models import model
from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.restplus import api
from matilda_discovery.services.lookup import LookupService

logger = logger_py_file.Logger()

lookup_ns = api.namespace('lookup', description='namespace for lookup data changes endpoint')

@lookup_ns.route('/matilda/hostdiscovery/lookup')
class ImportLookup(Resource):

    def post(self):
        """
        accepts input lookup files and updates the corresponding tables
        """
        try:
            logger.debug(f"Waiting to process the uploaded lookup file")

            if request.files.get('os_lookup'):
                logger.debug(f"Received os lookup data")
                input_lookup_data = pd.read_excel(request.files.get('os_lookup'))
                lookup_service_object = LookupService()
                lookup_service_object.update_lookup_table(input_lookup_data, model.OS_Lookup, table_name="os_lookup")
                return {"response": "OS Lookup file received successfully."}

            elif request.files.get('service_lookup'):
                logger.debug(f"Received service lookup data")
                input_lookup_data = pd.read_excel(request.files.get('service_lookup'))
                lookup_service_object = LookupService()
                lookup_service_object.update_lookup_table(input_lookup_data, model.Service_Lookup, table_name="service_lookup")
                return {"response": "Service Lookup file received successfully."}

            else:
                raise Exception(f"Did not receive expected file name")

        except Exception as e:
            logger.error(f"Failed to import lookup data in ImportLookup endpoint. Error: {e}")
            return {"error": e}

    def get(self):
        """
        accepts name of the lookup table needed and returns the table in .xlsx format
        """
        try:
            logger.debug(f"Waiting to process the input file name")

            if request.args.get('file_name') == "os_lookup":
                logger.debug(f"Received request for downloading os_lookup table data")
                lookup_service_object = LookupService()
                lookup_table_data = lookup_service_object.download_lookup_table(file_name="os_lookup")
                return send_file(lookup_table_data, as_attachment=True)

            elif request.args.get('file_name') == "service_lookup":
                logger.debug(f"Received request for downloading service_lookup table data")
                lookup_service_object = LookupService()
                lookup_table_data = lookup_service_object.download_lookup_table(file_name="service_lookup")
                return send_file(lookup_table_data, as_attachment=True)

            else:
                raise Exception(f"Received unsupported file name. Given Filename: {request.args.get('file_name')}")

        except Exception as e:
            logger.error(f"Failed to send the lookup data. Error: {e}")
            return {'error': e}
