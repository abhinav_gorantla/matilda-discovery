from flask import request
from flask_restplus import Resource

from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.restplus import api
from matilda_discovery.services.eol_lookup import EOL_Date as EOL_Date_Service

logger = logger_py_file.Logger()

eol_ns = api.namespace('eol', description='namespace for EOL endpoint')


@eol_ns.route('/matilda/eoldates/eol')
class EOL_Date(Resource):

    def get(self):
        """
         accepts the type, name and version input and prints the opearting system/service EOL date.
        """
        try:
            logger.debug(f"Entered the EOL end point")
            req_data = request.args

            if req_data.get('type') and req_data.get('name') and req_data.get('version'):
                eol_service_object = EOL_Date_Service()
                resp = eol_service_object.fetch_eol(req_data)

                logger.debug(f"Successfully called EOL service to get the EOL date")
                return resp
            else:
                logger.error(f"Failed to retrieve EOL date. Missing parameter. Given input: {req_data}")

        except Exception as e:
            logger.error(f"Failed to completed task list service through task endpoint. Error: {e}")
            return None
