from flask_restplus import Resource


from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()

from matilda_discovery.restplus import api
from matilda_discovery.services.service_port import ServicePortService
from matilda_discovery.utils.util import json_formatter

service_port_ns = api.namespace('service_port', description='namespace for service_port endpoint')

@service_port_ns.route('/matilda/hostdiscovery/service/port')
class ServicePort(Resource):

    #@api.marshal_with()
    def get(self):
        """
        Returns services and respective ports from service_port table
        """
        try:
            logger.debug(f"Calling service_port endpoint")

            service_port_service_object = ServicePortService()
            resp = service_port_service_object.service_port_details()
            json_formatter(resp)

            logger.debug(f"Successfully called dashboard service through dashboard endpoint")
            return resp

        except Exception as e:
            logger.error(f"Failed to fetch service_port information")
            return {'ERROR': e}


