import os

from flask import request
from flask_restplus import Resource

from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.restplus import api
from matilda_discovery.services.dashboard import task_services_details
from matilda_discovery.services.scheduler import Scheduler
from matilda_discovery.services.task import TaskCreation as TaskCreationService, TaskList as TaskListService, TaskList
from matilda_discovery.utils import util

logger = logger_py_file.Logger()

import json
from matilda_discovery.utils.util import json_formatter

task_ns = api.namespace('task', description='namespace for task endpoint')


@task_ns.route('/matilda/hostdiscovery/initiate')
class TaskCreation(Resource):

    #@api.expect(task_creation_model, validate=True)
    def post(self):
        """
        posts the request data obtained from the UI into the DB by creating task for every new discovery and aslo creates a new record in host table for every ip in the given list.
        :return:list of items. contains task_name, and list of dictionaries of host_ip's and host_id's under the generated task_id for the discovery
        """
        try:
            logger.debug(f"Calling task creation service from task creation endpoint")
            payload = json.loads(request.data)
            request_data = payload['request']
            task_creation_service_object = TaskCreationService()
            host_list_resp = task_creation_service_object.create_discovery_task(request_data)

            logger.debug(f"Successfully completed task creation service from task creation endpoint. Response: {host_list_resp}")
            return host_list_resp

        except Exception as e:
            logger.error(f"Failed while calling task creation service from task creation endpoint. Error: {e}")
            return None


@task_ns.route('/matilda/hostdiscovery/task/reinitiate')
class TaskReinitiate(Resource):

    def post(self):
        """
        fetches the request from UI and reinitiates the discovery of all the host present under the conditions sent in the post body by UI
        :return: None, reinitiates the discovery.
        """
        try:
            logger.debug(f"Re-initiating task discovery")
            payload = json.loads(request.data)
            reinitiate_data = payload['request']
            if reinitiate_data.get('task_id') is not None:
                task_list_object = TaskList()
                utilization_job_ids_list = task_list_object.reinitiate_task(reinitiate_data)
                for utilization_job_id in utilization_job_ids_list:
                    scheduler_obj = Scheduler()
                    scheduler_obj.job_pause_resume_delete(job_id=utilization_job_id, status='delete')
                logger.debug("Successfully reinitiated task")
                return {"response":"Reinitiated the hosts under the task_id : "+ str(reinitiate_data.get('task_id'))}
            else:
                return {"response": "Please provide a task_id"}
        except Exception as e:
            logger.error(f"Exception occurred while reinitiating the task.Exception : {e}")
            return None



@task_ns.route('/matilda/hostdiscovery/fetch')
class TaskFetch(Resource):
    #@api.marshal_with(task_list_model)
    def get(self):
        """
        fetches list of task details from the task table for the given account_id
        :return: list of dictionaries,list of  all the tasks and respective details for the given account_id
        """
        try:
            logger.debug(f"Calling task list service through task endpoint to fetch list of tasks")
            task_list_service_object = TaskListService()
            if request.args.get('account_id') is not None:
                resp = task_list_service_object.fetch_task_list(int(request.args.get('account_id')))
                json_formatter(resp)
            else:
                logger.error(f"Error: Account ID not provided")
                return {'ERROR':'Account ID not provided'}
            if resp is not None:
                logger.debug(f"Successfully completed task list service through task endpoint")
                return resp
            else:
                logger.debug(f"Empty task list obtained for the account_id :{request.args.get('account_id')}")
                return None

        except Exception as e:
            logger.error(f"Failed to completed task list service through task endpoint. Error: {e}")
            return None
#NEWUI
@task_ns.route('/matilda/new_hostdiscovery/fetch')
class new_TaskFetch(Resource):
    #@api.marshal_with(task_list_model)
    def get(self):
        """
        fetches list of task details from the task table for the given account_id
        :return: list of dictionaries,list of  all the tasks and respective details for the given account_id
        """
        try:
            logger.debug(f"Calling task list service through task endpoint to fetch list of tasks")
            task_list_service_object = TaskListService()
            if request.args.get('account_id') is not None:
                resp = task_list_service_object.new_fetch_task_list(int(request.args.get('account_id')))
                json_formatter(resp)
            else:
                logger.error(f"Error: Account ID not provided")
                return {'ERROR':'Account ID not provided'}
            if resp is not None:
                logger.debug(f"Successfully completed task list service through task endpoint")
                return resp
            else:
                logger.debug(f"Empty task list obtained for the account_id :{request.args.get('account_id')}")
                return None

        except Exception as e:
            logger.error(f"Failed to completed task list service through task endpoint. Error: {e}")
            return None

@task_ns.route('/matilda/task_summary/services')
class TaskSummary(Resource):
    #@api.marshall(task_creation_model, validate=True)
    def get(self):
        """
        gets the services, operating system and devices summary data for a particular task
        :return:list of items.list of list of dictionaries containing services and operating and devices summary
        """
        try:
            logger.debug(f"Calling task summary details of services operatign systems, devices  through endpoint")
            if request.args.get('account_id') is not None and request.args.get('task_id') is not None:
                resp = task_services_details(account_id=request.args.get('account_id'),task_id=request.args.get('task_id'))
            else:
                logger.error(f"Error: Account ID not provided or task_id not provided")
                return {'ERROR': 'Account ID or Task_ID not provided'}

            return resp

        except Exception as e:
            logger.error(f"Failed to fetch task service summary details. Error: {e}")
            return None

@task_ns.route('/matilda/discovery/completedtask/list')
class CompletedTaskList(Resource):

    #@api.marshal_with(task_list_model)
    def get(self):
        """
        fetches the succesfully completed tasks under the account_id
        :return:
        """
        try:
            if request.args.get('account_id') is not None:
                logger.debug(f"Fetching successfully completed task under the account_id: {request.args.get('account_id')}")
                task_handler_object=TaskList()
                completed_task_list=task_handler_object.completed_task_list(account_id=request.args.get('account_id'))
                json_formatter(completed_task_list)
                return completed_task_list
            else:
                return {'response' : 'account_id not provided'}
        except Exception as e:
            logger.error(f"Exception occurred while initiating the functionality of fetching completed task list for account_id: {request.args.get('account_id')}. Error: {e}")
            return None


@task_ns.route('/matilda/hostdiscovery/inputfileupload/initiate')
class UploadInput(Resource):
    def post(self):
        try:
            logger.debug("Uploading Input data file...")
            file = request.files
            excelfile = file['upload_input']
            file_name = excelfile.filename
            payload = json.loads(file['payload'].read())

            file_name='Input_' + str(util.get_datetime(req_format=CreateInfra.file_date_format.value)) + '_'+ file_name
            path = os.path.normpath(os.path.join(os.path.dirname(__file__), CreateInfra.upload.value))
            util.create_dir_file(path)
            file_to_upload = os.path.join(path + '/' + file_name)
            if os.path.exists(file_to_upload):
                os.remove(file_to_upload)
            excelfile.save(file_to_upload)

            request_data = dict(filename=file_name,
                                name=payload['request'].get('name'),
                                account_id=payload['request'].get('account_id'),
                                created_by=payload['request'].get('created_by'),
                                execution_mode=payload['request'].get('execution_mode'),
                                scheduled_time=payload['request'].get('scheduled_time'),
                                utilization=payload['request'].get('utilization'),
                                utilization_interval=payload['request'].get('utilization_interval'),
                                utilization_period=payload['request'].get('utilization_period'),
                                job_trigger_type='input_request_file_upload')
            scheduler_obj = Scheduler()
            scheduler_obj.create_job(request=request_data, job_type='input_request_file_upload')
            return{"Response":"Successful uploaded the Input data file"}
        except Exception as e:
            logger.debug(f"Exception occurred while uploading input data file at the endpoint. Error: {e}")
            return None


