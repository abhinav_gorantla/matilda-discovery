from flask import request
from flask_restplus import Resource
import os
from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.restplus import api
from matilda_discovery.services.scheduler import Scheduler
from matilda_discovery.services.validation import Validation
from matilda_discovery.utils import util
from matilda_discovery.utils.util import json_formatter

logger = logger_py_file.Logger()

import json

validation_ns = api.namespace('validation', description='namespace for validation endpoint')


@validation_ns.route('/matilda/hostdiscovery/validation')
class ValidationRequest(Resource):

    #@api.expect(task_creation_model, validate=True)
    def post(self):
        """
        """
        try:
            payload = json.loads(request.data)
            request_data = payload['request']
            scheduler_obj = Scheduler()  #creating a task level job ['job_trigger_type'] = 'validation_task_job' and job_type='validation_request' #this job will create individual jobs
            scheduler_obj.create_job(request=dict(requests=request_data,job_trigger_type='validation_task_job'),job_type='validation_request')
            return dict(response='Request Submitted')
        except Exception as e:
            logger.error(f"Failed while calling validation service endpoint. Error: {e}")
            return None

@validation_ns.route('/matilda/discovery/validation/tasklist')
class ValidationTaskList(Resource):

    #@api.marshal_with(task_list_model)
    def get(self):
        """
        """
        try:
            validation_object = Validation()
            validation_requests = validation_object.get_validation_task_list()
            json_formatter(validation_requests)
            summary = dict(total=len(validation_requests), submitted=0, inprogress=0, completed=len(validation_requests))
            return dict(response=validation_requests,summary=summary)
        except Exception as e:
            return None

@validation_ns.route('/matilda/discovery/validation/list')
class ValidationList(Resource):

    #@api.marshal_with(task_list_model)
    def get(self):
        """
        """
        try:
            task_id=request.args.get('task_id')
            validation_object = Validation()
            validation_requests, validation_list_summary = validation_object.get_validation_list(task_id)
            json_formatter(validation_requests)
            json_formatter(validation_list_summary)
            return dict(response=validation_requests,summary=validation_list_summary[0])
        except Exception as e:
            return None


@validation_ns.route('/matilda/hostdiscovery/inputfileupload/validation')
class UploadInput(Resource):
    def post(self):
        try:
            logger.debug("Uploading Valadation Input data file...")
            file = request.files
            excelfile = file['upload_input']
            file_name = excelfile.filename
            payload = json.loads(file['payload'].read())

            file_name='Input_' + str(util.get_datetime(req_format=CreateInfra.file_date_format.value)) + '_'+ file_name
            path = os.path.normpath(os.path.join(os.path.dirname(__file__), CreateInfra.upload.value))
            util.create_dir_file(path)
            file_to_upload = os.path.join(path + '/' + file_name)
            if os.path.exists(file_to_upload):
                os.remove(file_to_upload)
            excelfile.save(file_to_upload)

            request_data = dict(filename=file_name,
                                name=payload['request'].get('name'),
                                job_trigger_type='validation_request_file_upload')
            scheduler_obj = Scheduler()
            scheduler_obj.create_job(request=request_data, job_type='validation_request_file_upload')
            return{"Response":"Successful uploaded the Valadation Input data file"}
        except Exception as e:
            logger.debug(f"Exception occurred while uploading Valadation input data file at the endpoint. Error: {e}")
            return None

#to read the encrypted password using id(id in the validation table)
@validation_ns.route('/matilda/discovery/validation/getpassword')
class ValidationPassword(Resource):

    #@api.marshal_with(task_list_model)
    def get(self):
        """
        """
        try:
            id = request.args.get('id')
            validation_object = Validation()
            password = validation_object.get_validation_password(id)
            return dict(password=password)
        except Exception as e:
            return None
