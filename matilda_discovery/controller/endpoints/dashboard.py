from flask_restplus import Resource
from matilda_discovery.restplus import api
from flask import request
from matilda_discovery.services import dashboard as dashboard_service

from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.services.dashboard import Dashboard

logger = logger_py_file.Logger()

from matilda_discovery.utils.util import json_formatter

dashboard_ns = api.namespace('dashboard', description='namespace for dashboard endpoint')

@dashboard_ns.route('/matilda/hostdiscovery/dashboard/summary')
class DashboardDetails(Resource):

    #@api.marshal_with(dashboard_response_model)
    def get(self):
        """
        Accepts account_id and optional task_id. Calls dashboard service to get details about utilization, stage and task status.
        """
        logger.debug(f"Calling dashboard service through dashboard endpoint")

        if request.args.get('account_id') is not None:
            account_id = int(request.args.get('account_id'))
            task_id = request.args.get('task_id')
            resp = dashboard_service.dashboard_details(account_id, task_id)

            logger.debug(f"Successfully called dashboard service through dashboard endpoint")
            json_formatter(resp)
            return resp
        else:
            logger.error(f"Error: Account ID not provided")
            return {'ERROR':'Account ID not Provided'}


@dashboard_ns.route('/matilda/hostdiscovery/new_dashboard/summary')
class DashboardDetails(Resource):

    #@api.marshal_with(dashboard_response_model)
    def get(self):
        """
        Accepts account_id and optional task_id. Calls dashboard service to get details about utilization, stage and task status and various other dashboard details.
        """
        logger.debug(f"Calling dashboard service through dashboard endpoint")

        if request.args.get('account_id') is not None:
            account_id = int(request.args.get('account_id'))
            task_id = request.args.get('task_id')

            dashboard_object = Dashboard(account_id=account_id,task_id=task_id)
            response = dashboard_object.new_dashboard_details(account_id=account_id,task_id=task_id)

            logger.debug(f"Successfully called dashboard service through dashboard endpoint")
            json_formatter(response)
            return response
        else:
            logger.error(f"Error: Account ID not provided")
            return {'ERROR':'Account ID not Provided'}


