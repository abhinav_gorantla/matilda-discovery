import json
import os

from flask import request, send_file
from flask_restplus import Resource

from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.restplus import api
from matilda_discovery.services.application import Applications,ApplicationList
from matilda_discovery.services.application import ApplicationDetails as ApplicationDetailsService
from matilda_discovery.services.scheduler import Scheduler
from matilda_discovery.utils import util
from matilda_discovery.utils.util import json_formatter

logger = logger_py_file.Logger()

applications_ns = api.namespace('applications', description='namespace for applications endpoint')


@applications_ns.route('/matilda/hostdiscovery/applications')
class ApplicationsEntry(Resource):

    def post(self):
        """
        posts the request data obtained from the Service discovery. request data contains host_id,service_id,ip,applications list
        :return:sucess message
        """
        try:
            logger.debug(f"Calling task creation service from task creation endpoint")
            payload = json.loads(request.data)
            request_data = payload['request']
            scheduler_obj = Scheduler()
            request_data['job_trigger_type'] = 'applications_ip_information_update_job'
            scheduler_obj.create_job(request=request_data, job_type='applications_ip_information_update_job')
            logger.debug(f"Successfully completed applications information update. Response: {request_data}")
            return {'response': "Success"}

        except Exception as e:
            logger.error(f"Failed while calling update applications information endpoint. Error: {e}")
            return None

@applications_ns.route('/matilda/hostdiscovery/applications/file/download')
class DownloadInventory(Resource):
    def get(self):
        """
        accepts account id and generates Excel to download
        :return:file to download
        """

        try:
            logger.debug("Dowloading Template file...")
            if request.args.get('account_id') is not None:
                account_id =request.args.get('account_id')
                inventory_object = Applications()
                template = inventory_object.download_excel(account_id)
                return send_file(template, as_attachment=True)
        except Exception as e:
            logger.error(f"Exception occurred while downloading Template at the endpoint. Error: {e}")
            return None

#app template
@applications_ns.route('/matilda/hostdiscovery/application/template')
class DownloadInventory(Resource):
    def get(self):
        """
        accepts accound id and generates Excel to download
        :return:file to download
        """
        try:
            logger.debug("Dowloading Template file...")
            if request.args.get('account_id') is not None:
                inventory_object = Applications()
                template = inventory_object.download_application_template()
                return send_file(template, as_attachment=True)
        except Exception as e:
            logger.error(f"Exception occurred while dowloading Template at the endpoint. Error: {e}")
            return None

#app host mapping template
@applications_ns.route('/matilda/hostdiscovery/applicationhostmapping/template')
class DownloadInventory(Resource):
    def get(self):
        """
        accepts accound id and generates Excel to download
        :return:file to download
        """

        try:
            logger.debug("Dowloading Template file...")
            if request.args.get('account_id') is not None:
                inventory_object = Applications()
                template = inventory_object.download_application_host_mapping_template()
                return send_file(template, as_attachment=True)
        except Exception as e:
            logger.error(f"Exception occurred while downloading Template at the endpoint. Error: {e}")
            return None

#export app data
@applications_ns.route('/matilda/hostdiscovery/application/export')
class DownloadInventory(Resource):
    def get(self):
        """accepts accound id and generates Excel to download
           :return:file to download
        """
        try:
            logger.debug("Exporting application data...")
            if request.args.get('account_id') is not None:
                account_id =request.args.get('account_id')
                inventory_object = Applications()
                template = inventory_object.export_application_data(account_id)
                return send_file(template, as_attachment=True)
        except Exception as e:
            logger.error(f"Exception occurred while Exporting application data at the endpoint. Error: {e}")
            return None

#export app host mapping data
@applications_ns.route('/matilda/hostdiscovery/applicationhostmapping/export')
class DownloadInventory(Resource):
    def get(self):
        """
            accepts account id and generates Excel to download
            :return:file to download
        """
        try:
            logger.debug("Exporting applicationHostmapping data...")
            if request.args.get('account_id') is not None:
                account_id =request.args.get('account_id')
                inventory_object = Applications()
                template = inventory_object.export_application_host_mapping_data(account_id)
                return send_file(template, as_attachment=True)
        except Exception as e:
            logger.error(f"Exception occurred while Exporting applicationHostmapping data at the endpoint. Error: {e}")
            return None


# app import
@applications_ns.route('/matilda/hostdiscovery/inputfileupload/application/import')
class ExportApplication(Resource):
    def post(self):
        try:
            logger.debug("Importing Application data file...")
            file = request.files
            excelfile = file['upload_input']
            file_name = excelfile.filename
            payload = json.loads(file['payload'].read())

            file_name = 'Input_' + str(util.get_datetime(req_format=CreateInfra.file_date_format.value)) + '_' + file_name
            path = os.path.normpath(os.path.join(os.path.dirname(__file__), CreateInfra.upload.value))
            util.create_dir_file(path)
            file_to_upload = os.path.join(path + '/' + file_name)
            if os.path.exists(file_to_upload):
                os.remove(file_to_upload)
            excelfile.save(file_to_upload)

            request_data = dict(filename=file_name, account_id=payload['request'].get('account_id'),job_trigger_type='import_application')
            scheduler_obj = Scheduler()
            scheduler_obj.create_job(request=request_data,job_type='import_application')
            return {"Response": "Successful Imported Application data file"}

        except Exception as e:
            logger.error(f"Exception occurred while Importing Application data file at the endpoint. Error: {e}")
            return None


# app host mapping import
@applications_ns.route('/matilda/hostdiscovery/inputfileupload/applicationhostmapping/import')
class ExportApplicationHostMapping(Resource):
    def post(self):
        try:
            logger.debug("Exporting ApplicationHostMapping data file...")
            file = request.files
            excelfile = file['upload_input']
            file_name = excelfile.filename
            payload = json.loads(file['payload'].read())

            file_name='Input_' + str(util.get_datetime(req_format=CreateInfra.file_date_format.value)) + '_'+ file_name
            path = os.path.normpath(os.path.join(os.path.dirname(__file__), CreateInfra.upload.value))
            util.create_dir_file(path)
            file_to_upload = os.path.join(path + '/' + file_name)
            if os.path.exists(file_to_upload):
                os.remove(file_to_upload)
            excelfile.save(file_to_upload)

            request_data = dict(filename=file_name, account_id=payload['request'].get('account_id'),job_trigger_type='import_application_host_mapping')
            scheduler_obj = Scheduler()
            scheduler_obj.create_job(request=request_data, job_type='import_application_host_mapping')
            return{"Response":"Successful Exported ApplicationHostMapping the Input data file"}
        except Exception as e:
            logger.debug(f"Exception occurred while Exporting ApplicationHostMapping data file at the endpoint. Error: {e}")
            return None


@applications_ns.route('/matilda/hostdiscovery/application/details')
class ApplicationDetails(Resource):

    def get(self):
        """
        Accepts app id and cloud provider and returns  respective details
        """
        logger.debug(f"Calling application service through application endpoint")

        try:
            if request.args.get('app_id') is not None and request.args.get('cloud_provider') is not None:
                application_service = ApplicationDetailsService()
                application_details = application_service.application_assessment_details(app_id=int(request.args.get('app_id')), cloud_provider=request.args.get('cloud_provider'))
                util.json_formatter(application_details)

                logger.debug(f"Successfully fetched app details for app_id: {int(request.args.get('app_id'))}")
                return application_details

            else:
                logger.debug(f"Empty host list returned for app_id: {int(request.args.get('app_id'))}")
                return None

        except Exception as e:
            logger.error(f"Failed to retreive app details. Error: {e}")
            return None


@applications_ns.route('/matilda/hostdiscovery/applications_list')
class AssetsApplicationsList(Resource):

    def get(self):
        """
        fetches al the application information and the assessment made on the applications
        return : list, discovered applicatins and assessments made on those applications.
        """
        try:
            logger.debug("Initiating the process of fetching applications details for the successfully discovered ip's")
            if request.args.get('account_id') is not None and request.args.get('cloud_provider') is not None:
                application_service_object = ApplicationList()
                response = application_service_object.get_application_assets(cloud_provider=request.args.get('cloud_provider'))
                json_formatter(response)
                return response
            else:
                return {"response": "account_id not provided"}

        except Exception as e:
            logger.error(f"Exception occurred while initiating the process of fetching the application list. Exception : {e}")
            return None

#NEW UI
#Assets- Application list
@applications_ns.route('/matilda/new_hostdiscovery/applications_list')
class new_AssetsApplicationsList(Resource):

    def get(self):
        """
        fetches al the application information and the assessment made on the applications
        return : list, discovered applicatins and assessments made on those applications.
        """
        try:
            logger.debug("Initiating the process of fetching applications details for the successfully discovered ip's")
            if request.args.get('account_id') is not None and request.args.get('cloud_provider') is not None:
                application_service_object = ApplicationList()
                response = application_service_object.new_get_application_assets(cloud_provider=request.args.get('cloud_provider'))
                json_formatter(response)
                return response
            else:
                return {"response": "account_id not provided"}

        except Exception as e:
            logger.error(f"Exception occurred while initiating the process of fetching the application list. Exception : {e}")
            return None


@applications_ns.route('/matilda/hostdiscovery/applications/dashboard')
class AssetsApplicationsDashboard(Resource):

    def get(self):
        """
        fetches al the application information and the assessment made on the applications
        return : list, discovered applicatins and assessments made on those applications.
        """
        try:
            logger.debug("Initiating the process of fetching applications dashboard summary")
            if request.args.get('account_id') is not None and request.args.get('cloud_provider') is not None:
                application_service_object = ApplicationList()
                response = application_service_object.get_application_dashboard_summary(cloud_provider=request.args.get('cloud_provider'))
                json_formatter(response)
                return response
            else:
                return {"response": "account_id or cloud_provider is not provided"}

        except Exception as e:
            logger.error(f"Exception occurred while initiating the process of fetching the application dashboard summary. Exception : {e}")
            return None
#NEWUI
#ApplicationDashboard
@applications_ns.route('/matilda/new_hostdiscovery/applications/dashboard')
class new_AssetsApplicationsDashboard(Resource):

    def get(self):
        """
        fetches al the application information and the assessment made on the applications
        return : list, discovered applicatins and assessments made on those applications.
        """
        try:
            logger.debug("Initiating the process of fetching applications dashboard summary")
            if request.args.get('account_id') is not None and request.args.get('cloud_provider') is not None:
                application_service_object = ApplicationList()
                response = application_service_object.new_get_application_dashboard_summary(cloud_provider=request.args.get('cloud_provider'))
                json_formatter(response)
                return response
            else:
                return {"response": "account_id or cloud_provider is not provided"}

        except Exception as e:
            logger.error(f"Exception occurred while initiating the process of fetching the application dashboard summary. Exception : {e}")
@applications_ns.route('/matilda/hostdiscovery/application_recommendation/reinitiate')
class ReinitiateHostRecommendation(Resource):

    def post(self):
        """
        Removes existing recommendations and restarts it for all successful recommendations. If host id is passed, recommendation will be reset only for that id
        """
        try:
            logger.debug(f"Initiating application recommendations process")

            application_service_object = ApplicationList()
            application_service_object.application_recommendation_analysis()

            return {"response":"successfully completed application recommendation analysis"}

        except Exception as e:
            logger.error(f"Exception occurred while initiating the process of re-initiating application recommendation. Exception : {e}")
            return None
