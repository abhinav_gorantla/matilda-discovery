from flask import request
from flask_restplus import Resource

from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.restplus import api
from matilda_discovery.services.vcenter import TaskCreation as TaskCreationService

logger = logger_py_file.Logger()

import json

vcenter_ns = api.namespace('vcenter', description='namespace for task endpoint')


@vcenter_ns.route('/matilda/hostdiscovery/vcenter/initiate')
class TaskCreation(Resource):

    #@api.expect(task_creation_model, validate=True)
    def post(self):
        """
        posts the request data obtained from the UI into the DB by creating task for every new discovery and aslo creates a new record in host table for every ip in the given list.
        :return:list of items. contains task_name, and list of dictionaries of host_ip's and host_id's under the generated task_id for the discovery
        """
        try:
            logger.debug(f"Storing Vcenter credentials in Task table")
            payload = json.loads(request.data)
            request_data = payload['request']
            print(request_data)
            task_creation_service_object = TaskCreationService()
            host_list_resp = task_creation_service_object.create_discovery_task(request_data)

            logger.debug(f"Successfully stored vcenter credentials. Response: {'Vcenter'}")
            return host_list_resp

        except Exception as e:
            logger.error(f"Failed while storing vcenter credentials. Error: {e}")
            return None

