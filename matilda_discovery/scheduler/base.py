from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from apscheduler.schedulers.background import BackgroundScheduler

from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db import connection
from matilda_discovery.utils import util

data = util.get_properties(property_file_path=CreateInfra.etc_properties_path.value, section='database')

scheduler = BackgroundScheduler()
# default_jobstore = {
# 'default': SQLAlchemyJobStore(url='mysql+mysqldb://' + data['username'] + ':' + data['password'] + '@' + data['url'] + '/' + data['dbname'])
# }

default_jobstore = {
'default': SQLAlchemyJobStore(engine=connection.get_engine())
}

executors = {
    'default': {'type': 'threadpool', 'max_workers': 3000},
    # 'processpool': ProcessPoolExecutor(max_workers=1000)
}

job_defaults = {
    'coalesce': True,
    'max_instances': 100,
    'misfire_grace_time':3600
}

def configure():
    scheduler.configure(jobstores=default_jobstore, executors=executors, job_defaults=job_defaults)

def start():
    configure()
    scheduler.start()




