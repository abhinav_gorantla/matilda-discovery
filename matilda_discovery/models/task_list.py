from flask_restplus import fields
from matilda_discovery.restplus import api

discovered_ip_list_fields = api.model('discovered_ip_list_fields',
                                        {
                                            'Range': fields.List(fields.String(required=False, default='NULL', description='xxxxxx')),
                                            'Cidr': fields.List(fields.String(required=False, default='NULL', description='xxxxxx'))
                                        })

user_entered_ports_fields = api.model('user_port_input_fields',
    {
        "service_type": fields.String(required=False, default='NULL', description='xxxxxx'),
        "service_port": fields.String(required=False, default='NULL', description='xxxxxx')
    })

response_fields = api.model('response_fields',
    {
        "task_id": fields.Integer(required=False, default=0, description='xxxxxx'),
        "status": fields.String(required=False, default='NULL', description='xxxxxx'),
        "Created_Date": fields.String(required=False, default='NULL', description='xxxxxx'),
        "created_by": fields.String(required=False, default='NULL', description='xxxxxx'),
        "start_date": fields.String(required=False, default='NULL', description='xxxxxx'),
        "end_date": fields.String(required=False, default='NULL', description='xxxxxx'),
        "Updated_Date": fields.String(required=False, default='NULL', description='xxxxxx'),
        "updated_by": fields.String(required=False, default='NULL', description='xxxxxx'),
        "Hosts_Count": fields.String(required=False, default='NULL', description='xxxxxx'),
        "login_mode": fields.String(required=False, default='NULL', description='xxxxxx'),
        "account_id": fields.String(required=False, default='NULL', description='xxxxxx'),
        "discovery_type": fields.String(required=False, default='NULL', description='xxxxxx'),
        "discovered_ip_list": fields.Nested(discovered_ip_list_fields),
        "user_entered_ports": fields.List(fields.Nested(user_entered_ports_fields)),
        "scheduled_time": fields.String(required=False, default='NULL', description='xxxxxx'),
        "execution_mode": fields.String(required=False, default='NULL', description='xxxxxx'),
        "Success": fields.Integer(required=False, default=0, description='xxxxxx'),
        "Failed": fields.Integer(required=False, default=0, description='xxxxxx'),
    })

summary_fields = api.model('summary_fields',
                           {
                                "total_tasks": fields.Integer(required=False, default=0, description='xxxxxx'),
                                "submitted": fields.Integer(required=False, default=0, description='xxxxxx'),
                                "in_progress": fields.Integer(required=False, default=0, description='xxxxxx'),
                                "completed": fields.Integer(required=False, default=0, description='xxxxxx'),
                           })

task_list_model = api.model('task_list_model',
                            {
                                'response': fields.Nested(response_fields),
                                'summary': fields.Nested(summary_fields),
                            })


x = \
{
"response":
    [
    {
        "task_id": 375,
        "status": "completed",
        "Created_Date": "06-14-2019 09:15:31 AM",
        "created_by": "Matilda_Discovery_1",
        "start_date": "06-14-2019 09:15:34 AM",
        "end_date": "06-14-2019 09:16:05 AM",
        "Updated_Date": "06-14-2019 09:16:05 AM",
        "updated_by": "Discovery_Engine",
        "Hosts_Count": "32",
        "login_mode": "Credentials",
        "account_id": "007",
        "discovery_type": "Host Discovery",
        "discovered_ip_list":
            {
                "Range": [],
                "Cidr": [ "192.168.20.123/27" ]
            },
        "user_entered_ports":
            [
                {"service_type": "weblogic", "service_port":"7002"},
                {"service_type": "tomcat", "service_port":"7001"},
            ],
        "scheduled_time": None,
        "execution_mode": None,
        "Success": 1,
        "Failed": 29
    },
    ],

"summary":
    {
        "total_tasks": 141,
        "submitted": 8,
        "in_progress": 8,
        "completed": 125
    }
}