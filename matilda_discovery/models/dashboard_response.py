from flask_restplus import fields
from matilda_discovery.restplus import api


stages_common_fields = api.model('stages_common_fields',
{
    'Label': fields.String(required=False,default='NULL',  description='xxxxxx'),
    'Current': fields.Integer(required=False,default=0,  description='xxxxxx'),
    'Total': fields.Integer(required=False,default=0,  description='xxxxxx'),
    'Percent': fields.Float(required=False,default=0.0,  description='xxxxxx')
}
)

stage_fields = api.model('stage_fields',
{
    'Ping': fields.Nested(stages_common_fields),
    'Device': fields.Nested(stages_common_fields),
    'Login': fields.Nested(stages_common_fields),
    'Discovery': fields.Nested(stages_common_fields)
 }
 )

utilization_common_fields = api.model('utilization_common_fields',
{
    "Used": fields.String(required=False, default='NULL', description='xxxxxx'),
    "Available": fields.String(required=False, default='NULL', description='xxxxxx'),
    "Capacity": fields.String(required=False, default='NULL', description='xxxxxx'),
    "Percent": fields.Float(required=False, default=0.0, description='xxxxxx'),
}
)

utilization_cpu_fields = api.model('utilization_cpu_fields',
{
    "Percent": fields.Float(required=False, default=0.0, description='xxxxxx'),
    "CpuCount": fields.Float(required=False, default=0.0, description='xxxxxx'),
    "CpuCores": fields.Float(required=False, default=0.0, description='xxxxxx'),
}
)

utilization_fields = api.model('stage_fields',
{
    'memory': fields.Nested(utilization_common_fields),
    'cpu': fields.Nested(utilization_cpu_fields),
    'Storage': fields.Nested(utilization_common_fields),
 }
 )

other_common_fields_1 = api.model('other_common_fields_1', {
    "name": fields.String(required=False, default='NULL', description='xxxxxx'),
    "value": fields.Integer(required=False, default=0, description='xxxxxx'),
})

other_common_fields_2 = api.model('other_common_fields_2', {
    "name": fields.String(required=False, default='NULL', description='xxxxxx'),
    "value": fields.Integer(required=False, default=0, description='xxxxxx'),
    'data': fields.Nested(other_common_fields_1),
})

host_distribution_fields = api.model('host_distribution_fields',{
    "Label": fields.String(required=False, default='NULL', description='xxxxxx'),
    "data": fields.Nested(other_common_fields_1),
})

dashboard_response_model = api.model('discovery_dashboard model',
{
    "status": fields.String(required=False,default='NULL',  description='xxxxxx'),
    "StartedTime": fields.String(required=False,default='00/00/0000 00:00:00',  description='xxxxxx'),
    "ElapsedTime": fields.String(required=False,default='00:00:00',  description='xxxxxx'),
    "Stages": fields.Nested(stage_fields),
    "utilization": fields.Nested(utilization_fields),
    "HostDistribution": fields.Nested(host_distribution_fields),
    "operating_system": fields.Nested(other_common_fields_2),
    "Application_Services": fields.Nested(other_common_fields_2),
    "Devices": fields.Nested(other_common_fields_2),
})


# --------------------------------------------------------------------------- #

# user_fields = api.model('User', {
#     'id': fields.Integer,
#     'name': fields.String,
# })
#
# user_list_fields = api.model('UserList', {
#     'users': fields.List(fields.Nested(user_fields)),
# })
#
# wild = fields.Wildcard(fields.String)
# wildcard_fields = {'*': wild}