dashboard_response_template = None
    # {
    #         "Status": "NULL", "StartedTime": "NULL", "ElapsedTime": "NULL",
    #         "Stages": {
    #             "Ping": {"label": "Device Pingable", "Current": "NULL", "Total": "NULL", "Percent": "NULL"},
    #             "Device": {"label": "Device Identification", "Current": "NULL", "Total": "NULL", "Percent": "NULL"},
    #             "Login": {"label": "Login", "Current": "NULL", "Total": "NULL", "Percent": "NULL"},
    #             "Discovery": {"label": "Discovery", "Current": "NULL", "Total": "NULL", "Percent": "NULL"}},
    #         "Utilization": {
    #             "Memory": {"Used": "NULL", "Available": "NULL", "Capacity": "NULL", "Percent": "NULL"},
    #             "Cpu": {"Percent": "NULL", "Physical_Processors": "NULL", "Physical_Cores": "NULL","Logical_Processors":"NULL"},
    #             "Storage": {"Used": "NULL", "Available": "NULL", "Capacity": "NULL", "Percent": "NULL"}},
    #         "HostDistribution": [],
    #         "Operating_System": [],
    #         "Application_Services": [],
    #         "Devices": []
    #         }

# utilization details
memory_template = {"Used": 0, "Available": 0, "Capacity": 0, "Percent": 0}
cpu_template = {"Physical_Processors": 0, "Physical_Cores": 0,"Logical_Processors":0,"Percent":0}  # change in UI also
storage_template = {"Used": 0, "Available": 0, "Capacity": "NULL", "Percent": "NULL"}
utilization_template = {"Memory": memory_template, "Cpu": cpu_template, "Storage": storage_template}

# stage details
ping_template = {"label": "Device Pingable", "Current": 0, "Total": 0, "Percent": 0}
device_template = {"label": "Device Identification","Current": 0, "Total": 0, "Percent": 0}
login_template = {"label": "Login", "Current": 0, "Total": 0, "Percent": 0}
discovery_template = {"label": "Discovery", "Current": 0, "Total": 0, "Percent": 0}

# host distribution details
host_distribution_details_template = None
#{" label": "Region", "data": [{"name": "NULL", "value": "NULL"}, ]},{" label": "DataCenter", "data": [{"name": "NULL", "value": "NULL"}, ]},{" label": "LineofBusiness", "data": [{"name": "NULL", "value": "NULL"}, ]},{" label": "Project", "data": [{"name": "NULL", "value": "NULL"}, ]},{" label": "Environment", "data": [{"name": "NULL", "value": "NULL"}, ]}]
# os details
os_details_template = None

# applications service details
application_service_details_template = None

# device details
device_details_template = None

#task_services_summary
services_summary = {"Services": None}

#application_dashboard_template
application_dashboard = {
                        "total": "",
                        "existing": [ {"name": "Yearly on Premise","value": "","Savings":""}, {"name": "Yearly on demand","value": "","Savings": ""}, {"name": "Reserved 1yr","value": "","Savings": ""}, {"name": "Reserved 3yr","value": "","Savings": ""} ],
                        "recommended": [ { "name": "Yearly on Premise", "value": "", "Savings": "" }, { "name": "Yearly on demand", "value": "", "Savings": "" }, { "name": "Reserved 1yr", "value": "", "Savings": "" }, { "name": "Reserved 3yr", "value": "", "Savings": "" } ],
                        "cloud_suitability": "",
                        "migration_strategy": { "chart_data": { "Rehost": None, "Rebuild": None, "Refactor": None },
                                                "description": [ "All the host under these application need to be lift and shifted", "One or more hosts under these applications need to be upgraded ", "One or more hosts under these applications need to be refactored" ] },
                        "migration_priority": { "chart_data": {},
                                                "description": [ "Applications contains vulnerable hosts", "Application contains hosts wich doesnt have enough support for long run", "Applications contain long lasting host with the support" ] },
                        "business_group": { "bar": { "Prod": None, "QA": None, "Dev": None }, "pie": [ { "value": None, "name": None, "bar": { "Prod": None, "QA": None, "Dev": None } }, { "value": None, "name": None, "bar": { "Prod": None, "QA": None, "Dev": None } }, { "value": None, "name": None, "bar": { "Prod": None, "QA": None, "Dev": None} } ] },
                        "business_criticality": { "High": None, "Medium": None, "Low": None }
                        }


application_ui_null_dashboard = {
                                "total": 0,
	                            "existing": [{"name": "Yearly on Premise","value": 0,"Savings": ""},{"name": "Yearly on demand","value":None,"Savings": None},{"name": "Reserved 1yr","value": None,"Savings": None},{"name": "Reserved 3yr", "value": None, "Savings": None } ],

                                "recommended": [{ "name": "Yearly on Premise", "value": 0, "Savings": "" }, { "name": "Yearly on demand", "value": None, "Savings": None }, { "name": "Reserved 1yr", "value": None, "Savings": None }, { "name": "Reserved 3yr", "value": None, "Savings": None } ],
                                "cloud_suitability": "",
                                "migration_strategy":
                                    {
                                                    "chart_data": "",
                                                    "description": ["All the host under these application need to be lift and shifted", "One or more hosts under these applications need to be upgraded ", "One or more hosts under these applications need to be refactored"]
                                    },
                                "migration_priority":
                                    {
                                        "chart_data": "",
                                        "description": [ "Applications contains vulnerable hosts", "Application contains hosts wich doesnt have enough support for long run", "Applications contain long lasting host with the support" ]
                                    },
                                "business_group": {
                                    "bar": "",
                                    "pie": []
                                },
                                "business_criticality": ""
                            }

application_topology_template = \
             [
              {
                "name": None,
                "children":
                    [
                        {
                        "name": "Infrastructure",
                        "children": None
                        },
                        {
                            "name": "Service",
                            "children": None
                        },
                        {
                            "name": "Database",
                            "children": None
                        }
                    ]
              }
             ]


account_dashboard = {
                                                "kpi_activities": {
                                                    "discoveries_status": [{
                                                        "count": 5,
                                                        "title": "discoveries"
                                                    }],
                                                    "discoveries_categories": [{
                                                            "count": None,
                                                            "title": "Devices"
                                                        },
                                                        {
                                                            "count": None,
                                                            "title": "OS"
                                                        },
                                                        {
                                                            "count": None,
                                                            "title": "Services"
                                                        }
                                                    ]
                                                },
                                                "discovery_stages": [
                                                ],

                                                "host_service_bardata": {
                                                    "total": 48,
                                                    "up": 33,
                                                    "down": 15
                                                },
                                                "health_pie_data": [{
                                                        "value": 560,
                                                        "name": "Closed"
                                                    },
                                                    {
                                                        "value": 359,
                                                        "name": "Active"
                                                    },
                                                    {
                                                        "value": 201,
                                                        "name": "Open"
                                                    }
                                                ],
                                                "host_distribution": [],
                                                "utilization": {
                                                    "memory": {
                                                        "used": None,
                                                        "available": None,
                                                        "capacity": None,
                                                        "percent": None
                                                    },
                                                    "cpu": {
                                                        "physical_processors": None,
                                                        "physical_cores": None,
                                                        "logical_processors": None,
                                                        "percent": None
                                                    },
                                                    "storage": {
                                                        "used": None,
                                                        "available": None,
                                                        "capacity": None,
                                                        "percent": None
                                                    }
                                                },
                                                "latest_completed_discoveries": {
                                                    "discoveries": [{
                                                            "host_id": 12112,
                                                            "discovery_name": "discovery name 1",
                                                            "status": "completed",
                                                            "created_on": "21st jan 2020",
                                                            "start_date": "30th jan 2020",
                                                            "start_time": "05:30 am",
                                                            "end_date": "14th feb 2020",
                                                            "end_time": "06:00 pm",
                                                            "scheduled_on": "30th jan 2020",
                                                            "discovered": "10",
                                                            "total": "20"
                                                        }
                                                    ]
                                                },
                                                "device_discovered_bar_data": {
                                                    "switch": 23,
                                                    "nas": 23,
                                                    "computer": 34,
                                                    "server": 23
                                                },
                                                "donut_details": {

                                                    "os_chart": [ ],


                                                    "servicechart": [{
                                                                        "name": "Application Server",
                                                                        "value": 68,
                                                                        "bar_stack_data": [
                                                                            {
                                                                            "category": "jboss",
                                                                            "values": [{
                                                                                    "key": "Expired",
                                                                                    "value": 5
                                                                                },
                                                                                {
                                                                                    "key": "Active",
                                                                                    "value": 10
                                                                                },
                                                                                {
                                                                                    "key": "Soon",
                                                                                    "value": 4
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            "category": "Tomcat",
                                                                            "values": [{
                                                                                    "key": "Expired",
                                                                                    "value": 6
                                                                                },
                                                                                {
                                                                                    "key": "Active",
                                                                                    "value": 5
                                                                                },
                                                                                {
                                                                                    "key": "Soon",
                                                                                    "value": 6
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            "category": "Weblogic",
                                                                            "values": [{
                                                                                    "key": "Expired",
                                                                                    "value": 4
                                                                                },
                                                                                {
                                                                                    "key": "Active",
                                                                                    "value": 10
                                                                                },
                                                                                {
                                                                                    "key": "Soon",
                                                                                    "value": 10
                                                                                }
                                                                            ]
                                                                        }]
                                                                    },
                                                                    {
                                                                        "name": "Database",
                                                                        "value": 72,
                                                                        "bar_stack_data": [
                                                                            {
                                                                            "category": "Mssql",
                                                                            "values": [{
                                                                                    "key": "Expired",
                                                                                    "value": 10
                                                                                },
                                                                                {
                                                                                    "key": "Active",
                                                                                    "value": 15
                                                                                },
                                                                                {
                                                                                    "key": "Soon",
                                                                                    "value":5
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            "category": "Oracle",
                                                                            "values": [{
                                                                                    "key": "Expired",
                                                                                    "value": 6
                                                                                },
                                                                                {
                                                                                    "key": "Active",
                                                                                    "value": 3
                                                                                },
                                                                                {
                                                                                    "key": "Soon",
                                                                                    "value":1
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            "category": "Mysql",
                                                                            "values": [{
                                                                                    "key": "Expired",
                                                                                    "value": 4
                                                                                },
                                                                                {
                                                                                    "key": "Active",
                                                                                    "value": 16
                                                                                },
                                                                                {
                                                                                    "key": "Soon",
                                                                                    "value":12
                                                                                }
                                                                            ]
                                                                        }]
                                                                    },
                                                                    {
                                                                        "name": "Web Server",
                                                                        "value": 46,
                                                                        "bar_stack_data": [
                                                                            {
                                                                            "category": "Apache",
                                                                            "values": [{
                                                                                    "key": "Expired",
                                                                                    "value": 4
                                                                                },
                                                                                {
                                                                                    "key": "Active",
                                                                                    "value": 12
                                                                                },
                                                                                {
                                                                                    "key": "Soon",
                                                                                    "value": 3
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            "category": "Nginx",
                                                                            "values": [{
                                                                                    "key": "Expired",
                                                                                    "value": 6
                                                                                },
                                                                                {
                                                                                    "key": "Active",
                                                                                    "value": 6
                                                                                },
                                                                                {
                                                                                    "key": "Soon",
                                                                                    "value": 2
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            "category": "IIS",
                                                                            "values": [{
                                                                                    "key": "Expired",
                                                                                    "value": 7
                                                                                },
                                                                                {
                                                                                    "key": "Active",
                                                                                    "value": 4
                                                                                },
                                                                                {
                                                                                    "key": "Soon",
                                                                                    "value": 1
                                                                                }
                                                                            ]
                                                                        }]
                                                                    }
                                                                                                            ]

                                                                                                    }
                                                                                                    }

Dashboard_Stage_template =[{
                            "category": "ping",
                            "values": []
                        },
                        {
                            "category": "device",
                            "values": []
                        },
                        {
                            "category": "login",
                            "values": []
                        },
                        {
                            "category": "discovery",
                            "values": []
                        }]

dashboard_os_detail_template = {
					"category": None,
					"values": [{
							"key": "Expired",
							"value": None
						},
						{
							"key": "Active",
							"value": None
						},
						{
							"key": "Expires Soon",
							"value": None
						}
					]
				}