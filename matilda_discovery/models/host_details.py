from flask_restplus import fields
from matilda_discovery.restplus import api

host_details_fields = api.model('host_details_fields',
                           {
                                "platform": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "operating_system": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "release_version": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "version": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "BootUpDate": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "CoresCount": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "MemSize": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "InstanceType": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "ServiceProvider": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "name": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "BIOS_Configuration": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "instruction_set": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "pagefile_size": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "internet_access": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "timezone": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "installed_on": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Uptime": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "sku": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "TotalListeningPorts": fields.Integer(required=False, default=0, description='xxxxxx'),
                                "TotalInterfaces": fields.Integer(required=False, default=0, description='xxxxxx'),
                                "TotalServices": fields.Integer(required=False, default=0, description='xxxxxx')
                           })

additional_details_fields = api.model('additional_details_fields',
                                      {
                                          "Servicename": fields.String(required=False, default='NULL', description='xxxxxx'),
                                          "ServiceDiscoveryID": fields.String(required=False, default='NULL', description='xxxxxx'),
                                          "version": fields.String(required=False, default='NULL', description='xxxxxx'),
                                          "Port": fields.String(required=False, default='NULL', description='xxxxxx'),
                                          "CPU(%)": fields.String(required=False, default='NULL', description='xxxxxx'),
                                          "Mem(%)": fields.String(required=False, default='NULL', description='xxxxxx'),
                                          "Path": fields.String(required=False, default='NULL', description='xxxxxx'),

                                      })

services_fields = api.model('services_fields',
                           {
                                "Servicename": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "status": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "ServiceType": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "AdditionalDetails": fields.List(fields.Nested(additional_details_fields))
                           })

memory_information_fields = api.model('memory_information_fields',
                           {
                                "Used": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Available": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Capacity": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Used_Percent": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Memory_Type": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Memory_Speed": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "FreeMem": fields.List(fields.String(required=False, default='NULL', description='xxxxxx'),)
                           })

cpu_information_fields = api.model('cpu_information_fields',
                           {
                                "Utilization_Percent": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Available_Percent": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Capacity_Percent": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Devicename": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "CoresCount": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "CPUCount": fields.String(required=False, default='NULL', description='xxxxxx')
                           })

disk_usage_fields = api.model('disk_usage_fields',
                           {
                                "Filesystem": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Capacity": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Used": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Available": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Used_Percent": fields.String(required=False, default='NULL', description='xxxxxx')
                           })

storage_information_fields = api.model('storage_information_fields',
                           {
                                "Disk": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Type": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Partition": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Logical_Volume": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Owner": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Group": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Permissions": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Total_Space": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Used_Space": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Available_Space": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Used_Percent": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Mounted": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Model": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Compressed": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Vol_SerialNo": fields.String(required=False, default='NULL', description='xxxxxx')
                           })

packages_fields = api.model('packages_fields',
                           {
                                "Packagename": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "PackageVersion": fields.String(required=False, default='NULL', description='xxxxxx')
                           })

interfaces_fields = api.model('interfaces_fields',
                           {
                                "Interfacename": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Description": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "IPAddress": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "MACAddress": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "PktMaxSize": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Received_Packets": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Sent_Packets": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Speed": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "DHCP_Enabled": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "DNS_Server": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Subnet_Mask": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Type": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "status": fields.String(required=False, default='NULL', description='xxxxxx'),
                           })

user_groups_fields = api.model('user_groups_fields',
                           {
                                "username": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Groupname": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "SID": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "GroupDescription": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "UserType": fields.String(required=False, default='NULL', description='xxxxxx'),
                           })

ports_fields = api.model('ports_fields',
                           {
                                "Port": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Interface": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "TX/RX_Bytes": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "status": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Service": fields.String(required=False, default='NULL', description='xxxxxx'),
                           })

connected_services_fields = api.model('connected_services_fields',
                           {
                                "LocalService": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "Direction": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "ConnectedHost": fields.String(required=False, default='NULL', description='xxxxxx'),
                                "ConnectedService": fields.String(required=False, default='NULL', description='xxxxxx'),
                           })

host_details_model = api.model('host_details_model',
                            {
                                'HostId': fields.Integer(required=False, default=0, description='xxxxxx'),
                                'Discoveryname': fields.String(required=False, default='NULL', description='xxxxxx'),
                                'HostIp': fields.String(required=False, default='NULL', description='xxxxxx'),
                                'Host_details': fields.Nested(host_details_fields),
                                'Services': fields.List(fields.Nested(services_fields)),
                                'Memory_Information': fields.Nested(memory_information_fields),
                                'Cpu_Information': fields.Nested(cpu_information_fields),
                                'Disk_usage': fields.Nested(disk_usage_fields),
                                'Storage_Information': fields.List(fields.Nested(storage_information_fields)),
                                'Packages': fields.List(fields.Nested(packages_fields)),
                                'Interfaces': fields.List(fields.Nested(interfaces_fields)),
                                'Usergroups': fields.List(fields.Nested(user_groups_fields)),
                                'Ports': fields.List(fields.Nested(ports_fields)),
                                'Connected_Services': fields.List(fields.Nested(connected_services_fields)),
                            })



