from flask_restplus import fields
from matilda_discovery.restplus import api

ip_list_fields = api.model('ip_list_fields',
    {
        "ip_address": fields.String(required=False, default='NULL', description='xxxxxx'),
        "type": fields.String(required=False, default='NULL', description='xxxxxx')
    })

user_port_input_fields = api.model('user_port_input_fields',
    {
        "service_type": fields.String(required=False, default='NULL', description='xxxxxx'),
        "service_port": fields.String(required=False, default='NULL', description='xxxxxx')
    })

request_fields = api.model('request_fields',
    {
        "name": fields.String(required=False, default='NULL', description='xxxxxx'),
        "ip_list": fields.List(fields.Nested(ip_list_fields)),
        "user_port_input": fields.List(fields.Nested(user_port_input_fields)),
        "login_mode": fields.String(required=False, default='NULL', description='xxxxxx'),
        "domain": fields.String(required=False, default='NULL', description='xxxxxx'),
        "username": fields.String(required=False, default='NULL', description='xxxxxx'),
        "password": fields.String(required=False, default='NULL', description='xxxxxx'),
        "account_id": fields.String(required=False, default='NULL', description='xxxxxx'),
        "created_by": fields.String(required=False, default='NULL', description='xxxxxx'),
        "execution_mode": fields.String(required=False, default='NULL', description='xxxxxx'),
        "scheduled_time": fields.String(required=False, default='NULL', description='xxxxxx'),
        "community": fields.String(required=False, default='NULL', description='xxxxxx'),
        "utilization": fields.String(required=False, default='NULL', description='xxxxxx'),
        "interval": fields.String(required=False, default='NULL', description='xxxxxx'),
        "period": fields.String(required=False, default='NULL', description='xxxxxx')
    })

task_creation_model = api.model('task_creation_model', {'request': fields.Nested(request_fields)})


x = {
"request":
    {
        "name":"abctest5",
        "IP_List":
            [
                {   "Ipaddress":"192.168.10.102",
                    "Type":"range"
                },
                {
                    "Ipaddress":"192.168.10.102",
                    "Type":"cidr"
                }
            ],
       "User_port_input":
           [
               {"service_type": "weblogic", "service_port":"7002"},
               {"service_type": "tomcat", "service_port":"7001"},
           ],
       "login_mode":"Credentials",
       "domain" : "abctest2",
       "username": "Administrator",
       "password": "Matilda@123",
       "account_id":"007",
       "created_by":"Gopal",
       "execution_mode":"immediate",
       "scheduled_time":"08-24-2019 7:42:00",
       "community":"matilda",
       "utilization":"No",
       "interval":"3",
       "period":"1"
  }
}


