from flask_restplus import fields
from matilda_discovery.restplus import api


response_fields = api.model('response_fields model',
    {
        "HostId": fields.String(required=False,default='NULL',  description='xxxxxx'),
        "name": fields.String(required=False,default='NULL',  description='xxxxxx'),
        "HostIp": fields.String(required=False,default='NULL',  description='xxxxxx'),
        "Stage": fields.String(required=False,default='NULL',  description='xxxxxx'),
        "platform": fields.String(required=False,default='NULL',  description='xxxxxx'),
        "DeviceType": fields.String(required=False,default='NULL',  description='xxxxxx'),
        "operating_system": fields.String(required=False,default='NULL',  description='xxxxxx'),
        "Release version": fields.String(required=False,default='NULL',  description='xxxxxx'),
        "BootUpDate": fields.String(required=False,default='NULL',  description='xxxxxx'),
        "CoresCount": fields.String(required=False,default='NULL',  description='xxxxxx'),
        "MemSize": fields.String(required=False,default='NULL',  description='xxxxxx'),
        "status": fields.String(required=False,default='NULL',  description='xxxxxx'),
        "reason": fields.String(required=False,default='NULL',  description='xxxxxx'),
        "TaskId": fields.Integer(required=False,default=0,  description='xxxxxx'),
        "InstanceType": fields.String(required=False,default='NULL',  description='xxxxxx'),
        "ServiceProvider": fields.String(required=False,default='NULL',  description='xxxxxx'),
        "name": fields.String(required=False,default='NULL',  description='xxxxxx'),
        "BIOS_Configuration": fields.String(required=False,default='NULL',  description='xxxxxx'),
        "Gateway_IP": fields.String(required=False,default='NULL',  description='xxxxxx')
    })

summary_fields = api.model('summary_fields',
                           {
                                "submitted": fields.Integer(required=False,default=0,  description='xxxxxx'),
                                "scheduled": fields.Integer(required=False,default=0,  description='xxxxxx'),
                                "in_progress": fields.Integer(required=False,default=0,  description='xxxxxx'),
                                "Success": fields.Integer(required=False,default=0,  description='xxxxxx'),
                                "Failed": fields.Integer(required=False,default=0,  description='xxxxxx'),
                           })

devices_fields = api.model('devices_fields',
                           {
                                "Servers": fields.Integer(required=False,default=0,  description='xxxxxx'),
                                "Printers": fields.Integer(required=False,default=0,  description='xxxxxx'),
                                "Switches": fields.Integer(required=False,default=0,  description='xxxxxx'),
                                "Firewalls": fields.Integer(required=False,default=0,  description='xxxxxx'),
                                "VMware": fields.Integer(required=False,default=0,  description='xxxxxx'),
                                "Unknown_Devices": fields.Integer(required=False,default=0,  description='xxxxxx'),
                           })


host_list_model = api.model('host_list_model',
                            {
                                'response': fields.List(fields.Nested(response_fields)),
                                'summary': fields.Nested(summary_fields),
                                'Devices': fields.Nested(devices_fields)
                            })

