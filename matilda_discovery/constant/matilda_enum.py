from enum import Enum
import os
import platform

import collections

class CreateInfra(Enum):

    hosted_platform = platform.system()
    current_location = os.path.dirname(__file__)
    # current_location = os.getcwd()

    if hosted_platform == 'Linux':
        CURRENT_PROJECT_NAME = current_location.split("/")[-2]

        CURRENT_LOG_PATH = "/var/log/matilda/"
        if not os.path.exists(CURRENT_LOG_PATH):
            try:
                os.makedirs(CURRENT_LOG_PATH)
            except OSError as exception:
                print(f"Failed to create CURRENT_LOG_PATH Directory. Exception: {exception}")

    elif hosted_platform == 'Windows':
        CURRENT_PROJECT_NAME = current_location.split("\\")[-2]

        CURRENT_LOG_PATH = "C:\\Matilda\\logs\\"
        if not os.path.exists(CURRENT_LOG_PATH):
            try:
                os.makedirs(CURRENT_LOG_PATH)
            except OSError as exception:
                print(f"Failed to create CURRENT_LOG_PATH Directory. Exception: {exception}")

    # queue for logging
    log_queue = collections.deque()
    scheduler_log_name = "scheduler"

    # Flask settings
    FLASK_SERVER_NAME = 'localhost:5010'
    FLASK_DEBUG = True  # Do not use debug mode in production

    # Generate keys using this function: Fernet.generate_key()
    LOG_ENCRYPTION_KEY = b'kgKiEMVtAsBLMl3QBGBY7ikk6iDmccNv8t24JGwqw7g='  # Same key is used for decryption.

    # datetime formats
    file_date_format = "%m-%d-%Y_%I_%M_%p"
    default_datetime_format = "%Y-%m-%d %H:%M:%S"
    utilization_datetime_format = "%d %b, %H:%M"
    report_datetime_format = "%m-%d-%Y_%I-%M-%S_%p"
    us_datetime_format = "%m-%d-%Y %I:%M %p"
    cover_page_date_format = "%m-%d-%Y"

    etc_properties_path = '../../etc/db_configurations.ini'
    resource_properties_path = '../resources/cmd_resource.ini'
    db_queries_path = '../db/sql_scripts/sql_quries.ini'

    Linux = 'Linux_txt'
    Windows = 'Windows_txt'

    linux_raw_data_path = '/opt/matilda/data/raw_data_archieve/'
    linux_reports_path = '/opt/matilda/data/reports_archieve/'

    win_raw_data_path = 'C:\\Matilda\\Data\\RawDataArchieve\\'
    win_reports_path = 'C:\\Matilda\\Data\\ReportsArchieve\\'

    plots = '../matilda_discovery/services/reports/plots/'
    template = '../matilda_discovery/services/reports/templates/'
    upload='../../temp/'
    temp = '../temp/'
    input = '../temp/'



    # cloud_metadata

    aws_EBS_price_list = {"US East (N. Virginia)": 0.045, "Asia Pacific (Singapore)": 0.054,"Europe (London)":0.053,"Asia Pacific (Sydney)":0.054}  # Throughput Optimized HDD (st1) Volumes lookup for different regions

    supported_regions = {"US East (N. Virginia)": "us-east-1", "Asia Pacific (Singapore)": "ap-southeast-1","Asia Pacific (Sydney)":"ap-southeast-2"}



    #cloud details

    on_premise_vm_price =  1100

    on_premise_application_price = 3000

    aws_EBS_hdd_price = 0.054 # it is EBS storage price per month per gb from the above lookup "aws_EBS_price_list"

    datacenter_region = "ap-southeast-2" # value for teh respective region in the above dictinary "supported_regions"


    #standard HDD rates

    azure_manged_disk_hdd_price ={
                                    "32" : 1.536,
                                    "64" : 3.008,
                                    "128" : 5.888,
                                    "256" : 11.328,
                                    "512" : 21.760,
                                    "1024" : 40.960,
                                    "2048" : 81.920,
                                    "4096" : 163.840,
                                    "8192" : 327.680,
                                    "16384" : 655.360,
                                    "32767" : 1310.720
                                } # this is price for the respective gb per month and it is for us region. the whole US region cost remains same

    # azure_manged_disk_hdd_price = {
    #                                 "32": 1.69,
    #                                 "64": 3.31,
    #                                 "128": 6.48,
    #                                 "256": 12.46,
    #                                 "512": 23.94,
    #                                 "1024": 45.06,
    #                                 "2048": 85.60,
    #                                 "4096": 157.68,
    #                                 "8192": 288.32,
    #                                 "16384": 576.65,
    #                                 "32767": 1153.29
    #                             }  # this is price for the respective gb per month for the  UK south alone which is LONDON Region


    # common AWS and Azure vcpu_memory_mapping combinations wich contain prices in our DB dump.
    cloud_vcpu_memory_mapping = {
                                "2": [4, 8, 16],
                                "4": [16, 32],
                                "8": [32, 64],
                                "16": [32, 64, 128],
                                "32": [128, 256],
                                "64": [256, 512]
                            }

    #current_existing_DB_Combinations
    existing_AWS_Dump_combinations = {
                                    "2": [1, 2, 4, 8,16],
                                    "4": [16,32],
                                    "8": [32,64],
                                    "16": [32,64,128],
                                    "32": [128,256],
                                    "64": [256,512]
                                }

    existing_Azure_Dump_combinations = {
                                    "2": [4, 8,16],
                                    "4": [16, 32],
                                    "8": [32, 64],
                                    "16": [32, 64, 128],
                                    "32": [128, 256],
                                    "64": [256, 512]
                                }

    # assessment values
    os_overview_revision = ["LTS", "Decommission", "Upgrade Immediately", "Upgrade Recommended"]
    os_overview_cpu_usage = ["Low", "Moderate", "High"]
    os_overview_memory_usage = ["Low", "Moderate", "High"]
    os_overview_storage_usage = ["Low", "Moderate", "High"]
    os_overview_activity = ["Idle", "Low", "Moderate", "High"]
    os_overview_memory_type = ["Regular", "Micro", "Macro"]

    os_cloud_compatible = ["No", "Yes"]
    os_cloud_move_priority = ["Migrate later", "Migrate immediately", "Migrate soon"]
    os_cloud_migration_strategy = ["Rehost", "Rebuild", "Rehost with optional upgrade"]
    os_cloud_criticality = ["Low", "Moderate", "High"]

    service_overview_criticality = ["Low", "Moderate", "High"]
    service_overview_eosl = ["Active", "Expired"]
    service_overview_move_priority = ["Migrate later", "Migrate immediately", "Migrate soon"]
    service_overview_migration_strategy = ["Rehost", "Refactor"]

    #application details
    application_scoring = {"High": 10, "Medium": 5, "Low": 0}

    # filters for port table while fetching connections in dependencies
    port_number_filter = [3389, 22, 5985, 5986, 443]
    port_service_filter = ['explorer', 'svchost', 'MpCmdRun', 'git-remote-ht', 'Idle', 'MsMpEng', 'System']


    # /matilda_discovery/services
    # INITIATEHOSTDISCOVERY = 'initiatehostdiscovery'
    # ## WINDOWS CMDS ##
    # http = "https://"
    # ws = "/wsman"
    # port = "5986"
    #
    # # Success and Failure Response
    # submit_req = {'response': 'Request submitted', 'statusCode': '200'}
    # success_resp = {'response': 'Authentication is successful', 'statusCode': '200'}
    # failure_resp = {'response': "Invalid IP or username or password", 'statusCode': '400'}
    #
    # # Configfile path
    # # For Windows
    # # configpath = '\\resources\\cmd_resource.ini'
    # # For Linux

    #
    # # Logging Config
    # # For Windows
    # # loggingconfig = '\\resources\\log_config.py'
    # # For Linux
    # loggingconfig = '/resources/log_config.py'
    #
    #

    #

    #
