import multiprocessing
from flask import Flask, Blueprint
from flask_cors import CORS
from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.controller.endpoints.dashboard import dashboard_ns
from matilda_discovery.controller.endpoints.dependency import dependency_ns
from matilda_discovery.controller.endpoints.task import task_ns
from matilda_discovery.controller.endpoints.host import host_ns
from matilda_discovery.controller.endpoints.reports import report_ns
from matilda_discovery.controller.endpoints.assets import assets_ns
from matilda_discovery.controller.endpoints.service_port import service_port_ns
from matilda_discovery.controller.endpoints.applications import applications_ns
from matilda_discovery.controller.endpoints.lookup import lookup_ns
from matilda_discovery.controller.endpoints.utiliazation import utilization_ns
from matilda_discovery.controller.endpoints.validation import validation_ns
from matilda_discovery.controller.endpoints.vcenter import vcenter_ns
from matilda_discovery.controller.endpoints.eol_lookup import eol_ns
from matilda_discovery.restplus import api
from matilda_discovery.scheduler import base
from matilda_discovery.services.scheduler import Scheduler
from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()


app = Flask(__name__)
CORS(app)

def configure_app(flask_app):
    """
    configures server name (ip and port number)
    """
    logger.debug("Configuring app")
    flask_app.config['SERVER_NAME'] = CreateInfra.FLASK_SERVER_NAME.value
    logger.debug("Configuration successful")


def initialize_app(flask_app):
    """
    initializes all the namespaces created for endpoints and makes a blueprint for this app which can be reused later
    """
    # work on this later to examine how this works
    # configure_app(flask_app)
    logger.debug("Initializing app")
    blueprint = Blueprint('endpoints', __name__, url_prefix='/endpoints')
    api.add_namespace(dashboard_ns)
    api.add_namespace(task_ns)
    api.add_namespace(host_ns)
    api.add_namespace(report_ns)
    api.add_namespace(service_port_ns)
    api.add_namespace(dependency_ns)
    api.add_namespace(assets_ns)
    api.add_namespace(applications_ns)
    api.add_namespace(utilization_ns)
    api.add_namespace(lookup_ns)
    api.add_namespace(vcenter_ns)
    api.add_namespace(eol_ns)
    api.add_namespace(validation_ns)

    flask_app.register_blueprint(blueprint)
    api.init_app(app)
    logger.debug("Initialization successful")


def main():
    """
    app fetches network information from range of ip's provided.
    """
    initialize_app(app)
    logger.debug("Starting app")
    base.start()
    sc = Scheduler()
    sc.start_scheduler_jobs()
    app.run(host='0.0.0.0', port=5010, debug=CreateInfra.FLASK_DEBUG.value, use_reloader=False)  # leave host as '0.0.0.0' Change the required ip in app_app_configurations.py file.
    logger.debug("Closing app")


def my_scheduler():
    logger.debug("Starting scheduler")
    base.start()
    sc = Scheduler()
    sc.start_scheduler_jobs()
    app.run(host='0.0.0.0', debug=CreateInfra.FLASK_DEBUG.value, use_reloader=False)  # leave host as '0.0.0.0' Change the required ip in app_app_configurations.py file.
    logger.debug("Closing scheduler")


if __name__ == "__main__":
    main()
    # p1 = multiprocessing.Process(target=main)
    # p1.start()
    #
    # p2 = multiprocessing.Process(target=my_scheduler)
    # p2.start()

