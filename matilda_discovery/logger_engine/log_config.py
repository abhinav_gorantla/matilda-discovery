import datetime
from matilda_discovery.constant.matilda_enum import CreateInfra


def get_log_config(given_id='root'):

    return \
        {
            'version': 1,
            'handlers':
                {
                    'console': {
                        'class': 'logging.StreamHandler',
                        'level': 'DEBUG',
                        'formatter': 'format1',
                        'stream': 'ext://sys.stdout',
                    },
                    'handler1': {
                        'class': 'logging.handlers.RotatingFileHandler',
                        'level': 'DEBUG',
                        'formatter': 'format1',
                        'filename': CreateInfra.CURRENT_LOG_PATH.value + str(given_id) + "____" + datetime.datetime.now().strftime("%Y_%m_%d") + '.log',
                        'mode': 'a',
                        'maxBytes': 1024 * 1024 * 50,
                        'backupCount': 1000,
                    },
                },
            'formatters':
                {
                    'format1': {
                        'format': '%(message)s',
                    },
                },
            'loggers':
                {
                    'ip_level': {
                        'level': 'DEBUG',
                        'handlers': ['console', 'handler1']
                    },
                }
        }
