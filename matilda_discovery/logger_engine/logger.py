import datetime
import inspect
import logging
import sys
import time
from logging.config import dictConfig

from cryptography.fernet import Fernet

from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.logger_engine.log_config import get_log_config


class Logger:

    def get_datetime(self, req_format=CreateInfra.default_datetime_format.value):
        """
        Returns current datetime in default format. If req_format is provided, corresponding datetime format will be used.
        :param req_format: constant from CreateInfra defining datetime format to be used
        :return: str, current datetime
        """
        return str(datetime.datetime.now().strftime(req_format))


    def simple_encrypt(self, message):
        """
        Accepts a string message and returns an encrypted message using pre-defined key.
        :param message: String. Any message to be encrypted.
        :return: encrypted message
        """
        try:
            key = CreateInfra.LOG_ENCRYPTION_KEY.value
            cipher_suite = Fernet(key)
            encoded_text = cipher_suite.encrypt(str.encode(message))

            return encoded_text.decode()
            # decoded_text = cipher_suite.decrypt(encoded_text) # if we need to decrypt in future
            # return decoded_text.decode()

        except Exception as e:
            return "Message could not be encrypted"


    def parse_stack_details(self, stack_data):
        """
        parse and returns details from inspect.stack()
        :param stack_data: inspect.stack()
        :return: path, function_name, line_number extracted for previous call of input stack data
        """
        path, function_name, line_number = "path not found", "function name not found", "line number not found"

        try:

            if stack_data:
                if stack_data[1][1]:
                    path = "." + str(stack_data[1][1].split(CreateInfra.CURRENT_PROJECT_NAME.value, 1)[1])
                if stack_data[1][3]:
                    function_name = str(stack_data[1][3])
                if stack_data[0][3] == "error":
                    line_number = "line " + str(sys.exc_info()[2].tb_lineno)
                elif stack_data[1][2]:
                    line_number = "line " + str(stack_data[1][2])

            return path, function_name, line_number

        except Exception as e:
            print(f"logger parse_stack_details error: {e} | error class: {e.__class__}")
            return path, function_name, line_number


    def info(self, message, logger_ip='root', encrypt=False, log_flag=False):
        """
        Inserts log message into a queue and logs it later using default logging. If any current_ip is passed, message will be logged to the corresponding file. If encrypt is enabled, message will be encrypted.
        :param message: message to be logged
        :param logger_ip: optional ip to be logged into. If no left empty, message will be logged into root.log file
        :param encrypt: If set to true, message will be encrypted.
        :param log_flag: If set to true, message will be logged. If set to false, message will be added to the logging queue.
        :return: None
        """
        try:
            if logger_ip is None:
                logger_ip = 'root'

            if log_flag is False:
                path, function_name, line_number = self.parse_stack_details(inspect.stack())

                if not encrypt:
                    CreateInfra.log_queue.value.insert(0, {logger_ip: {"INFO": str(self.get_datetime() + ' - ' + path + " - " + function_name + " - " + line_number + " - " + message)}} )
                else:
                    CreateInfra.log_queue.value.insert(0, {logger_ip: {"INFO": str(self.get_datetime() + ' - ' + path + " - " + function_name + " - " + line_number + " - " + self.simple_encrypt(message))}} )

            else:
                logging._acquireLock()

                dictConfig(get_log_config(logger_ip))
                logger = logging.getLogger('ip_level')
                logger.info(('{:15s} {} {}'.format(logger_ip, " |      INFO.      | ", message)))

                logging._releaseLock()

        except Exception as e:
            logging._releaseLock()
            print(f"Error while logging. Input message: {message} | logger_ip: {logger_ip}. Error: {e}")


    def debug(self, message, logger_ip='root', encrypt=False, log_flag=False):
        """
        Inserts log message into a queue and logs it later using default logging. If any current_ip is passed, message will be logged to the corresponding file. If encrypt is enabled, message will be encrypted.
        :param message: message to be logged
        :param logger_ip: optional ip to be logged into. If no left empty, message will be logged into root.log file
        :param encrypt: If set to true, message will be encrypted.
        :param log_flag: If set to true, message will be logged. If set to false, message will be added to the logging queue.
        :return: None
        """
        try:
            if logger_ip is None:
                logger_ip = 'root'

            if log_flag is False:
                path, function_name, line_number = self.parse_stack_details(inspect.stack())

                if not encrypt:
                    CreateInfra.log_queue.value.insert(0, {logger_ip: {"DEBUG": str(self.get_datetime() + ' - ' + path + " - " + function_name + " - " + line_number + " - " + message)}} )
                else:
                    CreateInfra.log_queue.value.insert(0, {logger_ip: {"DEBUG": str(self.get_datetime() + ' - ' + path + " - " + function_name + " - " + line_number + " - " + self.simple_encrypt(message))}} )

            else:
                logging._acquireLock()

                dictConfig(get_log_config(logger_ip))
                logger = logging.getLogger('ip_level')
                logger.debug(('{:15s} {} {}'.format(logger_ip, " |      DEBUG      | ", message)))

                logging._releaseLock()

        except Exception as e:
            logging._releaseLock()
            print(f"Error while logging. Input message: {message} | logger_ip: {logger_ip}. Error: {e}")


    def error(self, message, logger_ip='root', encrypt=False, log_flag=False):
        """
        Inserts log message into a queue and logs it later using default logging. If any current_ip is passed, message will be logged to the corresponding file. If encrypt is enabled, message will be encrypted.
        :param message: message to be logged
        :param logger_ip: optional ip to be logged into. If no left empty, message will be logged into root.log file
        :param encrypt: If set to true, message will be encrypted.
        :param log_flag: If set to true, message will be logged. If set to false, message will be added to the logging queue.
        :return: None
        """
        try:
            if logger_ip is None:
                logger_ip = 'root'

            if log_flag is False:
                path, function_name, line_number = self.parse_stack_details(inspect.stack())

                if not encrypt:
                    CreateInfra.log_queue.value.insert(0, {logger_ip: {"ERROR": str(self.get_datetime() + ' - ' + path + " - " + function_name + " - " + line_number + " - " + str(sys.exc_info()[0]) + " - " + message)}} )
                else:
                    CreateInfra.log_queue.value.insert(0, {logger_ip: {"ERROR": str(self.get_datetime() + ' - ' + path + " - " + function_name + " - " + line_number + " - " + str(sys.exc_info()[0]) + " - " + self.simple_encrypt(message))}} )

            else:
                logging._acquireLock()

                dictConfig(get_log_config(logger_ip))
                logger = logging.getLogger('ip_level')
                logger.error(('{:15s} {} {}'.format(logger_ip, " |  --! ERROR !--  | ", message)))

                logging._releaseLock()

        except Exception as e:
            logging._releaseLock()
            print(f"Error while logging. Input message: {message} | logger_ip: {logger_ip}. Error: {e}")


    def warning(self, message, logger_ip='root', encrypt=False, log_flag=False):
        """
        Inserts log message into a queue and logs it later using default logging. If any current_ip is passed, message will be logged to the corresponding file. If encrypt is enabled, message will be encrypted.
        :param message: message to be logged
        :param logger_ip: optional ip to be logged into. If no left empty, message will be logged into root.log file
        :param encrypt: If set to true, message will be encrypted.
        :param log_flag: If set to true, message will be logged. If set to false, message will be added to the logging queue.
        :return: None
        """
        try:
            if logger_ip is None:
                logger_ip = 'root'

            if log_flag is False:
                path, function_name, line_number = self.parse_stack_details(inspect.stack())

                if not encrypt:
                    CreateInfra.log_queue.value.insert(0, {logger_ip: {"WARNING": str(self.get_datetime() + ' - ' + path + " - " + function_name + " - " + line_number + " - " + message)}} )
                else:
                    CreateInfra.log_queue.value.insert(0, {logger_ip: {"WARNING": str(self.get_datetime() + ' - ' + path + " - " + function_name + " - " + line_number + " - " + self.simple_encrypt(message))}} )

            else:
                logging._acquireLock()

                dictConfig(get_log_config(logger_ip))
                logger = logging.getLogger('ip_level')
                logger.warning(('{:15s} {} {}'.format(logger_ip, " |  == WARNING ==  | ", message)))

                logging._releaseLock()

        except Exception as e:
            logging._releaseLock()
            print(f"Error while logging. Input message: {message} | logger_ip: {logger_ip}. Error: {e}")


    def critical(self, message, logger_ip='root', encrypt=False, log_flag=False):
        """
        Inserts log message into a queue and logs it later using default logging. If any current_ip is passed, message will be logged to the corresponding file. If encrypt is enabled, message will be encrypted.
        :param message: message to be logged
        :param logger_ip: optional ip to be logged into. If no left empty, message will be logged into root.log file
        :param encrypt: If set to true, message will be encrypted.
        :param log_flag: If set to true, message will be logged. If set to false, message will be added to the logging queue.
        :return: None
        """
        try:
            if logger_ip is None:
                logger_ip = 'root'

            if log_flag is False:
                path, function_name, line_number = self.parse_stack_details(inspect.stack())

                if not encrypt:
                    CreateInfra.log_queue.value.insert(0, {logger_ip: {"CRITICAL": str(self.get_datetime() + ' - ' + path + " - " + function_name + " - " + line_number + " - " + message)}} )
                else:
                    CreateInfra.log_queue.value.insert(0, {logger_ip: {"CRITICAL": str(self.get_datetime() + ' - ' + path + " - " + function_name + " - " + line_number + " - " + self.simple_encrypt(message))}} )

            else:
                logging._acquireLock()

                dictConfig(get_log_config(logger_ip))
                logger = logging.getLogger('ip_level')
                logger.critical(('{:15s} {} {}'.format(logger_ip, " | !! CRITICAL. !! | ", message)))

                logging._releaseLock()

        except Exception as e:
            logging._releaseLock()
            print(f"Error while logging. Input message: {message} | logger_ip: {logger_ip}. Error: {e}")


    logging_functions = \
            {
                'DEBUG': debug,
                'INFO': info,
                'ERROR': error,
                'WARNING': warning,
                'CRITICAL': critical,
           }
    
    def write_logs(self):
        """
        Reads logs in the logging queue and logs them into corresponding files and log level.
        :return: None
        """
        while 1:
            while CreateInfra.log_queue.value:
                log_item = CreateInfra.log_queue.value.pop()
                ip, temp = log_item.popitem()
                level, msg = temp.popitem()
                self.logging_functions[level](self, logger_ip=ip, message=msg, log_flag=True)
                time.sleep(0.001)

            time.sleep(5)
        # self.write_logs()
