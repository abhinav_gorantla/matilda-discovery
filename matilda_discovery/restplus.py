from flask_restplus import Api

import traceback
from sqlalchemy.orm.exc import NoResultFound

from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()


api = Api(version='1.0', title='Matilda Discovery Demo',
          description='matilda discovery refactor demo 1')


@api.errorhandler
def default_error_handler(e):
    logger.error(traceback.print_exc())
    # message = 'An unhandled exception occurred.'
    return {'message': str(e)}, getattr(e, 'code', 400)

    # if not settings.FLASK_DEBUG:
    #     return {'message': message}, 400


@api.errorhandler(NoResultFound)
def database_not_found_error_handler(e):
    logger.warning(traceback.format_exc())
    return {'message': 'A database result was required but none was found.'}, 404