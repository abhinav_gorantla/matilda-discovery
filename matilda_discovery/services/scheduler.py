import datetime

from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db.handler import host as host_handler
from matilda_discovery.db.handler import report as report_handler
from matilda_discovery.db.handler import task as task_handler
from matilda_discovery.db.handler import vcenter as vcenter_handler
from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.scheduler.base import scheduler
from matilda_discovery.services import initiate_discovery as discovery_handler
from matilda_discovery.services import vcenter as initiatevcenter_handler
from matilda_discovery.services.application import Applications
from matilda_discovery.services.host import CoreServiceUpdate
from matilda_discovery.services.initiate_ping_snmp import ping_snmp
from matilda_discovery.services.recommendation.assets import Infrastructure
from matilda_discovery.services.recommendation.recommendation import Recommendation
from matilda_discovery.services.reports.reports_generations import Reports
from matilda_discovery.services.task import TaskCreation as TaskCreationService
from matilda_discovery.services.utilizations.utilization import utilization
from matilda_discovery.services.validation import Validation
from matilda_discovery.utils import util

logger = logger_py_file.Logger()


class Scheduler:
    """
    contains methods that handle task related data retrieval
    """

    def start_scheduler_jobs(self):
        for each_job in scheduler.get_jobs():
            if each_job.name == 'Scheduler.open_request_exe':
                scheduler.remove_job(each_job.id)
            if each_job.name == 'Scheduler.create_recommendation_job':
                scheduler.remove_job(each_job.id)

        scheduler.add_job(self.open_request_exe, 'interval', seconds=15)
        #scheduler.add_job(self.create_recommendation_job, 'interval', hours=24)
        scheduler.add_job(self.trigger_write_logs, None)

    @staticmethod
    def trigger_write_logs():
        logger.write_logs()

    def trigger_job(self, request):
        try:
            if request.get('job_trigger_type') == 'snmp':
                ping_snmp_object = ping_snmp()
                ping_snmp_object.ping_snmp_check(request)
            if request.get('job_trigger_type') == 'discovery':
                discovery_object = discovery_handler.Discovery(req_data=request)
                req = discovery_object.discovery()
                if req:
                    self.create_job(request=req, job_type='utilization')
                    self.create_job(request=dict(host_id = request.get('host_id'),job_trigger_type='otherservice'), job_type='otherservice')
                    self.create_job(request=dict(host_id=request.get('host_id'),current_ip= request.get('current_ip'),job_trigger_type='onprem_actions'), job_type='onprem_actions')
            if request.get('job_trigger_type') == 'utilization':
                utilization_object = utilization(request)
                utilization_object.utilization(logger_ip=request['current_ip'])
            if request.get('job_trigger_type') == 'vcenter':
                inditiate_vcenter_object = initiatevcenter_handler.InitiateVcenter()
                inditiate_vcenter_object.initiate_vcenter(request)
            if request.get('job_trigger_type') == 'report':
                reports_object = Reports(request)
                reports_object.reports_generation()
            if request.get('job_trigger_type') == 'input_request_file_upload':
                task_creation_service_object = TaskCreationService()
                task_creation_service_object.file_input_data(request)
            if request.get('job_trigger_type') == 'import_application':
                applications_object = Applications()
                applications_object.import_application_data(request_data=request)
            if request.get('job_trigger_type') == 'import_application_host_mapping':
                applications_object = Applications()
                applications_object.import_application_host_mapping_data(request)
            if request.get('job_trigger_type') == 'applications_ip_information_update_job':
                applications_object = Applications()
                applications_object.applications_information_update(request)
            if request.get('job_trigger_type') == 'validation_task_job':
                validation_object = Validation(req_data=request)
                validation_requests = validation_object.validation_request()
                for item in validation_requests:
                    item['job_trigger_type'] = 'validation_job'
                    self.create_job(request=item, job_type='validation_job')
            if request.get('job_trigger_type') == 'validation_job':
                validation_object = Validation(req_data=request)
                validation_object.input_validation()
            if request.get('job_trigger_type') == 'validation_request_file_upload':
                validation_object = Validation(req_data=request)
                validation_requests = validation_object.file_input_data()
                for item in validation_requests:
                    item['job_trigger_type'] = 'validation_job'
                    self.create_job(request=item, job_type='validation_job')
            if request.get('job_trigger_type') == 'otherservice':
                core_service = CoreServiceUpdate()
                core_service.other_service_update(host_id=request.get('host_id'))
            if request.get('job_trigger_type') == 'onprem_actions':
                onprem_actions_object = Infrastructure()
                onprem_actions_object.onprem_actions(host_id=request.get('host_id'), logger_ip=request.get('current_ip'))

        except Exception as e:
            logger.debug(f"Exception occurred while triggering {request.get('job_trigger_type')} job. Exception: {e}")

    def create_job(self, request=None,job_type=None):
        logger.debug(f"Creating job for {job_type}")
        try:
            if job_type in ['discovery', 'vcenter']:
                try:
                    if request.get('execution_mode') == 'scheduled':
                        job = scheduler.add_job(self.trigger_job, 'date', run_date=request.get('scheduled_time'), args=[request])
                        logger.debug(logger_ip=request['current_ip'], message=f"Created scheduled job for ip: {request.get('current_ip')}; job id: {job.id}")
                    elif request.get('execution_mode') == 'immediate':
                        job = scheduler.add_job(self.trigger_job, None, [request])
                        logger.debug(f"Created immediate job for ip: {request.get('current_ip')}; job id: {job.id}")
                except Exception as e:
                    handler_object = host_handler.HandleHostDetails()
                    handler_object.update_host_table(host_id=request.get('host_id'), stage=2, status="failed",
                                                     end_date=util.get_datetime())

            if job_type == 'utilization':
                try:
                    if request.get('utilization') == 'Y':
                        if request.get('interval') not in [None, []] and request.get('period') not in [None, []]:
                            logger.debug(f"Creating utilization Jobs for task id: {request.get('task_id')}")
                            request['job_trigger_type'] = 'utilization'
                            starttime = datetime.time(0, 0, 0)
                            if request.get('execution_mode') == 'scheduled': starttime = request.get('scheduled_time')
                            if request.get('execution_mode') == 'immediate': starttime = datetime.datetime.now().replace(second=0, microsecond=0)
                            endtime = starttime + datetime.timedelta(weeks=request.get('period'))
                            job = scheduler.add_job(self.trigger_job, 'interval', minutes=request.get('interval') * 60, start_date=starttime, end_date=endtime,args=[request])
                            utilization_object = utilization()
                            utilization_object.utilization_job_host_mapping_update(host_id=request.get('host_id'), job_id=job.id, job_status='resume', logger_ip=request.get('current_ip'))
                            logger.debug(f"Successfully created utilization jobs for task id: {request.get('task_id')}")
                except Exception as e:
                    logger.debug(f"Exception occurred while creating utilization jobs for task_id: {request.get('task_id')} Exception: {e}")

            if job_type in ['snmp','input_request_file_upload','import_application','import_application_host_mapping','applications_ip_information_update_job','validation_request','validation_request_file_upload','validation_job','otherservice','onprem_actions','report']:
                try:
                    logger.debug(f"Creating job for :{job_type}")
                    job = scheduler.add_job(self.trigger_job, None, [request])
                    logger.debug(f"Creating job for {job_type}; job id: {job.id}")
                except Exception as e:
                    logger.debug(f"Exception occurred while creating Input_file_upload_job with Exception: {e}")

        except Exception as e:
            logger.debug(f"Exception occurred while creating job for {job_type} taskid: {request.get('task_id')} IP:{request.get('current_ip')} Exception: {e}")
        return None

    def trigger_recommendation(self, host_details=None, host_id=None, app_assessment_flag=False):
        """
        creates a job for running migration analysis for the discovered ip's and runs parallely
        """
        try:
            if host_details is None:

                host_handler_object = host_handler.HandleHostDetails()
                if host_id:
                    logger.debug(f"Restarting recommendations for host id: {host_id} ")
                    host_handler_object.reset_recommendation(host_id)
                else:
                    logger.debug(f"Restarting recommendations for all successful recommendations")
                    host_handler_object.reset_recommendation()

            else:
                logger.debug(logger_ip=host_details['ip'], message=f"created job for the recommendation request of the ip : {host_details['ip']}")

                cloud_provider_list = ['AWS', 'Azure','GCP']
                recommendation_service_object = Recommendation()
                for cloud_item in cloud_provider_list:
                    host_details['cloud_provider'] = cloud_item
                    infrastructure_migration_assessment_response = recommendation_service_object.infrastructure_migration_assessment(host_details, logger_ip=host_details['ip'])
                    recommendation_service_object.infrastructure_price_recommendation(host_details, infrastructure_migration_assessment_response, logger_ip=host_details['ip'])

                recommendation_service_object.service_recommendation(host_details['id'], logger_ip=host_details['ip'])

                assesment_object = Infrastructure()
                assesment_object.infrastructure_overview(host_details, logger_ip=host_details['ip'])

        except Exception as e:
            if host_details:
                logger.error(logger_ip=host_details['ip'], message=f"Exception occurred while fetching migration requests from DB. Exception :{e}")
            else:
                logger.error(f"Exception in trigger recommendation. Error: {e}")

    def create_recommendation_job(self, recommendations_req=None, host_id=None):
        """
        adds a job for every recommendation request that is picked up by the open_request_exe  where each request contain the list of host's whose job_entry is 3 and device_type is compute
        :param recommendations_req: list, list of all host records whose discovery is completed successfully
        :return: None
        """

        app_assessment_flag = False

        if recommendations_req:
            for i in range(len(recommendations_req)):
                recommendation_item = recommendations_req[i]
                if i == len(recommendations_req) - 1:
                    app_assessment_flag = True

                scheduler.add_job(self.trigger_recommendation, None, [recommendation_item, None, app_assessment_flag])

        else:
            scheduler.add_job(self.trigger_recommendation, None, [None, host_id])

    def open_request_exe(self):
        """
        collects different open request from the DB based on the job_entry for various pourposes.
        :return: None, triggers different operation for various operations
        """
        try:
            host_handler_object = host_handler.HandleHostDetails()
            snmp_requests_data, discovery_requests_data, recommendations_req = host_handler_object.get_new_discovery_requests()

            if len(snmp_requests_data)!=0 and len(discovery_requests_data) != 0 and len(recommendations_req) != 0:
                logger.debug(logger_ip=CreateInfra.scheduler_log_name.value, message=f" Fetched the new requests. Number of snmp requests: {len(snmp_requests_data)}. Number of discovery requests:{len(discovery_requests_data)}. Number of recommendation requests: {len(recommendations_req)}")

            if not recommendations_req in [None, []]:
                self.create_recommendation_job(recommendations_req)

            if not snmp_requests_data in [None, []]:
                for itemsnmp in snmp_requests_data:
                    snmpreq = \
                        {
                            'task_id': itemsnmp.get('task_id'),
                            'host_id': itemsnmp.get('id'),
                            'current_ip': itemsnmp.get('ip'),
                            'community': itemsnmp.get('community'),
                            'job_trigger_type':'snmp'
                        }
                    self.create_job(request=snmpreq,job_type='snmp')

            if not discovery_requests_data in [None, []]:
                for item_openreq in discovery_requests_data:
                    task_handler_object = task_handler.HandleTaskList()
                    task_details_response = task_handler_object.get_task_details(item_openreq.get('task_id'))
                    task_details_response = task_details_response[0]
                    credential_details_response = item_openreq['credentials_information']
                    credentials = credential_details_response[0]
                    utilization_request_details = host_handler_object.get_utilization_request_details(host_id=item_openreq.get('id'))
                    utilization_request_details = utilization_request_details[0]

                    req = dict \
                            (
                            discovery_type=task_details_response.get('discovery_type'),
                            name=task_details_response.get('name'),
                            task_id=item_openreq.get('task_id'),
                            host_id=item_openreq.get('id'),
                            current_ip=item_openreq.get('ip'),
                            username=credentials.get('username'),
                            password=util.simple_decrypt(credentials.get('password')),
                            execution_mode=task_details_response.get('execution_mode'),
                            scheduled_time=task_details_response.get('scheduled_time'),
                            utilization=utilization_request_details.get('utilization'),
                            interval = utilization_request_details.get('utilization_interval'),
                            period = utilization_request_details.get('utilization_period'),
                            job_trigger_type='discovery'
                        )
                    if item_openreq.get('device_type') == 'Unknown Device': req['servertype'] = 'UknownServer'
                    if item_openreq.get('device_type') == 'compute': req['servertype'] = item_openreq.get('platform')
                    if item_openreq.get('discovery_type') == 'vcenter': req['servertype'] = 'UnknownServer'

                    self.create_job(request=req,job_type='discovery')

            #vcenter
            vcenter_handler_object = vcenter_handler.Vcenter()
            vcenter_requests_data = vcenter_handler_object.get_new_vcenter_requests()

            if len(vcenter_requests_data)!=0:
                logger.debug(logger_ip=CreateInfra.scheduler_log_name.value, message=f" Fetched the new requests. Number of vcenter requests: {len(vcenter_requests_data)}.")

            if not vcenter_requests_data in [None, []]:
                for vcenter_item in vcenter_requests_data:
                    vcenter_request = dict \
                            (
                            name=vcenter_item.get('name'),
                            task_id=vcenter_item.get('id'),
                            url=vcenter_item.get('url'),
                            username=vcenter_item.get('username'),
                            password=vcenter_item.get('password'),
                            execution_mode=vcenter_item.get('execution_mode'),
                            scheduled_time=vcenter_item.get('scheduled_time'),
                            job_trigger_type='vcenter'
                        )
                    self.create_job(request=vcenter_request,job_type='vcenter')


        except Exception as e:
            logger.error(f"Exception occurred while retrieving new requests to process. Exception: {e}")
        return None


    def job_pause_resume_delete(self, job_id=None, status=None):
        try:
            message = None
            if status == 'pause':
                scheduler.pause_job(job_id)
                message = 'Utilization paused'
            if status == 'resume':
                scheduler.resume_job(job_id)
                message = 'Utilization resumed'
            if status == 'delete':
                scheduler.remove_job(job_id)
                message = 'Utilization job deleted'
            return True, dict(message=message)
        except Exception as e:
            logger.error(f"Exception occurred while {status} job. Exception: {e}")
            return False, dict(message="Failed job action")


    def job_status(self, job_id=None):
        jobstate = scheduler.get_job(job_id).__getattribute__('next_run_time')
        if jobstate:
            status = 'resume'
        else:
            status = 'pause'
        return status




