import copy

from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db.connection import get_engine
from matilda_discovery.db.handler.assets import HandleAssets
from matilda_discovery.db.handler.dashboard import DashboardData,TaskSummary
from matilda_discovery.db.queries_helper import execute_dict_of_queries
from matilda_discovery.models import templates
from matilda_discovery.utils import util

from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.utils.util import get_properties

logger = logger_py_file.Logger()



def dashboard_details(account_id, task_id=None):
    """
    Accepts account_id, optional task_id and returns utilization details, stage details and task status and host_distribution details.
    :param account_id: int, mandatory field
                        every user will have a different account_id with which the whole users data can be retrieved.
    :param task_id: int,  task_id an optional parameter
                    every discovery will have a unique task_id, where the data a particular discovery cna be retrieved using task_id
    :return: dictionary of status,start_time,elapsed_time of task_id if task_id mentioned along with utilization,host_distribution,operating_system,Application_Services,Devices
    """
    try:
        logger.debug(f"Fetching dashboard details for the account_id={account_id}")
        response = {}
        dashboard_handler_object = DashboardData()

        if task_id is not None:
            app_status, started_time, elapsed_time = fetch_task_status_details(dashboard_handler_object, task_id)
        else:
            app_status, started_time, elapsed_time=None,None,None

        response["Status"] = app_status
        response["StartedTime"] = started_time
        response["ElapsedTime"] = elapsed_time
        response["Utilization"] = fetch_utilization_details(dashboard_handler_object, account_id, task_id)
        response["Stages"] = fetch_stage_details(dashboard_handler_object, account_id, task_id)
        response["HostDistribution"] = fetch_host_distribution_details(dashboard_handler_object, account_id)
        response["Operating_System"] = fetch_operating_system_details(dashboard_handler_object, account_id, task_id)
        response["Application_Services"] = fetch_application_service_details(dashboard_handler_object, account_id, task_id)
        response["Devices"] = fetch_device_details(dashboard_handler_object, account_id, task_id)

        logger.debug(f"Dashboard service completed")

    except Exception as e:
        logger.error(f" Exception occurred in dashboard service. Error: {e}")
        response = templates.dashboard_response_template

    return response


def fetch_task_status_details(dashboard_handler_object, task_id=None):
    """
    Accepts only task_id and returns task status of a particular task
    :param dashboard_handler_object: object, dashboard handler object used to access dashboard handler methods
    :param task_id: int,  task_id an optional parameter
                every discovery will have a unique task_id, where the data of a particular discovery can be retrieved using task_id
    :return:  tuple containing app_status, started_time, elapsed_time of the task
    """

    try:
        logger.debug(f"Fetching task status details")
        app_status = "NULL"
        started_time = "NULL"
        elapsed_time = "NULL"
        task_status_query_results = dashboard_handler_object.task_status_details(task_id)

        task_status_info = task_status_query_results["Task_Status"][0]

        if task_status_info['Task_Status'] == "in_progress" or task_status_info['Task_Status'] == "submitted":
            app_status = "in_progress"
            completed_time = util.get_datetime()
        elif task_status_info['Task_Status'] == "completed":
            app_status = "completed"
            completed_time = task_status_info['Task_end_date']

        started_time = task_status_info['Task_start_date']
        elapsed_time = util.get_time_difference(started_time, completed_time)
        logger.debug(f"Successfully fetched task status details")

    except Exception as e:
        logger.error(f"Fetching task status details failed due to error : {e}")

    return app_status, started_time, elapsed_time


def fetch_utilization_details(dashboard_handler_object, account_id, task_id=None):
    """
    Accepts account_id, optional task_id and returns utilization details.
    :param dashboard_handler_object: object, dashboard handler object used to access dashboard handler methods
    :param account_id: int, mandatory field
                    every user will have a different account_id with which the whole users data can be retrieved.
    :param task_id: int, optional parameter
                    every discovery will have a unique task_id, where the data of a particular discovery cna be retrieved using task_id
    :return: dictionaries of list of memory, Storage,Cpu details
    """
    logger.debug(f"fetching utilization details for account_id: {account_id}. task_id : {task_id} if provided")

    try:
        memory_details = templates.memory_template
        cpu_details = templates.cpu_template
        storage_details = templates.storage_template
        utilization_details = templates.utilization_template

        utilization_details_query_results = dashboard_handler_object.utilization_details(account_id, task_id)


        # SAMPLE RESULT :
        # {
        #   'Memory_Details': [{'Used_Memory': 23.8, 'Available_Memory': 4.2, 'Total_Memory': 28.0, 'Used_Memory_Percent': 72.9}],
        #   'Storage_Details': [{'Used_Space': 1575.44, 'Available_Space': 2477.94, 'Total_Space': 3616.18, 'Used_Storage_Percent': 5.44}],
        #   'CPU_Details': [{'utilization_percent': 0.0, 'Cpu_Cores': 42.0, 'Cpu_Count': 14.0}]
        # }
        memory_results = utilization_details_query_results["memory_details"][0]
        cpu_results = utilization_details_query_results["cpu_details"][0]
        storage_results = utilization_details_query_results["storage_details"][0]

        memory_details["Used"] = util.none_check_return_zero(util.str_concat(memory_results['used_memory'], " GB"))
        memory_details["Available"] = util.none_check_return_zero(util.str_concat(memory_results['available_memory'], " GB"))
        memory_details['Capacity'] = util.none_check_return_zero(util.str_concat(memory_results['total_memory'], " GB") )
        memory_details['Percent'] = util.none_check_return_zero(memory_results['used_memory_percent'])

        cpu_details["Physical_Processors"] = util.none_check_return_zero(cpu_results['physical_processors'])
        cpu_details['Physical_Cores'] = util.none_check_return_zero(cpu_results['physical_cores'])
        cpu_details['Logical_Processors'] = util.none_check_return_zero(cpu_results['logical_processors'])
        cpu_details["Percent"] = util.none_check_return_zero(cpu_results['cpu_utilization_percent'])

        storage_details["Used"] = util.none_check_return_zero(util.str_concat(storage_results['used_space'], " TB"))
        storage_details["Available"] = util.none_check_return_zero(util.str_concat(storage_results['available_space'], " TB"))
        storage_details["Capacity"] = util.none_check_return_zero(util.str_concat(storage_results['total_space'], " TB"))
        storage_details["Percent"] = util.none_check_return_zero(storage_results['used_storage_percent'])

        utilization_details["Memory"] = memory_details if memory_details is not None else {templates.memory_template}
        utilization_details["Cpu"] = cpu_details if cpu_details is not None else {templates.cpu_template}
        utilization_details["Storage"] = storage_details if storage_details is not None else {templates.storage_template}

        if utilization_details["Memory"]['Percent'] ==0 and utilization_details["Cpu"]['Percent']==0 and utilization_details["Storage"]['Percent']==0:
            return None

        return utilization_details

    except Exception as e:
        logger.error(f"Failed to fetch utilization details. Error: {e}")
        return None


def fetch_stage_details(dashboard_handler_object, account_id, task_id=None):
    """
    Accepts account_id, optional task_id and returns stage details of the all the host_id's under the account_id or task_id details.
    :param dashboard_handler_object: object, dashboard handler object used to access dashboard handler methods
    :param account_id: int, mandatory field
                    every user will have a different account_id with which the whole users data can be retrieved.
    :param task_id: int,  task_id an optional parameter
                every discovery will have a unique task_id, where the data a particular discovery cna be retrieved using task_id
    :return:  list of Dictionaries of ping, device login and discovery counts
    """
    try:
        logger.debug(f"Fetching stage details for account_id {account_id}. task_id: {task_id} if provided")
        ping_details = templates.ping_template
        device_details = templates.device_template
        login_details = templates.login_template
        discovery_details = templates.discovery_template
        stage_details = []

        success_count, total_count, discovery_total_count, discovery_success, login_total_count, device_total_count, ping_total_count = 0,0,0,0,0,0,0
        login_successfull_count,devices_identified,total_device_count,ping_failed_count,login_total=0,0,0,0,0

        stage_details_query_results = dashboard_handler_object.stage_details(account_id, task_id)


        success_stage_count = stage_details_query_results.get("success_stage_count")
        total_stage_count = stage_details_query_results.get("total_stage_count")
        device_identification_count_details = stage_details_query_results.get("device_identification_count")
        login_count_details = stage_details_query_results.get("login_identification_count")
        #ping_count_details = stage_details_query_results.get("ping_count")


        if device_identification_count_details:
            for each in device_identification_count_details:
                if each["device_identification_flag"]==1:
                    devices_identified = each["count"]
                    total_device_count = total_device_count + each["count"]
                else:
                    total_device_count=total_device_count+each["count"]

        if login_count_details:
            for each in login_count_details:
                if each["login_flag"]==1:
                    login_successfull_count = each["count"]
                    login_total_count = login_total_count + each["count"]
                else:
                    login_total_count=login_total_count+each["count"]

        if success_stage_count is not None:
            for each in success_stage_count:
                if each["stage"] == 1:
                    ping_success = each["success_count"]
                elif each["stage"] == 2:
                    device_success = each["success_count"]
                elif each["stage"] == 3:
                    login_success = each["success_count"]
                elif each["stage"] == 4:
                    discovery_success = each["success_count"]
        else:
            pass

        if total_stage_count:
            for each in total_stage_count:
                if each["stage"] == 1:
                    ping_total_count = each["total_count"]
                elif each["stage"] == 2:
                    device_total_count = each["total_count"]
                elif each["stage"] == 3:
                    login_total = each["total_count"]
                elif each["stage"] == 4:
                    discovery_total_count = each["total_count"]

        discovery_details["Total"] = util.none_check_return_zero(discovery_total_count)
        discovery_details["Current"] = util.none_check_return_zero(discovery_success)
        discovery_details["Percent"] = float(util.none_check_return_zero(util.percent(discovery_details["Current"], discovery_details["Total"])))

        login_details["Total"] = util.none_check_return_zero(login_total_count)
        login_details["Current"] = util.none_check_return_zero(login_successfull_count)
        login_details["Percent"] = float(util.none_check_return_zero(util.percent(login_details["Current"], login_details["Total"])))

        device_details["Total"] = util.none_check_return_zero(total_device_count)#device_total_count + login_details["Total"])
        device_details["Current"] = util.none_check_return_zero(devices_identified)
        device_details["Percent"] = float(util.none_check_return_zero(util.percent(device_details["Current"], device_details["Total"])))

        ping_details["Total"] = util.none_check_return_zero(ping_total_count + device_total_count +login_total+discovery_total_count)
        ping_details["Current"] = util.none_check_return_zero(device_details["Total"])
        ping_details["Percent"] = float(util.none_check_return_zero(util.percent(ping_details["Current"], ping_details["Total"])))

        stage_details.append(ping_details)
        stage_details.append(device_details)
        stage_details.append(login_details)
        stage_details.append(discovery_details)

        if stage_details[0]['Percent']==0.0 and stage_details[1]['Percent']==0.0 and stage_details[2]['Percent']==0.0 and stage_details[3]['Percent']==0.0:
            return None
        
        logger.debug(f"Successfully fetched stage details")
        return stage_details

    except Exception as e:
        logger.error(f'Exception occurred while fetching stage details. Error: {e}')
        return None


def fetch_host_distribution_details(dashboard_handler_object, account_id):
    """
    Accepts account_id, optional task_id and returns host_distribution of the all the host_id's under the account_id or task_id details.
    :param dashboard_handler_object: object, dashboard handler object used to access dashboard handler methods
    :param account_id: int, mandatory field
                    every user will have a different account_id with which the whole users data can be retrieved.
   :return:  list of Dictionaries of data center, redino .... #need to change this
    """
    try:
        logger.debug(f"Fetching host distribution details for account_id: {account_id}")
        host_distribution = []

        host_distribution_query_results=dashboard_handler_object.host_distribution_details(account_id)

        region_result = three_tuples_to_dictionary("region","status", "region_count", host_distribution_query_results["group_by_region"])
        data_center_result = three_tuples_to_dictionary("data_center","status", "data_center_count", host_distribution_query_results["group_by_data_center"])
        line_of_business_result = three_tuples_to_dictionary("line_of_business","status", "line_of_business_count", host_distribution_query_results["group_by_line_of_business"])
        project_result = three_tuples_to_dictionary("project","status","project_count", host_distribution_query_results["group_by_project"])
        environment_result = three_tuples_to_dictionary("environment","status","environment_count", host_distribution_query_results["group_by_environment"])

        host_distribution.append({"label": "Region", "bar_stack_data": region_result})
        host_distribution.append({"label": "DataCenter", "bar_stack_data": data_center_result})
        host_distribution.append({"label": "LineofBusiness", "bar_stack_data": line_of_business_result})
        host_distribution.append({"label": "Project", "bar_stack_data": project_result})
        host_distribution.append({"label": "Environment", "bar_stack_data": environment_result})

        logger.debug(f"Successfully fetched host distribution details for account_id: {account_id}")

        return host_distribution if host_distribution is not None else templates.host_distribution_details_template

    except Exception as e:
        logger.error(f"Exception occurred while fetching host distribution details.Error: {e}")
        return templates.host_distribution_details_template



def fetch_operating_system_details(dashboard_handler_object, account_id, task_id=None):
    """
    Accepts account_id, optional task_id and returns operating system of the all the host_id's under the account_id or task_id details.
    :param dashboard_handler_object: object, dashboard handler object used to access dashboard handler methods
    :param account_id: int, mandatory field
                    every user will have a different account_id with which the whole users data can be retrieved.
    :param task_id: int, task_id an optional parameter
                every discovery will have a unique task_id, where the data a particular discovery cna be retrieved using task_id
    :return:  list of Dictionaries of operating systems and services.
    """
    try:
        logger.debug(f"Fetching dashboard operating system details")
        os_details = []
        os_details_query_results = dashboard_handler_object.operating_system_details(account_id, task_id)

        # sample query_results:
        # [
        #     {'operating_system': 'CentOS Linux', 'version': '7', 'value': 1},
        #     {'operating_system': 'CentOS Linux', 'version': '7 (Core)', 'value': 3}
        # ]

        temp_details = {}
        for result in os_details_query_results:
            if result['operating_system'] not in temp_details.keys():
                temp_details[result['operating_system']] = [result['value']]
                temp_details[result['operating_system']].append([{'name': result['operating_system']+' '+result['version'], 'value': result['value']}])
            else:
                temp_details[result['operating_system']][0] = temp_details[result['operating_system']][0] + result['value']
                temp_details[result['operating_system']][1].append({'name': result['operating_system']+' '+result['version'], 'value': result['value']})

        # temp_details sample output:
        # {'CentOS Linux': [4, [{'name': '7', 'value': 1},
        #                       {'name': '7 (Core)', 'value': 3}]],
        #  'Ubuntu': [10, [{'name': '14.04.5 LTS, Trusty Tahr', 'value': 1},
        #                  {'name': '16.04', 'value': 2},
        #                  {'name': '16.04.2 LTS (Xenial Xerus)', 'value': 4},
        #                  {'name': '16.04.5 LTS (Xenial Xerus)', 'value': 1}]]
        #  }

        # expected output format: [ {name:os_name, value:os_count, data:[{name:os_name+version_name, value:version_count},..]},.. ]
        for temp_key, temp_value in temp_details.items():
            os_details.append({"name": temp_key, "value": temp_value[0], "data": temp_value[1]})

        logger.debug("Successfully fetched operating system details")
        os_details = os_details if os_details is not None else templates.os_details_template
        return os_details
    except Exception as e:
        logger.error(f"Failed to fetch operating system details. Error: {e}")
        return templates.os_details_template

def fetch_application_service_details(dashboard_handler_object, account_id, task_id=None):
    """
    Accepts account_id, optional task_id and returns application services details of the all the host_id's under the account_id or task_id details.
    :param dashboard_handler_object: object, dashboard handler object used to access dashboard handler methods
    :param account_id: int, mandatory field
                    every user will have a different account_id with which the whole users data can be retrieved.
    :param task_id: int, task_id an optional parameter
                every discovery will have a unique task_id, where the data a particular discovery cna be retrieved using task_id
    :return:  list of Dictionaries of operating systems and services.
    """
    logger.debug(f"Fetching application service details")

    application_service_details = []
    try:

        application_service_query_results = dashboard_handler_object.application_service_details(account_id, task_id)

        # sample query_results:
        # [
        #     {'Service_Type': 'Application Server', 'Service_name': 'jboss', 'service_count': 310},
        #     {'Service_Type': 'Web Server', 'Service_name': 'nginx', 'service_count': 1405}
        # ]

        temp_details = {}
        for result in application_service_query_results:
            if result['service_type'] not in temp_details.keys():
                temp_details[result['service_type']] = [result['service_count']]
                temp_details[result['service_type']].append([{'name': result['service_name'], 'value': result['service_count']}])
            else:
                temp_details[result['service_type']][0] = temp_details[result['service_type']][0] + result['service_count']
                temp_details[result['service_type']][1].append({'name': result['service_name'], 'value': result['service_count']})

        # temp_details sample output:
        # {
        #     'Application Server': [1837, [{'name': 'jboss', 'value': 310},
        #                                   {'name': 'weblogic', 'value': 577}]
        #                                  ],
        #     'Application/Web Server': [25, [{'name': 'iis', 'value': 25}] ],
        #     'Database': [576, [
        #                           {'name': 'mysql', 'value': 558},
        #                           {'name': 'sqlserver', 'value': 12}
        #                       ]],
        #     'Web Server': [1778, [
        #                               {'name': 'apache', 'value': 373},
        #                               {'name': 'nginx', 'value': 1405}]
        #                           ]
        # }

        # expected output format: [ {name:service_name, value:service_count, data:[{name:service_type, value:service_type count},..]},.. ]
        for temp_key, temp_value in temp_details.items():
            application_service_details.append({"name": temp_key, "value": temp_value[0], "data": temp_value[1]})

        logger.debug(f"Successfully fetched service details for account_id: {account_id}.task_id: {task_id}")

        return  application_service_details if application_service_details is not None else templates.application_service_details_template

    except Exception as e:
        logger.error(f"Fetching application service details has failed. Error: {e}")
        return templates.application_service_details_template


def fetch_device_details(dashboard_handler_object, account_id, task_id=None):
    """
    Accepts account_id, optional task_id and returns device details of the all the host_id's under the account_id or task_id details.
    :param dashboard_handler_object: object, dashboard handler object used to access dashboard handler methods
    :param account_id: int, mandatory field
                    every user will have a different account_id with which the whole users data can be retrieved.
    :param task_id: int, task_id an optional parameter
                every discovery will have a unique task_id, where the data a particular discovery cna be retrieved using task_id
    :return:  list of Dictionaries of device type details
    """
    logger.debug(f"Fetching device details")

    device_details = []
    temp_details = {}
    try:

        device_details_query_results = dashboard_handler_object.device_details(account_id, task_id)

        # sample query_results:
        # [
        #     {'device_type': 'Compute', 'device_count': 326},
        #     {'device_type': 'Unknown Device', 'device_count': 127}
        # ]

        for result in device_details_query_results:
            if result['device_type'] not in temp_details.keys():
                temp_details[result['device_type']] = [result['device_count']]
                temp_details[result['device_type']].append([{'name': result['device_type'], 'value': result['device_count']}])
            else:
                temp_details[result['device_type']][0] = temp_details[result['device_type']][0] + result['device_count']
                temp_details[result['device_type']][1].append({'name': result['device_type'], 'value': result['device_count']})

        # temp_details sample output:
        # {
        #     'Compute': [326, [{'name': 'Compute', 'value': 326}]],
        #     'NAS': [2, [{'name': 'NAS', 'value': 2}]],
        #     'Unknown Device': [127, [{'name': 'Unknown Device', 'value': 127}]]
        # }

        # expected output format: [ {name:device_name, value:device_count, data:[{name:device_name, value:device_count},..]},.. ]
        for temp_key, temp_value in temp_details.items():
            device_details.append({"name": temp_key, "value": temp_value[0], "data": temp_value[1]})

        logger.debug("Successfully fetched device details")
        return device_details if device_details is not None else templates.device_details_template

    except Exception as e:
        logger.error(f"Fetching device details has failed due to error {e}")
        return templates.device_details_template

def task_services_details(account_id,task_id):
    """
    prepares the summary of the services and operating systems and devices details of a particular task
    :return: list of dictionaries. services, operating systems , devices details in list of dictionaries
    """
    try:
        logger.debug(f"Collecting services summary for the task_id: {task_id}")
        result, response = {}, {}
        task_summary_object = TaskSummary()
        task_service_summary =task_summary_object.task_services_summary(account_id, task_id)

        if task_service_summary is not None:
            for each in task_service_summary:
                result[each['service_name']]=each['count']
            response['Services']=result
            logger.debug(f"Successfully collected the services summary of the task_id: {task_id}")
        else:
            response = templates.services_summary
            logger.debug(f"Obtained empty results for the services summary for the task_id : {task_id}")

        return response

    except Exception as e:
        logger.error(f"Exception occurred while collected services and operating system details for the task_id: {task_id}. Error: {e} ")
        return templates.services_summary


def reports_stage_details(account_id, task_id=None):
    """
    Accepts account_id, optional task_id and returns stage details of the all the host_id's under the account_id or task_id details.
    :param dashboard_handler_object: object, dashboard handler object used to access dashboard handler methods
    :param account_id: int, mandatory field
                    every user will have a different account_id with which the whole users data can be retrieved.
    :param task_id: int,  task_id an optional parameter
                every discovery will have a unique task_id, where the data a particular discovery cna be retrieved using task_id
    :return:  list of Dictionaries of ping, device login and discovery counts
    """
    try:
        dashboard_handler_object = DashboardData()
        logger.debug(f"Fetching stage details for account_id {account_id}. task_id: {task_id} if provided")
        ping_details, device_details ,login_details , discovery_details ={},{},{},{}
        stage_details = []

        success_count, total_count, discovery_total_count, discovery_success, login_total_count, device_total_count, ping_total_count = 0, 0, 0, 0, 0, 0, 0
        stage_details_query_results = dashboard_handler_object.stage_details(account_id, task_id)

        if "success_stage_count" in stage_details_query_results:
            success_stage_count = stage_details_query_results["success_stage_count"]
        else:
            success_stage_count = None

        if "total_stage_count" in stage_details_query_results:
            total_stage_count = stage_details_query_results["total_stage_count"]
        else:
            total_stage_count = None

        if success_stage_count is not None:
            for each in success_stage_count:
                if each["stage"] == 1:
                    ping_success = each["success_count"]
                elif each["stage"] == 2:
                    device_success = each["success_count"]
                elif each["stage"] == 3:
                    login_success = each["success_count"]
                elif each["stage"] == 4:
                    discovery_success = each["success_count"]
        else:
            pass

        for each in total_stage_count:
            if each["stage"] == 1:
                ping_total_count = each["total_count"]
            elif each["stage"] == 2:
                device_total_count = each["total_count"]
            elif each["stage"] == 3:
                login_total_count = each["total_count"]
            elif each["stage"] == 4:
                discovery_total_count = each["total_count"]

        discovery_details["Total"] = util.none_check_return_zero(discovery_total_count)
        discovery_details["Discovered"] = util.none_check_return_zero(discovery_success)
        discovery_details["Failed"]=discovery_details["Total"]-discovery_details["Discovered"]
        discovery_details["Success_Percent"] = float(util.none_check_return_zero(util.percent(discovery_details["Discovered"], discovery_details["Total"])))
        discovery_details["Title"] = "Discovery"


        login_details["Total"] = util.none_check_return_zero(login_total_count + discovery_details["Total"])
        login_details["Logged In"] = util.none_check_return_zero(discovery_details["Total"])
        login_details["Failed"] = login_details["Total"] - login_details["Logged In"]
        login_details["Success_Percent"] = float(util.none_check_return_zero(util.percent(login_details["Logged In"], login_details["Total"])))
        login_details["Title"] = "Login"

        device_details["Total"] = util.none_check_return_zero(device_total_count + login_details["Total"])
        device_details["Identified"] = util.none_check_return_zero(login_details["Total"])
        device_details["Failed"] = device_details["Total"] - device_details["Identified"]
        device_details["Success_Percent"] = float(util.none_check_return_zero(util.percent(device_details["Identified"], device_details["Total"])))
        device_details["Title"] = "Device Identification"


        ping_details["Total"] = util.none_check_return_zero(ping_total_count + device_details["Total"])
        ping_details["Pinged"] = util.none_check_return_zero(device_details["Total"])
        ping_details["Failed"] = ping_details["Total"] - ping_details["Pinged"]
        ping_details["Success_Percent"] = float(util.none_check_return_zero(util.percent(ping_details["Pinged"], ping_details["Total"])))
        ping_details["Title"] = "Ping"

        stage_details.append(ping_details)
        stage_details.append(device_details)
        stage_details.append(login_details)
        stage_details.append(discovery_details)

        if stage_details[0]['Success_Percent'] == 0.0 and stage_details[1]['Success_Percent'] == 0.0 and stage_details[2]['Success_Percent'] == 0.0 and stage_details[3]['Success_Percent'] == 0.0:
            return None

        logger.debug(f"Successfully fetched stage details")
        return stage_details

    except Exception as e:
        logger.error(f'Exception occurred while fetching stage details. Error: {e}')
        return None


def three_tuples_to_dictionary(param1, param2,param3, query_result):
    """
    Converts the tuple data coming from the DB into a reasonable dictionary format.
    param1,param2,param3 :- headers of the columns of the resultant
    return :- List of dictionaries of the host_distribution data.
    """

    # takes different types of host_distribution data stored in DB based on status of discovery as input and creates into a structured json formater
    try:
        logger.debug(f"Preparing Host Distribution data in a structure json format")
        unique_list,final_list=[],[]
        final_dict ={}
        for each in query_result:
            temp_value = [{'key': 'Failed', 'value': None}, {'key': 'Success', 'value': None}]
            if each[param1] not in unique_list:
                unique_list.append(each[param1])
                sub_dict={}
                sub_dict['category'] = each[param1]
                if each[param2] == "failed":
                    temp_value[0]["value"]=each[param3]
                else:
                    temp_value[1]["value"] = each[param3]

                sub_dict['values']=temp_value
                final_dict[each[param1]]=sub_dict
            else:
                if each[param2] == "failed":
                    final_dict[each[param1]]['values'][0]["value"]=each[param3]
                else:
                    final_dict[each[param1]]['values'][1]["value"] = each[param3]
                #final_dict[each[param1]]['values'].append({'key':each[param2],'value':each[param3]})
        final_list=list(final_dict.values())
        logger.debug(f"Successfully prepared structured host_distribution data in a structured format")
        return final_list
    except Exception as e:
        logger.error(f"Exception occurred while conversion of host_distribution tuples to list data. Exception {e}")
        return None


class Dashboard:


    def __init__(self,account_id=None,task_id=None):

        self.engine = get_engine()
        dashboard_queries = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Account_Dashboard")

        self.dashboard_query_results = execute_dict_of_queries(dashboard_queries, self.engine, account_id=account_id,task_id=task_id, info="Account Level Dashboard Details")

    def new_dashboard_details(self,account_id=None, task_id=None):
        """
        Accepts account_id, optional task_id and returns utilization details, stage details and task status and host_distribution details.
        :param account_id: int, mandatory field
                            every user will have a different account_id with which the whole users data can be retrieved.
        :return: dictionary of status,start_time,elapsed_time of task_id if task_id mentioned along with utilization,host_distribution,operating_system,Application_Services,Devices
        """
        try:
            logger.debug(f"Fetching dashboard details for the account_id={account_id}")
            dashboard_handler_object = DashboardData()

            response = templates.account_dashboard

            response["kpi_activities"] = self.kpi_activities(account_id=account_id,task_id=task_id)

            response["discovery_stages"] = self.new_stage_details(account_id=account_id,task_id=task_id)

            response["host_distribution"] = self.new_host_distribution_details(account_id=account_id,task_id=task_id)

            response['donut_details']["os_chart"] = self.new_operating_system_details(account_id=account_id,task_id=task_id)

            #response['donut_details']["service_chart"] = self.new_service_details(account_id)

            response["utilization"] = self.new_utilization_details(account_id=account_id,task_id=task_id)

            response["latest_completed_discoveries"]["discoveries"] = self.new_completed_discoveries(account_id=account_id,task_id=task_id)

            response["device_discovered_bar_data"] = self.new_device_discovered_details(account_id=account_id,task_id=task_id)

            if task_id:
                response["pie_progress_data"] = self.new_all_host_status_count(account_id=account_id,task_id=task_id,status_details = response["kpi_activities"]["discoveries_status"])

                response.update(self.task_utilization_details(account_id=account_id,task_id=task_id))

                response.pop("utilization")




            # response["Utilization"] = fetch_utilization_details(dashboard_handler_object, account_id )
            # response["Stages"] = fetch_stage_details(dashboard_handler_object, account_id )
            # response["HostDistribution"] = fetch_host_distribution_details(dashboard_handler_object, account_id)
            # response["Operating_System"] = fetch_operating_system_details(dashboard_handler_object, account_id)
            # response["Application_Services"] = fetch_application_service_details(dashboard_handler_object, account_id)
            # response["Devices"] = fetch_device_details(dashboard_handler_object, account_id)

            logger.debug(f"Fetching dashboard details successfully completed")
            return response
        except Exception as e:
            logger.error(f" Exception occurred in dashboard service. Error: {e}")
            response = templates.account_dashboard

        return response


    def kpi_activities(self,account_id=None,task_id=None):
        """
        Returns the status details of all the hosts, and OS count, Device count, services count under the account
        """
        try:
            logger.debug(f"Fetching status details for all the hosts under the account_id {account_id}")
            total_count=0
            discoveries_categories_list=[]
            kpi_activities_response={}

            status_details = self.dashboard_query_results["status_details"]
            #status_details.append({"title":"In Progress","count":next((x["count"] for x in status_details if x["title"]=="In_progress"),0)})

            for item in status_details:
                item["title"]=util.UI_format_1(item["title"])
                total_count = total_count + item["count"]

            status_details.insert(0,{"title":"Discoveries","count":total_count})

            kpi_activities_response["discoveries_status"]=status_details

            discoveries_categories_list.append({"count":self.dashboard_query_results["device_count"][0]["device_count"],"title":"devices"})

            discoveries_categories_list.append({"count": self.dashboard_query_results["os_count"][0]["os_count"], "title": "os"})

            discoveries_categories_list.append({"count": self.dashboard_query_results["services_count"][0]["services_count"], "title": "services"})

            kpi_activities_response["discoveries_categories"]=discoveries_categories_list
            #status_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Application_Queries", property_name="price_query")
            return kpi_activities_response

        except Exception as e:
            logger.error(f"Exception occurred while calculating the status details at account_level for the account_id :{account_id}. Exception :{e}")
            return None

    def new_stage_details(self,account_id=None, task_id=None):
        """
        fetch the stage details based on ping, snmp,login and discovery details
        """
        try:
            logger.debug(f"Fetches all the stage details for the whole account_id {account_id}")
            ping_details,device_details,login_details,discovery_details=[],[],[],[]
            stage_template = copy.deepcopy(templates.Dashboard_Stage_template)

            dashboard_handler_object = DashboardData()
            stage_details = fetch_stage_details(dashboard_handler_object, account_id)

            ping_details.append({"key":"Total","value":stage_details[0]["Total"]})
            ping_details.append({"key": "Success", "value": stage_details[0]["Current"]})
            ping_details.append({"key": "Failed", "value": stage_details[0]["Total"]-stage_details[0]["Current"]})
            stage_template[0]["values"].extend(ping_details)

            device_details.append({"key": "Total", "value": stage_details[1]["Total"]})
            device_details.append({"key": "Success", "value": stage_details[1]["Current"]})
            device_details.append({"key": "Failed", "value": stage_details[1]["Total"] - stage_details[1]["Current"]})
            stage_template[1]["values"].extend(device_details)

            login_details.append({"key": "Total", "value": stage_details[2]["Total"]})
            login_details.append({"key": "Success", "value": stage_details[2]["Current"]})
            login_details.append({"key": "Failed", "value": stage_details[2]["Total"]-stage_details[2]["Current"]})
            stage_template[2]["values"].extend(login_details)

            discovery_details.append({"key": "Total", "value": stage_details[3]["Total"]})
            discovery_details.append({"key": "Success", "value": stage_details[3]["Current"]})
            discovery_details.append({"key": "Failed", "value": stage_details[3]["Total"]-stage_details[3]["Current"]})
            stage_template[3]["values"].extend(discovery_details)

            return stage_template
        except Exception as e:
            logger.error(f"Exception occurred while preparing stage details data for dashboard. Exception :{e}")
            return None

    def new_host_distribution_details(self,account_id=None,task_id=None):
        """
        fetches all the host-distribution details for the whole account
        """
        try:
            logger.debug(f"Fetching all the host_distribution details for the account_id {account_id}")

            dashboard_handler_object = DashboardData()
            host_distribution_details = fetch_host_distribution_details(dashboard_handler_object, account_id)
            i=1
            for each in host_distribution_details:
                each['id']=i
                i+=1
            return host_distribution_details

        except Exception as e:
            logger.error(f"Exception occurred while fetching host distribution details. Exception {e}")
            return None


    def new_operating_system_details(self,account_id=None,task_id=None):
        """
        fetches all the operating system details for the account_id
        """
        response={}
        try:
            logger.debug(f"Fetching the OS details for the account_id :{account_id}")

            #operating_system_count_query = get_properties(property_file_path=CreateInfra.db_queries_path.value,section="Infrastructure_Assessment_Queries", property_name="operating_system_count_query")
            #operating_system_count_details = execute_dict_of_queries({"operating_system_count_query": operating_system_count_query}, self.engine,cloud_provider="AWS", info="")
            operating_system_count_details = self.dashboard_query_results['os_version_details']
            response['os_details'] = None

            if operating_system_count_details:
                bar, pie, os_details = {}, [], {}
                unique_os_list=[]
                os_chart =[]
                os_chart_dict={}
                for each_os_detail in operating_system_count_details:
                    if each_os_detail['operating_system'] not in unique_os_list:
                        unique_os_list.append(each_os_detail['operating_system'])
                        os_chart_dict[each_os_detail['operating_system']] = {'name':each_os_detail['operating_system'],'value':each_os_detail['count'],'bar_stack_data':[templates.dashboard_os_detail_template]}
                        os_chart_dict[each_os_detail['operating_system']]['bar_stack_data'][0]['category'] = each_os_detail["version"]
                        for each_status in os_chart_dict[each_os_detail['operating_system']]['bar_stack_data'][0]['values']:
                            if each_status['key'] == each_os_detail['eol_status']:
                                each_status['value'] = each_os_detail['count']

                    else:
                        os_chart_dict[each_os_detail['operating_system']]['value'] = os_chart_dict[each_os_detail['operating_system']]['value'] + each_os_detail['count']
                        version_template = copy.deepcopy(templates.dashboard_os_detail_template)
                        version_template['category'] = each_os_detail["version"]
                        for each_status in version_template["values"]:
                            if each_status['key'] == each_os_detail['eol_status']:
                                each_status['value'] = each_os_detail['count']

                        os_chart_dict[each_os_detail['operating_system']]['bar_stack_data'].append(version_template)

                os_version_details = list(os_chart_dict.values())
                os_details = pie

                return os_version_details
        except Exception as e:
            logger.error( f"Error in os_details. Error: {e}")
            os_details = {}
            os_details["pie"] = []
            return os_details


    def new_service_details(self,account_id=None,task_id=None):
        """
        fetches all the underlysing services and respective count for the given account_id and task_id
        """

        try:
            logger.debug(f"Fetching all the services for the given account_id :{account_id} and task_id {task_id}")
            service_details_query_result = self.dashboard_query_results["service_count"]
            pie =[]

            for item in service_details_query_result:
                if not pie:
                    temp = {'name': item["name"], 'value': item['count'], 'bar': {}}
                    temp['bar'][item['version']] = {"Active": 0, "Soon": 0, "Expired": 0}
                    temp['bar'][item['version']][item['eol_status']] += item['count']
                    pie.append(temp)
                else:
                    flag = 0
                    for pie_item in pie:
                        if pie_item.get('name') == item["operating_system"]:
                            pie_item['value'] += item['count']
                            pie_item['bar'][item['version']] = {"Active": 0, "Soon": 0, "Expired": 0}
                            pie_item['bar'][item['version']][item['eol_status']] += item['count']
                            flag = 1
                    if flag == 0:
                        temp = {'name': item["operating_system"], 'value': item['count'], 'bar': {}}
                        temp['bar'][item['version']] = {"Active": 0, "Soon": 0, "Expired": 0}
                        temp['bar'][item['version']][item['eol_status']] += item['count']
                        pie.append(temp)


        except Exception as e:
            logger.error(f"Exception occured while fetching dashboard service details for the account_id={account_id} and task_id {task_id}. Exception {e}")
            return None



    def new_utilization_details(self,account_id=None,task_id=None):
        """
            fetches the utlization details for the whole account_id. CPU,Storage,Memory utilization details
        """
        try:
            logger.debug(f"Fetching utilization details for the account_id {account_id}")
            dashboard_handler_object = DashboardData()

            memory_details = templates.memory_template
            cpu_details = templates.cpu_template
            storage_details = templates.storage_template
            utilization_details = templates.utilization_template

            utilization_details_query_results = dashboard_handler_object.utilization_details(account_id)

            # SAMPLE RESULT :
            # {
            #   'Memory_Details': [{'Used_Memory': 23.8, 'Available_Memory': 4.2, 'Total_Memory': 28.0, 'Used_Memory_Percent': 72.9}],
            #   'Storage_Details': [{'Used_Space': 1575.44, 'Available_Space': 2477.94, 'Total_Space': 3616.18, 'Used_Storage_Percent': 5.44}],
            #   'CPU_Details': [{'utilization_percent': 0.0, 'Cpu_Cores': 42.0, 'Cpu_Count': 14.0}]
            # }
            memory_results = utilization_details_query_results["memory_details"][0]
            cpu_results = utilization_details_query_results["cpu_details"][0]
            storage_results = utilization_details_query_results["storage_details"][0]

            memory_details["Used"] = util.none_check_return_zero(util.str_concat(memory_results['used_memory'], " GB"))
            memory_details["Available"] = util.none_check_return_zero(util.str_concat(memory_results['available_memory'], " GB"))
            memory_details['Capacity'] = util.none_check_return_zero(util.str_concat(memory_results['total_memory'], " GB"))
            memory_details['Percent'] = util.none_check_return_zero(memory_results['used_memory_percent'])

            cpu_details["Physical_Processors"] = util.none_check_return_zero(cpu_results['physical_processors'])
            cpu_details['Physical_Cores'] = util.none_check_return_zero(cpu_results['physical_cores'])
            cpu_details['Logical_Processors'] = util.none_check_return_zero(cpu_results['logical_processors'])
            cpu_details["Percent"] = util.none_check_return_zero(cpu_results['cpu_utilization_percent'])

            storage_details["Used"] = util.none_check_return_zero(util.str_concat(storage_results['used_space'], " TB"))
            storage_details["Available"] = util.none_check_return_zero(util.str_concat(storage_results['available_space'], " TB"))
            storage_details["Capacity"] = util.none_check_return_zero(util.str_concat(storage_results['total_space'], " TB"))
            storage_details["Percent"] = util.none_check_return_zero(storage_results['used_storage_percent'])

            utilization_details["Memory"] = memory_details if memory_details is not None else {templates.memory_template}
            utilization_details["Cpu"] = cpu_details if cpu_details is not None else {templates.cpu_template}
            utilization_details["Storage"] = storage_details if storage_details is not None else {templates.storage_template}

            if utilization_details["Memory"]['Percent'] == 0 and utilization_details["Cpu"]['Percent'] == 0 and utilization_details["Storage"]['Percent'] == 0:
                return None

            return utilization_details

        except Exception as e:
            logger.error(f"Exception occurred while calculating utilization details for the account dashboard. Exception : {e}")
            return None

    def new_completed_discoveries(self,account_id=None,task_id=None):
        """
        fetches the latest successfull 5 discoveries details under the account_id.
        """
        try:
            logger.debug(f"Fetching latest successfully discovered ip's details")
            completed_discovery_response =[]
            successfull_discovered_details = self.dashboard_query_results["latest_discoveries"]
            task_id_list = []
            for each in successfull_discovered_details:
                if each["task_id"] not in task_id_list:
                    if len(completed_discovery_response)==3:
                        break
                    host_details= {"task_id":each["task_id"],"discovery_name":each["task_name"],"status":each["status"],"start_date":each["start_date"],"created_on":each["created_datetime"],
                                "end_date":each["end_date"],"scheduled_on":each["scheduled_time"],"total":each["total"],"discovered":0}
                    if each["host_status"]=="success":
                        host_details["discovered"]=each["total"]
                    task_id_list.append(each["task_id"])
                    completed_discovery_response.append(host_details)

                else:
                    existing_host_details = next((sub for sub in completed_discovery_response if sub['task_id'] == each["task_id"]), None)

                    existing_host_details["total"] = existing_host_details["total"]+each["total"]


            return completed_discovery_response

        except Exception as e:
            logger.error(f"Exception occurred while fetching latest task details for the dashboard. Exception {e}")
            return None

    def new_device_discovered_details(self,account_id=None,task_id=None):
        """
        fetches distinct devices count for the given account_id
        """
        try:
            logger.debug(f"Fetching distinct devices and respective count")
            device_count_response = {}
            successfull_discovered_details = self.dashboard_query_results["distinct_device_count"]
            for each in successfull_discovered_details:
                device_count_response[each["device_type"]]=each["count"]
            return device_count_response
        except Exception as e:
            logger.error(f"Exception occurred while fetching distinct device details are being fetched.Error :{e}")
            return None

    def new_all_host_status_count(self,account_id=None,task_id=None,status_details=None):
        """
        fetches the status details of all the hosts present under the given task_id
        """
        try:
            logger.debug(f"Fetching the status of all the tasks under the task_id: {task_id}")
            status_details = self.dashboard_query_results["hosts_status_details"]

            return status_details
        except Exception as e:
            logger.error(f"Exception occurred while calculating the hosts_status count for the task_id {task_id}. Exception {e}")
            return None