from matilda_discovery.db.handler.host import HandleHostDetails
from matilda_discovery.services.snmp.snmp_check import SnmpService
from matilda_discovery.services.ping.ping_check import PingService
from matilda_discovery.utils import util

from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()



class ping_snmp:

    def __init__(self):
        pass

    def ping_snmp_check(self,req_data): # done
        logger.debug(logger_ip=req_data.get('current_ip'), message=f"Performing ping_snmp check for host id: {req_data.get('host_id')}, host ip: {req_data.get('current_ip')}")
        ip_address = req_data.get('current_ip')
        host_id = req_data.get('host_id')
        task_id=req_data.get('task_id')
        ping_object=PingService()
        if ping_object.ping_check_status(ip_address,host_id,task_id):
            logger.debug(logger_ip=req_data.get('current_ip'), message=f"Device successfully pinged for host_ip {ip_address} with host_id:  {host_id}")
            snmp_object=SnmpService()
            snmp_object.device_type_details(req_data)
        else:
            logger.debug(logger_ip=req_data.get('current_ip'), message=f"Ping device failed for ip:  {ip_address}")
            host_handler_object = HandleHostDetails()
            host_handler_object.update_host_table(host_id=host_id, ip=ip_address, reason="host unreachable", status="failed", stage=1, start_date=util.get_datetime(), logger_ip=ip_address)


