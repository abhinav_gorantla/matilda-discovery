from matilda_discovery.db.handler.eol_lookup import EOL_Date as EOL_Date_Handler
from matilda_discovery.logger_engine import logger as logger_py_file

logger = logger_py_file.Logger()


class EOL_Date:
    """
    contains methods that retrieve data related to operating system version EOL date and service EOL date from database.
    """
    def __init__(self):
        pass

    def fetch_eol(self, request_data, logger_ip=None):
        """
        accepts request data containing type, name and version and returns corresponding eol date using eol handler
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"In EOL Date service. Fetching EOL date")

            if str.lower(request_data['type']) == "operating system":
                query_name = "os_eol_date"
            elif str.lower(request_data['type']) == "service":
                query_name = "service_eol_date"
            else:
                raise Exception(f"Unsupported type given: {request_data['type']}")

            eol_date_handler = EOL_Date_Handler()
            resp = eol_date_handler.fetch_service_os_eol(name=request_data['name'], version=request_data['version'], query_name=query_name)

            logger.debug(logger_ip=logger_ip, message=f"Successfully got EOL date from handler. resp = {resp}")
            return resp

        except Exception as e:
            logger.error(f"Failed to get EOL date from handler. Error: {e}")
            return None

