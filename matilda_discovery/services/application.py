import copy
import json
import os
import pandas as pd
from matilda_discovery.db.handler.recommendation import CreateRecommendation
from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db import queries_helper
from matilda_discovery.db.connection import get_engine
from matilda_discovery.db.handler.application import HandleApplicationDetails
from matilda_discovery.db.handler.host import HandleHostDetails
from matilda_discovery.db.queries_helper import execute_dict_of_queries
from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.models import templates
from matilda_discovery.services.topology import NetworkTopology
from matilda_discovery.utils import util
from matilda_discovery.utils.util import get_properties, savings_caluclation

logger = logger_py_file.Logger()


class Applications:

    def __init__(self):
        self.handle_host_object = HandleHostDetails()
        self.engine = get_engine()
        self.task_id = None
        self.file_name = "Applications" + ".xlsx"
        self.recommendation_object = CreateRecommendation()

    def fetch_application_upstream_downstream_ips(self, application_ip):

        try:
            logger.debug("Fethcing  applications upstraem downstream IP's from ports table")
            res = self.recommendation_object.fetch_application_upstream_downstream_ips(application_ip)
            ips = []
            for item in res.get('query_iplist'):
                ips.append(item.get('connected_ip'))
            return ips
        except Exception as e:
            logger.debug(
                f"Exception occurred while fetching applications upstraem downstream IP's from ports table. Error: {e}")
            return None

    def applications_information_update(self, request):
        try:
            logger.debug("Updating application information for given IP from service discovery")
            ip = request.get("ip")
            applications = request.get("applications")
            service_discovery_id=request.get("service_id")
            ip_list = [ip]
            result = self.fetch_application_upstream_downstream_ips(ip)
            while result:
                ip = result.pop()
                print(ip)
                if ip not in ip_list:
                    ip_list.append(ip)
                    output = self.fetch_application_upstream_downstream_ips(application_ip=ip)
                    result.extend([x for x in output if x not in ip_list])
                    if ip in result:
                        result.remove(ip)

            for applicationitem in applications:
                for iplistitem in ip_list:
                    application_id = self.update_applications(application_info=dict(name=applicationitem))
                    host_id = self.get_ip_hostid_info(ip=iplistitem)
                    if host_id:
                        self.update_application_host_mapping(
                            id_info=dict(application_id=application_id, host_id=host_id, service_discovery_id=service_discovery_id))
        except Exception as e:
            logger.debug(
                f"Exception occurred while updating application information for given IP from service discovery. Error: {e}")
            return None

    def update_application_host_mapping(self, id_info=None):
        try:
            logger.debug("Updating application and host mapping information")
            if id_info:
                self.handle_host_object.update_application_host_mapping(application_host_mapping_info=id_info)
        except Exception as e:
            logger.debug(f"Exception occurred while application and host mapping information. Error: {e}")
            return None

    def update_applications(self, application_info=None):
        try:
            logger.debug("Updating applications in application table")
            if application_info:
                application_id = self.handle_host_object.update_application_information(
                    application_details=application_info)
                return application_id
        except Exception as e:
            logger.debug(f"Exception occurred while Updating applications in application table. Error: {e}")
            return None

    def import_applications(self, application_info=None):
        try:
            logger.debug("Updating applications in application table")
            if application_info:
                application_id = self.handle_host_object.import_application_information(application_details=application_info)
                return application_id
        except Exception as e:
            logger.debug(f"Exception occurred while Updating applications in application table. Error: {e}")
            return None

    def get_ip_hostid_info(self, ip=None):
        try:
            logger.debug("Fetching host id from host table from given IP")
            if ip:
                host_id = self.handle_host_object.get_hostid(ip)
                return host_id
        except Exception as e:
            logger.debug(f"Exception occurred while Fetching host id from host table. Error: {e}")
            return None

    def download_excel(self, account_id):
        """
                              Generates Excel file with host, application tables
                        """
        try:
            logger.debug("Generating and Downloading Template... ")
            application_template = util.get_properties(property_file_path=CreateInfra.db_queries_path.value,
                                                       section="Application_Queries", property_name="Applications")
            query_response = queries_helper.execute_dict_of_queries({"applications": application_template}, self.engine,
                                                                    info="Applications", account_id=account_id)
            directory = os.path.normpath(os.path.join(os.path.dirname(__file__), CreateInfra.temp.value))
            util.create_dir_file(directory)
            file = os.path.join(directory + '/' + self.file_name)
            with pd.ExcelWriter(file, engine='xlsxwriter') as writer:
                for i in query_response.get('applications'):
                    col = [*i]
                    break
                util.json_formatter(query_response)
                resp = pd.read_json(json.dumps(query_response.get('applications')))
                resp.to_excel(writer, sheet_name="Application_Template", index=False, columns=col, freeze_panes=(1, 0))
                workbook = writer.book
                worksheet = writer.sheets['Application_Template']
                header_format= workbook.add_format({'fg_color': '#D7E4BC'})
                for col_num, val in enumerate(resp.columns.values):
                    worksheet.write(0, col_num, val, header_format)
            writer.save()
            writer.close()
            return file
        except Exception as e:
            logger.debug(f"Exception occurred while Generating and Downloading Template. Error: {e}")
            return None

    #Blank application template
    def download_application_template(self):
        """
                              Generates Excel file with host, application tables
                        """
        try:
            logger.debug("Generating and Downloading Application_Template... ")

            directory = os.path.normpath(os.path.join(os.path.dirname(__file__), CreateInfra.temp.value))
            util.create_dir_file(directory)
            file = os.path.join(directory + '/' + 'Applications.xlsx')
            data = []
            df = pd.DataFrame(dict(Application_Name=data,
                                   App_Code=data,
                                   App_Owner=data,
                                   Business_Group=data,
                                   Environment=data,
                                   Business_Critical=data,
                                   Availability=data,
                                   Performance=data,
                                   Cloud_Suitability=data,
                                   Target_Deployment=data,
                                   Migration_Strategy=data,
                                   Integration_Complexity=data,
                                   Security=data,
                                   Compliance=data,
                                   Elasticity=data,
                                   Utilization=data,
                                   Maturity=data,
                                   Migration_Complexity=data,
                                   Migration_Priority=data))

            with pd.ExcelWriter(file, engine='xlsxwriter') as writer:
                df.to_excel(writer, sheet_name='Application_Template', startrow=1, header=False)

                workbook = writer.book
                worksheet = writer.sheets['Application_Template']
                header_format= workbook.add_format({'fg_color': '#D7E4BC'})
                for col_num, val in enumerate(df.columns.values):
                    worksheet.write(0, col_num, val, header_format)
            writer.save()
            writer.close()
            return file
        except Exception as e:
            logger.debug(f"Exception occurred while Generating and Downloading Application_Template. Error: {e}")
            return None

    #Blank application_host mapping template
    def download_application_host_mapping_template(self):
        try:
            logger.debug("Generating and Downloading Applications_Host_Mapping... ")

            directory = os.path.normpath(os.path.join(os.path.dirname(__file__), CreateInfra.temp.value))
            util.create_dir_file(directory)
            file = os.path.join(directory + '/' + 'Applications_Host_Mapping.xlsx')
            data = []
            df = pd.DataFrame(dict(IP_Address=data,
                                   App_Code=data,
                                   Application_Name=data,
                                   Discovered_Name=data,
                                   HostName=data,
                                   OperatingSystem=data,
                                   Vcpu_Count=data,
                                   Instance_Type=data,
                                   Memory=data))
            with pd.ExcelWriter(file, engine='xlsxwriter') as writer:
                df.to_excel(writer, sheet_name='App_Host_Mapping_Template', startrow=1, header=False)

                workbook = writer.book
                worksheet = writer.sheets['App_Host_Mapping_Template']
                header_format = workbook.add_format({'fg_color': '#D7E4BC'})
                for col_num, val in enumerate(df.columns.values):
                    worksheet.write(0, col_num, val, header_format)
            writer.save()
            writer.close()
            return file
        except Exception as e:
            logger.debug(f"Exception occurred while Generating and Downloading Applications_Host_Mapping. Error: {e}")
            return None

    #export app data
    def export_application_data(self, account_id):
        """
        Generates Excel file with host, application tables
        """
        try:
            logger.debug("Generating and Downloading Application data... ")
            application_template = util.get_properties(property_file_path=CreateInfra.db_queries_path.value,
                                                       section="Stand_Alone_Queries", property_name="Export_Application")
            query_response = queries_helper.execute_dict_of_queries({"applications": application_template}, self.engine,
                                                                    info="Export_Application", account_id=account_id)
            directory = os.path.normpath(os.path.join(os.path.dirname(__file__), CreateInfra.temp.value))
            util.create_dir_file(directory)
            file = os.path.join(directory + '/' + self.file_name)
            with pd.ExcelWriter(file, engine='xlsxwriter') as writer:
                for i in query_response.get('applications'):
                    col = [*i]
                    break
                util.json_formatter(query_response)
                resp = pd.read_json(json.dumps(query_response.get('applications')))
                resp.to_excel(writer, sheet_name="Applications", index=False, columns=col, freeze_panes=(1, 0))
                workbook = writer.book
                worksheet = writer.sheets['Applications']
                header_format = workbook.add_format({'fg_color': '#D7E4BC'})
                for col_num, val in enumerate(resp.columns.values):
                    worksheet.write(0, col_num, val, header_format)
            writer.save()
            writer.close()
            return file
        except Exception as e:
            logger.debug(f"Exception occurred while Generating and Downloading Application data. Error: {e}")
            return None

    #export app host mapping data
    def export_application_host_mapping_data(self, account_id):
        """
                                      Generates Excel file with host, application tables
                                """
        try:
            logger.debug("Generating and Downloading Export_Application_Host_Mapping data... ")
            application_template = util.get_properties(property_file_path=CreateInfra.db_queries_path.value,
                                                       section="Stand_Alone_Queries", property_name="Export_Application_Host_Mapping")
            query_response = queries_helper.execute_dict_of_queries({"applications": application_template}, self.engine,
                                                                    info="Export_Application_Host_Mapping", account_id=account_id)
            directory = os.path.normpath(os.path.join(os.path.dirname(__file__), CreateInfra.temp.value))
            util.create_dir_file(directory)
            file = os.path.join(directory + '/' + self.file_name)
            with pd.ExcelWriter(file, engine='xlsxwriter') as writer:
                for i in query_response.get('applications'):
                    col = [*i]
                    break
                util.json_formatter(query_response)
                resp = pd.read_json(json.dumps(query_response.get('applications')))
                resp.to_excel(writer, sheet_name="App_Host_Mapping", index=False, columns=col, freeze_panes=(1, 0))
                workbook = writer.book
                worksheet = writer.sheets['App_Host_Mapping']
                header_format = workbook.add_format({'fg_color': '#D7E4BC'})
                for col_num, val in enumerate(resp.columns.values):
                    worksheet.write(0, col_num, val, header_format)
            writer.save()
            writer.close()
            return file
        except Exception as e:
            logger.debug(f"Exception occurred while Generating and Downloading Export_Application_Host_Mapping data. Error: {e}")
            return None

    # import app data
    def import_application_data(self, request_data=None):
        """
                       accepts file name, updates application table and application host mapping tables
                """
        try:

            app_table_names = dict(Application_Name='name',
                            Discovered_Name='discovered_name',
                            App_Code='app_code',
                            App_Owner='app_owner',
                            Business_Group='business_group',
                            Environment='environment',
                            Business_Critical='business_critical',
                            Availability='availability',
                            Performance='performance',
                            Cloud_Suitability='cloud_suitability',
                            Target_Deployment='target_deployment',
                            Migration_Strategy='migration_strategy',
                            Integration_Complexity='integration_complexity',
                            Security='security',
                            Compliance='compliance',
                            Elasticity='elasticity',
                            Utilization='utilization',
                            Maturity='maturity',
                            Migration_Complexity='migration_complexity',
                            Migration_Priority='migration_priority')

            logger.debug("Updating the applications Information from excel file... ")
            filename = request_data.get('filename')
            path = os.path.normpath(os.path.join(os.path.dirname(__file__), CreateInfra.temp.value + filename))
            print(path)
            template_data = pd.ExcelFile(path).parse()
            for index, row in template_data.iterrows():
                app_info = {}
                val = row.to_dict()
                if str(val.get("Application_Name")) == 'nan':
                    continue

                for i in val.keys():
                    if str(val[i]) != 'nan' and i in app_table_names.keys():
                        app_info[app_table_names[i]] = val[i]
                print(app_info)
                self.import_applications(application_info=app_info)
            # os.remove(path)
            return None
        except Exception as e:
            logger.debug(f"Exception occurred while Updating the applications Information from excel file. Error: {e}")
            return None

    # import app host mapping data
    def import_application_host_mapping_data(self, request_data=None):
        """
                       accepts file name, updates application table and application host mapping tables
                """
        try:
            appnames = dict(Application_Name = 'name', App_Code = 'app_code')
            logger.debug("Updating the applications Information from excel file... ")
            filename = request_data.get('filename')
            path = os.path.normpath(os.path.join(os.path.dirname(__file__), CreateInfra.temp.value + filename))
            print(path)
            template_data = pd.ExcelFile(path).parse()
            for index, row in template_data.iterrows():
                app_info = {}
                val = row.to_dict()
                if str(val.get("Application_Name")) == 'nan':
                    continue

                for i in val.keys():
                    if str(val[i]) != 'nan' and i in appnames.keys():
                        app_info[appnames[i]] = val[i]
                print(app_info)
                application_id = self.update_applications(application_info=app_info)
                host_id = self.get_ip_hostid_info(ip=val.get('IP_Address'))
                if host_id:
                    self.update_application_host_mapping(id_info=dict(application_id=application_id, host_id=host_id))
            # os.remove(path)
            return None
        except Exception as e:
            logger.debug(f"Exception occurred while Updating the applications Information from excel file. Error: {e}")
            return None


class ApplicationList:
    """
    contains methods that retrieve data related to host from database.
    """
    def __init__(self):
        pass


    def get_application_dashboard_summary(self,cloud_provider=None):
        """
        Fetches details of application dashboard.
        :param cloud_provider:string,AWS or Azure
        :return:dict, dashboard data points
        """
        try:
            logger.debug(f"Fetching dashboard details for cloud provider.{cloud_provider}")
            response = templates.application_dashboard # application dashboard template in a particular json format
            application_handler_object = HandleApplicationDetails()
            queries_result = application_handler_object.application_dashboard_details(cloud_provider=cloud_provider)

            #Data is being dumped into the application_dashboard_template and it is prepared from output obtained from the queries result

            response['total'] = queries_result['application_count'][0].get('count') if queries_result['application_count'] not in [[],{},None] else None

            if response['total'] not in [None,0,[],{}]:

                each_application_on_premise_value = CreateInfra.on_premise_application_price.value

                on_premise_value = 24200#int(each_application_on_premise_value) * int(response['total'])

                response['existing'][0]["value"] = on_premise_value

                response['recommended'][0]["value"] = on_premise_value

                if queries_result['actual_cloud_prices'] not in [[], {}, None]:

                    existing_prices=queries_result['actual_cloud_prices'][0]

                    response['existing'][1]["value"] = existing_prices.get('on_demand_price')
                    response['existing'][1]["Savings"] = savings_caluclation(on_premise_value,response['existing'][1]["value"])

                    response['existing'][2]["value"] = existing_prices.get('reserved_1_year_price')
                    response['existing'][2]["Savings"] = savings_caluclation(on_premise_value,response['existing'][2]["value"])

                    response['existing'][3]["value"] = existing_prices.get('reserved_3_year_price')
                    response['existing'][3]["Savings"] = savings_caluclation(on_premise_value,response['existing'][3]["value"])

                if queries_result['recommended_cloud_prices'] not in [[], {}, None]:

                    recommended_prices = queries_result['recommended_cloud_prices'][0]

                    response['recommended'][1]["value"] = recommended_prices.get('recommended_on_demand_price')
                    response['recommended'][1]["Savings"] = savings_caluclation(on_premise_value, response['recommended'][1]["value"])

                    response['recommended'][2]["value"] = recommended_prices.get('recommended_1_year_price')
                    response['recommended'][2]["Savings"] = savings_caluclation(on_premise_value, response['recommended'][2]["value"])

                    response['recommended'][3]["value"] = recommended_prices.get('recommended_3_year_price')
                    response['recommended'][3]["Savings"] = savings_caluclation(on_premise_value, response['recommended'][3]["value"])

                if queries_result['migration_suitability'] not in [[], {}, None]:
                    response["cloud_suitability"] = queries_result['migration_suitability']

                if queries_result['migration_strategy'] not in [[], {}, None]:
                    temp_dict = {}
                    for each in queries_result['migration_strategy']:
                        temp_dict[each['migration_strategy']]=each['count']

                    response['migration_strategy']['chart_data'].update(temp_dict)

                if queries_result['migration_priority'] not in [[], {}, None]:
                    temp_dict = {}
                    for each in queries_result['migration_priority']:
                        temp_dict[each['migration_priority']]=each['count']

                    response['migration_priority']['chart_data'].update(temp_dict)

                if queries_result['environments'] not in [[], {}, None]:
                    temp_dict = {}
                    for each in queries_result['environments']:
                        temp_dict[each['environment']]=each['count']
                    response['business_group']['bar'].update(temp_dict)

                if queries_result['business_criticality'] not in [[], {}, None]:
                    temp_dict = {}
                    for each in queries_result['business_criticality']:
                        temp_dict[each['business_critical']]=each['count']
                    response['business_criticality'].update(temp_dict)

                if queries_result['business_and_environments'] not in [[], {}, None]:
                    business_group_count = {}
                    final_dict = {}

                    for each_result in queries_result['business_and_environments']:
                        if each_result["business_group"] in final_dict.keys():
                            final_dict[each_result["business_group"]].update({each_result["environment"]:each_result["environment_count"]})
                            business_group_count[each_result["business_group"]]+=each_result["business_group_count"]
                        else:
                            sub_dict={each_result["environment"]:each_result["environment_count"]}
                            final_dict.update({each_result["business_group"]:sub_dict})
                            business_group_count[each_result["business_group"]]=each_result["business_group_count"]

                    business_group_list=[]
                    for each in final_dict:
                        business_group_list.append({ "value": business_group_count.get(each), "name": each, "bar": final_dict.get(each)})

                    response["business_group"]["pie"]=business_group_list
            else:
                response= templates.application_ui_null_dashboard

            return response
        except Exception as e:
            logger.error(f"Exception occured while anlysing dashboard values for cloud provider {cloud_provider}.Exception {e}")
            response = templates.application_dashboard
            return response

#NEW UI
#ApplicationDashboard
    def new_get_application_dashboard_summary(self,cloud_provider=None):
        """
        Fetches details of application dashboard.
        :param cloud_provider:string,AWS or Azure
        :return:dict, dashboard data points
        """
        try:
            logger.debug(f"Fetching dashboard details for cloud provider.{cloud_provider}")
            #response = templates.application_dashboard # application dashboard template in a particular json format
            response = {}
            application_handler_object = HandleApplicationDetails()
            queries_result = application_handler_object.application_dashboard_details(cloud_provider=cloud_provider)

            #Mock data

            response['total'] = 12

            existing = \
                [
                    {
                        "name": "Yearly on Premise",
                        "value": 24200,
                        "Savings": ""
                    },
                    {
                        "name": "Yearly on demand",
                        "value": 12196.0,
                        "Savings": 49.6
                    },
                    {
                        "name": "Reserved 1yr",
                        "value": 7195.0,
                        "Savings": 70.27
                    },
                    {
                        "name": "Reserved 3yr",
                        "value": 4700.0,
                        "Savings": 80.58
                    }
                ]
            recommended = \
                [
                    {
                        "name": "Yearly on Premise",
                        "value": 24200,
                        "Savings": ""
                    },
                    {
                        "name": "Yearly on demand",
                        "value": 6731.0,
                        "Savings": 72.19
                    },
                    {
                        "name": "Reserved 1yr",
                        "value": 3995.0,
                        "Savings": 83.49
                    },
                    {
                        "name": "Reserved 3yr",
                        "value": 2645.0,
                        "Savings": 89.07
                    }
                ]
            cloud_suitability = \
                [
                    {
                        "name": "Cloud",
                        "value": 8
                    },
                    {
                        "name": "Onprem",
                        "value": 4
                    }
                ]
            migration_strategy = \
                {
                    "chart_data": {
                        "Rehost": 8,
                        "Rebuild": 4,
                        "Refactor": None
                    },
                    "description": [
                        "All the host under these application need to be lift and shifted",
                        "One or more hosts under these applications need to be upgraded ",
                        "One or more hosts under these applications need to be refactored"
                    ]
                }
            migration_priority = \
                {
                    "chart_data": {
                        "Migrate later": 6,
                        "Migrate soon": 6
                    },
                    "description": [
                        "Applications contains vulnerable hosts",
                        "Application contains hosts which doesnt have enough support for long run",
                        "Applications contain long lasting host with the support"
                    ]
                }
            business_group = \
                {
                    "bar": {
                        "Prod": 12,
                        "QA": None,
                        "Dev": None

                    },
                    "pie": [
                        {
                            "value": 4,
                            "name": "Dev",
                            "bar": {
                                "Prod": 4
                            }
                        },
                        {
                            "value": 2,
                            "name": "Prod",
                            "bar": {
                                "Prod": 2
                            }
                        },
                        {
                            "value": 6,
                            "name": "QA",
                            "bar": {
                                "null": 6
                            }
                        }



                    ]

            }

            donutDetails = \
                {
                    "businessChart":
                        [
                            {
                                "name": "Prod",
                                "value": 5,
                                "bar_stack_data": [{
                                    "category": "Ap1",
                                    "values": [
                                        {
                                            "key": "Active",
                                            "value": 3
                                        },
                                    ]
                                },
                                    {
                                        "category": "App2",
                                        "values": [
                                            {
                                                "key": "Active",
                                                "value": 2
                                            }
                                        ]
                                    },
                                    {
                                        "category": "App3",
                                        "values": [
                                            {
                                                "key": "Active",
                                                "value": 0
                                            },
                                        ]
                                    }]
                            },
                            {
                                "name": "Test",
                                "value": 0,
                                "bar_stack_data": [
                                    {
                                        "category": "Testapp",
                                        "values":
                                            [
                                                {
                                                    "key": "Active",
                                                    "value": 0
                                                },
                                            ]
                                    }

                                ]
                            },
                            {
                                "name": "Release",
                                "value": 0,
                                "bar_stack_data": [
                                    {
                                        "category": "app1",
                                        "values": [
                                            {
                                                "key": "Active",
                                                "value": 0
                                            },
                                        ]
                                    },
                                    {
                                        "category": "app34",
                                        "values": [
                                            {
                                                "key": "Active",
                                                "value": 0
                                            },
                                        ]
                                    },
                                    {
                                        "category": "app34",
                                        "values": [
                                            {
                                                "key": "Active",
                                                "value": 0
                                            },
                                        ]
                                    }

                                ]
                            }

                        ]
                }
            business_criticality = \
                {
                    "High": 4,
                    "Medium": 6,
                    "Low": 2
                }
            response['existing'] = existing
            response['recommended'] = recommended
            response['cloud_suitability'] = cloud_suitability
            response['migration_strategy'] = migration_strategy
            response['migration_priority'] = migration_priority
            response['business_group'] = business_group
            response['donutDetails'] = donutDetails
            response['business_criticality'] = business_criticality


            return response
        except Exception as e:
            logger.error(f"Exception occured while anlysing dashboard values for cloud provider {cloud_provider}.Exception {e}")
            return response


    def get_application_assets(self,cloud_provider=None):
        """
        fetches the list of applications under the account and respective details of those applications.
        """
        try:
            logger.debug(f"fetching list of applications and the underlying details")
            application_handler_object = HandleApplicationDetails()
            response = application_handler_object.fetch_application_detail_list()
            for each_application in response:

                host_handler_object = HandleHostDetails()
                host_id_list, host_ip_list = host_handler_object.get_hosts_in_app(app_id=each_application['id'])
                host_count=len(host_ip_list)

                services_count,db_count=self.get_application_services_details(application_id=each_application['id'])

                each_application['infrastructure_count'] = host_count
                each_application['service_count'] = services_count
                each_application['db_count'] = db_count

                application_handler_object = HandleApplicationDetails()
                migration_analysis=application_handler_object.fetch_application_recommendation_detail(application_id=each_application['id'],cloud_provider=cloud_provider)

                if migration_analysis:
                    del migration_analysis['id']
                    del migration_analysis['application_id']
                else:
                    migration_analysis={
                                        "cloud_provider" : None,
                                        "migration_priority" : None,
                                        "migration_complexity" : None,
                                        "migration_strategy" : None,
                                        "on_demand_price" : None,
                                        "reserved_1_year_price" : None,
                                        "reserved_3_year_price" : None,
                                        "recommended_on_demand_price" : None,
                                        "recommended_1_year_price" : None,
                                        "recommended_3_year_price" : None
                                        }
                each_application.update(migration_analysis)
            return response
        except Exception as e:
            logger.error(f"Exception occurred while fetching the application list at service level. Exception: {e}")
            return None

#NEW UI
#Assets- Application list
    def new_get_application_assets(self,cloud_provider=None):
        """
        fetches the list of applications under the account and respective details of those applications.
        """
        try:
            logger.debug(f"fetching list of applications and the underlying details")
            application_handler_object = HandleApplicationDetails()
            response = application_handler_object.fetch_application_detail_list()
            response1 = []
            for each_application in response:

                host_handler_object = HandleHostDetails()
                host_id_list, host_ip_list = host_handler_object.get_hosts_in_app(app_id=each_application['id'])
                host_count=len(host_ip_list)

                services_count,db_count=self.get_application_services_details(application_id=each_application['id'])

                each_application['infrastructure_count'] = host_count
                each_application['service_count'] = services_count
                each_application['db_count'] = db_count

                application_handler_object = HandleApplicationDetails()
                migration_analysis=application_handler_object.fetch_application_recommendation_detail(application_id=each_application['id'],cloud_provider=cloud_provider)

                if migration_analysis:
                    del migration_analysis['id']
                    del migration_analysis['application_id']
                else:
                    migration_analysis={
                                        "cloud_provider" : None,
                                        "migration_priority" : None,
                                        "migration_complexity" : None,
                                        "migration_strategy" : None,
                                        "on_demand_price" : None,
                                        "reserved_1_year_price" : None,
                                        "reserved_3_year_price" : None,
                                        "recommended_on_demand_price" : None,
                                        "recommended_1_year_price" : None,
                                        "Recommended_3_year_price" : None
                                        }
                each_application.update(migration_analysis)
                #ui_dict = {response}
                new_ui_list = ["ID", "Name", "DiscoveredName", "AppCode", "AppOwner",
                               "BusinessGroup", "Environment", "BusinessCritical", "Availability",
                               "Performance", "CloudSuitability", "TargetDeployment", "MigrationStrategy",
                               "IntegrationComplexity", "Security", "Compliance", "Elasticity", "Utilization",
                               "Maturity", "MigrationComplexity", "MigrationPriority", "InfrastructureCount",
                               "ServiceCount", "DBCount", "CloudProvider", "MigrationSuitability",
                               "OndemandPrice", "Reserved1YearPrice", "Reserved3YearPrice", "RecommendedOnDemandPrice",
                               "Recommended1YearPrice", "Recommended3YearPrice"]
                final_dict = dict(zip(new_ui_list, list(each_application.values())))

                response1.append(final_dict)

            return response1
        except Exception as e:
            logger.error(f"Exception occurred while fetching the application list at service level. Exception: {e}")
            return None

    def get_application_services_details(self,application_id=None):
        """
        fetches all the infrastructures and services present under the given application id
        :param application_id: unique id for every application in the application table
        :return: dict, count of infra and databases for a application
        """
        try:
            logger.debug(f"Fetching the count of hosts and services under the application")
            services_count, db_count = 0, 0
            application_handler_object= HandleApplicationDetails()
            services_details = application_handler_object.fetch_applications_infra_sevices_details(application_id)

            if services_details not in [[],{},None]:
                for each_service in services_details.get("services_types_count"):
                    if each_service['type'] == "Database":
                        db_count = each_service.get('services_count')
                    else:
                        services_count=services_count+each_service.get('services_count')
                logger.debug(f"Successfully fetched the count of services under the application")
                return services_count,db_count
            else:
                return None,None
        except Exception as e:
            logger.debug(f"Exception occurred while preparing application analysis. Exception:{e}")
            return None,None

    def application_list_analysis(self,application_list=None):
        """
        Fetches list of applications and analyse each application for migration
        :param application_list: list, application and its details
        :return: None
        """
        try:
            logger.debug(f"Analysing application migration for the list of applications")
            application_recommendation_list=[]

            for each_application in application_list:
                application_detail_object= ApplicationDetails()

                for cloud_provider in ["AWS", "Azure","GCP"]:
                    each_application_recommendation = {}
                    each_application_recommendation['application_id'] = each_application["id"]
                    each_application_recommendation["cloud_provider"] = cloud_provider

                    each_application_recommendation["migration_strategy"],each_application_recommendation["migration_priority"],each_application_recommendation["migration_suitability"] =\
                        application_detail_object.application_analysis(application_id=each_application['id'],cloud_provider=cloud_provider)

                    application_pricing_details = application_detail_object.application_cloud_pricing_details(application_id=each_application["id"],cloud_provider=cloud_provider)

                    each_application_recommendation.update(application_pricing_details)

                    application_recommendation_list.append(each_application_recommendation)

            application_handler_object = HandleApplicationDetails()
            application_handler_object.update_application_recommendation_data(applicationinfo=application_recommendation_list)

            logger.debug(f"Successfully completed all the application migration analysis")

            return None
        except Exception as e:
            logger.error(f"Exception occurred while analysing migration analysis for all the application. Exception : {e}")
            return None

    def application_recommendation_analysis(self):
        """
        Initaties and application recommendation process
        :return:
        """
        logger.debug(f"Initiating Migration analysis for all applications")

        application_handler_object = HandleApplicationDetails()
        application_handler_object.delete_application_recommendation_details()

        application_list = application_handler_object.fetch_application_detail_list()

        self.application_list_analysis(application_list)

        pass


class ApplicationDetails:
    def __init__(self):
        self.handle_host_object = HandleHostDetails()
        self.engine = get_engine()

    def application_assessment_details(self, app_id, cloud_provider):
        """
        accepts app id and returns the information for different tabs shown in application 2nd page
        """
        try:
            logger.debug(f"Fetching application assessment tabs details for app_id: {app_id}")

            if str.lower(cloud_provider) not in ['aws', 'azure','gcp']:
                logger.error(f"Cloud provider not supported. given cloud_provider: {cloud_provider}")
                return None

            host_handler_object = HandleHostDetails()
            host_id_list, host_ip_list = host_handler_object.get_hosts_in_app(app_id)

            application_assessment = self.application_details(app_id)

            dependency_data = self.dependency_details(host_ip_list)
            infrastructure_data = self.infrastructure_details(host_id_list, cloud_provider)
            service_data = self.service_details(host_id_list)
            database_data = self.database_details(host_id_list)
            actions_data = self.action_details(host_id_list, cloud_provider)
            topology_data = self.topology_details(application_assessment,infrastructure_data,service_data,database_data)
            tco_data =  self.tco_analysis_details(host_id_list,cloud_provider)
            instance_dependency = self.instance_dependency(app_id=app_id)

            application_details = \
                {
                    "topology": topology_data,
                    "dependency": dependency_data,
                    "infrastructure": infrastructure_data,
                    "service": service_data,
                    "database": database_data,
                    "actions": actions_data,
                    "tco_data" : tco_data,
                    "instance_dependency" :instance_dependency
                }



            application_assessment["infrastructure_count"] = None
            if infrastructure_data:
                application_assessment["infrastructure_count"] = len(infrastructure_data)

            application_assessment["service_count"] = None
            if service_data:
                application_assessment["service_count"] = len(service_data)

            application_assessment["database_count"] = None
            if database_data:
                application_assessment["database_count"] = len(database_data)

            application_prices = self.application_price_details(host_id_list, cloud_provider)
            if application_prices:
                application_assessment.update(application_prices)

            response = \
                {
                    "application_assessment": application_assessment,
                    "application_details": application_details
                }

            logger.debug(f"Successfully fetched application assessment tabs details for app_id: {app_id}")
            return response

        except Exception as e:
            logger.error(f"Failed to fetch application assessment tabs details. Error: {e}")
            return None

    def application_price_details(self, host_id_list, cloud_provider):
        """
        accepts app id, cloud provider and returns application details of that app_id tied to the cloud provider
        """
        try:
            logger.debug(f"Fetching application price details for host_id_list: {host_id_list} and cloud provider: {cloud_provider}")

            application_handler_object = HandleApplicationDetails()
            response = application_handler_object.get_application_prices(host_id_list=host_id_list, cloud_provider=cloud_provider)

            logger.debug(f"Successfully fetched application price details for host_id_list: {host_id_list} and cloud provider: {cloud_provider}")
            return response

        except Exception as e:
            logger.error(f"Failed to fetch application price details for host_id_list: {host_id_list} and cloud provider: {cloud_provider}. Error: {e}")
            return None

    def application_details(self, app_id):
        """
        accepts app id and returns application details of that app_id
        """
        try:
            logger.debug(f"Fetching application details for host_id_list: {app_id}")

            application_handler_object = HandleApplicationDetails()
            response = application_handler_object.get_application_details(app_id=app_id)

            logger.debug(f"Successfully fetched application details for app id: {app_id}")
            return response

        except Exception as e:
            logger.error(f"Failed to fetch infrastructure tab details. Error: {e}")
            return None

    def infrastructure_details(self, host_id_list, cloud_provider):
        """
        accepts host_id_list and returns the list of infrastructure with price details for all the hosts specific to a cloud_provider
        """
        try:
            logger.debug(f"Fetching infrastructure details for host_id_list: {host_id_list}")

            host_handler_object = HandleHostDetails()
            response = host_handler_object.get_infrastructure(host_id_list=host_id_list, cloud_provider=cloud_provider)

            logger.debug(f"Successfully fetched infrastructure details for host_id_list: {host_id_list}")
            return response

        except Exception as e:
            logger.error(f"Failed to fetch infrastructure tab details. Error: {e}")
            return None

    def service_details(self, host_id_list):
        """
        accepts host_id_list and returns the list of service for all the hosts
        """
        try:
            logger.debug(f"Fetching infrastructure details for host_id_list: {host_id_list}")

            host_handler_object = HandleHostDetails()
            response = host_handler_object.get_service(host_id_list=host_id_list)

            logger.debug(f"Successfully fetched service details for host_id_list: {host_id_list}")
            return response

        except Exception as e:
            logger.error(f"Failed to fetch service tab details. Error: {e}")
            return None

    def database_details(self, host_id_list):
        """
        accepts host_id_list and returns the list of database for all the hosts
        """
        try:
            logger.debug(f"Fetching infrastructure details for host_id_list: {host_id_list}")

            host_handler_object = HandleHostDetails()
            response = host_handler_object.get_database(host_id_list=host_id_list)

            logger.debug(f"Successfully fetched database details for host_id_list: {host_id_list}")
            return response

        except Exception as e:
            logger.error(f"Failed to fetch database tab details. Error: {e}")
            return None

    def topology_details(self,application_assessment=None,infrastructure_data=None,service_data=None,database_data=None):
        """
        accepts app id and returns the information for topology_details tab shown in application 2nd page
        """
        try:

            app_id = application_assessment.get("id")
            logger.debug(f"Fetching topology tab details for app_id: {app_id}")

            response = templates.application_topology_template

            infra_list=[]
            for each in infrastructure_data:
                infra_list.append({'name':each.get('name')})

            services_list=[]
            for each in service_data:
                services_list.append({'name':each.get("name")})

            databases_list = []
            for each in database_data:
                databases_list.append({'name':each.get("name")})

            response[0]['name']=application_assessment.get('name')

            response[0]['children'][0]['children']=infra_list

            response[0]['children'][1]['children'] = services_list

            response[0]['children'][2]['children'] = databases_list

            logger.debug(f"Successfully fetched topology tab details for app_id: {app_id}")
            return response

        except Exception as e:
            logger.error(f"Failed to fetch topology tab details. Error: {e}")
            return None

    def tco_analysis_details(self,host_id_list,cloud_provider):
        """
        Caluclates all the prcing and TCO details for theunderlying infrastructure for the application.
        :param host_id_list: list, list of host_id under the given application
        :param cloud_provider: string, cloud_provider for which the TCO is being calculated
        :return: dict, TCO analysed details
        """
        try:
            on_premise_instances_list,actual_instances_list,recommended_instances_list,storage_price_details=[],[],[],[]
            instance_count, actual_total_storage_price,recommended_total_storage, recommended_total_storage_price=0,0,0,0
            #application_tco_template = templates.application_tco_template

            application_tco_template={
                                "OnpremPrice": None,
                                "ActualInstancePrice": None,
                                "RecommendedInstancePrice": None,
                                "StoragePrice": []
                            }

            application_tco_queries = get_properties(property_file_path=CreateInfra.db_queries_path.value,section="Application_TCO_Analysis")

            application_tco_details = execute_dict_of_queries(application_tco_queries, self.engine, info="application tco analysis",host_id_list=host_id_list,cloud_provider=cloud_provider)

            each_application_instance_tco_UI_template= {
                                                            "name": None,
                                                            "total": None,
                                                            "price": [{
                                                                "name": "OnDemandPrice",
                                                                "value": None
                                                            }, {
                                                                "name": "Reserved1YearPrice",
                                                                "value": None
                                                            }, {
                                                                "name": "Reserved3YearPrice",
                                                                "value": None
                                                            }]
                                                        }

            for each_instance in application_tco_details.get('actual_application_tco_details'):
                each_instance_template=copy.deepcopy(each_application_instance_tco_UI_template)
                each_instance_template['name'] = each_instance.get('instance_type')
                each_instance_template["total"] = each_instance.get("count")
                each_instance_template["price"][0]["value"] = each_instance.get("on_demand_price")
                each_instance_template["price"][1]["value"] = each_instance.get("reserved_1_year_price")
                each_instance_template["price"][2]["value"] = each_instance.get("reserved_3_year_price")
                instance_count=instance_count+each_instance.get("count")
                actual_total_storage_price = actual_total_storage_price+each_instance.get('actual_storage_price')
                actual_instances_list.append(each_instance_template)

            for each_instance in application_tco_details.get('recommended_application_tco_details'):
                each_instance_template=copy.deepcopy(each_application_instance_tco_UI_template)
                each_instance_template['name'] = each_instance.get('recommended_instance_type')
                each_instance_template["total"] = each_instance.get("count")
                each_instance_template["price"][0]["value"] = each_instance.get("recommended_on_demand_price")
                each_instance_template["price"][1]["value"] = each_instance.get("recommended_1_year_price")
                each_instance_template["price"][2]["value"] = each_instance.get("recommended_3_year_price")
                recommended_total_storage = recommended_total_storage+each_instance.get("recommended_storage")
                recommended_total_storage_price = recommended_total_storage_price + each_instance.get("recommended_storage_price")
                recommended_instances_list.append(each_instance_template)

            each_instance_template = copy.deepcopy(each_application_instance_tco_UI_template)
            each_instance_template['name'] = "VM"
            each_instance_template["total"] = instance_count
            each_instance_template["price"][0]["value"] = instance_count* CreateInfra.on_premise_vm_price.value
            each_instance_template["price"][1]["value"] = instance_count* CreateInfra.on_premise_vm_price.value
            each_instance_template["price"][2]["value"] = instance_count* CreateInfra.on_premise_vm_price.value

            on_premise_instances_list.append(each_instance_template)

            application_tco_template["OnpremPrice"] =on_premise_instances_list
            application_tco_template['ActualInstancePrice'] = actual_instances_list
            application_tco_template['RecommendedInstancePrice'] = recommended_instances_list

            if cloud_provider == "AWS":
                application_storage_details={'name':"EBS","value":None}
            else:
                application_storage_details = {'name': "Managed Disk","value":None}


            storage_price_per_unit = CreateInfra.aws_EBS_hdd_price.value

            total_actual_storage= round(actual_total_storage_price/(storage_price_per_unit*12),2)

            storage_price_details.append({"name": "onPremStoragePrice", "value": 150})
            storage_price_details.append({"name": "onPremStorage", "value": total_actual_storage})
            storage_price_details.append({"name": "ActualStoragePrice", "value": actual_total_storage_price})
            storage_price_details.append({"name": "ActualStorage", "value": total_actual_storage})
            storage_price_details.append({"name": "RecommendedStoragePrice", "value": recommended_total_storage_price})
            storage_price_details.append({"name": "RecommendedStorage", "value": recommended_total_storage})

            storage_price_details.append({"name": "ActualStoragePricePerGB", "value": str(storage_price_per_unit)+"/ GB / Month"})
            storage_price_details.append({"name": "RecommendedStoragePricePerGB", "value": str(storage_price_per_unit)+"/ GB / Month"})

            application_storage_details['price']=storage_price_details

            application_tco_template["StoragePrice"].append(application_storage_details)

            print(application_tco_details)

            return application_tco_template

        except Exception as e:
            logger.error(f"Exception occurred while calculating TCO analysis for the application ,cloud_provider {cloud_provider}. Exception as {e}")
            return None


    def instance_dependency(self,app_id=None):
        """
        fetches teh dependents applications the current applications at infrastructure level.
        app_id : unique id for very application
        """
        try:
            logger.debug("Fetching details of all the applications depended based on infrastructure level")
            infra_dependency_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Stand_Alone_Queries",
                                   property_name="Application_Dependency_Infra")

            query_results = queries_helper.execute_dict_of_queries({"infra_dependency_query" :infra_dependency_query}, self.engine, info="Application dependency ifrastructure", application_id=app_id)
            application_dependency_details = query_results["infra_dependency_query"]
            return application_dependency_details

        except Exception as e:
            logger.error(f"Exception occurred while finding dependency of the application at instance level. Exception as {e}")
            return None


    def dependency_details(self, host_ip_list):
        """
        accepts app id and returns the information for dependency_details tab shown in application 2nd page
        """

        try:
            logger.debug(f"Fetching dependency tab details for host_ip_list: {host_ip_list}")

            topology_service_object = NetworkTopology()
            response = topology_service_object.get_network_dependency(ip_range_list=host_ip_list)

            logger.debug(f"Successfully fetched dependency tab details for host_ip_list: {host_ip_list}")
            return response

        except Exception as e:
            logger.error(f"Failed to fetch dependency tab details. Error: {e}")
            return None

    def action_details(self, host_id_list, cloud_provider):
        """
        accepts host_id_list and returns the list of actions for all the hosts
        """
        try:
            logger.debug(f"Fetching action details for host_id_list: {host_id_list}")

            host_handler_object = HandleHostDetails()
            response = host_handler_object.get_actions(host_id_list=host_id_list, cloud_provider=cloud_provider)

            logger.debug(f"Successfully fetched action details for host_id_list: {host_id_list}")
            return response

        except Exception as e:
            logger.error(f"Failed to fetch action tab details. Error: {e}")
            return None

        # app analysis

    def application_analysis(self,application_id=None,cloud_provider=None):
        """
        accepts app id and returns the information for dependency_details tab shown in application 2nd page
        """

        try:
            logger.debug(f"Analysing the application recommendations")
            application_migration_details_query = util.get_properties(property_file_path=CreateInfra.db_queries_path.value,
                                                       section="Application_Queries", property_name="Application_Analysis")
            application_migration_details_query_response = queries_helper.execute_dict_of_queries({"application_migration_details": application_migration_details_query}, self.engine,application_id=application_id,
                                                                     info="Application_Analysis",cloud_provider=cloud_provider)


            if application_migration_details_query_response not in [[],None,{}]:
                application_migration_details = application_migration_details_query_response.get('application_migration_details')

                strategy,priority,suitability =  "Rehost","Migrate later","Onprem"

                strategy_list= [each_application_details['strategy'] for each_application_details in application_migration_details]

                priority_list = [each_application_details['priority'] for each_application_details in application_migration_details]

                if "Rebuild" in strategy_list:
                    strategy ="Rebuild"
                elif "Refactor" in strategy_list:
                    strategy = "Refactor"

                if "Migrate soon" in priority_list:
                    priority ="Migrate soon"
                elif "Migrate immediately" in priority_list:
                    priority = "Migrate immediately"

                application_handler_object= HandleApplicationDetails()
                application_details = application_handler_object.get_application_details(application_id)

                elasticity = CreateInfra.application_scoring.value[application_details.get('elasticity')] if application_details.get('elasticity') else 0
                optimum_utilization = CreateInfra.application_scoring.value[application_details.get('utilization')] if application_details.get('utilization') else 0
                application_maturity = CreateInfra.application_scoring.value[application_details.get('maturity')] if application_details.get('maturity') else 0
                migration_ease = CreateInfra.application_scoring.value[application_details.get('migration_complexity')] if application_details.get('migration_complexity') else 0
                integration_ease = CreateInfra.application_scoring.value[application_details.get('integration_complexity')] if application_details.get('integration_complexity') else 0


                application_score = (elasticity*0.2903) + (optimum_utilization * 0.22581) + (application_maturity * 0.19355)\
                                    + (migration_ease * 0.16129) + (integration_ease * 0.12903)
                if application_score< 5:
                    suitability = "Onprem"
                else:
                    suitability = "Cloud"

                return strategy, priority,suitability
            else:
                return None,None,None

        except Exception as e:
            logger.error(f"Exception occurred while analysing application recommendations. Error: {e}")
            return None,None,None

    def application_cloud_pricing_details(self,application_id=None,cloud_provider=None):
        """
        Fetches the price of whole application including  the infrastructure and services underlying in the application
        :param application_id: unique id for every application
        :return: tuple, on_demand, reserved_1_year,reserved_3_year
        """
        try:
            logger.debug(f"Calculating price details of cloud migration for the application")

            host_handler_object = HandleHostDetails()
            host_id_list, host_ip_list = host_handler_object.get_hosts_in_app(app_id=application_id)
            on_demand, reserved_1_year_price, reserved_3_year_price, recommended_on_demand, recommended_reserved_1_year_price, recommended_reserved_3_year_price = 0, 0, 0, 0, 0, 0

            os_recommendation_prices = {}
            for each_host_id in host_id_list:
                os_recommendation_handler_object = CreateRecommendation()

                os_cloud_pricing_details_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Stand_Alone_Queries", property_name="Infra_Cloud_Pricing_Details")
                os_cloud_pricing_details_data = execute_dict_of_queries({"os_pricing_details": os_cloud_pricing_details_query}, self.engine, cloud_provider=cloud_provider,host_id=each_host_id, info="os pricing details of a host for a cloud provider")

                os_recommendation_details = os_cloud_pricing_details_data["os_pricing_details"][0] if os_cloud_pricing_details_data else None

                #os_recommendation_details = os_recommendation_handler_object.fetch_os_recommendation(host_id=each_host_id, cloud_provider=cloud_provider)
                if os_recommendation_details not in [None,[]]:
                    on_demand += os_recommendation_details["on_demand_price"] if os_recommendation_details["on_demand_price"] else 0
                    reserved_1_year_price += os_recommendation_details["reserved_1_year_price"] if os_recommendation_details["reserved_1_year_price"] else 0
                    reserved_3_year_price += os_recommendation_details["reserved_3_year_price"] if os_recommendation_details["reserved_3_year_price"] else 0
                    recommended_on_demand += os_recommendation_details["recommended_on_demand_price"] if os_recommendation_details["recommended_on_demand_price"] else 0
                    recommended_reserved_1_year_price += os_recommendation_details["recommended_1_year_price"] if os_recommendation_details["recommended_1_year_price"] else 0
                    recommended_reserved_3_year_price += os_recommendation_details["recommended_3_year_price"] if os_recommendation_details["recommended_3_year_price"] else 0

            os_recommendation_prices["on_demand_price"] = on_demand
            os_recommendation_prices["reserved_1_year_price"] = reserved_1_year_price
            os_recommendation_prices["reserved_3_year_price"] = reserved_3_year_price
            os_recommendation_prices["recommended_on_demand_price"] = recommended_on_demand
            os_recommendation_prices["recommended_1_year_price"] = recommended_reserved_1_year_price
            os_recommendation_prices["recommended_3_year_price"] = recommended_reserved_3_year_price

            return os_recommendation_prices
        except Exception as e:
            logger.error(f"Exception occurred while preparing application pricing data. Exception {e}")
            return None
