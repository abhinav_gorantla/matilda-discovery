from matilda_discovery.connector import cmd_handler
from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db.handler.validation import HandleValidations
from matilda_discovery.services.ping.ping_check import PingService
from matilda_discovery.services.snmp.snmp_check import SnmpService
from matilda_discovery.utils import util
import os
import pandas as pd
import telnetlib

class Validation:

    def __init__(self, req_data=None):
        self.req_data = req_data

    def input_validation(self):
        ip_address = self.req_data.get('current_ip')
        community = self.req_data.get('community')
        name = self.req_data.get('name')
        username = self.req_data.get('username')
        password = self.req_data.get('password')
        task_id = self.req_data.get('task_id')

        response = dict(ping='N',
                        telnet='N',
                        login='N',
                        reason='N',
                        cmd_execution='N',
                        ip=ip_address,
                        community=community,
                        task_name=name,
                        username=username,
                        password=util.simple_encrypt(password),
                        task_id=task_id)
        #Ping check
        ping_object = PingService()
        ping_status = ping_object.ping_validation(ip_address)
        if ping_status:
            response['ping']='Y'
            response['reason'] = 'Ping Sucess'
        else:
            response['reason'] = 'Unable to Ping'

        #Snmp check
        # if ping_status:
        #     snmp_object = SnmpService()
        #     if snmp_object.snmp_validation(ip_address,community):
        #         response['snmp']='Y'
        #         response['reason'] = 'Snmp Sucess'
        #     else:
        #         response['reason'] = 'Unable to do Snmp'

        #telnet
        conncetflag = False
        ports = ['22', '5985', '5986']
        port = None
        for port in ports:
            if not conncetflag:
                try:
                    telnetlib.Telnet(ip_address, port, 3)
                    conncetflag = True
                except:
                    conncetflag = False
        if conncetflag:
            response['telnet'] = 'Y'
            response['reason'] = 'Telnet Sucess. Port ' + port
        else:
            response['reason'] = 'Unable to Telnet'

        if ping_status:
            connector = cmd_handler.CMDHandler(self.req_data)
            ssh, out, linuxflag, linux_error = connector.ssh_connect()
            win_error =None
            winflag = False
            if linuxflag:
                response['login'] = 'Y'
                response['reason'] = 'Authentication Sucess'
                response['cmd_execution'] = 'Y'
            else:
                session, host_resp, winflag, win_error, e = connector.windows_session()
                if winflag:
                    response['login'] = 'Y'
                    response['reason'] = 'Authentication Sucess'
                    response['cmd_execution'] = 'Y' #need to run some script and update
            if not linuxflag or winflag:
                if 'Authentication failure' in [win_error, linux_error]:
                    response['reason']  = 'Authentication Failed'
                else:
                    response['reason'] = 'Unable to Connect'
        handler_validation = HandleValidations()
        handler_validation.update_validations(validations=response)
        return None


    #Request handling
    def validation_request(self):
        """
        """
        request= self.req_data
        request_data=request.get('requests')
        import uuid
        task_id = str(uuid.uuid4().fields[-1])[:8]
        try:
            validation_requests = []
            for item in request_data.get("ip_list"):
                ip_list = util.get_ip_list(item['ip_address'])
                for ip in ip_list:
                    instance_details = dict(current_ip=ip,
                                     name=request_data.get('name'),
                                     community=request_data.get('community'),
                                     username=request_data.get('username'),
                                     password=request_data.get('password'),
                                     task_id=task_id)

                    validation_requests.append(instance_details)
            return validation_requests
        except Exception as e:
            pass

    def get_validation_task_list(self):
        """
        """
        try:
            handler_validation = HandleValidations()
            validation_task_list = handler_validation.get_validation_task_list()
            return validation_task_list
        except Exception as e:
            pass

    def get_validation_list(self,task_id):
        """
        """
        try:
            handler_validation = HandleValidations()
            validation_task_list, validation_list_summary = handler_validation.get_validation_list(task_id)
            return validation_task_list, validation_list_summary
        except Exception as e:
            pass

    def file_input_data(self):
        try:
            request = self.req_data
            import uuid
            task_id = str(uuid.uuid4().fields[-1])[:8]
            validation_requests = []
            path = os.path.normpath(os.path.join(os.path.dirname(__file__), CreateInfra.input.value + request.get('filename')))
            inputfiledata = pd.ExcelFile(path)
            sheetlist = inputfiledata.sheet_names  # see all sheet names
            for sheetname in sheetlist:
                template_data = inputfiledata.parse(sheetname)
                for index, row in template_data.iterrows():
                    val = row.to_dict()
                    instance_details = dict(current_ip=val.get('IP'),
                                            name=request.get('name'),
                                            community=val.get('Community'),
                                            username=val.get('Username'),
                                            password=val.get('Password'),
                                            task_id=task_id)
                    validation_requests.append(instance_details)
            return validation_requests
        except Exception as e:
            return None

    def get_validation_password(self, id,logger_ip=None):
        """
        """
        try:
            handler_validation = HandleValidations()
            validation_data = handler_validation.get_validation_password(id=id)
            validation_password = util.simple_decrypt(validation_data.get('password'))
            print(validation_password)
            return validation_password
        except Exception as e:
            pass