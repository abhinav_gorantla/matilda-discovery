from datetime import datetime

import requests
import math
from flask import send_file
from numpy.core.defchararray import capitalize
from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db.connection import get_engine
from matilda_discovery.db.handler import host as host_handler
from matilda_discovery.db.handler import task as task_handler
from matilda_discovery.db.handler.host import HandleHostDetails
from matilda_discovery.db.handler.task import HandleTaskList
from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.utils import util
from matilda_discovery.utils.util import float_converter, nullcheck_return_list_firstentry
from matilda_discovery.db import queries_helper

logger = logger_py_file.Logger()

from matilda_discovery.services.topology import HostTopology


class HostDetails:
    """
    contains methods that retrieve data related to host from database.
    """

    def __init__(self):
        pass

    def host_details(self, host_id):
        """
        fetches details of complete host_details of a particular host_id
        :param host_id: integer,
        :return: dictionary, whole host_details from various tables in DB
        """
        try:
            host_handler_object = host_handler.HandleHostDetails()
            host_data = host_handler_object.get_host_results(host_id)[0]
            logger.debug(logger_ip=host_data.get('ip'), message=f"fetching host details in service level")

            task_handler_object = task_handler.HandleTaskList()
            task_info = task_handler_object.get_task_details(task_id=host_data['task_id'])[0]

            topology_service_object = HostTopology()
            engine = get_engine()
            incoming_nodes, incoming_connections = topology_service_object.get_ip_stream(engine, host_data.get('ip'),
                                                                                         "incoming")
            outgoing_nodes, outgoing_connections = topology_service_object.get_ip_stream(engine, host_data.get('ip'),
                                                                                         "outgoing")

            task_name = task_info['name']
            if host_data not in [None, []]:
                resp = dict(
                    host_id=host_data.get('id'),
                    discovery_name=task_name,
                    task_id=host_data.get('task_id'),
                    status=host_data.get('status'),
                    reason=host_data.get('reason'),
                    host_ip=host_data.get('ip'),
                    summary=self.get_summary(host_data),
                    # services=self.get_host_services(host_data),
                    storage_information=self.get_storage_info(host_data),
                    packages=self.get_packages(host_data),
                    interfaces=self.get_interfaces(host_data),
                    user_groups=self.get_user_groups(host_data),
                    dependency=topology_service_object.parse_network_map(incoming_nodes, incoming_connections,
                                                                         outgoing_nodes, outgoing_connections),
                    login_history=self.get_login_history(host_data),
                    certificates=self.get_certificates(host_data),
                    log_file_path=CreateInfra.CURRENT_LOG_PATH.value + host_data.get('ip') + ".log"
                )
                service_list, sys_service_list = self.get_host_services(host_data)
                resp['services'] = service_list
                resp['other_services'] = sys_service_list
                ports_info, connected_services = self.get_ports(host_data)
                resp['ports'] = {}
                resp['ports']['ports_overview'] = ports_info
                resp['ports']['services_overview'] = connected_services
                docker_info, docker_id = self.get_docker_info(host_data)
                images_info, image_ids = self.get_images_info(docker_id, host_handler_object)
                containers_info = self.get_containers_info(image_ids, host_handler_object)
                if docker_info:
                    resp['docker'] = {}
                    resp['docker']['Docker Information'] = docker_info
                    resp['docker']['Images'] = images_info
                    resp['docker']['Containers'] = containers_info
                else:
                    resp['docker'] = None
                logger.debug(f"Successfully returned host details of host_id {host_id}")
                resp = self.summary_count_details(resp)
                return resp

        except Exception as e:
            logger.error(f"exception occurred while generating host response. Error: {e}")
            return None

    def new_host_details(self, host_id, info="summary"):
        """
        fetches details of complete host_details of a particular host_id
        :param host_id: integer,
        :return: dictionary, whole host_details from various tables in DB
        """
        try:

            host_handler_object = host_handler.HandleHostDetails()
            host_data = host_handler_object.get_host_results(host_id)[0]
            logger.debug(logger_ip=host_data.get('ip'), message=f"fetching host details in service level")

            task_handler_object = task_handler.HandleTaskList()
            task_info = task_handler_object.get_task_details(task_id=host_data['task_id'])[0]

            # topology_service_object = HostTopology()
            engine = get_engine()
            # incoming_nodes, incoming_connections = topology_service_object.get_ip_stream(engine, host_data.get('ip'), "incoming")
            # outgoing_nodes, outgoing_connections = topology_service_object.get_ip_stream(engine, host_data.get('ip'), "outgoing")

            task_name = task_info['name']

            if info == "summary":
                resp = []
                if host_data not in [None, []]:
                    resp = self.new_get_summary(host_data)

                return resp

            elif info == "service":
                if host_data not in [None, []]:
                    service_list, sys_service_list, service_type_count = self.new_get_host_services(host_data, info="summary")

                    kpi_data = [{"label": "Services", "count": service_type_count["Total"]}]
                    if service_type_count["Applications"] > 0:
                        kpi_data.append({"label": "Applications", "count": service_type_count["Applications"]})
                    if service_type_count["Web"] > 0:
                        kpi_data.append({"label": "Web", "count": service_type_count["Web"]})
                    if service_type_count["Database"] > 0:
                        kpi_data.append({"label": "Database", "count": service_type_count["Database"]})

                    resp =\
                        {
                            "kpiData": kpi_data,
                            "services_data":
                                {
                                    "ip_address": host_data.get('ip'),
                                    "service_list": service_list
                                }
                        }

                    return resp

            elif info == "interface":
                if host_data not in [None, []]:
                    interfaces, interface_type_count, interface_status_count = self.new_get_interfaces(host_data, info="summary")

                    kpi_data = [{"label": "Interfaces", "count": interface_type_count["Total"]}]
                    if interface_type_count["Virtual"] > 0:
                        kpi_data.append({"label": "Virtual", "count": interface_type_count["Virtual"]})
                    if interface_type_count["Physical"] > 0:
                        kpi_data.append({"label": "Physical", "count": interface_type_count["Physical"]})

                    status_data = []
                    if interface_status_count["Up"] > 0:
                        status_data.append({"label": "Up", "count": interface_status_count["Up"]})
                    if interface_status_count["Down"] > 0:
                        status_data.append({"label": "Down", "count": interface_status_count["Down"]})

                    resp = \
                        {
                            "kpiData": kpi_data,
                            "status_data": status_data,
                            "interfaces_data":
                                {
                                    "host_id": host_data.get('id'),
                                    "host_list": interfaces
                                }
                        }

                    return resp

            elif info == "storage":
                if host_data not in [None, []]:
                    storage_list, storage_dashboard = self.new_get_storage_info(host_data)
                    kpi_data = \
                        [
                            {
                                "label": "Disks",
                                "count": storage_dashboard["Disks"]
                            },
                            {
                                "label": "Utilization",
                                "count": str(round(storage_dashboard["Utilization"], 2)) + " %",
                            },
                            {
                                "label": "Capacity",
                                "count": str(round(storage_dashboard["Capacity"], 2)) + " GB",
                            },
                            {
                                "label": "Used",
                                "count": str(round(storage_dashboard["Used"], 2)) + " GB",
                            }
                        ]
                    resp =\
                        {
                            "kpiData": kpi_data,
                            "storage_data":
                                {
                                    "ip_address": host_data.get('ip'),
                                    "storage_list": storage_list
                                }
                        }

                    return resp

            elif info == "packages":
                if host_data not in [None, []]:
                    resp =\
                        {
                            "packages_data":
                                {
                                    "ip_address": host_data.get('ip'),
                                    "packages_list": self.new_get_packages(host_data, info="summary")
                                }
                        }

                    return resp

            elif info == "user_group":
                if host_data not in [None, []]:
                    resp =\
                        {
                            "user_group_data":
                                {
                                    "ip_address": host_data.get('ip'),
                                    "user_group_list": self.new_get_user_groups(host_data, info="summary")
                                }
                        }

                    return resp

            elif info == "login_history":
                if host_data not in [None, []]:
                    resp =\
                        {
                            "login_history_data":
                                {
                                    "ip_address": host_data.get('ip'),
                                    "login_history_list": self.new_get_login_history(host_data, info="summary")
                                }
                        }

                    return resp

            elif info == "certificates":
                if host_data not in [None, []]:
                    resp =\
                        {
                            "certificates_data":
                                {
                                    "ip_address": host_data.get('ip'),
                                    "certificates_list": self.new_get_certificates(host_data, info="summary")
                                }
                        }

                    return resp


            if host_data not in [None, []]:
                resp = dict(
                    host_id=host_data.get('id'),
                    discovery_name=task_name,
                    task_id = host_data.get('task_id'),
                    status = host_data.get('status'),
                    reason = host_data.get('reason'),
                    host_ip=host_data.get('ip'),
                    summary=self.new_get_summary(host_data),
                    # services=self.get_host_services(host_data),
                    packages=self.new_get_packages(host_data),
                    interfaces=self.new_get_interfaces(host_data),
                    user_groups=self.new_get_user_groups(host_data), # no common fields between windows and others
                    # dependency=topology_service_object.parse_network_map(incoming_nodes, incoming_connections, outgoing_nodes, outgoing_connections),
                    login_history=self.new_get_login_history(host_data),
                    certificates=self.new_get_certificates(host_data),
                    log_file_path=CreateInfra.CURRENT_LOG_PATH.value + host_data.get('ip') + ".log"
                )
                service_list, sys_service_list = self.new_get_host_services(host_data)
                resp['services'] = service_list
                resp['other_services'] = sys_service_list
                ports_info, connected_services = self.new_get_ports(host_data) # gopal
                resp['ports'] = {}
                resp['ports']['ports_overview'] = ports_info
                resp['ports']['services_overview'] = connected_services
                docker_info, docker_id = self.new_get_docker_info(host_data) # abhinav
                images_info, image_ids = self.new_get_images_info(docker_id, host_handler_object)
                containers_info = self.new_get_containers_info(image_ids, host_handler_object)
                if docker_info:
                    resp['docker'] = {}
                    resp['docker']['Docker Information'] = docker_info
                    resp['docker']['Images'] = images_info
                    resp['docker']['Containers'] = containers_info
                else:
                    resp['docker'] = None
                logger.debug(f"Successfully returned host details of host_id {host_id}")
                resp = self.new_summary_count_details(resp)
                return resp

        except Exception as e:
            logger.error(f"exception occurred while generating host response. Error: {e}")
            return None

    @staticmethod
    def summary_count_details(response):
        try:
            total = 0
            for item in response.get('services'):
                if item.get('service_type') == 'Application Server' or 'Web Server':
                    total += 1
            summary_count = response.get('summary')
            summary_count['host_configuration'].append(
                {'name': 'Total Listening Ports', 'value': len(response.get('ports')['ports_overview'])})
            summary_count['host_configuration'].append(
                {'name': 'Total Interfaces', 'value': len(response.get('interfaces'))})
            summary_count['host_configuration'].append({'name': 'Total Services', 'value': total})
            response['summary'] = summary_count
            return response
        except Exception as e:
            logger.error(f"Failed to count summary_count_details Error: {e}")
            return response

    def get_host_details(self, host_data):
        """
        creates host_details of the  host fetched from DB
        :param host_data: dictionary of host_data of a particular host_id
        :return: dictionary, host_data obtained from host_table
        """
        logger.debug(f"calling get_host_details db helper")

        try:
            host = \
                {
                    'platform': host_data['platform'],
                    'operating_system': host_data['operating_system'],
                    'release_version': host_data['release_version'],
                    'version': host_data['version'],
                    'boot_up_date': host_data['system_date'],
                    'memory': host_data['memory'],
                    'instance_type': host_data['instance_type'],
                    'service_provider': host_data['service_provider'],
                    'host_name': host_data['name'],
                    'bios_configuration': host_data['bios_configuration'],
                    'instruction_set': host_data['instruction_set'],
                    'pagefile_size': host_data['pagefile_size'],
                    'internet_access': host_data['internet_access'],
                    'timezone': host_data['timezone'],
                    'installed_on': host_data['installed_on'],
                    'uptime': host_data['uptime'],
                    'sku': host_data['sku']
                }

            logger.debug(f"Successfully created host_details adn returned it")
            return host

        except Exception as e:
            logger.error(f"Failed to create host_data Error: {e}")
            return {}

    @staticmethod
    def get_host_services(host_data):
        """
        creates host_services data and returns it fro a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, host_services of a particular host_id
        """
        logger.debug(f"creating host_services data in db helper")

        try:
            services = host_data['service_information']
            service_list = []
            sys_service_list = []
            templist = []
            for item in services:
                # if item['name'] not in templist:
                #     templist.append(item['name'])
                service = \
                    {
                        'service_name': item['name'],
                        'status': item['status'],
                        'service_type': item['type'],
                        'version': item['version'],
                        'Port': item['port'],
                        'cpu_percent': item['cpu_percent'],
                        'memory_percent': item['memory_percent'],
                    }
                data = \
                    {
                        'service_name': item['name'],
                        'service_discovery_id': item['discovery_id'],
                        'path': item['path']
                    }
                service.update(dict(additional_details=[data]))
                if item.get('core_service') == 1:
                    service_list.append(service)
                if item.get('core_service') == 0:
                    sys_service_list.append(service)

            # else:
            #     data = \
            #         {
            #             'service_name': item['name'],
            #             'service_discovery_id': item['discovery_id'],
            #             'path': item['path']
            #         }
            #
            #     for serviceitem in service_list:
            #         if serviceitem['service_name'] == item['name']:
            #             serviceitem['additional_details'].append(data)

            logger.debug(f"Successfully created host_services data and returned it")
            return service_list, sys_service_list

        except Exception as e:
            logger.error(f"failed creating host_services data . Error: {e}")
            return []

    def get_memory_info(self, host_data):
        """
        creates memory_info data and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, memory_info of a particular host_id
        """
        logger.debug(f"creating memory_info data in db helper")

        try:
            meminfo = host_data['memory_information']
            for item in meminfo:
                memory = \
                    {
                        'used': item['used'],
                        'available': item['available'],
                        'capacity': item['total'],
                        'used_percent': item['used_percent'],
                        'memory_type': item['type'],
                        'memory_speed': item['speed']
                    }
                if host_data.get('platform') != 'Windows':
                    memory['free_memory'] = item['free']

                logger.debug(f"Successfully created memory_info data and returned it")
                return memory

        except Exception as e:
            logger.error(f"Failed creating memory_info data. Error: {e}")
            return {}

    def get_cpu_info(self, host_data):
        """
        creates cpu_info data and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, cpu_info of a particular host_id
        """
        logger.debug(f"creating cpu_info data in db helper")
        try:
            cpuinfo = host_data['cpu_information']
            for item in cpuinfo:
                cpu = \
                    {
                        'utilization_percent': item['utilization_percent'],
                        'available_percent': str(100 - float_converter(item['utilization_percent'])),
                        'capacity_percent': '100',
                        'device_name': item['model_name'],
                        'physical_processors': item['physical_processors'],
                        'physical_cores': item['physical_cores'],
                        'logical_processors': item['logical_processors']
                    }

                logger.debug(f"Successfully created  cpu_info in db helper")
                return cpu

        except Exception as e:
            logger.error(f"failed creating cpu_info. Error: {e}")
            return {}

    def get_disk_usage_info(self, host_data):
        """
        creates disk_usage info data and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, disk_usage of a particular host_id
        """
        logger.debug(f"creating disk_usage_info in db helper")

        try:
            storageinfo = host_data['storage_information']
            total_space = 0
            used_space = 0
            available_space = 0
            if host_data.get('platform') == 'Windows':
                for item in storageinfo:
                    total_space += float_converter(item['total'])
                    used_space += float_converter(item['used'])
                    available_space += float_converter(item['available'])
                percentage = round(util.percent(used_space, total_space), 2)  # check again. total is coming as 0

                storage = \
                    {
                        'capacity': round(total_space, 2),
                        'available': round(available_space, 2),
                        'used': round(used_space, 2),
                        'used_percent': percentage
                    }

                logger.debug(f"Successfully created win_disk details")
                return storage
            else:
                for item in storageinfo:
                    total_space += float_converter(item['total'])
                    used_space += float_converter(item['used'])
                    available_space += float_converter(item['available'])
                used_percentage = round(util.percent(used_space, total_space), 2)

                storage = \
                    {
                        'capacity': round(total_space, 2),
                        'available': round(total_space - used_space, 2),
                        'used': round(used_space, 2),
                        'used_percent': used_percentage
                    }

                logger.debug(f"Successfully created win_disk details")
                return storage

        except Exception as e:
            logger.error(f"Failed to create disk_usage_info. Error: {e}")
            return {}

    def get_login_history(self, host_data):
        """
        creates login_history info and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, login_history info of a particular host_id
        """
        logger.debug(f"In get_login_history in services")

        try:
            login_info = host_data['login_history_information']
            number_of_logins = len(login_info)
            login_history = []
            if login_info:
                if host_data.get('platform') == 'Windows':
                    if number_of_logins <= 21:
                        for i in range(number_of_logins):
                            item = login_info[i]
                            temp = \
                                {
                                    'user_name': item.get('user_name'),
                                    'server_name': item.get('server_name'),
                                    'server_ip': item.get('server_ip'),
                                    'time': str(item.get('time').strftime(CreateInfra.us_datetime_format.value) if item.get('time') else None),
                                    'action': item.get('action')
                                }
                            login_history.append(temp)

                    else:
                        for item in login_info[:21]:
                            temp = \
                                {
                                    'user_name': item.get('user_name'),
                                    'server_name': item.get('server_name'),
                                    'server_ip': item.get('server_ip'),
                                    'time': str(item.get('time').strftime(CreateInfra.us_datetime_format.value) if item.get('time') else None),
                                    'action': item.get('action')
                                }
                            login_history.append(temp)
                else:
                    if number_of_logins <= 21:
                        for i in range(number_of_logins):
                            item = login_info[i]
                            temp = \
                                {
                                    'user_name': item.get('user_name'),
                                    'server_ip': item.get('server_ip'),
                                    'time': str(item.get('time').strftime(CreateInfra.us_datetime_format.value) if item.get('time') else None)
                                }
                            login_history.append(temp)

                    else:
                        for item in login_info[:21]:
                            temp = \
                                {
                                    'user_name': item.get('user_name'),
                                    'server_ip': item.get('server_ip'),
                                    'time': str(item.get('time').strftime(CreateInfra.us_datetime_format.value) if item.get('time') else None)
                                }
                            login_history.append(temp)

                logger.debug(f"Successfully created  login_history info")
            else:
                logger.debug(f"Empty login_history Details")
            return login_history

        except Exception as e:
            logger.error(f"Failed at creating login_history. Error: {e}")
            return []

    def get_certificates(self, host_data, logger_ip=None):
        """
        fetches the certificates data collected for a particular host stored in the DB
        :param host_data: list,complete host_information obtained from DB
        :return: list, certificates details of a particular host
        """

        try:
            logger.debug(logger_ip=logger_ip, message=f"Fetchign certificates  details for the host")
            certificate_info = host_data['certificate_information']
            active = []
            expired = []
            certificates = []
            if certificate_info:
                if host_data["platform"] == "Windows":
                    for item in certificate_info:
                        temp = \
                            {
                                'certificate_name': item.get('certificate_name'),
                                'path': item.get('path'),
                                'issuer': item.get('issuer'),
                                'serial_number': item.get('serial_number'),
                                'thumb_print': item.get('thumb_print'),
                                'dns_name_list': item.get('dns_name_list'),
                                'subject': item.get('subject'),
                                'version': item.get('version'),
                                'ps_provider': item.get('ps_provider'),
                                'not_after': item.get('not_after'),
                                'not_before': item.get('not_before')
                            }
                        if item.get('not_after') >= datetime.now().replace(microsecond=0):
                            temp['status'] = "active"
                            certificates.append(temp)
                        if item.get('not_after') < datetime.now().replace(microsecond=0):
                            temp['status'] = "expired"
                            certificates.append(temp)
                elif host_data["platform"] in ["Linux", "AIX", "Solaris"] or "solaris" in str.lower(
                        host_data.get('platform')):
                    for item in certificate_info:
                        temp = \
                            {
                                'certificate_name': item.get('certificate_name'),
                                'path': item.get('path'),
                                'issuer': item.get('issuer'),
                                'serial_number': item.get('serial_number'),
                                'not_after': item.get('not_after'),
                                'not_before': item.get('not_before')
                            }
                        if item.get('not_after') >= datetime.now().replace(microsecond=0):
                            temp['status'] = "active"
                            certificates.append(temp)
                        if item.get('not_after') < datetime.now().replace(microsecond=0):
                            temp['status'] = "expired"
                            certificates.append(temp)
            else:
                logger.debug(f"Empty login_history Details")
            logger.debug(logger_ip=logger_ip, message=f"Successfully returned certificates.ps1 details for the host")
            return certificates

        except Exception as e:
            logger.error(f"Failed at fetching credentials. Error: {e}")
            return None

    def get_storage_info(self, host_data):
        """
        creates storage info data and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, storage_info of a particular host_id
        """
        logger.debug(f"creating win_storage details")

        try:
            storageinfo = host_data['storage_information']
            if host_data.get('platform') == 'Windows':
                storage_details = []
                for item in storageinfo:
                    storage = \
                        {
                            'Disk': item['disk'],
                            'Model': item['model'],
                            'Logical Disk': item['logical_disk'],
                            'File System': item['file_system'],
                            'Total Space (GB)': item['total'],
                            'Boot Partition': item['boot_partition'],
                            'Used Space (GB)': item['used'],
                            'Available Space (GB)': item['available'],
                            'used_percent': item['used_percent'],
                            'Compressed': item['compressed'],
                            'Volume Serial Number': item['volume_serial_number']
                        }
                    storage_details.append(storage)

                logger.debug(f"Successfully created win_storage_info and returned it")
                return storage_details

            if 'solaris' in str.lower(host_data.get('platform')):
                disk_info = []
                for item in storageinfo:
                    disk = \
                        {
                            'File System': item['file_system'],
                            'Total Space (GB)': item['total'],
                            'Used Space (GB)': item['used'],
                            'Available Space (GB)': item['available'],
                            'used_percent': item['used_percent'],
                            'Mounted': item['mounted']
                        }
                    disk_info.append(disk)

                logger.debug(f"Successfully created solaris_storage_info and returned it")
                return disk_info

            if host_data.get('platform') in ["Linux", "AIX"]:
                disk_info = []
                for item in storageinfo:
                    disk = \
                        {
                            # 'disk': item['disk'],
                            # 'type': item['type'],
                            # 'partition': item['partition'],
                            # 'owner': item['owner'],
                            # 'group': item['group'],
                            # 'permissions': item['permissions'],
                            'Total Space (GB)': item['total'],
                            'Used Space (GB)': item['used'],
                            'Available Space (GB)': item['available'],
                            'used_percent': item['used_percent'],
                            'Mounted': item['mounted'],
                        }
                    disk_info.append(disk)

                logger.debug(f"Successfully created  ubuntu_centos_storage_info and returned it")
                return disk_info

        except Exception as e:
            logger.error(f"Failed creating win_storage details. Error: {e}")
            return []

    def get_packages(self, host_data):
        """
        creates package info and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, package info of a particular host_id
        """
        logger.debug(f"calling get_packages db helper")

        try:
            package_info = host_data['package_information']
            packages = []
            if 'solaris' in str.lower(host_data.get('platform')):

                for item in package_info:
                    if item['name']:
                        package = \
                            {
                                'package_name': item.get('name'),
                                'package_version': item.get('version'),
                                'base_directory': item.get("basedir"),
                                'package_category': item.get('category'),
                                'package_arch': item.get('arch'),
                                'package_vendor': item.get('vendor'),
                                'package_insdate': item.get('insdate'),
                                'package_status': item.get('status')
                            }
                        packages.append(package)

            else:
                for item in package_info:
                    if item['name']:
                        pkg = \
                            {
                                'package_name': item['name'],
                                'package_version': item['version'],
                            }
                        packages.append(pkg)
            logger.debug(f"Successfully created  packages info")
            return packages

        except Exception as e:
            logger.error(f"Failed at creating packages. Error: {e}")
            return []

    def get_interfaces(self, host_data):
        """
        creates interfaces info and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, interfaces info of a particular host_id
        """
        logger.debug(f"creating interfaces data")

        try:
            interface_info = host_data['interface_information']
            interfaces = []
            for item in interface_info:
                interface = \
                    {
                        'Interface Name': item['name'],
                        'Description': item['description'],
                        'IP Address': item['ip_address'],
                        'Gateway': item['gateway'],
                        'Additional IP Address': item['additional_ip'],
                        'MAC Address': item['mac_address'],
                        'Maximum Packet Size': item['packet_max_size'],
                        'Received Packets': item['received_packets'],
                        'Sent Packets': item['sent_packets'],
                        'Speed (Mb/s)': item['speed'],
                        'DHCP Enabled': item['dhcp_enabled'],
                        'DNS Server': item['dns_server'],
                        'Subnet Mask': item['ip_subnet'],
                        'Type': item['type'],
                        'Status': item['status']
                    }
                interfaces.append(interface)

            logger.debug(f"Successfully created interfaces info")
            return interfaces

        except Exception as e:
            logger.error(f"Failed in creating interfaces info. Error: {e}")
            return []

    def get_user_groups(self, host_data):
        """
        creates user_group info and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, user_group info of a particular host_id
        """
        logger.debug(f"creating user_groups info")

        try:
            usergrps = host_data['user_group_information']
            user_groups = []
            for item in usergrps:
                user_group = {"Username": item['username']}

                if host_data.get('platform') == "Windows":
                    user_group_win = {
                        "Group Description": item['group_description'],
                        "User Type": item['user_type']
                    }
                    user_group.update(user_group_win)

                if host_data.get('platform') in ["Linux", "AIX", "Solaris"] or "solaris" in str.lower(
                        host_data.get('platform')):
                    user_group_linux = {
                        'Home Directory': item.get('home_directory'),
                        "Group Name": item.get('group_name'),
                        "User ID": item.get('user_id'),
                        "Password Expiry Date": item.get('password_expiry_date'),
                        "Last Login From": item.get('last_login'),
                        "Last Password Change": item.get('last_password_change'),
                        "Account Expiry Date": item.get('account_expiry_date'),
                        "Minimum Days For Password Change": item.get('min_days_for_pwd_change'),
                        "Maximum Days For Password Change": item.get('max_days_for_pwd_change'),
                        "Warning Days For Password Change": item.get('warning_days_for_pwd_change'),
                        "Password Inactive": item.get('password_inactive'),
                        "Password Status": item.get('password_status')
                    }
                    user_group.update(user_group_linux)

                user_groups.append(user_group)

            logger.debug(f"Successfully created user_groups info and returned it")
            return user_groups

        except Exception as e:
            logger.error(f"Failed creating user_group info. Error: {e}")
            return []

    def get_utilizations(self, host_data):
        """
        creates utilization info and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, utilization info of a particular host_id
        """
        logger.debug(f"creating user_groups info")

        try:
            utilization_info = host_data['utilization_information']
            cpu_data = {}
            memory_data = {}
            storage_data = {}
            for item in utilization_info:
                cpu_data[item.executiontime] = float_converter(item.cpu_percent)
                memory_data[item.executiontime] = float_converter(item.memory_percent)
                storage_data[item.executiontime] = float_converter(item.storage_percent)
            utilizations = {"Cpu": cpu_data, "Memory": memory_data, "Storage": storage_data}
            logger.debug(f"Successfully created utilizations info and returned it")
            return utilizations
        except Exception as e:
            logger.error(f"Failed creating utilizations info. Error: {e}")
            return []

    def get_ports(self, host_data):
        """
        creates port info and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, port_info of a particular host_id
        """
        logger.debug(f"creating ports info")
        try:
            portsdata = host_data['port_information']
            portsinfo = []
            connected_services = []
            for item in portsdata:
                port = \
                    {
                        'Local Address': item['local_address'],
                        'Local Port': item['number'],
                        'Foreign Address': item['foreign_address'],
                        'Foreign Port': item['foreign_port'],
                        'Status': item['status'],
                        'Service': item['local_service'],
                        'Interface': item['used_interface'],
                        'Program Name': item['service'],
                        'Protocol': item['protocol']
                    }

                if host_data.get("platform")=="Windows":
                    port['Cpu Percent']= item['memory_percent']
                    port['Memory Percent'] = item['cpu_percent']
                    port['CPU Time (secs)'] = item['cpu_time']

                portsinfo.append(port)
                if item['status'] == 'ACTIVE':
                    connectedservicedetails = \
                        {
                            'Local Address': item['local_address'],
                            'Local Port': item['number'],
                            'Connected Host': item['foreign_address'],
                            'Connected Port': item['foreign_port'],
                            'Program Name': item['service'],
                            'Direction': item['direction'],
                        }

                    connected_services.append(connectedservicedetails)

            final_ports_info = []
            for i in range(len(portsinfo)):
                if portsinfo[i] not in portsinfo[i + 1:]:
                    final_ports_info.append(portsinfo[i])
            final_connected_services = []
            for i in range(len(connected_services)):
                if connected_services[i] not in connected_services[i + 1:]:
                    final_connected_services.append(connected_services[i])

            logger.debug(f"Successfully created ports info")
            return final_ports_info, final_connected_services

        except Exception as e:
            logger.error(f"Failed creating ports info Error: {e}")
            return [], []

    def get_docker_info(self, host_data):
        """
        creates docker info and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, docker_info of a particular host_id
        """
        logger.debug(f"Creating Docker Information")
        try:
            dock_info = host_data.get('docker_information')

            if dock_info:
                docker_info = []

                docker_id = [id.get('id') for id in dock_info]
                for item in dock_info:
                    docker = \
                        {
                            'Name': item.get('name'),
                            'Version': item.get('version'),
                            'Build Time': item.get('build_time'),
                            'Root Directory': item.get('root_dir'),
                            'Images': item.get('images'),
                            'Containers': item.get('containers'),
                            'Containers Running': item.get('containers_running'),
                            'Containers Paused': item.get('containers_paused'),
                            'Containers Stopped': item.get('containers_stopped')
                        }
                    docker_info.append(docker)
                logger.debug(f"Successfully created docker information")
                return docker_info, docker_id[0]
            else:
                return [], []
        except Exception as e:
            logger.error(f"Failed to create Docker Information Error: {e}")
            return [], []

    def get_images_info(self, docker_info_id=None, host_handler_object=None):
        """
        creates docker images info and returns it for a particular docker_info_id
        :param docker images_data: dictionary, docker images obtained from DB
        :return: dictionary, docker images_info of a particular docker_info_id
        """
        logger.debug(f"Creating Docker Images Information")
        try:
            if docker_info_id:
                img_info = host_handler_object.get_docker_images(docker_info_id)
                if img_info:
                    image_ids = [id.get('id') for id in img_info]
                    images_info = []
                    for item in img_info:
                        image = \
                            {
                                'Image ID': item.get('image_id'),
                                'Repository': item.get('repository'),
                                'Created At': item.get('createdat'),
                                'Created Since': item.get('createdsince'),
                                'Tag': item.get('tag'),
                                'Containers': item.get('containers'),
                                'Size': item.get('size'),
                                'Virtual Size': item.get('virtualsize'),
                                'Shared Size': item.get('sharedsize')
                            }
                        images_info.append(image)
                    logger.debug(f"Successfully created docker images information")
                    return images_info, image_ids
                else:
                    return [], []
            else:
                return [], []
        except Exception as e:
            logger.error(f"Failed to create Docker Images Information Error: {e}")
            return [], []

    def get_containers_info(self, image_ids, host_handler_object):
        """
        creates docker images info and returns it for a particular docker_info_id
        :param docker images_data: dictionary, docker images obtained from DB
        :return: dictionary, docker images_info of a particular docker_info_id
        """
        logger.debug(f"Creating Docker Images Information")

        try:
            if image_ids:
                container_info = []
                for docker_image_id in image_ids:
                    cont_info = host_handler_object.get_docker_containers(docker_image_id)

                    if cont_info:
                        for item in cont_info:
                            container = \
                                {
                                    'Container ID': item.get('container_id'),
                                    'Name': item.get('names'),
                                    'Image': item.get('image'),
                                    'Created At': item.get('createdat'),
                                    'Running For': item.get('runningfor'),
                                    'Status': item.get('status'),
                                    'Networks': item.get('networks'),
                                    'Ports': item.get('ports'),
                                    'Memory Limit': item.get('memory_limit'),
                                    'Memory Usage': item.get('memory_usage'),
                                    'Memory Percentage': item.get('memory_percentage'),
                                    'CPU Percentage': item.get('cpu_percentage'),
                                    'Network Input': item.get('net_in'),
                                    'Network Output': item.get('net_out'),
                                    'Block Input': item.get('block_in'),
                                    'Block Output': item.get('block_out'),
                                    'Command': item.get('command'),
                                    'User ID': item.get('user_id')
                                }
                            container_info.append(container)
                logger.debug(f"Successfully created docker images information")
                return container_info
            else:
                return []
        except Exception as e:
            logger.error(f"Failed to create Docker Images Information Error: {e}")
            return []

    def get_summary(self, host_data):
        """
        creates summary of the host data for particular host_id
        :param host_data:dictionary, host_data obtained from DB
        :return: dictionary, port_info of a particular host_id
        """
        logger.debug(f"creating summary of Host info for host_id : {host_data['id']}")
        try:
            summary = \
                {
                    "host_information":

                        [
                            {'name': 'Host Name', 'value': host_data['name']},
                            {'name': 'Bios Configuration', 'value': host_data['bios_configuration']},
                            {'name': 'Operating System', 'value': host_data['operating_system']},
                            {'name': 'Version', 'value': host_data['version']},
                            {'name': 'Release Version', 'value': host_data['release_version']},
                            {'name': 'BootUpTime', 'value': host_data['system_date']},
                            {'name': 'Uptime', 'value': host_data['uptime']}
                        ],

                    "general_information":
                        [
                            {'name': 'Memory', 'value': str(round(host_data['memory'], 2)) + " GB"},
                            {'name': 'Instruction Set', 'value': host_data['instruction_set']},
                            {'name': 'Page File Size', 'value': host_data['pagefile_size']},
                            {'name': 'Internet Access', 'value': host_data['internet_access']},
                            {'name': 'Domain', 'value': host_data['domain']}
                        ],

                    "host_configuration":
                        [
                            {'name': 'Instance Type', 'value': host_data['instance_type']},
                            {'name': 'Service Provider', 'value': host_data['service_provider']},
                            {'name': 'Time Zone', 'value': host_data['timezone']},
                            {'name': 'Installed On', 'value': host_data['installed_on']}
                        ],
                    "utilization_information":
                        {

                            'CPU_utilization': self.get_cpu_info(host_data),  # need to veriry
                            'Memory': self.get_memory_info(host_data),
                            'Disk': self.get_disk_usage_info(host_data)
                        },
                    "Metrics": self.get_utilizations(host_data)

                }
            cpu_info = host_data['cpu_information'][0]
            cpu_details_list = [{'name': 'Physical Processors', 'value': cpu_info['physical_processors']},
                                {'name': 'Physical Cores', 'value': cpu_info[
                                    'physical_cores']}]  # ,{'name': 'Logical Processors', 'value': cpu_info['logical_processors']}]
            summary['general_information'] = summary['general_information'] + cpu_details_list
            logger.debug(f"Successfully created Summary info for host_id : {host_data['id']}")
            return summary
        except Exception as e:
            logger.error(f"Failed creating Summary info for host_id : {host_data['id']} Error: {e}")
            return [], []

    @staticmethod
    def new_summary_count_details(response):
        try:
            total = 0
            for item in response.get('services'):
                if item.get('service_type') == 'Application Server' or 'Web Server':
                    total += 1
            summary_count = response.get('summary')
            summary_count['host_configuration'].append({'name': 'Total Listening Ports', 'value': len(response.get('ports')['ports_overview'])})
            summary_count['host_configuration'].append({'name': 'Total Interfaces', 'value': len(response.get('interfaces'))})
            summary_count['host_configuration'].append({'name': 'Total Services', 'value': total})
            response['summary'] = summary_count
            return response
        except Exception as e:
            logger.error(f"Failed to count summary_count_details Error: {e}")
            return response

    def new_get_host_details(self, host_data):
        """
        creates host_details of the  host fetched from DB
        :param host_data: dictionary of host_data of a particular host_id
        :return: dictionary, host_data obtained from host_table
        """
        logger.debug(f"calling get_host_details db helper")

        try:
            host = \
                {
                    'platform': host_data['platform'],
                    'operating_system': host_data['operating_system'],
                    'release_version': host_data['release_version'],
                    'version': host_data['version'],
                    'boot_up_date': host_data['system_date'],
                    'memory': host_data['memory'],
                    'instance_type': host_data['instance_type'],
                    'service_provider': host_data['service_provider'],
                    'host_name': host_data['name'],
                    'bios_configuration': host_data['bios_configuration'],
                    'instruction_set': host_data['instruction_set'],
                    'pagefile_size': host_data['pagefile_size'],
                    'internet_access': host_data['internet_access'],
                    'timezone': host_data['timezone'],
                    'installed_on': host_data['installed_on'],
                    'uptime': host_data['uptime'],
                    'sku': host_data['sku']
                }

            logger.debug(f"Successfully created host_details adn returned it")
            return host

        except Exception as e:
            logger.error(f"Failed to create host_data Error: {e}")
            return {}

    @staticmethod
    def new_get_host_services(host_data, info="detailed"):
        """
        creates host_services data and returns it fro a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, host_services of a particular host_id
        """
        logger.debug(f"creating host_services data in db helper")

        try:
            if info == "summary":
                services = host_data['service_information']
                service_list = []
                sys_service_list = []
                service_type_count = {"Applications": 0, "Web": 0, "Database": 0, "Total": 0}

                for item in services:
                    if item.get('type'):
                        service_type_count["Total"] += 1

                        if str.lower(item['type']) == "application server":
                            service_type_count["Applications"] += 1
                        elif str.lower(item['type']) == "web server":
                            service_type_count["Web"] += 1
                        elif str.lower(item['type']) == "database":
                            service_type_count["Database"] += 1

                    service = \
                        {
                            'id': item['id'],
                            'host_id': item['host_id'],
                            'service': item['name'],
                            'port': item['port'],
                            'version': item['version'],
                            'cpu': item['cpu_percent'],
                            'memory': item['memory_percent'],
                            'type': item['type'],
                            'path': item['path'],
                            'service_discovery_id': item['discovery_id'],
                            'sub_service_list':[]
                        }
                    data = \
                        {
                            'service_name': item['name'],
                            'service_discovery_id': item['discovery_id'],
                            'path': item['path']
                        }
                    service.update(dict(additional_details=[data]))
                    if item.get('core_service') == 1:
                        service_list.append(service)
                    if item.get('core_service') == 0:
                        sys_service_list.append(service)

                logger.debug(f"Successfully created host_services data and returned it")
                return service_list, sys_service_list, service_type_count

            elif info == "detailed":
                services = host_data['service_information']
                service_list = []
                sys_service_list = []

                for item in services:
                    service = \
                        {
                            'service_name': item['name'],
                            'status': item['status'],
                            'service_type': item['type'],
                            'version': item['version'],
                            'Port': item['port'],
                            'cpu_percent': item['cpu_percent'],
                            'memory_percent': item['memory_percent'],
                        }
                    data = \
                        {
                            'service_name': item['name'],
                            'service_discovery_id': item['discovery_id'],
                            'path': item['path']
                        }
                    service.update(dict(additional_details=[data]))
                    if item.get('core_service') == 1:
                        service_list.append(service)
                    if item.get('core_service') == 0:
                        sys_service_list.append(service)

                logger.debug(f"Successfully created host_services data and returned it")
                return service_list, sys_service_list

        except Exception as e:
            logger.error(f"failed creating host_services data . Error: {e}")
            return []

    def new_get_memory_info(self, host_data):
        """
        creates memory_info data and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, memory_info of a particular host_id
        """
        logger.debug(f"creating memory_info data in db helper")

        try:
            mem_info = host_data['memory_information']
            for item in mem_info:
                memory = \
                    {
                        'used': item['used'],
                        'available': item['available'],
                        'capacity': item['total'],
                        'used_percent': item['used_percent'],
                        'memory_type': item['type'],
                        'memory_speed': item['speed']
                    }
                if host_data.get('platform') != 'Windows':
                    memory['free_memory'] = item['free']

                logger.debug(f"Successfully created memory_info data and returned it")
                return memory

        except Exception as e:
            logger.error(f"Failed creating memory_info data. Error: {e}")
            return {}

    def new_get_cpu_info(self, host_data):
        """
        creates cpu_info data and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, cpu_info of a particular host_id
        """
        logger.debug(f"creating cpu_info data in db helper")
        try:
            cpu_info = host_data['cpu_information']
            for item in cpu_info:
                cpu = \
                    {
                        'used_percent': item['utilization_percent'],
                        'free_percent': str(100 - float_converter(item['utilization_percent'])),
                        'model': item['model_name'],
                        'logical_processors': item['logical_processors'],
                    }

                logger.debug(f"Successfully created  cpu_info in db helper")
                return cpu

        except Exception as e:
            logger.error(f"failed creating cpu_info. Error: {e}")
            return {}

    def new_get_disk_usage_info(self, host_data):
        """
        creates disk_usage info data and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, disk_usage of a particular host_id
        """
        logger.debug(f"creating disk_usage_info in db helper")

        try:
            storageinfo = host_data['storage_information']
            total_space = 0
            used_space = 0
            available_space = 0
            if host_data.get('platform') == 'Windows':
                for item in storageinfo:
                    total_space += float_converter(item['total'])
                    used_space += float_converter(item['used'])
                    available_space += float_converter(item['available'])
                percentage = round(util.percent(used_space, total_space), 2)  # check again. total is coming as 0

                storage = \
                    {
                        'capacity': round(total_space, 2),
                        'available': round(available_space, 2),
                        'used': round(used_space, 2),
                        'used_percent': percentage
                    }

                logger.debug(f"Successfully created win_disk details")
                return storage
            else:
                for item in storageinfo:
                    total_space += float_converter(item['total'])
                    used_space += float_converter(item['used'])
                    available_space += float_converter(item['available'])
                used_percentage = round(util.percent(used_space, total_space), 2)

                storage = \
                    {
                        'capacity': round(total_space, 2),
                        'available': round(total_space - used_space, 2),
                        'used': round(used_space, 2),
                        'used_percent': used_percentage
                    }

                logger.debug(f"Successfully created win_disk details")
                return storage

        except Exception as e:
            logger.error(f"Failed to create disk_usage_info. Error: {e}")
            return {}

    def new_get_login_history(self, host_data, info="detailed"):
        """
        creates login_history info and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, login_history info of a particular host_id
        """
        logger.debug(f"In get_login_history in services")

        try:
            if info == "summary":
                login_info = host_data['login_history_information']
                number_of_logins = len(login_info)
                login_history = []
                if login_info:
                    if host_data.get('platform') == 'Windows':
                        for i in range(number_of_logins - 21, number_of_logins):
                            item = login_info[i]

                            temp = \
                                {
                                    'user_name': item.get('user_name'),
                                    'server_ip': item.get('server_ip'),
                                    'time': str(item.get('time').strftime(CreateInfra.us_datetime_format.value) if item.get('time') else None),
                                    "sub_login_history_list":
                                        [[
                                            {
                                                "label": "Server Name",
                                                "key": item.get('server_name'),
                                            },
                                            {
                                                "label": "Action",
                                                "key": item.get('action'),
                                            },
                                        ]]
                                }
                            login_history.append(temp)
                    else:
                        for i in range(number_of_logins - 21, number_of_logins):
                            item = login_info[i]
                            temp = \
                                {
                                    'user_name': item.get('user_name'),
                                    'server_ip': item.get('server_ip'),
                                    'time': str(item.get('time').strftime(CreateInfra.us_datetime_format.value) if item.get('time') else None),
                                    "sub_login_history_list": []
                                }
                            login_history.append(temp)


                    logger.debug(f"Successfully created  login_history info")
                else:
                    logger.debug(f"Empty login_history Details")
                return login_history

            elif info ==  "detailed":
                login_info = host_data['login_history_information']
                number_of_logins = len(login_info)
                login_history = []
                if login_info:
                    if host_data.get('platform') == 'Windows':
                        for i in range(number_of_logins - 21, number_of_logins):
                            item = login_info[i]

                            temp = \
                                {
                                    'user_name': item.get('user_name'),
                                    'server_name': item.get('server_name'),
                                    'server_ip': item.get('server_ip'),
                                    'time': str(item.get('time').strftime(CreateInfra.us_datetime_format.value) if item.get('time') else None),
                                    'action': item.get('action')
                                }
                            login_history.append(temp)
                    else:
                        for i in range(number_of_logins - 21, number_of_logins):
                            item = login_info[i]
                            temp = \
                                {
                                    'user_name': item.get('user_name'),
                                    'server_ip': item.get('server_ip'),
                                    'time': str(item.get('time').strftime(CreateInfra.us_datetime_format.value) if item.get('time') else None)
                                }
                            login_history.append(temp)

                    logger.debug(f"Successfully created  login_history info")
                else:
                    logger.debug(f"Empty login_history Details")
                return login_history

        except Exception as e:
            logger.error(f"Failed at creating login_history. Error: {e}")
            return []

    def new_get_certificates(self, host_data, info="summary", logger_ip=None):
        """
        fetches the certificates data colected for a particular host stored in the DB
        :param host_data: list,complete host_information obtained from DB
        :return: list, certificates details of a particular host
        """

        try:
            logger.debug(logger_ip=logger_ip, message=f"Fetchign certificates  details for the host")

            if info == "summary":
                certificate_info = host_data['certificate_information']
                certificates = []
                if certificate_info:
                    if host_data["platform"] == "Windows":
                        for item in certificate_info:
                            temp = \
                                {
                                    'certificate_name': item.get('certificate_name'),
                                    'path': item.get('path'),
                                    'issuer': item.get('issuer'),
                                    'serial_number': item.get('serial_number'),
                                    'not_after': item.get('not_after'),
                                    'not_before': item.get('not_before'),
                                    "sub_certificates_list":
                                        [[
                                            {
                                                "label": "Thumb Print",
                                                "key": item.get('thumb_print'),
                                            },
                                            {
                                                "label": "DNS Name List",
                                                "key": item.get('dns_name_list'),
                                            },
                                            {
                                                "label": "Subject",
                                                "key": item.get('subject'),
                                            },
                                            {
                                                "label": "Version",
                                                "key": item.get('version'),
                                            },
                                            {
                                                "label": "PS Provider",
                                                "key": item.get('ps_provider'),
                                            },
                                        ]]
                                }
                            certificates.append(temp)

                    elif host_data["platform"] in ["Linux", "AIX", "Solaris"]  or "solaris" in str.lower(host_data.get('platform')):
                        for item in certificate_info:
                            temp = \
                                {
                                    'certificate_name': item.get('certificate_name'),
                                    'path': item.get('path'),
                                    'issuer': item.get('issuer'),
                                    'serial_number': item.get('serial_number'),
                                    'not_after': item.get('not_after'),
                                    'not_before': item.get('not_before'),
                                    "sub_certificates_list": []
                                }
                            certificates.append(temp)

                else:
                    logger.debug(f"Empty login_history Details")

                logger.debug(logger_ip=logger_ip, message=f"Successfully returned certificates.ps1 details for the host")
                return certificates

            elif info == "detailed":
                certificate_info = host_data['certificate_information']
                certificates = []
                if certificate_info:
                    if host_data["platform"] == "Windows":
                        for item in certificate_info:
                            temp = \
                                {
                                    'certificate_name': item.get('certificate_name'),
                                    'path': item.get('path'),
                                    'issuer': item.get('issuer'),
                                    'serial_number': item.get('serial_number'),
                                    'not_after': item.get('not_after'),
                                    'not_before': item.get('not_before'),
                                    'thumb_print': item.get('thumb_print'),
                                    'dns_name_list': item.get('dns_name_list'),
                                    'subject': item.get('subject'),
                                    'version': item.get('version'),
                                    'ps_provider': item.get('ps_provider'),
                                }
                            certificates.append(temp)
                    elif host_data["platform"] in ["Linux", "AIX", "Solaris"] or "solaris" in str.lower(host_data.get('platform')):
                        for item in certificate_info:
                            temp = \
                                {
                                    'certificate_name': item.get('certificate_name'),
                                    'path': item.get('path'),
                                    'issuer': item.get('issuer'),
                                    'serial_number': item.get('serial_number'),
                                    'not_after': item.get('not_after'),
                                    'not_before': item.get('not_before')
                                }
                            certificates.append(temp)
                else:
                    logger.debug(f"Empty login_history Details")
                logger.debug(logger_ip=logger_ip, message=f"Successfully returned certificates.ps1 details for the host")
                return certificates

        except Exception as e:
            logger.error(f"Failed at fetching credentials. Error: {e}")
            return None

    def new_get_storage_info(self, host_data):
        """
        creates storage info data and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, storage_info of a particular host_id
        """
        logger.debug(f"creating win_storage details")

        try:

            storageinfo = host_data['storage_information']

            if storageinfo in [None, []]:
                raise Exception(f"Missing storage info in host data")

            if host_data.get('platform') == 'Windows':
                storage_details = []
                storage_dashboard = {"Disks": 0, "Capacity": 0, "Used": 0}
                for item in storageinfo:
                    storage_dashboard["Disks"] += 1
                    storage_dashboard["Capacity"] += item['total']
                    storage_dashboard["Used"] += item['used']

                    storage = \
                        {
                            'total': str.lower(str(round(float(item['total']), 2))) + " GB" if item['total'] else None,
                            'used': str.lower(str(round(float(item['used']), 2))) + " GB" if item['used'] else None,
                            'available': str.lower(str(round(float(item['available']), 2))) + " GB" if item['available'] else None,
                            'used_percent': str.lower(str(round(float(item['used_percent']), 2))) + " %" if item['used_percent'] else None,
                            "sub_storage_list":
                                [[
                                    {
                                        "label": "Disk",
                                        "key": item['disk']
                                    },
                                    {
                                        "label": "Model",
                                        "key": item['model']
                                    },
                                    {
                                        "label": "Logical Disk",
                                        "key": item['logical_disk']
                                    },
                                    {
                                        "label": "File System",
                                        "key": item['file_system']
                                    },
                                    {
                                        "label": "Boot Partition",
                                        "key": item['boot_partition']
                                    },
                                    {
                                        "label": "Compressed",
                                        "key": item['compressed']
                                    },
                                    {
                                        "label": "Volume Serial Number",
                                        "key": item['volume_serial_number']
                                    },
                                ]],
                        }

                    if item['used_percent'] >= 0 and item['used_percent'] < 20:
                        storage['type'] = "Low"
                    elif item['used_percent'] >= 20 and item['used_percent'] <= 80:
                        storage['type'] = "Moderate"
                    elif item['used_percent'] > 80 and item['used_percent'] <= 100:
                        storage['type'] = "High"

                    storage_details.append(storage)

                storage_dashboard["Utilization"] = 100 * (storage_dashboard["Used"] / storage_dashboard["Capacity"])

                logger.debug(f"Successfully created win_storage_info and returned it")
                return storage_details, storage_dashboard

            if 'solaris' in str.lower(host_data.get('platform')):
                storage_dashboard = {"Disks": 0, "Capacity": 0, "Used": 0}
                storage_info = []

                for item in storageinfo:
                    storage_dashboard["Disks"] += 1
                    storage_dashboard["Capacity"] += item['total']
                    storage_dashboard["Used"] += item['used']

                    storage = \
                        {
                            'total': str.lower(str(round(float(item['total']), 2))) + " GB" if item['total'] else None,
                            'used': str.lower(str(round(float(item['used']), 2))) + " GB" if item['used'] else None,
                            'available': str.lower(str(round(float(item['available']), 2))) + " GB" if item['available'] else None,
                            'used_percent': str.lower(str(round(float(item['used_percent']), 2))) + " %" if item['used_percent'] else None,
                            "sub_storage_list":
                                [[
                                    {
                                        "label": "File System",
                                        "key": item['file_system']
                                    },
                                    {
                                        "label": "Mounted",
                                        "key": item['mounted']
                                    },
                                ]]
                        }

                    if item['used_percent'] >= 0 and item['used_percent'] < 20:
                        storage['type'] = "Low"
                    elif item['used_percent'] >= 20 and item['used_percent'] <= 80:
                        storage['type'] = "Moderate"
                    elif item['used_percent'] > 80 and item['used_percent'] <= 100:
                        storage['type'] = "High"

                    storage_info.append(storage)

                storage_dashboard["Utilization"] = 100 * (storage_dashboard["Used"] / storage_dashboard["Capacity"])

                logger.debug(f"Successfully created solaris_storage_info and returned it")
                return storage_info, storage_dashboard

            if host_data.get('platform') in ["Linux", "AIX"]:
                storage_dashboard = {"Disks": 0, "Capacity": 0, "Used": 0}
                storage_info = []

                for item in storageinfo:
                    storage_dashboard["Disks"] += 1
                    storage_dashboard["Capacity"] += item['total']
                    storage_dashboard["Used"] += item['used']

                    storage = \
                        {
                            'total': str.lower(str(round(float(item['total']), 2))) + " GB" if item['total'] else None,
                            'used': str.lower(str(round(float(item['used']), 2))) + " GB" if item['used'] else None,
                            'available': str.lower(str(round(float(item['available']), 2))) + " GB" if item['available'] else None,
                            'used_percent': str.lower(str(round(float(item['used_percent']), 2))) + " %" if item['used_percent'] else None,
                            "sub_storage_list":
                                [[
                                    {
                                        "label": "Mounted",
                                        "key": item['mounted']
                                    },
                                ]]
                        }

                    if item['used_percent'] >= 0 and item['used_percent'] < 20:
                        storage['type'] = "Low"
                    elif item['used_percent'] >= 20 and item['used_percent'] <= 80:
                        storage['type'] = "Moderate"
                    elif item['used_percent'] > 80 and item['used_percent'] <= 100:
                        storage['type'] = "High"

                    storage_info.append(storage)

                storage_dashboard["Utilization"] = 100 * (storage_dashboard["Used"] / storage_dashboard["Capacity"])

                logger.debug(f"Successfully created  ubuntu_centos_storage_info and returned it")
                return storage_info, storage_dashboard


        except Exception as e:
            logger.error(f"Failed creating win_storage details. Error: {e}")
            return []

    def new_get_packages(self, host_data, info="detailed"):
        """
        creates package info and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, package info of a particular host_id
        """
        logger.debug(f"calling get_packages db helper")

        try:
            if info == "summary":
                package_info = host_data['package_information']
                packages = []

                if 'solaris' in str.lower(host_data.get('platform')):
                    for item in package_info:  # revisit this call. Lot of fields are null in db
                        if item['name']:
                            package = \
                                {
                                    'package_name': item.get('name'),
                                    'package_version': item.get('version'),
                                    "sub_package_list":
                                        [[
                                            {
                                                "label": "Base Directory",
                                                "key": item.get("basedir"),
                                            },
                                            {
                                                "label": "Repository",
                                                "key": item.get("repository"),
                                            },
                                            {
                                                "label": "Category",
                                                "key": item.get('category'),
                                            },
                                            {
                                                "label": "Vendor",
                                                "key": item.get('vendor'),
                                            },
                                            {
                                                "label": "Status",
                                                "key": item.get('status'),
                                            },
                                        ]]
                                }
                            packages.append(package)

                else:
                    for item in package_info:
                        if item['name']:
                            pkg = \
                                {
                                    'package_name': item['name'],
                                    'package_version': item['version'],
                                    "sub_package_list": []
                                }
                            packages.append(pkg)

                logger.debug(f"Successfully created  packages info")
                return packages

            elif info == "detailed":

                package_info = host_data['package_information']
                packages = []
                if 'solaris' in str.lower(host_data.get('platform')):

                    for item in package_info:
                        if item['name']:
                            package = \
                                {
                                    'package_name': item.get('name'),
                                    'package_version': item.get('version'),
                                    'base_directory': item.get("basedir"),
                                    'package_category': item.get('category'),
                                    'package_arch': item.get('arch'),
                                    'package_vendor': item.get('vendor'),
                                    'package_insdate': item.get('insdate'),
                                    'package_status': item.get('status')
                                }
                            packages.append(package)

                else:
                    for item in package_info:
                        if item['name']:
                            pkg = \
                                {
                                    'package_name': item['name'],
                                    'package_version': item['version'],
                                }
                            packages.append(pkg)
                logger.debug(f"Successfully created  packages info")
                return packages

        except Exception as e:
            logger.error(f"Failed at creating packages. Error: {e}")
            return []

    def new_get_interfaces(self, host_data, info="detailed"):
        """
        creates interfaces info and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, interfaces info of a particular host_id
        """
        logger.debug(f"creating interfaces data")

        try:
            if info == "summary":

                interface_info = host_data['interface_information']
                interfaces = []
                interface_type_count = {"Total": 0, "Virtual": 0, "Physical": 0}
                interface_status_count = {"Up": 0, "Down": 0}

                for item in interface_info:

                    interface_type_count["Total"] += 1
                    if item['type']:
                        if str.lower(item['type']) == "virtual":
                            interface_type_count["Virtual"] += 1
                        if str.lower(item['type']) == "physical":
                            interface_type_count["Physical"] += 1
                    if item['status']:
                        if str.lower(item["status"]) == "up":
                            interface_status_count["Up"] += 1
                        if str.lower(item["status"]) == "down":
                            interface_status_count["Down"] += 1

                    interface = \
                        {
                            'id': item['id'],
                            'name': item['name'],
                            'ip_address': item['ip_address'],
                            'type': item['type'],
                            'received_packets': item['received_packets'],
                            'sent_packets': item['sent_packets'],
                            'description': item['description'],
                            'status': item['status'],
                            'sub_interfaces_list':
                                [[
                                    {
                                        "label": "Gateway",
                                        "key": item['gateway']
                                    },
                                    {
                                        "label": "Additional IP Address",
                                        "key": item['additional_ip']
                                    },
                                    {
                                        "label": "MAC Address",
                                        "key": item['mac_address']
                                    },
                                    {
                                        "label": "Maximum Packet Size",
                                        "key": item['packet_max_size']
                                    },
                                    {
                                        "label": "Speed (Mb/s)",
                                        "key": item['speed']
                                    },
                                    {
                                        "label": "DHCP Enabled",
                                        "key": item['dhcp_enabled']
                                    },
                                    {
                                        "label": "DNS Server",
                                        "key": item['dns_server']
                                    },
                                    {
                                        "label": "Subnet Mask",
                                        "key": item['ip_subnet']
                                    },
                                ]],
                        }
                    interfaces.append(interface)

                logger.debug(f"Successfully created interfaces info")
                return interfaces, interface_type_count, interface_status_count

            else:
                interface_info = host_data['interface_information']
                interfaces = []
                for item in interface_info:
                    interface = \
                        {
                            'Interface Name': item['name'],
                            'Description': item['description'],
                            'IP Address': item['ip_address'],
                            'Gateway': item['gateway'],
                            'Additional IP Address': item['additional_ip'],
                            'MAC Address': item['mac_address'],
                            'Maximum Packet Size': item['packet_max_size'],
                            'Received Packets': item['received_packets'],
                            'Sent Packets': item['sent_packets'],
                            'Speed (Mb/s)': item['speed'],
                            'DHCP Enabled': item['dhcp_enabled'],
                            'DNS Server': item['dns_server'],
                            'Subnet Mask': item['ip_subnet'],
                            'Type': item['type'],
                            'Status': item['status']
                        }
                    interfaces.append(interface)

                logger.debug(f"Successfully created interfaces info")
                return interfaces

        except Exception as e:
            logger.error(f"Failed in creating interfaces info. Error: {e}")
            return []

    def new_get_user_groups(self, host_data, info="detailed"):
        """
        creates user_group info and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, user_group info of a particular host_id
        """
        logger.debug(f"creating user_groups info")

        try:
            if info == "summary":
                usergrps = host_data['user_group_information']
                user_groups = []
                for item in usergrps:
                    user_group = {"Username": item['username']}

                    if host_data.get('platform') == "Windows":
                        user_group_win = {
                            "Group Description": item['group_description'],
                            "User Type": item['user_type'],
                            "sub_user_list": []
                        }
                        user_group.update(user_group_win)

                    if host_data.get('platform') in ["Linux", "AIX", "Solaris"] or "solaris" in str.lower(host_data.get('platform')):
                        user_group_linux = {
                            'Home Directory': item.get('home_directory'),
                            "Group Name": item.get('group_name'),
                            "User ID": item.get('user_id'),
                            "Password Expiry Date": item.get('password_expiry_date'),
                            "Last Login": item.get('last_login'),
                            "Last Password Change": item.get('last_password_change'),
                            "Account Expiry Date": item.get('account_expiry_date'),
                            "Minimum Days For Password Change": item.get('min_days_for_pwd_change'),
                            "Maximum Days For Password Change": item.get('max_days_for_pwd_change'),
                            "Warning Days For Password Change": item.get('warning_days_for_pwd_change'),
                            "Password Inactive": item.get('password_inactive'),
                            "Password Status": item.get('password_status')
                        }
                        user_group.update(user_group_linux)

                    user_groups.append(user_group)

                logger.debug(f"Successfully created user_groups info and returned it")
                return user_groups

            elif info == "detailed":
                usergrps = host_data['user_group_information']
                user_groups = []
                for item in usergrps:
                    user_group = {"Username": item['username']}

                    if host_data.get('platform') == "Windows":
                        user_group_win = {
                            "Group Description": item['group_description'],
                            "User Type": item['user_type']
                        }
                        user_group.update(user_group_win)

                    if host_data.get('platform') in ["Linux", "AIX", "Solaris"] or "solaris" in str.lower(host_data.get('platform')):
                        user_group_linux = {
                            'Home Directory': item.get('home_directory'),
                            "Group Name": item.get('group_name'),
                            "User ID": item.get('user_id'),
                            "Password Expiry Date": item.get('password_expiry_date'),
                            "Last Login": item.get('last_login'),
                            "Last Password Change": item.get('last_password_change'),
                            "Account Expiry Date": item.get('account_expiry_date'),
                            "Minimum Days For Password Change": item.get('min_days_for_pwd_change'),
                            "Maximum Days For Password Change": item.get('max_days_for_pwd_change'),
                            "Warning Days For Password Change": item.get('warning_days_for_pwd_change'),
                            "Password Inactive": item.get('password_inactive'),
                            "Password Status": item.get('password_status')
                        }
                        user_group.update(user_group_linux)

                    user_groups.append(user_group)

                logger.debug(f"Successfully created user_groups info and returned it")
                return user_groups

        except Exception as e:
            logger.error(f"Failed creating user_group info. Error: {e}")
            return []

    def new_get_utilization(self, host_data):
        """
        creates utilization info and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, utilization info of a particular host_id
        """
        logger.debug(f"creating user_groups info")

        try:
            utilization_info = host_data['utilization_information']
            cpu_trend = {}
            memory_trend = {}
            storage_trend = {}

            for item in utilization_info:
                cpu_trend[item.executiontime] = float_converter(item.cpu_percent)
                memory_trend[item.executiontime] = float_converter(item.memory_percent)
                storage_trend[item.executiontime] = float_converter(item.storage_percent)

            logger.debug(f"Successfully created utilization info and returned it")
            return {"cpu_trend": cpu_trend, "memory_trend": memory_trend, "storage_trend": storage_trend}

        except Exception as e:
            logger.error(f"Failed creating utilizations info. Error: {e}")
            return []

    def new_get_ports(self, host_data):
        """
        creates port info and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, port_info of a particular host_id
        """
        logger.debug(f"creating ports info")
        try:
            portsdata = host_data['port_information']
            portsinfo = []
            connected_services = []
            for item in portsdata:
                port = \
                    {
                        'Local Address': item['local_address'],
                        'Local Port': item['number'],
                        'Foreign Address': item['foreign_address'],
                        'Foreign Port': item['foreign_port'],
                        'Cpu Percent': item['cpu_percent'],
                        'Memory Percent': item['memory_percent'],
                        'Status': item['status'],
                        'Service': item['local_service'],
                        'Interface': item['used_interface'],
                        'CPU Time (secs)': item['cpu_time'],
                        'Program Name': item['service'],
                        'Protocol': item['protocol'],
                    }

                portsinfo.append(port)
                if item['status'] == 'ACTIVE':
                    connectedservicedetails = \
                        {
                            'Local Address': item['local_address'],
                            'Local Port': item['number'],
                            'Connected Host': item['foreign_address'],
                            'Connected Port': item['foreign_port'],
                            'Program Name': item['service'],
                            'Direction': item['direction'],
                        }

                    connected_services.append(connectedservicedetails)

            final_ports_info = []
            for i in range(len(portsinfo)):
                if portsinfo[i] not in portsinfo[i + 1:]:
                    final_ports_info.append(portsinfo[i])
            final_connected_services = []
            for i in range(len(connected_services)):
                if connected_services[i] not in connected_services[i + 1:]:
                    final_connected_services.append(connected_services[i])

            logger.debug(f"Successfully created ports info")
            return final_ports_info, final_connected_services

        except Exception as e:
            logger.error(f"Failed creating ports info Error: {e}")
            return [], []

    def new_get_docker_info(self, host_data, info="detailed"):
        """
        creates docker info and returns it for a particular host_id
        :param host_data: dictionary, host_data obtained from DB
        :return: dictionary, docker_info of a particular host_id
        """
        logger.debug(f"Creating Docker Information")
        try:
            if info == "summary":
                dock_info = host_data.get('docker_information')
                if dock_info:
                    docker_info = []

                    docker_id = [id.get('id') for id in dock_info]
                    for item in dock_info:
                        docker = \
                            {
                                'Name': item.get('name'),
                                'Version': item.get('version'),
                                'Build Time': item.get('build_time'),
                                'Root Directory': item.get('root_dir'),
                                "sub_docker_info_list":
                                    [[
                                        {
                                            "label": "Images",
                                            "key": item.get('images'),
                                        },
                                        {
                                            "label": "Containers",
                                            "key": item.get('containers'),
                                        },
                                        {
                                            "label": "Containers Running",
                                            "key": item.get('containers_running'),
                                        },
                                        {
                                            "label": "Containers Paused",
                                            "key": item.get('containers_paused'),
                                        },
                                        {
                                            "label": "Containers Stopped",
                                            "key": item.get('containers_stopped'),
                                        },
                                    ]],
                            }
                        docker_info.append(docker)
                    logger.debug(f"Successfully created docker information")
                    return docker_info, docker_id[0]
                else:
                    return [], []

            elif info == "detailed":
                dock_info = host_data.get('docker_information')
                if dock_info:
                    docker_info = []

                    docker_id = [id.get('id') for id in dock_info]
                    for item in dock_info:
                        docker = \
                            {
                                'Name': item.get('name'),
                                'Version': item.get('version'),
                                'Build Time': item.get('build_time'),
                                'Root Directory': item.get('root_dir'),
                                'Images': item.get('images'),
                                'Containers': item.get('containers'),
                                'Containers Running': item.get('containers_running'),
                                'Containers Paused': item.get('containers_paused'),
                                'Containers Stopped': item.get('containers_stopped')
                            }
                        docker_info.append(docker)
                    logger.debug(f"Successfully created docker information")
                    return docker_info, docker_id[0]
                else:
                    return [], []

        except Exception as e:
            logger.error(f"Failed to create Docker Information Error: {e}")
            return [], []

    def new_get_images_info(self, docker_info_id=None, host_handler_object=None):
        """
        creates docker images info and returns it for a particular docker_info_id
        :param docker images_data: dictionary, docker images obtained from DB
        :return: dictionary, docker images_info of a particular docker_info_id
        """
        logger.debug(f"Creating Docker Images Information")
        try:
            if docker_info_id:
                img_info = host_handler_object.get_docker_images(docker_info_id)
                if img_info:
                    image_ids = [id.get('id') for id in img_info]
                    images_info = []
                    for item in img_info:
                        image = \
                            {
                                'Image ID': item.get('image_id'),
                                'Repository': item.get('repository'),
                                'Created At': item.get('createdat'),
                                'Created Since': item.get('createdsince'),
                                'Tag': item.get('tag'),
                                'Containers': item.get('containers'),
                                'Size': item.get('size'),
                                'Virtual Size': item.get('virtualsize'),
                                'Shared Size': item.get('sharedsize')
                            }
                        images_info.append(image)
                    logger.debug(f"Successfully created docker images information")
                    return images_info, image_ids
                else:
                    return [], []
            else:
                return [], []
        except Exception as e:
            logger.error(f"Failed to create Docker Images Information Error: {e}")
            return [], []

    def new_get_containers_info(self, image_ids, host_handler_object):
        """
        creates docker images info and returns it for a particular docker_info_id
        :param docker images_data: dictionary, docker images obtained from DB
        :return: dictionary, docker images_info of a particular docker_info_id
        """
        logger.debug(f"Creating Docker Images Information")

        try:
            if image_ids:
                container_info = []
                for docker_image_id in image_ids:
                    cont_info = host_handler_object.get_docker_containers(docker_image_id)

                    if cont_info:
                        for item in cont_info:
                            container = \
                                {
                                    'Container ID': item.get('container_id'),
                                    'Name': item.get('names'),
                                    'Image': item.get('image'),
                                    'Created At': item.get('createdat'),
                                    'Running For': item.get('runningfor'),
                                    'Status': item.get('status'),
                                    'Networks': item.get('networks'),
                                    'Ports': item.get('ports'),
                                    'Memory Limit': item.get('memory_limit'),
                                    'Memory Usage': item.get('memory_usage'),
                                    'Memory Percentage': item.get('memory_percentage'),
                                    'CPU Percentage': item.get('cpu_percentage'),
                                    'Network Input': item.get('net_in'),
                                    'Network Output': item.get('net_out'),
                                    'Block Input': item.get('block_in'),
                                    'Block Output': item.get('block_out'),
                                    'Command': item.get('command'),
                                    'User ID': item.get('user_id')
                                }
                            container_info.append(container)
                logger.debug(f"Successfully created docker images information")
                return container_info
            else:
                return []
        except Exception as e:
            logger.error(f"Failed to create Docker Images Information Error: {e}")
            return []

    def new_get_summary(self, host_data):
        """
        creates summary of the host data for particular host_id
        :param host_data:dictionary, host_data obtained from DB
        :return: dictionary, port_info of a particular host_id
        """
        logger.debug(f"creating summary of Host info for host_id : {host_data['id']}")
        try:
            summary = \
                [
                    {
                        "title": "KPI Activities",
                        "ip_address": host_data['ip'],
                        "host_id": host_data['id'],
                        "host_name": host_data['name'],
                        "card_type": "kpi-card",
                        "data":
                            [
                                {
                                    "title": host_data['name'],
                                    "type": "Host Name"
                                },
                                {
                                    "title": host_data['platform'],
                                    "type": "Platform"
                                },
                                {
                                    "title": host_data['operating_system'],
                                    "type": "Operating System"
                                },
                                {
                                    "title": host_data['version'] if host_data['platform'] == "Windows" else host_data['release_version'],
                                    "type": "Version"
                                },
                                {
                                    "title": host_data['instance_type'],
                                    "type": "Type"
                                },
                                {
                                    "title": host_data['service_provider'],
                                    "type": "Service Provider"
                                },
                                {
                                    "title": host_data['device_type'],
                                    "type": "Device Type"
                                },
                                {
                                    "title": host_data['memory'],
                                    "type": "Memory"
                                },
                                {
                                    "title": host_data['domain'],
                                    "type": "Domain"
                                },
                                {
                                    "title": host_data['status'],
                                    "type": "Status"
                                },
                                {
                                    "title": host_data['reason'],
                                    "type": "Reason"
                                },
                                {
                                    "title": host_data['status'],
                                    "type": "Status"
                                },
                                {
                                    "title": host_data['community'],
                                    "type": "Community"
                                },
                                {
                                    "title": host_data['internet_access'],
                                    "type": "Internet Access"
                                },
                                {
                                    "title": host_data['cpu_usage'],
                                    "type": "CPU Usage"
                                },
                                {
                                    "title": host_data['memory_usage'],
                                    "type": "Memory Usage"
                                },
                                {
                                    "title": host_data['storage_usage'],
                                    "type": "Disk Usage"
                                },
                                {
                                    "title": host_data['dependents'],
                                    "type": "Dependents"
                                },
                                {
                                    "title": host_data['start_date'],
                                    "type": "Start Date"
                                },
                                {
                                    "title": host_data['end_date'],
                                    "type": "End Date"
                                },
                                {
                                    "title": host_data['activity'],
                                    "type": "Activity"
                                },
                            ]
                    }
                ]

            storage_details, storage_summary = self.new_get_storage_info(host_data)
            if storage_summary not in [None, []]:
                storage_info = \
                    {
                        "title": "Storage",
                        "ip_address": host_data['ip'],
                        "host_id": host_data['id'],
                        "host_name": host_data['name'],
                        "card_type": "data-card",
                        "data":
                            [
                                {
                                    "label": "Disks",
                                    "count": storage_summary.get('Disks')
                                },
                                {
                                    "label": "Utilization",
                                    "count": round(100.0 * float(storage_summary.get('Used') / storage_summary.get('Capacity')), 2) if storage_summary.get('Used') and storage_summary.get('Capacity') else None,
                                    "unit": "%"
                                },
                                {
                                    "label": "Capacity",
                                    "count": storage_summary.get('Capacity'),
                                    "unit": "GB"
                                },
                                {
                                    "label": "Used",
                                    "count": storage_summary.get('Used'),
                                    "unit": "GB"
                                }
                        ]
                    }
                summary.append(storage_info)

            cpu_summary = self.new_get_cpu_info(host_data)
            memory_summary = self.new_get_memory_info(host_data)
            if cpu_summary not in [None, []] and memory_summary not in [None, []] and storage_summary not in [None, []]:
                utilization_info = \
                    {
                        "title": "Current Utilization",
                        "ip_address": host_data['ip'],
                        "host_id": host_data['id'],
                        "host_name": host_data['name'],
                        "card_type": "utilization",
                        "chart_cpu_data":
                            [
                                {
                                    "value": round(float(cpu_summary.get('free_percent')), 2) if cpu_summary.get('free_percent') else None,
                                    "name": "Free"
                                },
                                {
                                    "value": round(float(cpu_summary.get('used_percent')), 2) if cpu_summary.get('used_percent') else None,
                                    "name": "Used"
                                }
                            ],
                        "chart_memory_data":
                            [
                                {
                                    "value": round(float(memory_summary.get('free_memory')), 2) if memory_summary.get('free_memory') else None,
                                    "name": "Free"
                                },
                                {
                                    "value": round(float(memory_summary.get('used')), 2) if memory_summary.get('used') else None,
                                    "name": "Used"
                                }
                            ],
                        "chart_storage_data":
                            [
                                {
                                    "value": round(float(storage_summary.get('Capacity') - storage_summary.get('Used')), 2) if storage_summary.get('Capacity') and storage_summary.get('Used') else None,
                                    "name": "Free"
                                },
                                {
                                    "value": round(float(storage_summary.get('Used')), 2) if storage_summary.get('Used') else None,
                                    "name": "Used"
                                }
                            ],
                        "cpu_data":
                            [
                                {
                                    "factor": "CPU",
                                    "capacity": 100,
                                    "unit": "%",
                                    "usage": round(float(cpu_summary.get('used_percent')), 2) if cpu_summary.get('used_percent') else None
                                }
                            ],
                        "memory_data":
                            [
                                {
                                    "factor": "Memory",
                                    "capacity": round(float(memory_summary.get('capacity')), 2) if memory_summary.get('capacity') else None,
                                    "unit": "GB",
                                    "usage": round(float(memory_summary.get('used')), 2) if memory_summary.get('used') else None
                                }
                            ],
                        "storage_data":
                            [
                                {
                                    "factor": "Disk",
                                    "capacity": round(float(storage_summary.get('Capacity')), 2) if storage_summary.get('Capacity') else None,
                                    "unit": "GB",
                                    "usage": round(float(storage_summary.get('Used')), 2) if storage_summary.get('Used') else None
                                }
                            ]
                    }
                summary.append(utilization_info)

            utilization_trend = self.new_get_utilization(host_data)
            if utilization_trend not in [None, []]:
                utilization_trend_info = {
                        "title": "Utilization Trend",
                        "ip_address": host_data['ip'],
                        "host_id": host_data['id'],
                        "host_name": host_data['name'],
                        "sub_title": "Last Week",
                        "card_type": "trend",
                        "datasets":
                            {
                                "cpu": utilization_trend.get('cpu_trend'),
                                "memory": utilization_trend.get('memory_trend'),
                                "storage": utilization_trend.get('storage_trend')
                            }
                    }
                summary.append(utilization_trend_info)

            interfaces, interface_type_count, interface_status_count = self.new_get_interfaces(host_data, info="summary")
            if interfaces not in [None, []]:
                network_info = {
                        "title": "Network",
                        "ip_address": host_data['ip'],
                        "host_id": host_data['id'],
                        "host_name": host_data['name'],
                        "card_type": "data-card",
                        "data":
                            [
                                {
                                    "label": "Status",
                                    "count": interface_status_count.get('Down'),
                                    "status": "Down"
                                },
                                {
                                    "label": "Status",
                                    "count": interface_status_count.get('Up'),
                                    "status": "Up"
                                },
                                {
                                    "label": "Virtual",
                                    "count": interface_type_count.get('Virtual')
                                },
                                {
                                    "label": "Physical",
                                    "count": interface_type_count.get('Physical')
                                }
                            ]
                    }
                summary.append(network_info)

            service_list, sys_service_list, service_type_count = self.new_get_host_services(host_data, info="summary")
            if service_list not in [None, []]:
                service_info = {
                        "title": "Services",
                        "ip_address": host_data['ip'],
                        "host_id": host_data['id'],
                        "host_name": host_data['name'],
                        "card_type": "pie-chart",
                        "discovered": service_type_count.get('Total'),
                        "donut_chart_data":
                            [
                                {
                                    "value": service_type_count.get('Applications'),
                                    "name": "Applications"
                                },
                                {
                                    "value": service_type_count.get('Total'),
                                    "name": "Web"
                                },
                                {
                                    "value": service_type_count.get('Database'),
                                    "name": "Database"
                                }
                            ]
                    }
                summary.append(service_info)

            package_summary = self.new_get_packages(host_data, info="summary")
            if package_summary not in [None, []]:
                package_info = {
                        "title": "Packages",
                        "ip_address": host_data['ip'],
                        "host_id": host_data['id'],
                        "host_name": host_data['name'],
                        "card_type": "data-card",
                        "data":
                            [
                                {
                                    "label": "Packages",
                                    "count": len(package_summary)
                                },
                        ]
                    }
                summary.append(package_info)

            user_summary = self.new_get_user_groups(host_data, info="summary")
            if user_summary not in [None, []]:
                user_info = {
                        "title": "Users",
                        "ip_address": host_data['ip'],
                        "host_id": host_data['id'],
                        "host_name": host_data['name'],
                        "card_type": "data-card",
                        "data":
                            [
                                {
                                "label": "Users",
                                "count": len(user_summary)
                            },
                        ]
                    }
                summary.append(user_info)

            # final_ports_info, final_connected_services = self.new_get_ports(host_data)
            final_ports_info, final_connected_services = None, None
            if final_ports_info not in [None, []]:
                port_info = {
                        "title": "Ports",
                        "ip_address": host_data['ip'],
                        "host_id": host_data['id'],
                        "host_name": host_data['name'],
                        "card_type": "pie-chart",
                        "discovered": "",
                        "donut_chart_data":
                            [
                                {
                                    "value": "",
                                    "name": "Active"
                                },
                                {
                                    "value": "",
                                    "name": "Total"
                                }
                            ]

                    }
                summary.append(port_info)

            dependency_data = None
            if dependency_data not in [None, []]:
                dependency_info = {
                        "title": "Dependency Map",
                        "ip_address": host_data['ip'],
                        "host_id": host_data['id'],
                        "host_name": host_data['name'],
                        "card_type": "data-card",
                        "data":
                            [
                                {
                                    "label": "Dependent Servers",
                                    "count": None
                                },
                                {
                                    "label": "Web Services",
                                    "count": None
                                },
                                {
                                    "label": "App Services",
                                    "count": None
                                },
                                {
                                    "label": "Database Services",
                                    "count": None
                                }
                        ]
                    }
                summary.append(dependency_info)

            logger.debug(f"Successfully collected host summary data")
            return summary

        except Exception as e:
            logger.error(f"Failed creating Summary info for host_id : {host_data['id']} Error: {e}")
            return []


class HostList:
    """
    contains methods that retrieve data related to host from database of a given task_id.
    """

    def __init__(self):
        pass

    def host_list(self, task_id):
        """
        takes task_id as input and returns discovery details of all the hosts present under the task_id and statuses of all the
        host_ids along with types of devices and their counts present under the given task_id from databse.
        :param task_id: integer, under which list of host_id's will be present
        :return: dictionary of dictionaries which contain discovery_list of all host_ids, host_list_status adn statuses count, types of devices and their repective counts
        """
        logger.debug(f"fetching host_list details of task_id {task_id} ")
        try:
            handler_object = host_handler.HandleHostDetails()
            host_list_data = handler_object.get_host_list(task_id)

            host_details_list = []
            status_list = []
            device_type_list = []

            host_handler_object = host_handler.HandleHostDetails()

            for host in host_list_data:
                device_type_list.append(host['device_type'])
                status_list.append(host['status'])

                host_data = host_handler_object.get_host_results(host['id'])[0]

                host_details = \
                    {
                        'host_id': host['id'],
                        'host_ip': host['ip'],
                        'stage': host['stage'],
                        'platform': host['platform'],
                        'device_type': host['device_type'],
                        'operating_system': host['operating_system'],
                        'release_version': host['release_version'],
                        'boot_up_date': host['system_date'],
                        'memory': host['memory'],
                        'task_id': host['task_id'],
                        'instance_type': host['instance_type'],
                        'service_provider': host['service_provider'],
                        'hostname': host['name'],
                        'bios_configuration': host['bios_configuration'],
                        'gateway_ip': host['gateway_ip'],
                        'utilization_status': None
                    }
                jobdetails = host_handler_object.get_job_status(host_id=host.get('id'))
                if jobdetails:
                    host_details['utilization_status'] = 'resume' if jobdetails[0].get(
                        'job_status') == 'pause' else 'pause'

                if host.get('status'): host_details['status'] = capitalize(host.get('status'))
                if host.get('reason'): host_details['reason'] = capitalize(host.get('reason'))
                cpuinfo = nullcheck_return_list_firstentry(host_data.cpu_information)

                if cpuinfo not in [None, []]:
                    host_details['vcpu_count'] = cpuinfo['logical_processors']
                else:
                    host_details['vcpu_count'] = None
                host_details_list.append(host_details)

            host_list_status = dict \
                    (
                    submitted=status_list.count("submitted"),
                    scheduled=status_list.count("scheduled"),
                    in_progress=status_list.count("in_progress"),
                    success=status_list.count("success"),
                    failed=status_list.count("failed")
                )

            device_list = dict \
                    (
                    servers=device_type_list.count("Server"),
                    printers=device_type_list.count("Printer"),
                    switches=device_type_list.count("Switch"),
                    firewalls=device_type_list.count("Firewall"),
                    vmware=device_type_list.count("VMware ESXi"),
                    unknown_devices=device_type_list.count("Unknown Device")
                )

            host_list_response = dict(response=host_details_list, summary=host_list_status, devices=device_list)

            logger.debug(f"successfully returned host_list_responses of all the hosts of task_id {task_id} ")
            return host_list_response

        except Exception as e:
            logger.error(f"fetching details of host_list details of task_id {task_id} failed. Error: {e}")
            return None

#NewUI
#HostList
    def new_host_list(self, task_id):
        """
        takes task_id as input and returns discovery details of all the hosts present under the task_id and statuses of all the
        host_ids along with types of devices and their counts present under the given task_id from databse.
        :param task_id: integer, under which list of host_id's will be present
        :return: dictionary of dictionaries which contain discovery_list of all host_ids, host_list_status adn statuses count, types of devices and their repective counts
        """
        logger.debug(f"fetching host_list details of task_id {task_id} ")
        try:
            handler_object = host_handler.HandleHostDetails()
            host_list_data = handler_object.get_host_list(task_id)

            host_list_response= {}
            device_count=0
            host_details_list = []
            status_list = []
            device_type_list = []
            ip_list = []


            host_handler_object = host_handler.HandleHostDetails()

            for host in host_list_data:
                device_type_list.append(host['device_type'])
                if host['device_type'] not in [None, "Unknown Device", ""]:
                    device_count= device_count +1
                status_list.append(host['status'])
                ip_list.append(host['ip'])

                service_details = host.service_information

                unique_service_list = []
                if service_details not in [None, []]:
                    for item in service_details:
                        if item["name"] not in unique_service_list and item.get('core_service') == 1:
                            unique_service_list.append(item["name"])

                host_details = \
                    {
                        'ip_address': host['ip'],
                        'host_id': host['id'],
                        "services": unique_service_list,
                        "validation_msg": host['reason'],
                        "status": host['status'],
                        "Info":
                            [
                                {"label": "IP Address", "value": host['ip']},
                                {"label": "Device Type", "value": host['device_type']},
                                {"label": "Memory", "value": host['memory']},
                                {"label": "Instance Type", "value": host['instance_type']},
                                {"label": "Operating System", "value": host['operating_system']},
                                {"label": "Service Provider", "value": host['service_provider']},
                            ]

                    }

                cpuinfo = nullcheck_return_list_firstentry(host.cpu_information)
                if cpuinfo not in [None, []]:
                   logical_processors= {"label": "CPU Count", "value" : cpuinfo['logical_processors']}
                   physical_cores = {"label": "Core Count", "value" : cpuinfo['physical_cores']}
                else:
                    logical_processors = {"label": "CPU Count", "value" :  None}
                    physical_cores =  {"label": "Core Count", "value" :  None}
                host_details["Info"].append(logical_processors)
                host_details["Info"].append(physical_cores)

                jobdetails = host_handler_object.get_job_status(host_id=host.get('id'))
                if jobdetails:
                    host_details['utilization_status'] = 'resume' if jobdetails[0].get(
                        'job_status') == 'pause' else 'pause'
                host_details_list.append(host_details)
                if host.get('status'): host_details['status'] = capitalize(host.get('status'))
                if host_details['status'] == "Success": host_details['status'] = "Successful"
                if host.get('reason'): host_details['validation_msg'] = capitalize(host.get('reason'))

                service_count_query = util.get_properties(property_file_path=CreateInfra.db_queries_path.value,
                                                          section="services_count_task_id")
                x = get_engine()
                service_count = \
                    queries_helper.execute_dict_of_queries(service_count_query, x, task_id=task_id,
                                                           info="service count")['service_count'][0]
                total = status_list.__len__()
                discovered = status_list.count("success")
                recommendation_percentage = str((math.ceil(discovered / total) * 100)) + "% Discovered Successfully"

                host_list_status = \
                    {
                        "status": host.get('status'),
                        "planed_discovery": {
                            "name": "Planned Discovery",
                            "data": [
                                {
                                    "label": "IP Address",
                                    "count": ip_list.__len__(),
                                    "discovered": status_list.count("success"),
                                    "total": status_list.__len__()
                                }
                            ],
                            "pieprogressdata": [
                                {"value": status_list.count("success"), "name": "Success"},
                                {"value": status_list.count("failed"), "name": "Failed"}
                            ]
                        },
                        "discovered_so_far": {
                            "name": "Discovered so far",
                            "data": [
                                {
                                    "label": "Devices",
                                    "count": device_count
                                },
                                {
                                    "label": "OS",
                                    "count": device_type_list.count("compute")
                                },
                                {
                                    "label": "Services",
                                    "count": service_count['service_count']
                                }
                            ]
                        },
                        "recommendations": {
                            "name": "Recommendations",
                            "recommendation": recommendation_percentage
                        }
                    }
                if host_list_status['status'] == "success": host_list_status['status'] = "Completed"
                host_list_response = dict(summaryCardData=host_list_status, discoveryHostList=host_details_list)

            logger.debug(f"successfully returned host_list_responses of all the hosts of task_id {task_id} ")
            return host_list_response

        except Exception as e:
            logger.error(f"fetching details of host_list details of task_id {task_id} failed. Error: {e}")
            return None


    def services_assets_list(self, account_id=None):
        """
        fetches all the list of all services discovered under the account_id.
        :param account_id: int, unique id for every user account
        :return: list, services and the respective recommendation details.
        """
        try:
            logger.debug(f"Fetching services and respective recommendation details")

            final_result = {}
            host_handler_object = HandleHostDetails()
            service_recommendation_response = host_handler_object.get_service_recommendations_list(account_id)
            database_overview = []
            database_migration = []
            services_overview = []
            services_migration = []

            for item in service_recommendation_response:
                if item["type"] == "Database":
                    database_overview.append(item)
                    database_migration.append(item)
                else:
                    del item["message"]
                    services_overview.append(item)
                    services_migration.append(item)

            final_result = \
                {
                    "database_overview": database_overview,
                    "database_migration": database_migration,
                    "services_overview": services_overview,
                    "services_migration": services_migration,
                }

            logger.debug(f"Successfully fetched the service level assets for the account_id: {account_id}")
            return final_result

        except Exception as e:
            logger.error(f"Exception occurred while fetching services assets list. Exception :{e}")
            return None

    def get_applications_list(self, host_ip, **kwargs):
        """
        retunrs list of application running on the given IP wich will be obtained from the service discovery endpoint
        :return: list,list of applications running on the ip
        """
        try:
            logger.debug(f"Fetching applications details running under the ip: {host_ip}")

            applications_list = []
            servicediscoverydata = util.get_properties(property_file_path=CreateInfra.etc_properties_path.value,
                                                       section='servicediscoveryapi')
            url = 'http://' + servicediscoverydata['url'] + ':' + servicediscoverydata[
                'port'] + '/matilda/servicediscovery?ipAddress=' + str(host_ip)
            response = requests.get(url=url)
            applications_discovery_response = response.json()

            if applications_discovery_response['message'] == "Discovery is completed":
                for each_application in applications_discovery_response["applications"]:
                    applications_list.append({"name": each_application['applicationName']})
                logger.debug(f"Successfully fetched applications list for the given host_ip : {host_ip}")
                return applications_list
            elif applications_discovery_response['message'] == "Discovery is still in progress":
                applications_list.append({"name": "Application Discovery is in progress"})
                return applications_list
            else:
                logger.debug("Empty response obtained for the applications list from service discovery endpoint")
                return None

        except Exception as e:
            logger.error(
                f"Error occurred while fetching applications list from service discovery endpoint. Exception : {e}")
            return None


class HostReInitiate:
    """
    contains methods to reinitiate the host discovery for the given host id
    """

    def __init__(self):
        pass

    def host_reinitiate(self, input_request, logger_ip=None):
        """
        takes host_id and also credentials to update if required as  input and re-initiate the host discovery
        :param input_request: dict, contains host_id(required), username(optional), password(optional)
        :return: None
        """

        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Reinitiating the discovery for host_id  {input_request.get('host_id')}")

            host_id = input_request.get("host_id")

            host_creation_handler_object = host_handler.HandleHostDetails()
            current_host_details, credentials_response_data, job_host_mapping_data,ip_details = host_creation_handler_object.fetch_host_details(
                host_id, logger_ip=logger_ip)

            if current_host_details in [None, []]:
                raise Exception(f"Failed to get current host details for host id: {host_id}")

            credentials_info = None
            if input_request.get('username') and input_request.get('password'):
                credentials_info = dict \
                        (
                        username=input_request.get('username'),
                        password=util.simple_encrypt(input_request.get('password')),
                        key=credentials_response_data.get('key'),
                        domain=credentials_response_data.get('domain')
                    )

            host_info = dict \
                    (
                    ip=current_host_details.get('ip'),
                    task_id=current_host_details.get('task_id'),
                    status="submitted",
                    historic=0,
                    community=current_host_details.get('community'),
                    device_type=current_host_details.get('device_type') if current_host_details.get(
                        'device_type') else None,
                    platform=current_host_details.get('platform') if current_host_details.get('platform') else None
                )

            ip_details = dict(
                ip_list=ip_details.get('ip_list'),
                type=ip_details.get('type'),
                task_id=ip_details.get('task_id'),
                data_center=ip_details.get('data_center'),
                region=ip_details.get('region'),
                project=ip_details.get('project'),
                line_of_business=ip_details.get('line_of_business'),
                environment=ip_details.get('environment'),
                application=ip_details.get('application'),
                utilization=ip_details.get('utilization'),
                utilization_interval = ip_details.get('utilization_interval'),
                utilization_period = ip_details.get('utilization_period')
            )
            #ip_list_data.append(resp)

            if credentials_info is None:
                credentials_info = dict \
                        (
                        username=credentials_response_data.get('username'),
                        password=credentials_response_data.get('password'),
                        key=credentials_response_data.get('key'),
                        domain=credentials_response_data.get('domain')
                    )

            host_info['job_entry'] = 0
            host_creation_handler_object.insert_host_and_credentials_data(host_info, credentials_info,ip_details)

            task_handler_object = HandleTaskList()
            task_handler_object.update_discovery_task_list(logger_ip=current_host_details.get('ip'),
                                                           task_id=current_host_details.get('task_id'),
                                                           status="in_progress")

            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully re-initiated the host discovery for the host_id: {host_id}")
            return job_host_mapping_data

        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Re-initiate discovery for host id {input_request.get('host_id')} is failed. Error: {e}")
            return None


class HostIPStatus:

    def recent_host_ip_details(self, host_ip):
        """
        accepts host_ip and returns the status and host_id of hte most recent discovery happened on that IP.
        :param host_ip: string, ip_address of a host
        :return:  dictionary,most recent successfully discovered ip's host_id and details
        """
        try:
            logger.debug(f"Fetching host ip details for the host_ip: {host_ip}")
            host_handler_object = HandleHostDetails()
            host_ip_details = host_handler_object.recently_discovered_host_details(host_ip)

            if host_ip_details:
                resp = {"Host_Id": host_ip_details['id'], "Host_Message": "Host succesfully discovered",
                        "Host_Status": host_ip_details['status'], "Host_Start_Date": host_ip_details['start_date'],
                        "Host_End_Date": host_ip_details['end_date']}
            else:
                resp = {"response": "No successful discoveries made with " + host_ip}

            return resp
        except Exception as e:
            logger.error(f"Exception occured while fetching host_ip details for host_ip: {host_ip}")
            return None


class HostLevelServiceDiscovery:

    def prepare_application_list(self, host_list):
        """
        fetches list of application running on a particular ip and dumps data into DB
        :return: return None
        """
        try:
            logger.debug(f"Fetching application details of each ip")
            for each_host in host_list:
                application_list = []  # self.get_applications_list(each_host['id'],each_host['ip'],logger_ip=each_host['ip'])
                application_table_data = []

                if application_list:
                    for each_application in application_list:
                        application_table_data.append({'host_id': each_host['id'], 'name': each_application['name']})

                host_handler_object = HandleHostDetails()
                host_handler_object.update_application_list(logger_ip=each_host['ip'],
                                                            application_list=application_table_data)
        except Exception as e:
            logger.debug(
                f"Exception occurred while preparing and updating the aplication list detaisl . Exception :- {e} ")
            return None


class IpLevelLogService:
    """
    fethecs the location of the log file and reads the content of it and sends back to the end point
    """

    def __init__(self):
        pass

    def fetch_log_file_content(self, host_ip, logger_ip=None):
        """
        fetches the content of the log file and sends back to the endpoint
        :param host_ip: atring, ip-address of the hsot
        :return: string,content of the log file
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"fetching the content fo the lof file for the ip : {logger_ip}")
            log_file_path = CreateInfra.CURRENT_LOG_PATH.value + host_ip + ".log"
            return send_file(log_file_path, as_attachment=True)

        except Exception as e:
            logger.error(f"Exception occurred while sending the log file content for the ip : {host_ip}. Error: {e}")
            return None


class CoreServiceUpdate:
    """
    """

    def __init__(self):
        pass

    def core_service_update(self, service, logger_ip=None):
        """
        """
        logger.debug(f"Registering core service {service} ")
        try:
            handler_object = host_handler.HandleHostDetails()
            handler_object.update_core_service_list(service=service, logger_ip=logger_ip)
            response = handler_object.update_as_core_service(service=service)
            return response

        except Exception as e:
            logger.error(f"fetching details of host_list details of task_id {None} failed. Error: {e}")
            return None

    def other_service_update(self, host_id, logger_ip=None):
        """

        """
        logger.debug(f"Registering other service {host_id} ")
        try:
            handler_object = host_handler.HandleHostDetails()
            response = handler_object.update_as_other_service(host_id=host_id)
            return response

        except Exception as e:
            logger.error(f"Failed to update other service. Error: {e}")
            return None

class ErrorsList:
    """
    """

    def __init__(self):
        pass

    def get_errors_list(self, account_id=None, logger_ip=None):
        """
        """
        try:
            logger.debug(f"Fetch errors list for account_id: {account_id}")
            handler_object = host_handler.HandleHostDetails()
            errors_data = handler_object.get_error_list(account_id=account_id, logger_ip=logger_ip)
            return errors_data

        except Exception as e:
            logger.error(f"Exception occurred while fetching errors list for account_id: {account_id}). Exception :{e}")
            return None
