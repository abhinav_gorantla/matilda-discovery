from matilda_discovery.db.handler import task as task_handler
from matilda_discovery.db.handler.service_port import ServicePortHandler

from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()



class ServicePortService:

    @staticmethod
    def service_port_details():
        """
        fetches services and respective ports from service_port table
        :return: dictionary
        """
        try:
            logger.debug(f"Collecting services and port numbers from service_port table")

            service_port_handler_object = ServicePortHandler()
            resp = service_port_handler_object.get_service_port_details()

            if resp not in [None, []]:
                logger.debug(f"Successfully collected services and port numbers from service_port table")
                response = []
                for item in resp:
                    response.append(dict(Service_Name=item['service_name'], Port=item['port']))
                return response
            else:
                raise Exception()

        except Exception as e:
            logger.error(f"Failed to collect services and port numbers from service_port table. Error: {e}")
            return None
