import datetime
import os
import ssl
from pyVim.connect import SmartConnect

import pandas as pd

from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db.handler import host as host_handler
from matilda_discovery.db.handler import task as task_handler
from matilda_discovery.db.models import model
from matilda_discovery.db.handler import vcenter as vcenter_handler
from matilda_discovery.logger_engine import logger as logger_py_file

logger = logger_py_file.Logger()

from matilda_discovery.utils import util


class TaskCreation:
    """
    contains methods that update the database when task is created.
    """

    def __init__(self):
        pass

    def create_discovery_task(self, request_data):
        """
        creates task-list, host_name,ip_list,task_id for all the given ip's in new discovery
        :param request_data: dictionary, pay load from the post call
        return: dictionary of task_id and list of host_id and host_ip's.
        """
        logger.debug("Task creation service invoked. Creating tasks.")
        response = {}
        try:
            request_data['host_username'] = request_data.get('username').__repr__()

            task_creation_handler_object = task_handler.HandleTaskCreation()

            time_info = util.get_datetime()
            if request_data.get('execution_mode') != "immediate":
                time_info = datetime.datetime.strptime(request_data.get('scheduled_time'),
                                                       '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')

            task_request = dict \
                    (
                    name=request_data.get('name'),
                    url=request_data.get('url'),
                    username=request_data.get('username'),
                    password=request_data.get('password'),
                    discovery_type="vcenter",
                    created_by=request_data.get('created_by'),
                    status="submitted",
                    account_id=request_data.get('account_id'),
                    execution_mode=request_data.get('execution_mode').lower(),
                    scheduled_time=time_info,
                    created_datetime=util.get_datetime()
                )
            task_id = task_creation_handler_object.create_discovery_task_list(task_request)
            print(task_id)

            logger.debug(f"Successfully created tasks")


        except Exception as e:
            logger.error(f"create_discovery_task service failed. Failed to create tasks. Error: {e}")

        return response


class InitiateVcenter:
    """
    contains methods that update the database when task is created.
    """

    def __init__(self):
        pass

    def initiate_vcenter(self, req):
        """
        creates task-list, host_name,ip_list,task_id for all the given ip's in new discovery
        :param req: dictionary, pay load from the post call
        :return: dictionary of task_id and list of host_id and host_ip's.
        """


        try:
            logger.debug(logger_ip="vcenter", message=f"Fetching Vcenter Data for task_id {req['task_id']}.")
            response = {}
            vcenter_handler_object = vcenter_handler.Vcenter()
            host_creation_handler_object = host_handler.HandleHostDetails()

            ssl._create_default_https_context = ssl._create_unverified_context
            s = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
            s.verify_mode = ssl.CERT_NONE
            si = SmartConnect(host=req.get('url'), user=req.get('username'), pwd=req.get('password'))
            content = si.content
            dcinfo = content.rootFolder.childEntity


            for dcitem in dcinfo:
                dcname = dcitem.name
                dcdata = {'name': dcname,'task_id':req['task_id'], 'status': dcitem.overallStatus,
                          'vcenter_host_count': len(dcitem.hostFolder.childEntity[0].host),
                          'cluster_count': len(dcitem.hostFolder.childEntity),
                          'network_count': len(dcitem.networkFolder.childEntity)}
                dc_id = vcenter_handler_object.create_datacenter(dcdata)  # dc db update
                clinfo = dcitem.hostFolder.childEntity

                for clitem in clinfo:
                    hostinfo = clitem.host
                    clname = clitem.name
                    cl_data = {'datacenter_id': dc_id, 'name': clname, 'vcenter_host_count': clitem.summary.numHosts,
                               'effective_vcenter_host_count': clitem.summary.numEffectiveHosts,
                               'cpu_core_count': clitem.summary.numCpuCores,
                               'cpu_thread_count': clitem.summary.numCpuThreads,
                               'effective_cpu_count': clitem.summary.effectiveCpu,
                               'total_cpu': clitem.summary.totalCpu,
                               'effective_memory': round(float(clitem.summary.effectiveMemory)/1024, 2),
                               'total_memory': round(float(clitem.summary.totalMemory)/ 1073741824, 2),
                               'status': clitem.summary.overallStatus,
                               'power_off_vm_count': clitem.summary.usageSummary.poweredOffVmCount}

                    cl_id = vcenter_handler_object.create_cluster(cl_data)  # cl db update

                    for hostitem in hostinfo:
                        vminfo = hostitem.vm
                        hostip = hostitem.name
                        vcenterhost_data = {'cluster_id': cl_id, 'ip': hostip,
                                            "status": hostitem.summary.overallStatus,
                                            'cpu_usage': hostitem.summary.quickStats.overallCpuUsage,
                                            'memory_usage': round(float(hostitem.summary.quickStats.overallMemoryUsage)/1024, 2),
                                            'cpu_clock_speed': hostitem.summary.hardware.cpuMhz,
                                            'cpu_core_count': hostitem.summary.hardware.numCpuCores,
                                            'cpu_model': hostitem.summary.hardware.cpuModel,
                                            'cpu_thread_count': hostitem.summary.hardware.numCpuThreads,
                                            'interface_count': hostitem.summary.hardware.numNics,
                                            'uuid': hostitem.summary.hardware.uuid,
                                            'vendor': hostitem.summary.hardware.vendor,
                                            'model': hostitem.summary.hardware.model,
                                            'hypervisor_version': hostitem.config.product.fullName}

                        vcenterhost_id = vcenter_handler_object.create_vcenterhost(vcenterhost_data)  # vcenterhost db update

                        for vmitem in vminfo:

                            vmip = vmitem.summary.guest.ipAddress



                            if vmip is None:
                                vmip = hostip

                            host_info = dict(ip=vmip,
                                             name=vmitem.name,
                                             operating_system=vmitem.summary.guest.guestFullName,
                                             instance_type= "VM",
                                             device_type="compute",
                                             power_status=vmitem.summary.runtime.powerState,
                                             task_id=req.get('task_id'),
                                             memory=round(float(vmitem.summary.config.memorySizeMB) / 1024, 2),
                                             status="submitted",
                                             historic=0)

                            host_id = host_creation_handler_object.create_individual_host(host_info)
                            cpu_info = dict(host_id=host_id, logical_processors=vmitem.summary.config.numCpu)
                            memory_info = dict(host_id=host_id, total=round(float(vmitem.summary.config.memorySizeMB) / 1024, 2))

                            host_creation_handler_object.update_table(table_info=memory_info, table_model=model.Memory)
                            host_creation_handler_object.update_table(table_info=cpu_info, table_model=model.CPU)

                            credentials_data = dict(host_id=host_id,
                                                    username=req.get('username'),
                                                    password=util.simple_encrypt(req.get('password')),
                                                    key=None,
                                                    domain=None)
                            host_creation_handler_object.insert_host_and_credentials_data(host_data=None, credentials_data=credentials_data,logger_ip=vmitem.summary.guest.ipAddress)

                            ip_list_data=[]
                            resp = dict(
                                ip_list=vmitem.summary.guest.ipAddress,
                                host_id=host_id,
                                type="Vcenter",
                                task_id=req.get('task_id'),
                                data_center="Other",
                                region="Other",
                                project="Other",
                                line_of_business="Other",
                                environment="Other",
                                application ="Other"
                                )
                            resp['utilization'] = "N"
                            resp['utilization_interval'] = 0
                            resp['utilization_period'] =  0
                            ip_list_data.append(resp)

                            task_creation_handler_object = task_handler.HandleTaskCreation()
                            task_creation_handler_object.create_iplist(ip_list_data)

                            host_creation_handler_object.update_host_table(logger_ip=vmitem.summary.guest.ipAddress, host_id=host_id, job_entry=2)

        except Exception as e:
            logger.error(logger_ip="vcenter", message=f"Initiate_vcenter service failed. Failed to get vcenter data. Error: {e}")




