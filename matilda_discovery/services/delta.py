from matilda_discovery.db.connection import get_engine
from matilda_discovery.db.handler.delta import HandleHostDelta
from matilda_discovery.logger_engine import logger as logger_py_file

logger = logger_py_file.Logger()


class HostDetailsDelta:

    def __init__(self):
        self.engine = get_engine()

    def host_details_delta(self, current_id, previous_id, logger_ip=None):
        """
        accepts 2 host id's and returns delta of host details between them
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Fetching delta of host details for current id: {current_id} and previous id: {previous_id}")

            if previous_id is None:
                # get most recent id
                pass


            delta_handler_object = HandleHostDelta()
            delta_info = delta_handler_object.host_delta_data(current_id=current_id, previous_id=previous_id, logger_ip=logger_ip)

            logger.debug(logger_ip=logger_ip, message=f"Successfully fetched delta of host details for current id: {current_id} and previous id: {previous_id}")
            return delta_info

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to fetch delta of host details for current id: {current_id} and previous id: {previous_id}. Error: {e}")
            return {}
