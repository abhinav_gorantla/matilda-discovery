import platform
import subprocess

from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()

from matilda_discovery.db.handler.host import HandleHostDetails
from matilda_discovery.db.handler.task import HandleTaskList
from matilda_discovery.utils import util


class PingService:
    def __init__(self):
        pass

    def ping_check_status(self,ip_address,host_id,task_id): # done
        """
        returns true or false if the device is able to be pinged or not.
        :param ip_address: string, ip_address of the host
        :param host_id : int,host_id an unique id for a ip_address under a task
        :param task_id: int, task_id under which a new discovery of the current ip_address is being discovered
        :return:boolean, true or false of the ping status.
        """
        try:
            subprocess.check_output("ping -{} 1 {}".format('n' if platform.system().lower() == "windows" else 'c', ip_address), shell=True)

        except Exception as e:
            logger.debug(logger_ip=ip_address, message=f"Failed pinging the device of the Host: {ip_address} and Updating Stage as '1' to DB. Exception : {e} ")
            host_handler_object = HandleHostDetails()
            host_handler_object.update_host_table(host_id=host_id, ip=ip_address, reason="host unreachable", status="failed", stage=1,job_entry = 3,start_date=util.get_datetime(),end_date=util.get_datetime(),logger_ip=ip_address)
            task_handler_object=HandleTaskList()
            task_handler_object.task_status_update(task_id=task_id, logger_ip=ip_address)
            logger.debug(logger_ip=ip_address, message=f"Successfully Updated Stage Details to DB of the Host: {ip_address} ")
            return False

        logger.debug(logger_ip=ip_address, message=f"==Successfully got Response from the Host: {ip_address} and Updating Stage as '1' to DB ")
        host_handler_object = HandleHostDetails()
        host_handler_object.update_host_table(logger_ip=ip_address, host_id=host_id, reason="host reachable", stage=1, start_date=util.get_datetime())
        logger.debug(logger_ip=ip_address, message=f" Successfully Updated ping Stage Details to DB of the Host: {ip_address} ")

        return True

    def ping_validation(self, ip_address):  # done
        """
        """
        try:
            subprocess.check_output("ping -{} 1 {}".format('n' if platform.system().lower() == "windows" else 'c', ip_address), shell=True)
        except Exception as e:
            return False
        return True
