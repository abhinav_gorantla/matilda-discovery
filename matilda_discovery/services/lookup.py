import json
import os

import pandas as pd
from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db.connection import get_engine
from matilda_discovery.db.handler.host import HandleHostDetails
from matilda_discovery.utils import util
from matilda_discovery.db.handler.lookup import HandleLookup
from matilda_discovery.logger_engine import logger as logger_py_file

logger = logger_py_file.Logger()


class LookupService:

    def __int__(self):
        self.engine = get_engine()

    def update_lookup_table(self, input_lookup_data, table_model, table_name):
        """
        Accepts input excel file, db model and updates the corresponding model table with input file
        """
        try:
            logger.debug(f"In Update Lookup Service call")

            host_handler = HandleHostDetails()
            host_handler.update_table(table_info=None, table_model=table_model, truncate_first=True)

            engine = get_engine()
            input_lookup_data.to_sql(name=table_name, con=engine, schema=None, if_exists="replace", index=False,)

            logger.debug(f"Successfully completed Update Table service call")

        except Exception as e:
            logger.error(f"Failed in update lookup table service call for table: {table_model}. Error: {e}")


    def download_lookup_table(self, file_name):
        """
        accepts file name and returns corresponding lookup table in excel format
        """
        try:
            logger.debug("Exporting {file_name}} data in excel format")

            lookup_handler_object = HandleLookup()

            if file_name == 'os_lookup':
                query_response = lookup_handler_object.get_table(property_name='os_lookup_query')
            elif file_name == 'service_lookup':
                query_response = lookup_handler_object.get_table(property_name='service_lookup_query')
            else:
                raise Exception(f"Unsupported filename given. Given filename: {file_name}")

            if query_response is None:
                raise Exception(f"Failed to get table data from handler")

            directory = os.path.normpath(os.path.join(os.path.dirname(__file__), CreateInfra.temp.value))
            util.create_dir_file(directory)
            file = os.path.join(directory + '/' + file_name)

            with pd.ExcelWriter(file, engine='xlsxwriter') as writer:
                for i in query_response:
                    col = [*i]
                    break
                util.json_formatter(query_response)
                resp = pd.read_json(json.dumps(query_response))
                resp.to_excel(writer, sheet_name="lookup_data", index=False, columns=col, freeze_panes=(1, 0))
                workbook = writer.book
                worksheet = writer.sheets['lookup_data']
                header_format = workbook.add_format({'fg_color': '#D7E4BC'})
                for col_num, val in enumerate(resp.columns.values):
                    worksheet.write(0, col_num, val, header_format)

            writer.save()
            writer.close()

            logger.debug(f"Successfully exported {file_name} data.")
            return file

        except Exception as e:
            logger.debug(f"Failed to export {file_name} data in excel format. Error: {e}")
            return None
