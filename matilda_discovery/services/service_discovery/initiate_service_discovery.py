import json

import requests

from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db import connection
from matilda_discovery.db.models import model
from matilda_discovery.utils import util

from matilda_discovery.logger_engine import logger as logger_py_file

logger = logger_py_file.Logger()


def initiate_service_discovery(connector, servicetype=None, path=None, platform=None, logger_ip=None):
    """
    initiates service discovery for teh discovered service in a particular host and it will send the respective details needed for the service discovery endpoint
    :param task_id: int,unique id for every task created for a particular discovery
    :param discovery_name: String, name of a new discovery created
    :param servicetype: name of the service file discovered
    :param ip:string, ip_address of the host
    :param uname: string, username for the host to login
    :param password: string, password to login into the host
    :param platform: string, platform of the host
    :param path: path where the location of the service is found.
    :return: integer, service disocvery id obtained from the service_discovery endpoint
    """
    service_id = None
    source = "HostDiscovery"
    try:

        logger.debug(logger_ip=logger_ip, message=f"Initiating service discovery for host_ip : {logger_ip}")

        if connector.ip:  # change this into to actual logger ip
            logger_ip = connector.ip

        if servicetype in ['iis', 'jboss', 'tomcat', 'weblogic', 'mysql', 'mssql']:
            logger.debug(logger_ip=logger_ip, message=f"Service discovery for service {servicetype}")
            if servicetype in ['mysql', 'mssql']:
                platform = 'db'
            servicediscoverydata = util.get_properties(property_file_path=CreateInfra.etc_properties_path.value, section='servicediscoveryapi')
            url = 'http://' + servicediscoverydata['url'] + ':' + servicediscoverydata['port'] + '/matilda/servicediscovery'

            session = connection.get_session()
            service_username = session.query(model.Credentials.username).filter_by(host_id=connector.host_id).all()[0][0].replace('\\', '\\\\')

            data = "{\n\"source\":\"" + source + "\",\n\"sourceId\":\"" + str(connector.task_id) + "\",\n\"hostId\":\"" + str(connector.host_id) + "\",\n\"serviceDiscoveryName\":\"" + connector.discoveryname + "\",\n\"servicePath\": \"" + path + "\",\n\"ipAddress\": \"" + connector.ip + "\",\n\"serviceType\": \"" + servicetype + "\",\n\"userName\": \"" + service_username + "\",\n\"password\": \"" + connector.password + "\",\n\"platform\": \"" + platform + "\"\n}\n\n\n\n\n"

            logger.debug(logger_ip=logger_ip, message=f"Request object for initiating service discovery for ip: {connector.ip} ServiceType: {servicetype}, data = {data}")

            r = requests.post(url=url, data=data, headers={'content-type': 'application/json'})

            logger.debug(logger_ip=logger_ip, message=f"service discovery logs for: {connector.ip} ServiceType: {servicetype} status_code: {r.status_code} reason: {r.reason}")
            response = json.loads(r.text)
            logger.debug(logger_ip=logger_ip, message=f"service discovery response for the service {servicetype} is : {response}")

            service_id = str(response.get('serviceDiscoveryId'))
    except Exception as e:
        logger.error(f"Exception occurred while initiating service discovery for ip: {connector.ip} Exception: {e}")
        return service_id
    return service_id
