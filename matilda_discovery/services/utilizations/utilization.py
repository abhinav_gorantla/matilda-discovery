import inspect

from matilda_discovery.connector import cmd_handler
from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db.handler import host as host_handler
from matilda_discovery.db.handler.host import HandleHostDetails
from matilda_discovery.services.discovery.linux import Linux
from matilda_discovery.services.discovery.windows import Windows
from matilda_discovery.utils import util
from matilda_discovery.utils.util import shell_to_json

from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()



# This class is used to re initiate the discovery for host or task level
class utilization:

    def __init__(self, req_data=None):
        self.req_data = req_data

    def utilization(self, first_utilization=False,logger_ip = None):
        try:
            logger.debug(logger_ip=logger_ip,message=f"Fetching and updating utilization records for the host_ip")
            host_handler_object = HandleHostDetails()
            host_info = host_handler_object.get_host_results(self.req_data.get('host_id'))
            resp = host_info[0]
            ports_info=None
            utiliztioninfo = {'host_id': self.req_data.get('host_id'),'executiontime':util.get_datetime(req_format=CreateInfra.utilization_datetime_format.value)}
            connector = cmd_handler.CMDHandler(self.req_data)
            if first_utilization or (resp.get('status') == 'success' and resp.get('platform') is not None):
                if resp.get('platform') == 'Windows':
                    session, host_resp, winflag, win_error, e = connector.windows_session(logger_ip=logger_ip)
                    if winflag:
                        windows_object = Windows(connector)
                        utiliztion_data = windows_object.execute_powershell_script(logger_ip=logger_ip,cmd=util.get_script(name=inspect.stack()[0][3],version=connector.version), info="utiliztion")[0]
                        utiliztioninfo.update(utiliztion_data)
                        interface_info = windows_object.interface(logger_ip=logger_ip, utiliztion=True)
                        windows_object.ports(interface_info, logger_ip=logger_ip)

                    else:
                        #host_handler = HandleHostDetails()
                        path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
                        error_data = {"host_id": self.req_data.get('host_id'), "ip": self.req_data.get('current_ip'), "type":path + "utilization", "error": win_error}
                        host_handler_object.update_error_table(error_data=error_data, logger_ip=logger_ip)


                if resp.get('platform') == 'Linux':
                    ssh, out, flag, linux_error = connector.ssh_connect(logger_ip=logger_ip)
                    if flag:
                        linux_object = Linux(connector)
                        utiliztion_info, err = connector.execute_shell(util.get_script(name=inspect.stack()[0][3], operatingsystem=connector.operatingsystem), cmd_info="Utilization", logger_ip=logger_ip)
                        utiliztion_data = shell_to_json(utiliztion_info,info=inspect.stack()[0][3], logger_ip=logger_ip)[0]
                        utiliztioninfo.update(utiliztion_data)
                        interface_info = linux_object.interface(logger_ip=logger_ip, utiliztion=True)
                        ports_info=linux_object.ports(interface_info, logger_ip=logger_ip, utiliztion=True)

                    else:
                        path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
                        error_data = {"host_id": self.req_data.get('host_id'), "ip": self.req_data.get('current_ip'), "type":path + "utilization", "error": linux_error}
                        host_handler_object.update_error_table(error_data=error_data, logger_ip=logger_ip)


                host_handler_object.update_utilization_info(utiliztioninfo, logger_ip=logger_ip)
                host_handler_object.update_ports_info(ports_info, logger_ip=logger_ip)

            logger.debug(logger_ip=logger_ip, message=f" Successfully updated utilization details of the Host: {logger_ip} in DB host_id: {self.req_data.get('host_id')} ,utilization_data :{utiliztioninfo}")

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f" Failed to get utilization details for the Host: {logger_ip} host_id: {self.req_data.get('host_id')} Error: {e} ")
        return None

    def utilization_job_host_mapping_update(self,host_id=None, job_id=None, job_status=None, logger_ip=None):
        try:

            logger.debug(logger_ip=logger_ip, message=f"utilization job host mapping update {host_id}")

            host_handler_object = host_handler.HandleHostDetails()
            host_handler_object.update_utilization_job_host_mapping(host_id=host_id, job_id=job_id,job_type='utilization',job_status=job_status)

            logger.debug(logger_ip=logger_ip, message=f" Successfully updated utilization job host mapping update for hostid: {host_id}")
            return None

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f" Failed to updated utilization job host mapping update update for hostid Error: {e} ")
        return None

    def get_job_id(self,logger_ip=None):
        try:
            host_id = self.req_data.get('host_id')
            status = self.req_data.get('status')
            host_handler_object = host_handler.HandleHostDetails()
            jobdetails = host_handler_object.get_job_id(host_id=host_id, job_type='utilization')
            job_id = jobdetails.get('job_id')
            return dict(job_id=job_id,status=status)
        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f" Failed to get job id from job host mapping table.Error: {e} ")
        return None

