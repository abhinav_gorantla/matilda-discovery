from matilda_discovery.db.handler.host import HandleHostDetails
from matilda_discovery.db.handler.device_lookup import Device_Type
from pysnmp.hlapi import *

from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()




class SnmpService:

    def __init__(self):
        pass

    def device_type_details(self,req_data): # done
        host_id = req_data.get('host_id')
        ip = req_data.get('current_ip')
        try:
            logger.debug(logger_ip=ip, message=f"Retrieving device_type Details of the Host: {ip} through SNMP")
            errorIndication, errorStatus, errorIndex, varBinds = next(
                getCmd(SnmpEngine(),
                       CommunityData(req_data.get('community')),
                       UdpTransportTarget((ip, 161)),
                       ContextData(),
                       ObjectType(ObjectIdentity('SNMPv2-MIB', 'sysObjectID', 0))))

            if errorIndication:
                logger.error(logger_ip=ip, message=f" Unable to retrieve device_type details of the host: {ip}.reason: {errorIndication} ")
                host_handler_object = HandleHostDetails()
                host_handler_object.update_host_table(logger_ip=ip,device_type="Unknown Device",host_id=host_id, stage=2, job_entry=2,device_identification_flag=2)
            elif errorStatus:
                # print('%s at %s' % (errorStatus.prettyPrint(), errorIndex and varBinds[int(errorIndex) - 1][0] or '?')) # change later
                pass

            for varBind in varBinds:
                a = (' = '.join([x.prettyPrint() for x in varBind]))
                b = a.split('::')
                mib = b[2]
                logger.debug(logger_ip=ip, message=f" Successfully detected  MIB Details of the Host: {ip} ")
                device_check_object=Device_Type()
                resp = device_check_object.get_device_type(mib,host_id=host_id, logger_ip=ip)
                device_type = [d['device_type'] for d in resp if 'device_type' in d]
                platform = [d['platform'] for d in resp if 'platform' in d]
                logger.debug(logger_ip=ip, message=f" Updating Host: {ip} ; Device Type: {device_type[0]} to DB  ")
                host_handler_object = HandleHostDetails()

                if device_type[0] == 'Compute':
                    host_handler_object.update_host_table(device_type="compute", platform=platform[0], host_id=host_id, stage=2, job_entry=2,device_identification_flag=1,logger_ip=ip)
                else:
                    host_handler_object.update_host_table(device_type=device_type[0], platform=platform[0], host_id=host_id, stage=2, job_entry=3,status="success",device_identification_flag=1, logger_ip=ip)

                logger.debug(logger_ip=ip, message=f"Successfully updated device_type details of the Host: {ip} in DB ")

        except Exception as e:
            logger.error(logger_ip=ip, message=f" Failed Get Device Type through SNMP for the Host: {ip} and Updating 'Unknown Device' to DB  Exception : {e} ")
            host_handler_object = HandleHostDetails()
            host_handler_object.update_host_table(device_type="Unknown Device", platform=None, host_id=host_id, stage=2, job_entry=2, logger_ip=ip)


    def snmp_validation(self,ip_address,community):
        try:
            errorIndication, errorStatus, errorIndex, varBinds = next(
                getCmd(SnmpEngine(),
                       CommunityData(community),
                       UdpTransportTarget((ip_address, 161)),
                       ContextData(),
                       ObjectType(ObjectIdentity('SNMPv2-MIB', 'sysObjectID', 0))))

            if errorIndication:
                return False
            elif errorStatus:
                return False

            for varBind in varBinds:
                a = (' = '.join([x.prettyPrint() for x in varBind]))
                b = a.split('::')
                mib = b[2]
                device_check_object=Device_Type()
                resp = device_check_object.get_device_type(mib)
                device_type = [d['device_type'] for d in resp if 'device_type' in d]
                if device_type[0] == 'Compute':
                    return True
                else:
                   return True

        except Exception as e:
            return False