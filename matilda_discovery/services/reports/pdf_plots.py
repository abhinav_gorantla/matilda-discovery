from pathlib import Path
import os
import numpy as np
import pandas as pd
import seaborn as sns
from PyPDF2 import pdf
from matplotlib.pylab import plt

from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db import queries_helper
from matilda_discovery.db.connection import get_engine
from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.services.dashboard import reports_stage_details
from matilda_discovery.services.reports.cover_page import CoverPage, AssessmentCoverPage
from matilda_discovery.utils import util

logger = logger_py_file.Logger()


class PdfPlots:

    def __init__(self, req_data):
        self.name = req_data.get('name')
        self.req_data = req_data
        self.account_id = req_data.get('account_id')
        self.task_id = req_data.get('task_id')
        self.report_id = req_data.get('id')
        self.type = req_data.get('type')
        self.cloud_provider = req_data.get('cloud_provider')
        self.engine = get_engine()

    def create_plotting(self):
        """
        Creates plotting based on data being collected.
        :param: Json Data. It contains the data executed by the queries in summary section
        """
        resp = ""
        try:
            if "Summary Report" in self.type:
                logger.debug("Initiating plotting's for the PDF report")
                # self.createfile()
                self.donut_chart()
                self.stack_bar()
                self.server_text()
                self.body_text()
                cover_obj = CoverPage()
                resp = cover_obj.cover_page(self.req_data)
            if "Cloud Migration Report" in self.type:
                logger.debug("Initiating assessment plotting's for the PDF report")
                # self.createfile()
                self.stack_bar()
                self.database_text()
                self.body_text()
                assessment_cover_obj = AssessmentCoverPage()
                resp = assessment_cover_obj.cover_page(self.req_data)
        except Exception as e:
            logger.error(f"Exception occurred while generating summary report. \n Exception: {e}")
        return resp

    def donut_chart(self):
        """
        Creates donut pie chart and dynamic text for the discovery stages.
        :parm: Using existing Dashboard stages functionality for creating grouped bar chart for stages.
        :return: Generates .png and .txt file.
        """
        try:
            logger.debug(f"Creating grouped bar chart for the account_id: {self.account_id} ")

            resp = reports_stage_details(account_id=self.account_id)
            for item in resp:
                col = [*item]
                labels = [col[1], col[2]]
                values = [item.get(col[1]), item.get(col[2])]
                total = int(item.get(col[0]))
                plt.subplots(figsize=(4, 3))
                plt.subplots_adjust(0.00, 0.15, 0.86, 0.85)
                color = ['#76A1CC', '#0AA2B0', '#05668D', '#733E91', '#66D8FB', '#028090', '#4A7CFA', '#50808E', '#C81D25', '#4A7C59', '#FF5A5F', '#1D3557', '#E63946', '#E2C3FF', '#53FFFE', '#0B3954', '#84B59F']
                plt.axis('equal')
                my_circle = plt.Circle((0, 0), 0.7, color='white')
                plt.pie(values, colors=color, autopct='%1.1f%%', pctdistance=1.18)
                kwargs = dict(size=14, va='center')
                text = "Total" + ":" + str(total)
                plt.text(0, 0, text, ha='center', **kwargs)
                p = plt.gcf()
                p.gca().add_artist(my_circle)
                plt.title(item.get(col[4]), fontsize=12)
                plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), ncol=2, labels=['%s : %1.f' % (l, s) for l, s in zip(labels, values)])
                plt.savefig(str(Path(__file__).parent) + "/plots/" + item.get(col[4]))
                plt.close()
            temp_str_1 = "During the scan we found "
            temp_str_2 = "Out of which, we are able to complete discovery for "
            str_1 = temp_str_1 + str(resp[0].get('Total')) + " devices. "
            percent = (resp[3].get('Discovered') / resp[0].get('Total')) * 100
            str_2 = temp_str_2 + str(resp[3].get('Discovered')) + " devices i.e.," + "(" + str(round(percent, 2)) + "%" + ") successfully."
            text = str_1 + str_2
            with open(str(Path(__file__).parent) + "/plots/stages.txt", 'w+') as fh:
                fh.write(text)
                fh.close()
            logger.debug(f"Successfully created Donut Pie chart for the account_id: {self.account_id} and stored to {CreateInfra.plots.value}")
        except Exception as e:
            logger.error(f"Exception occurred while generating Donut Pie chart. \n Exception: {e}")
        return None

    def stack_bar(self):
        """
        Creates stacked bar charts for PDF Summary Report
        :param: JSON data executed from SQL queries
        :return: Generates a .png file
        """
        try:
            logger.debug(f"Generating stack bar chart for the account_id: {self.account_id}")
            queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section='summary_stack_bar')
            query_response = queries_helper.execute_report_queries(queries, self.engine, info="stack_bar", account_id=self.account_id)
            for item in query_response:
                for info in item.get('query_data'):
                    col = [*info]
                    break
                name, count, type, legend = [], [], [], []
                for x in item.get('query_data'):
                    if x.get(col[0]) not in type:
                        type.append(x.get(col[0]))
                    if x.get(col[1]) not in name:
                        name.append(x.get(col[1]))
                    if x.get(col[1]) and x.get(col[2]):
                        nam = x.get(col[1]) + "  :  " + str(x.get(col[2]))
                        legend.append(nam)
                for x in item.get('query_data'):
                    temp = [0 for range in type]
                    index = type.index(x.get(col[0]))
                    temp[index] = x.get(col[2])
                    count.append(temp)
                data = np.array(count)
                plt.figure(figsize=(15, 8))
                plt.subplots_adjust(right=0.57, left=0.04)
                color = ['#76A1CC', '#0AA2B0', '#05668D', '#733E91', '#66D8FB', '#028090', '#4A7CFA', '#50808E', '#C81D25', '#4A7C59', '#FF5A5F', '#1D3557', '#E63946', '#E2C3FF', '#53FFFE', '#0B3954', '#84B59F']
                if len(type) <= 1:
                    X = np.arange(data.shape[1])
                    highest = 0
                    for i in range(data.shape[0]):
                        plt.bar(X, data[i], bottom=np.sum(data[:i], axis=0), width=0.25, color=color[i % len(color)])
                        highest += data[i]
                    plt.xticks(X, type, fontsize=14)
                    if (max(highest) >= 500) and (max(highest) <= 1000):
                        plt.yticks(np.arange(0, max(highest) + 100, 50))
                    elif (max(highest) >= 300) and (max(highest) <= 500):
                        plt.yticks(np.arange(0, max(highest) + 100, 25))
                    elif (max(highest) >= 100) and (max(highest) <= 300):
                        plt.yticks(np.arange(0, max(highest) + 70, 15))
                    elif (max(highest) >= 1) and (max(highest) <= 100):
                        plt.yticks(np.arange(0, max(highest) + 30, 10))
                    elif (max(highest) >= 1) and (max(highest) <= 50):
                        plt.yticks(np.arange(0, max(highest) + 5, 5))
                    plt.xlim(-1, 1)
                    plt.title(item.get('name'), fontsize=22)
                    plt.legend(legend, loc='center left', bbox_to_anchor=(1, 0.5), fontsize=15)
                    plt.savefig(str(Path(__file__).parent) + "/plots/" + item.get('name'))
                    plt.close()
                else:
                    X = np.arange(data.shape[1])
                    highest = 0
                    for i in range(data.shape[0]):
                        plt.bar(X, data[i], bottom=np.sum(data[:i], axis=0), width=0.25, color=color[i % len(color)])
                        highest += data[i]
                    plt.xticks(X, type, fontsize=14)
                    if (max(highest) >= 500) and (max(highest) <= 1000):
                        plt.yticks(np.arange(0, max(highest) + 80, 50))
                    elif (max(highest) >= 300) and (max(highest) <= 500):
                        plt.yticks(np.arange(0, max(highest) + 60, 25))
                    elif (max(highest) >= 100) and (max(highest) <= 300):
                        plt.yticks(np.arange(0, max(highest) + 40, 15))
                    elif (max(highest) >= 1) and (max(highest) <= 100):
                        plt.yticks(np.arange(0, max(highest) + 30, 10))
                    elif (max(highest) >= 1) and (max(highest) <= 50):
                        plt.yticks(np.arange(0, max(highest) + 5, 5))
                    plt.title(item.get('name'), fontsize=22)
                    plt.legend(legend, loc='center left', bbox_to_anchor=(1, 0.5), fontsize=15)
                    plt.savefig(str(Path(__file__).parent) + "/plots/" + item.get('name'))
                    plt.close()
                logger.debug(f"Successfully generated stack bar chart for the account_id: {self.account_id} and stored to {CreateInfra.plots.value + item.get('name')} ")
        except Exception as e:
            logger.debug(f"Unable to generate staked bar chart for the account id {self.account_id} \n Exception {e}")

    def server_text(self):
        """
        Creates dynamic text for application services
        :param: Data executed from the SQL queries
        :return: Generates .txt file
        """
        try:
            engine = get_engine()
            queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section='server_text_details')
            query_response = queries_helper.execute_dict_of_queries(queries, engine, info="inventory_report", account_id=10)
            temp_str_1 = "In Discovery we have identified"
            for items in query_response['query_1']:
                temp_str_1 = temp_str_1 + ' ' + str(items['type']) + ':' + str(items['server_count']) + ', '
            x = temp_str_1[:-2]
            with open(str(Path(__file__).parent) + "/plots/server.txt", 'w+') as fh:
                fh.write(x)
                fh.close()
        except Exception as e:
            logger.debug(f"Exception {e}")

    def body_text(self):
        """
        Creates dynamic text for Operating system
        :param: Data executed from the SQL queries
        :return: generates .txt file
        """
        try:
            engine = get_engine()
            queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section='os_text_details')
            query_response = queries_helper.execute_dict_of_queries(queries, engine, info="inventory_report", account_id=10)
            temp_str_1 = "In this Datacenter we have "
            temp_str_2 = ". Furthermore, we have "
            for items in query_response['query_1']:
                temp_str_1 = temp_str_1 + str(items['platform_count']) + ' ' + str(items['platform']) + ' systems, '
            for items in query_response['query_2']:
                temp_str_2 = temp_str_2 + ' ' + str(items['operating_system']) + ': ' + str(items['value']) + ', '
            x = (temp_str_1[:-2] + temp_str_2[:-2])
            with open(str(Path(__file__).parent) + "/plots/os.txt", 'w+') as fh:
                fh.write(x)
                fh.close()
        except Exception as e:
            logger.debug(f"Exception {e}")

    def database_text(self):
        """
        Creates dynamic text for application services
        :param: Data executed from the SQL queries
        :return: Generates .txt file
        """
        try:
            engine = get_engine()
            queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section='database_text')
            query_response = queries_helper.execute_dict_of_queries(queries, engine, info="inventory_report", account_id=10)
            temp_str_1 = "In Discovery we have identified"
            for items in query_response['query_1']:
                temp_str_1 = temp_str_1 + ' ' + str(items['name']) + ':' + str(items['service_count']) + ', '
            x = temp_str_1[:-2]
            with open(str(Path(__file__).parent) + "/plots/database.txt", 'w+') as fh:
                fh.write(x)
                fh.close()
        except Exception as e:
            logger.debug(f"Exception {e}")

    def bar_charts(self):
        """
            Creates Bar charts for the PDF Summary Report
            :param: JSON data executed from SQL queries
            :return: .png format files
            """
        try:
            summary_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section='summary_report_bar')
            response = queries_helper.execute_report_queries(summary_queries, self.engine, info="bar_charts", account_id=self.account_id)
            for i in response:
                df1 = pd.DataFrame(i['query_data'])
                col = None
                for item in i['query_data']:
                    col = [*item]
                    break
                labels = col[0]
                value_data = col[1]
                sns.set(style="whitegrid")
                g = sns.barplot(x=labels, y=value_data, hue=labels, data=df1, dodge=False)

                def change_width(g, new_value):
                    for patch in g.patches:
                        current_width = patch.get_width()
                        diff = current_width - new_value
                        # we change the bar width
                        patch.set_width(new_value)
                        # we recenter the bar
                        patch.set_x(patch.get_x() + diff * .5)

                change_width(g, .2)
                plt.savefig(CreateInfra.plots.value + i['name'])
                plt.close()
        except Exception as e:
            logger.error(f"Exception occurred while generating summary report. \n Exception: {e}")
            return None

    def table(self):
        """
         Creating Tables for PDF Summary Reports
         :param: JSON data executed from SQL queries
         :Return: Generates a table
        """
        try:
            queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section='summary_report_table')
            query_response = queries_helper.execute_report_queries(queries, self.engine, info="stack_bar", account_id=self.account_id)
            epw = pdf.w - 2 * pdf.l_margin
            col_width = epw / 3
            for i in query_response:
                for info in i.get('query_data'):
                    col = [*info]
                    break

            ip, name, usedpercent = [], [], []

            for x in i.get('query_data'):
                if x.get(col[0]) not in ip:
                    ip.append(x.get(col[0]))
                if x.get(col[1]) not in name:
                    name.append(x.get(col[1]))
                if x.get(col[0]) not in usedpercent:
                    usedpercent.append(x.get(col[0]))
                    # ip = x.get(col[0])
                    # name = x.get(col[1])
                    # usedpercent = x.get(col[2])

                    print(x.get(col[0]))
            data = [col, [ip[0], name[0], usedpercent[0]], [ip[1], name[1], usedpercent[1]]]
            # epw = pdf.w - 2 * pdf.l_margin
            # col_width = epw / 3

            th = self.font_size

            for row in data:
                for datum in row:
                    self.cell(col_width, 2 * th, str(datum), border=1)

                    self.ln(2 * th)

            self.output('table.pdf')

        except Exception as e:
            logger.error(f"Exception occurred while generating summary report. \n Exception: {e}")
            return None

    def createfile(self):
        # path = CreateInfra.plots.value
        # print((Path(__file__).parent + '/plots/'))
        path = Path(__file__).parent/'plots'
        print(path)
        if not os.path.exists(path):
            os.mkdir(path)
        else:
            logger.debug("Directory already Exists")


# a = PdfPlots(req_data=dict(account_id=10, type="summary_report", name="Matilda_Cloud_Solutions"))
# a.createfile()
