import json
import platform
import pandas as pd
from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db.handler.report import ReportDetails
from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.utils import util

logger = logger_py_file.Logger()


class ExcelReport:

    def __init__(self, req_data):
        self.name = req_data.get('name')
        self.account_id = req_data.get('account_id')
        self.task_id = req_data.get('task_id')
        self.report_id = req_data.get('id')
        self.report_type = req_data.get('type')
        pass

    def creating_xlsx_sheets(self, final_dump):
        """
            This function generates Excel reports
            :param: JSON data from database
        """
        try:
            response = None
            report_details_object = ReportDetails()
            if final_dump:
                logger.debug(f"Preparing File Name of the Report")
                # Preparing unique .xlsx report name by appending current system date and time
                file_name = self.name + "_" + util.get_datetime(req_format=CreateInfra.us_datetime_format.value) + ".xlsx"
                logger.debug(f"Preparing Report Location for Excel Generator")
                # Preparing absolute path with file name to pass as a parameter to create sheets.
                hosted_platform = platform.system()
                directory = None
                if hosted_platform == 'Linux':
                    directory = CreateInfra.linux_reports_path.value
                if hosted_platform == 'Windows':
                    directory = CreateInfra.win_reports_path.value
                util.create_dir_file(directory)
                file = directory + file_name
                # Preparing .xlsx
                logger.debug(f"Preparing Summary Excel Workbook")
                with pd.ExcelWriter(file, engine='xlsxwriter') as writer:
                    col = ""
                    for val in final_dump:
                        for i in val['query_data']:
                            col = [*i]
                            break
                        util.json_formatter(val['query_data'])
                        resp = pd.read_json(json.dumps(val['query_data']))
                        resp.to_excel(writer, sheet_name=val.get("name"), index=False, columns=col, freeze_panes=(1,0))
                        workbook = writer.book
                        worksheet = writer.sheets[val.get("name")]
                        header_format = workbook.add_format({'fg_color': '#D7E4BC', 'bold': True, 'border': 1})
                        for col_num, val in enumerate(resp.columns.values):
                            worksheet.write(0, col_num, val, header_format)
                    writer.save()
                    writer.close()
                logger.debug(f"Successfully Prepared Summary Excel Workbook")
                # Preparing dictionary to update to database.
                type = self.report_type.replace("_", " ")
                req_data = dict(id=self.report_id, account_id=self.account_id, task_id=self.task_id,
                                location=file, type = type.title(), name=self.name, file_name=file_name, created=util.get_datetime(
                        req_format=CreateInfra.us_datetime_format.value), reason="Success", status="Completed")
                logger.debug(f"Updating Report Details to Reports table")
                # Calling SQLAlchemy to update the prepared dictionary.
                response = report_details_object.update_reports(req_data)
                logger.debug(f"**** Successfully Generated Report ****")
            else:
                if self.account_id and self.task_id:
                    logger.debug(f"Unable to fetch Summary Details with provided TASK_ID: {self.task_id}")
                    req_data = dict(id=self.report_id, reason="Task ID: {} not Found".format(self.task_id),
                                    status="Failed")
                    response = report_details_object.update_reports(req_data)
                elif self.account_id:
                    logger.debug(f"Unable to fetch Summary Details with provided Account_ID: {self.task_id}")
                    req_data = dict(id=self.report_id, reason="Account ID:{} not Found".format(self.account_id),
                                    status="Failed")
                    response = report_details_object.update_reports(req_data)
            return response
        except Exception as e:
            logger.debug(f"Exception occurred while generating Excel Report \n Exception {e}")
