import os
import platform
from PyPDF2 import PdfFileMerger
from fpdf import FPDF
import pandas as pd
from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db import queries_helper
from matilda_discovery.db.connection import get_engine
from matilda_discovery.utils import util
from matilda_discovery.logger_engine import logger as logger_py_file
from pathlib import Path

logger = logger_py_file.Logger()

pdf_files = ['titlepage.pdf', 'sample_report.pdf']


class PdfReport(FPDF):

    def header(self):
        """
        This method overrides the base method provided by FPDF to create a customized header
        :param: Matilda Cloud logo in .png format
        """
        try:
            self.image(str(Path(__file__).parent) + "/templates/MatildaCloudLogo.png", 10, 10, 43)
            self.ln(20)
        except Exception as e:
            logger.error(f"Exception while creating header for pdf report. Exception {e}")

    def footer(self):
        """
        This method overrides the base method provided by FPDF to create a customized footer
        """
        try:
            # self.set_xy(0,5)
            self.set_y(-15)
            self.set_font('Arial', 'I', 9)
            self.set_text_color(0, 0, 0)
            # self.cell(1)
            self.cell(0, 5, 'Matilda Cloud 2019 Copyright', 0, 0, 'R')
        except Exception as e:
            logger.error(f"Exception occoured while creating footer for pdf report. Exception {e}")

    def report_generation(self, req_data):
        """
        Creates the pdf report by adding title, images, body.
        :param:
        :return: generates the report in .pdf format
        """
        reports = ""
        if req_data.get('type') == "Summary Report":
            reports = ['overview', 'stages', 'operating system', 'application', 'tables', 'confidential']

        elif req_data.get('type') == "Cloud Migration Report":
            reports = ['os_recommendation', 'middleware_recommendation', 'confidential']
        try:
            for report in reports:

                self.add_page()
                if "overview" in report:
                    self.overview_summary()
                elif "operating system" in report:
                    self.os_summary()
                elif "application" in report:
                    self.applications_summary()
                elif "stages" in report:
                    self.stages_summary()
                elif "os_recommendation" in report:
                    self.os_recommendation(req_data)
                elif "middleware_recommendation" in report:
                    self.middleware_recommendation()
                elif "tables" in report:
                    self.table(req_data)
                elif "confidential" in report:
                    self.confidential()
            self.output(str(Path(__file__).parent) + "/plots/sample_report.pdf")
            self.close()
            response = self.file(req_data)
            return response
        except Exception as e:
            logger.error(f"Exception occurred during sample report creation. \n Exception {e} ")

    def report_title(self, label, yaxis):
        """
        Creates a title names in the PDF Summary report
        :param: uses FPDF module to set font, text color, fill color and cell
        :return: it returns the title names for PDF Summary report
        """
        try:

            # Arial 12
            self.set_font('Helvetica', '', 18)
            # # Text Color
            self.set_text_color(58, 95, 205)
            # self.set_text_color(72, 61, 139)
            # Background color
            self.set_fill_color(255, 255, 255)
            # Title
            self.cell(0, yaxis, label, 0, 1, 'L', 0)
            # Line break
            self.ln(4)
        except Exception as e:
            logger.error(f"Exception occurred during title creation. \n Exception {e} ")
        return None

    def report_body(self, yaxis, name):
        """
        Creates body(text) in the PDF Summary report.
        :param: uses FPDF module to set font, text color, fill color and cell
        :return: It returns body(lines of text) to PDF Summary report
        """
        try:
            # Read text file
            if name:
                with open(name, 'rb') as fh:
                    txt = fh.read().decode('latin-1')
                self.set_font('Helvetica', '', 12)
                self.set_text_color(0, 0, 0)
                self.multi_cell(0, yaxis, txt)
                self.ln()
        except Exception as e:
            logger.error(f"Exception occurred during Body(text) creation. \n Exception {e} ")
        return None

    def body_multiple(self, yaxis, name):
        """
        Creates body(text) in the PDF Summary report.
        :param: uses FPDF module to set font, text color, fill color and cell
        :return: It returns body(lines of text) to PDF Summary report
        """
        try:
            # Read text file
            if name:
                with open(name, 'rb') as fh:
                    txt = fh.read().decode('latin-1')
                self.set_font('Helvetica', '', 8)
                self.set_text_color(0, 0, 0)
                self.multi_cell(0, yaxis, txt)
                self.ln()
        except Exception as e:
            logger.error(f"Exception occurred during Body(text) creation. \n Exception {e} ")
        return None

    def overview_summary(self):
        """
        Creates a page for overview_summary in the PDF report
        :param: Calls the function report_title, report_body and FPDF module to set the image
        :return: Returns a page with title, text and image.
        """
        try:
            self.report_title(yaxis=5, label="Summary")

            self.report_body(yaxis=8, name=str(Path(__file__).parent) + "/templates/summary.txt")
        except Exception as e:
            logger.error(f"Exception occurred while creating overview_summary. Exception {e}")
        return None

    def stages_summary(self):
        """
        Creates a page for stages_summary in the PDF report
        :param: Calls the function report_title, report_body and FPDF module to set the image
        :return: Returns a page with title, text and image.
        """
        try:

            self.report_title(yaxis=5, label="Stages Summary")
            self.report_body(yaxis=5, name=str(Path(__file__).parent) + "/plots/stages.txt")
            self.image(name=str(Path(__file__).parent) + "/plots/Ping.png", x=8, y=65, w=115, h=90)
            self.image(name=str(Path(__file__).parent) + "/plots/Device Identification.png", x=100, y=65, w=115, h=90)
            self.image(name=str(Path(__file__).parent) + "/plots/Login.png", x=7, y=180, w=115, h=90)
            self.image(name=str(Path(__file__).parent) + "/plots/Discovery.png", x=101, y=180, w=115, h=90)
        except Exception as e:
            logger.error(f"Exception occurred while creating stages summary. Exception{e}")
        return None

    def os_summary(self):
        """
        Creates a page for OS_Summary in the PDF report
        :param: Calls the function report_title, report_body and FPDF module to set the image
        :return: Returns a page with title, text and image.
        """
        try:

            self.report_title(yaxis=5, label="Platform Summary ")
            self.report_body(yaxis=5, name=str(Path(__file__).parent) + "/plots/os.txt")
            self.image(name=str(Path(__file__).parent) + "/plots/Operating Systems.png", x=27, y=55, w=170, h=80)
            pdf = PdfReport()
            self.report_title(yaxis=90, label="")
            self.report_title(yaxis=0, label="Recommendations")
            list = []
            # self.set_font('Times', '', 10.0)
            # epw = pdf.w - 2.2 * pdf.l_margin
            # col_width = epw / 4
            engine = get_engine()
            queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value,
                                          section='os_recommendation_table')
            query_response = queries_helper.execute_report_queries(queries, engine, info="os_table", account_id=10)
            # for item in query_response:
            #     for info in item.get('query_data'):
            #         col = [*info]
            #         break
            # for item in query_response:
            #     df = pd.DataFrame(item.get('query_data'))
            #     list.append(['Operating_System', 'Version', 'Support_Date','Recommendation'])
            #     for index, row in df.iterrows():
            #         list.append([row[col[0]], row[col[1]], row[col[2]], row[col[3]]])
            #     page_height = self.get_y() + 1
            #     self.set_font('Helvetica', '', 7)
            #     self.set_line_width(.1)
            #     # self.set_auto_page_break(True, 20.0)
            #     self.accept_page_break()
            #     bottom_margin = 20
            #     widths = [35, 55, 30, 40]
            #     line_height = 4
            #     row_height = 2 * line_height
            #     y = page_height
            #     count = 0
            #     h = 20
            #     for row in list:
            #         th = self.font_size
            #         x = self.l_margin
            #         y += row_height
            #         count += 1
            #         for i in range(0, 4):
            #             self.set_xy(x, y)
            #
            #             if count==1:
            #             # if count <= i:
            #             #     count += 1
            #                 self.set_text_color(0, 0, 0)
            #                 self.set_fill_color(70, 130, 180)
            #                 # pdf.rect(x, y, widths[i], row_height)
            #                 self.set_font('Times', '', 11)
            #                 self.multi_cell(widths[i], 3.2 * th, str(row[i]), 1, 'L', fill=1)
            #             elif self.get_y() + h > self.page_break_trigger:
            #                 # if y > (page_height - bottom_margin - row_height):
            #                         # self.add_page()
            #
            #                     print(self.set_y(150))
            #                     y = pdf.get_y()
            #                     print(self.get_y())
            #                     self.add_page()
            #                     print("fnsaf")
            #                     # self.set_xy(x, y)
            #                     self.set_text_color(0, 0, 0)
            #                     self.set_font('Times', '', 9)
            #                     print(widths[i])
            #                     self.rect(x, y, widths[i], row_height)
            #                     print(row[i])
            #                     self.multi_cell(widths[i], line_height, str(row[i]), 0, 'L', fill=0)
            #             # elif self.get_y() + h > self.page_break_trigger:
            #             #     self.add_page()
            #             #     self.set_xy(x, y)
            #             #     self.set_text_color(0, 0, 0)
            #             #     self.set_font('Times', '', 9)
            #             #     self.rect(x, y, widths[i], row_height)
            #             #     self.multi_cell(widths[i], line_height, str(row[i]), 0, 'L', fill=0)
            #             #     self.set_xy(x, y)
            #             else:
            #                 self.set_xy(x, y)
            #                 self.set_text_color(0, 0, 0)
            #                 self.set_font('Times', '', 9)
            #                 self.rect(x, y, widths[i], row_height)
            #                 self.multi_cell(widths[i], line_height, str(row[i]), 0, 'L', fill=0)
            #
            #             if i < len(widths):
            #                 x += widths[i]
            #     self.ln(2)
            # self.ln(2)
            for item in query_response:
                for info in item.get('query_data'):
                    col = [*info]
                    break
            for item in query_response:
                df = pd.DataFrame(item.get('query_data'))
                list.append(
                    ['OperatingSystem', 'Version', 'SupportDate', 'EOSL', 'MigrationStrategy', 'Recommendation'])
                # check = item.get('name')
                for index, row in df.iterrows():
                    list.append([row[col[0]], row[col[1]], row[col[2]], row[col[3]], row[col[4]], row[col[5]]])
                    th = self.font_size
                    self.set_text_color(0, 0, 0)
                count = 0
                self.set_font('Times', '', 9)
                self.set_text_color(0, 0, 0)
                self.set_fill_color(255, 255, 255)
                widths = [26, 49, 20, 15, 26, 55]
                x = self.l_margin
                # self.cell(0, 6, check, 0, 1, 'L', 1)
                for row in list:
                    count += 1
                    for i in range(0, 6):
                        if count == 1:
                            # self.set_fill_color(128)
                            self.set_fill_color(70, 130, 180)
                            self.cell(widths[i], 1.1 * th, str(row[i]), border=1, fill=1)
                        else:
                            self.cell(widths[i], 1.1 * th, str(row[i]), border=1, fill=0)
                        if i < len(widths):
                            x += widths[i]
                    self.ln(1.1 * th)
                self.ln(1.1 * th)
                list.clear()

        except Exception as e:
            logger.error(f"Exception occurred while creating os summary pdf report. Exception {e}")

        return

    def applications_summary(self):
        """
        Creates a page for applications_summary in the PDF report
        :param: Calls the function report_title, report_body and FPDF module to set the image
        :return: Returns a page with title, text and image.
        """
        try:

            self.report_title(yaxis=5, label="Middleware Summary ")
            self.report_body(yaxis=5, name=str(Path(__file__).parent) + "/plots/server.txt")
            self.image(name=str(Path(__file__).parent) + "/plots/Servers.png", x=27, y=55, w=170,
                       h=80)
            pdf = PdfReport()
            self.report_title(yaxis=90, label="")
            self.report_title(yaxis=0, label="Recommendations")
            list = []
            # self.set_font('Times', '', 10.0)
            # epw = pdf.w - 2.2 * pdf.l_margin
            # col_width = epw / 5
            engine = get_engine()
            queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value,
                                          section='server_recommendation_table')
            query_response = queries_helper.execute_report_queries(queries, engine, info="server table", account_id=10)
            # for item in query_response:
            #     for info in item.get('query_data'):
            #         col = [*info]
            #         break
            # for item in query_response:
            #     df = pd.DataFrame(item.get('query_data'))
            #     list.append(['Type', 'Name', 'Version', 'Support_Date', 'Recommendation'])
            #     for index, row in df.iterrows():
            #         list.append([row[col[0]], row[col[1]], row[col[2]], row[col[3]], row[col[4]]])
            #     page_height = self.get_y() + 1
            #     self.set_font('Helvetica', '', 7)
            #     self.set_line_width(.1)
            #     self.set_auto_page_break(False)
            #     widths = [40, 50, 25, 25, 30]
            #     line_height = 4
            #     row_height = 2 * line_height
            #     y = page_height
            #     count = 0
            #     max = 0
            #     for row in list:
            #         th = self.font_size
            #         x = self.l_margin
            #         y += row_height
            #         count +=1
            #         # max += 1
            #         for i in range(0, 5):
            #             self.set_xy(x, y)
            #             if count==1:
            #             # if count <= i:
            #             #     count += 1
            #                 self.set_text_color(0, 0, 0)
            #                 self.set_fill_color(70, 130, 180)
            #                 # pdf.rect(x, y, widths[i], row_height)
            #                 self.set_font('Times', '', 11)
            #                 self.multi_cell(widths[i], 3.2 * th, str(row[i]), 1, 'L', fill=1)
            #
            #             else:
            #                 self.set_text_color(0, 0, 0)
            #                 self.set_font('Times', '', 9)
            #                 self.rect(x, y, widths[i], row_height)
            #                 self.multi_cell(widths[i], line_height, str(row[i]), 0, 'L', fill=0)
            #             if i < len(widths):
            #                 x += widths[i]
            for item in query_response:
                for info in item.get('query_data'):
                    col = [*info]
                    break
            for item in query_response:
                df = pd.DataFrame(item.get('query_data'))
                list.append(['Type', 'Service', 'SupportDate', 'EOSL', 'MigrationStrategy', 'Recommendation'])
                # check = item.get('name')
                for index, row in df.iterrows():
                    list.append([row[col[0]], row[col[1]], row[col[2]], row[col[3]], row[col[4]], row[col[5]]])
                    th = self.font_size
                    self.set_text_color(0, 0, 0)
                count = 0
                self.set_font('Times', '', 9)
                self.set_text_color(0, 0, 0)
                self.set_fill_color(255, 255, 255)
                widths = [35, 35, 20, 20, 30, 45]
                x = self.l_margin
                max_per_page = 10
                # self.cell(0, 6, check, 0, 1, 'L', 1)
                for row in list:
                    count += 1
                    for i in range(0, 6):
                        if count == 1:
                            # self.set_fill_color(128)
                            self.set_fill_color(70, 130, 180)
                            self.cell(widths[i], 1.2 * th, str(row[i]), border=1, fill=1)
                        else:
                            self.cell(widths[i], 1.2 * th, str(row[i]), border=1, fill=0)
                    self.ln(1.2 * th)
                self.ln(1.2 * th)
                list.clear()
        except Exception as e:
            logger.error(f"Exception occurred while cresting application summary report. Exception {e}")

        return None

    def os_recommendation(self, req_data):
        try:
            self.report_title(yaxis=5, label="Platform Overview")
            self.report_body(yaxis=5, name=str(Path(__file__).parent) + "/plots/os.txt")
            self.image(name=str(Path(__file__).parent) + "/plots/Operating Systems.png", x=27, y=55, w=170, h=80)
            pdf = PdfReport()
            self.report_title(yaxis=80, label="")
            self.report_title(yaxis=0, label="Migration Analysis")
            list = []
            engine = get_engine()
            queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value,
                                          section='assesment_summary_os_recommendation_table')
            query_response = queries_helper.execute_report_queries(queries, engine, info="OS Migration Analysis",
                                                                   account_id=10,
                                                                   cloud_provider=req_data.get('cloud_provider'))
            # for item in query_response:
            #             #     for info in item.get('query_data'):
            #             #         col = [*info]
            #             #         break
            #             # for item in query_response:
            #             #     df = pd.DataFrame(item.get('query_data'))
            #             #     list.append(['Platform', 'OperatingSystem','Count','Compatible', 'MigrationStrategy', 'Recommendation'])
            #             #     for index, row in df.iterrows():
            #             #         list.append([row[col[0]], row[col[1]], row[col[2]], row[col[3]], row[col[4]], row[col[5]]])
            #             #     page_height = self.get_y() + 1
            #             #     self.set_font('Helvetica', '', 7)
            #             #     self.set_line_width(.1)
            #             #     self.set_auto_page_break(False)
            #             #     widths = [28, 40, 20, 23, 32, 35]
            #             #     line_height = 4
            #             #     row_height = 2 * line_height
            #             #     y = page_height
            #             #     count = 0
            #             #     for row in list:
            #             #         print(f"ROW{row}")
            #             #         th = self.font_size
            #             #         x = self.l_margin
            #             #         y += row_height
            #             #         for i in range(0, 6):
            #             #             self.set_xy(x, y)
            #             #             if count <= i:
            #             #                 count += 1
            #             #                 self.set_text_color(0, 0, 0)
            #             #                 self.set_fill_color(70, 130, 180)
            #             #                 # pdf.rect(x, y, widths[i], row_height)
            #             #                 self.set_font('Times', '', 11)
            #             #                 self.multi_cell(widths[i], 3.2 * th, str(row[i]), 1, 'L', fill=1)
            #             #             else:
            #             #                 self.set_text_color(0, 0, 0)
            #             #                 self.set_font('Times', '', 9)
            #             #                 self.rect(x, y, widths[i], row_height)
            #             #                 self.multi_cell(widths[i], line_height, str(row[i]), 0, 'L', fill=0)
            #             #             if i < len(widths):
            #             #                 x += widths[i]
            for item in query_response:
                for info in item.get('query_data'):
                    col = [*info]
                    break
            for item in query_response:
                df = pd.DataFrame(item.get('query_data'))
                list.append(
                    ['Platform', 'OperatingSystem', 'Count', 'Compatible', 'MigrationStrategy', 'Recommendation'])
                # check = item.get('name')
                for index, row in df.iterrows():
                    list.append([row[col[0]], row[col[1]], row[col[2]], row[col[3]], row[col[4]], row[col[5]]])
                    th = self.font_size
                    self.set_text_color(0, 0, 0)
                count = 0
                self.set_font('Times', '', 9)
                self.set_text_color(0, 0, 0)
                self.set_fill_color(255, 255, 255)
                widths = [20, 65, 18, 22, 28, 40]
                x = self.l_margin
                # self.cell(0, 6, check, 0, 1, 'L', 1)
                for row in list:
                    count += 1
                    for i in range(0, 6):
                        if count == 1:
                            # self.set_fill_color(128)
                            self.set_fill_color(70, 130, 180)
                            self.cell(widths[i], 1.1 * th, str(row[i]), border=1, fill=1)
                        else:
                            self.cell(widths[i], 1.1 * th, str(row[i]), border=1, fill=0)
                        if i < len(widths):
                            x += widths[i]
                    self.ln(1.1 * th)
                self.ln(1.1 * th)
                list.clear()
        except Exception as e:
            logger.error(f"Exception occurred while creating OS Recommmedation pdf report. Exception {e}")

    def middleware_recommendation(self):
        self.report_title(yaxis=5, label="Database Overview")
        self.report_body(yaxis=5, name=str(Path(__file__).parent) + "/plots/database.txt")
        self.image(name=str(Path(__file__).parent) + "/plots/Database.png", x=27, y=55, w=170, h=80)

    def hostdistribution(self):
        # self.add_page()
        self.report_title(label="HostDistribution Summary")
        self.report_body(name="C:\\Users\\Spoorthy27\\PycharmProjects\\example1\\fp_c2.txt")
        self.image(name='C:\\Users\\Spoorthy27\\PycharmProjects\\example1\\Region_bar.jpg', x=0, y=50, w=150,
                   h=90)
        self.image(name='C:\\Users\\Spoorthy27\\PycharmProjects\\example1\\DataCenter_bar.jpg', x=0, y=150, w=150,
                   h=90)
        self.add_page()
        self.image(name='C:\\Users\\Spoorthy27\\PycharmProjects\\example1\\Line_of_business_bar.jpg', x=0, y=40, w=150,
                   h=90)
        self.image(name='C:\\Users\\Spoorthy27\\PycharmProjects\\example1\\Project_bar.jpg', x=0, y=160, w=150,
                   h=90)
        return None

    def devices(self):
        self.report_title(label="Devices Summary")
        self.report_body(name="C:\\Users\\Spoorthy27\\PycharmProjects\\example1\\fp_c2.txt")
        self.image(name='C:\\Users\\Spoorthy27\\PycharmProjects\\example1\\Devices_pie.jpg', x=0, y=55, w=150,
                   h=120)

        return None

    def table(self, req_data):
        """
         Creating Tables for PDF Summary Reports
         :param: JSON data executed from SQL queries
         :Return: Generates a table
        """
        try:
            pdf = PdfReport()
            self.report_title(yaxis=5, label="Utilization Summary")
            self.report_body(yaxis=5, name=str(Path(__file__).parent) + "/templates/Utilizations_txt.txt")
            list = []
            # self.add_page()
            self.set_font('Times', '', 10.0)
            epw = pdf.w - 2 * pdf.l_margin
            col_width = epw / 3

            engine = get_engine()
            queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value,
                                          section='summary_report_table')
            query_response = queries_helper.execute_report_queries(queries, engine, info="stack_bar", account_id=10)
            # print(f"Q RESP {query_response}")
            # check = query_response.__getattribute__("name")
            # print(check)
            for item in query_response:
                for info in item.get('query_data'):
                    col = [*info]
                    break
            for item in query_response:
                df = pd.DataFrame(item.get('query_data'))
                list.append(['Ip', 'Name', 'Used_Percent'])
                check = item.get('name')
                for index, row in df.iterrows():
                    list.append([row[col[0]], row[col[1]], row[col[2]]])
                    th = self.font_size
                    self.set_text_color(0, 0, 0)
                count = 0
                self.set_font('Times', '', 9)
                self.set_text_color(0, 0, 0)
                self.set_fill_color(255, 255, 255)
                self.cell(0, 6, check, 0, 1, 'L', 1)
                for row in list:
                    count += 1
                    for datum in row:
                        if count == 1:
                            # self.set_fill_color(128)
                            self.set_fill_color(70, 130, 180)
                            self.cell(col_width, 2 * th, str(datum), border=1, fill=1)
                        else:
                            self.cell(col_width, 2 * th, str(datum), border=1, fill=0)
                    self.ln(2 * th)
                self.ln(2 * th)
                list.clear()

        except Exception as e:
            logger.error(f"Exception occurred while creating Table data. Exception {e}")

    def confidential(self):
        self.report_title(yaxis=5, label="Confidentiality and Intellectual Property Rights")
        self.body_multiple(yaxis=5, name=str(Path(__file__).parent) + "/templates/confidential.txt")

    def file(self, req_data):
        """
        Creates a Merge PDF Summary Report
        :Param: appends two pdf reports
        :Return: Returns original PDF Generated Summary Report
        """
        try:
            merger = PdfFileMerger()
            for files in pdf_files:
                merger.append(str(Path(__file__).parent) + "/plots/" + files)
            hosted_platform = platform.system()
            directory = ""
            if hosted_platform == 'Linux':
                directory = CreateInfra.linux_reports_path.value
            if hosted_platform == 'Windows':
                directory = CreateInfra.win_reports_path.value
            util.create_dir_file(directory)
            file_name = req_data.get('name') + "_" + util.get_datetime(
                req_format=CreateInfra.report_datetime_format.value) + ".pdf"
            file = directory + file_name
            if not os.path.exists(file):
                merger.write(file)
                merger.close()
            return dict(location=file, file_name=file_name)
        except Exception as e:
            logger.error(f"Exception occurred while merging report. Exception {e}")

# a = PdfReport()
