import os
import shutil
from fpdf import FPDF
from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.services.reports.pdf_report import PdfReport
from pathlib import Path
from matilda_discovery.utils import util

logger = logger_py_file.Logger()


class CoverPage(FPDF):

    def header(self):
        """
        This method overrides the base method provided by FPDF to create a customized header
        parm: Cover page template in .jpg format
        parm: Matilda Cloud logo in .png format
        """
        try:
            logger.debug(f"Adding header for cover page in Summary Report")

            self.image(str(Path(__file__).parent) + "/templates/CoverPage.jpg", 0, 0, 210, 297)
            self.image(str(Path(__file__).parent) + "/templates/MatildaCloudLogo.png", 15, 10, 43)

            self.set_font('Arial', '', 10)
            self.cell(60)
            self.set_text_color(255, 255, 255)
            self.cell(0, 110, txt=+ 30*' '+'Discovery Summary Report Generated on ', ln=1)
            self.cell(0, -90, txt=+ 53*' ' + util.get_datetime(req_format=CreateInfra.cover_page_date_format.value), ln=1, align="C")

            self.set_font('Helvetica', '', 25)
            self.set_text_color(58, 95, 205)
            self.cell(0, 160, txt=+ 7 * ' ' + "Discovery", ln=1, align="C")
            self.cell(0, -130, txt=+ 7 * ' ' + "Summary", ln=1, align="C")
            self.cell(0, 160, txt=+ 3 * ' ' + "Report", ln=1, align="C")
        except Exception as e:
            logger.debug(f"Exception occurred while adding header on PDF. \n EXCEPTION {e} ")

    def footer(self):
        """
        This method overrides the base method provided by FPDF to create a customized footer
        """
        try:
            logger.debug(f"Adding footer for cover page in Summary Report")
            self.set_text_color(128)
            self.set_y(-15)
            self.set_font('Arial', 'I', 8)
            self.set_text_color(128)
            # self.cell(1)
            self.cell(0, 5, 'Matilda Cloud 2019 Copyright', 0, 0, 'R')
        except Exception as e:
            logger.debug(f"Exception occurred while adding footer on PDF. \n EXCEPTION {e} ")

    def cover_page(self, req_data):
        """
        Creates a cover page for Pdf Summary Report
        return: Cover page in .pdf format
        """
        response = ""
        try:
            logger.debug(f"Creating cover page for Summary Report")
            self.add_page()
            self.output(str(Path(__file__).parent) + "/plots/titlepage.pdf")
            self.close()
            report_obj = PdfReport()
            response = report_obj.report_generation(req_data)
            # self.deletefile()

        except Exception as e:
            logger.debug(f"Exception occurred during cover page creation. \n Exception {e} ")
        return response

    def deletefile(self):
        """
        Deletes the folder after PDF Summary report is created
        :Param: Used CreateInfra for the path of the folder
        :Return: Empty Folder
        """
        try:
            logger.debug(f"Deleting the plots folder")
            for file_object in os.listdir(CreateInfra.plots.value):
                file_object_path = os.path.join(CreateInfra.plots.value, file_object)
                if os.path.isfile(file_object_path) or os.path.islink(file_object_path):
                    os.unlink(file_object_path)
                else:
                    shutil.rmtree(file_object_path)
        except Exception as e:
            logger.debug(f"Exception occurred while deleting the temporary files for PDF. \n EXCEPTION {e} ")
        return


class AssessmentCoverPage(FPDF):

    def header(self):
        """
        This method overrides the base method provided by FPDF to create a customized header
        parm: Cover page template in .jpg format
        parm: Matilda Cloud logo in .png format
        """
        try:
            logger.debug(f"Adding header for cover page in Summary Report")

            self.image(str(Path(__file__).parent) + "/templates/CoverPage.jpg", 0, 0, 210, 297)
            self.image(str(Path(__file__).parent) + "/templates/MatildaCloudLogo.png", 15, 10, 43)
            self.set_font('Arial', '', 10)
            self.cell(60)
            self.set_text_color(255, 255, 255)
            self.cell(0, 110, txt=+ 30 * ' ' + 'Cloud Migration Report Generated on ', ln=1)
            self.cell(0, -90, txt=+ 53 * ' ' + util.get_datetime(req_format=CreateInfra.cover_page_date_format.value), ln=1, align="C")

            self.set_font('Helvetica', '', 25)
            self.set_text_color(58, 95, 205)
            self.cell(0, 160, txt=+ 2 * ' ' + "Cloud", ln=1, align="C")
            self.cell(0, -130, txt=+ 7 * ' ' + "Migration", ln=1, align="C")
            self.cell(0, 160, txt=+ 3 * ' ' + "Report", ln=1, align="C")
        except Exception as e:
            logger.debug(f"Exception occurred while adding header on PDF. \n EXCEPTION {e} ")

    def footer(self):
        """
        This method overrides the base method provided by FPDF to create a customized footer
        """
        try:
            logger.debug(f"Adding footer for cover page in Summary Report")
            self.set_text_color(128)
            self.set_y(-15)
            self.set_font('Arial', 'I', 8)
            self.set_text_color(128)
            # self.cell(1)
            self.cell(0, 5, 'Matilda Cloud 2019 Copyright', 0, 0, 'R')
        except Exception as e:
            logger.debug(f"Exception occurred while adding footer on PDF. \n EXCEPTION {e} ")

    def cover_page(self, req_data):
        """
        Creates a cover page for Pdf Summary Report
        return: Cover page in .pdf format
        """
        response = ""
        try:
            logger.debug(f"Creating cover page for Summary Report")
            self.add_page()
            self.output(str(Path(__file__).parent) + "/plots/titlepage.pdf")
            self.close()
            report_obj = PdfReport()
            response = report_obj.report_generation(req_data)
            # self.deletefile()

        except Exception as e:
            logger.debug(f"Exception occurred during cover page creation. \n Exception {e} ")
        return response

    def deletefile(self):
        """
        Deletes the folder after PDF Summary report is created
        :Param: Used CreateInfra for the path of the folder
        :Return: Empty Folder
        """
        try:
            logger.debug(f"Deleting the plots folder")

            for file_object in os.listdir(CreateInfra.plots.value):

                file_object_path = os.path.join(CreateInfra.plots.value, file_object)
                if os.path.isfile(file_object_path) or os.path.islink(file_object_path):
                    os.unlink(file_object_path)
                else:
                    shutil.rmtree(file_object_path)
        except Exception as e:
            print(f" EXCEPTION during deletion {e}")
        return
