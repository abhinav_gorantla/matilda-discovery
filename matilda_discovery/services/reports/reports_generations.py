import platform
import shutil

from flask import send_file

from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db import queries_helper
from matilda_discovery.db.connection import get_engine
from matilda_discovery.db.handler.report import ReportDetails
from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.services.reports.excel_reports import ExcelReport
from matilda_discovery.services.reports.pdf_plots import PdfPlots
from matilda_discovery.utils import util

logger = logger_py_file.Logger()


# This class is used to generate summary reports for a given Account_ID and/or Task_ID
class Reports:

    def __init__(self, req_data=None):
        self.req_data = req_data
        self.name = req_data.get('name')
        self.account_id = req_data.get('account_id')
        self.task_id = req_data.get('task_id')
        self.report_id = req_data.get('id')
        self.report_type = req_data.get('type')
        self.cloud_provider = req_data.get('cloud_provider')
        self.engine = get_engine()

    def reports_generation(self):
        """
        This function is used to generate report based on the type provided by the user.
        :parm: report type -
        :parm: account_id or task_id -
        :return: None
        """
        try:
            report_details_object = ReportDetails()

            if "Inventory Report" in self.report_type:
                try:
                    logger.debug(f"Fetching inventory queries to generate Inventory Report")
                    report_details_object.update_reports(
                        req_data=dict(id=self.report_id, status="Generating Report", format="Excel"))
                    inventory_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value,
                                                            section='inventory_report')
                    query_response = queries_helper.execute_report_queries(inventory_queries, self.engine,
                                                                           info="inventory_report",
                                                                           account_id=self.account_id,
                                                                           task_id=self.task_id)
                    excel_report_object = ExcelReport(req_data=self.req_data)
                    excel_report_object.creating_xlsx_sheets(query_response)
                    logger.debug("**** Successfully Generated Inventory Report ****")
                    return query_response
                except Exception as e:
                    logger.error(f"Exception Occurred Generating Inventory Report. Exception {e} ")
                    return None

            elif "Cloud Consolidation Report" in self.report_type:
                try:
                    logger.debug(f"Fetching cloud consolidation queries to generate Cloud Consolidation Report ")
                    report_details_object.update_reports(
                        req_data=dict(id=self.report_id, status="Generating Report", format="Excel"))
                    cloud_consolidation_queries = util.get_properties(
                        property_file_path=CreateInfra.db_queries_path.value, section='consolidation_report')
                    query_response = queries_helper.execute_report_queries(cloud_consolidation_queries, self.engine,
                                                                           info="cloud_consolidation_report",
                                                                           account_id=self.account_id,
                                                                           task_id=self.task_id,
                                                                           cloud_provider=self.cloud_provider)
                    excel_report_object = ExcelReport(req_data=self.req_data)
                    excel_report_object.creating_xlsx_sheets(query_response)
                    logger.debug("**** Successfully Generated Cloud Consolidation Report ****")
                    return query_response
                except Exception as e:
                    logger.error(f"Exception Occurred Generating Cloud Consolidation Report. Exception {e} ")
                    return None

            if "Summary Report" in self.report_type:
                try:
                    logger.debug(f"Fetching summary queries for generating Summary Report")
                    report_details_object.update_reports(
                        req_data=dict(id=self.report_id, status="Generating Report", format="Pdf"))
                    pdf_report_object = PdfPlots(self.req_data)
                    resp = pdf_report_object.create_plotting()
                    # data = dict(id=self.report_id, account_id=self.account_id, type=type.title(), task_id=self.task_id, location=resp.get('location'), name=self.name, file_name=resp.get('file_name'), created=util.get_datetime(req_format=CreateInfra.us_datetime_format.value), reason="Success", status="Completed")
                    data = dict(id=self.report_id, account_id=self.account_id, type=self.report_type,
                                task_id=self.task_id, location=resp.get('location'), name=self.name,
                                file_name=resp.get('file_name'),
                                created=util.get_datetime(req_format=CreateInfra.us_datetime_format.value),
                                reason="Success", status="Completed")
                    report_details_object.update_reports(data)
                    logger.debug("**** Successfully Generated Summary Report ****")
                except Exception as e:
                    logger.error(f"Exception Occurred Generating Summary Report. Exception {e} ")
                return None

            if "Cloud Migration Report" in self.report_type:
                try:
                    logger.debug(f"Fetching summary queries for generating Summary Report")
                    report_details_object.update_reports(
                        req_data=dict(id=self.report_id, status="Generating Report", format="Pdf"))
                    pdf_report_object = PdfPlots(self.req_data)
                    resp = pdf_report_object.create_plotting()
                    # data = dict(id=self.report_id, account_id=self.account_id, type=type.title(), task_id=self.task_id, location=resp.get('location'), name=self.name, file_name=resp.get('file_name'), created=util.get_datetime(req_format=CreateInfra.us_datetime_format.value), reason="Success", status="Completed")
                    data = dict(id=self.report_id, account_id=self.account_id, type=self.report_type,
                                task_id=self.task_id,
                                location=resp.get('location'), name=self.name, file_name=resp.get('file_name'),
                                created=util.get_datetime(req_format=CreateInfra.us_datetime_format.value),
                                reason="Success", status="Completed")
                    report_details_object.update_reports(data)
                    logger.debug("**** Successfully Generated Cloud Migration Report ****")
                except Exception as e:
                    logger.error(f"Exception occurred while generating Cloud Migration Report. Exception {e}")
                return None

            if "Raw Report" in self.report_type:
                """
                This function is used to generate RAW Report as Zip file as a download.
                """
                try:
                    logger.debug(f"Compressing RAW report folder")
                    report_details_object.update_reports(
                        req_data=dict(id=self.report_id, status="Generating RAW Report", format="Zip"))
                    resp = ""
                    file_name = ""
                    hosted_platform = platform.system()
                    if hosted_platform == 'Linux':
                        resp = shutil.make_archive(CreateInfra.linux_raw_data_path.value, 'zip',
                                                   CreateInfra.linux_raw_data_path.value)
                        file_name = "raw_data_archieve.zip"
                    if hosted_platform == 'Windows':
                        resp = shutil.make_archive(CreateInfra.win_raw_data_path.value, 'zip',
                                                   CreateInfra.win_raw_data_path.value)
                        file_name = "RawDataArchieve.zip"
                    type = self.report_type.replace("_", " ")
                    data = dict(id=self.report_id, account_id=self.account_id, type=type.title(), task_id=self.task_id,
                                location=resp, name=self.name, file_name=file_name,
                                created=util.get_datetime(req_format=CreateInfra.us_datetime_format.value),
                                reason="Success", status="Completed")
                    report_details_object.update_reports(data)
                    logger.debug("**** Successfully Generated RAW Report ****")
                except Exception as e:
                    logger.error(f"Exception occurred while generating RAW Report. \n EXCEPTION: {e} ")
        except Exception as e:
            logger.error(f"Exception occurred while preparing report {self.report_type}. \n EXCEPTION : {e}")
            return None


class ReportsListGeneration:

    def __init__(self):
        pass

    def get_reports_list(self, req_data):
        """
        fetches the reports list from db
        :return:
        """
        try:
            logger.debug("Fetching reports from DB")
            report_details_object = ReportDetails()
            reports_list = report_details_object.get_reports_list(req_data)
            list = []
            for item in reports_list:
                report = dict(id=item['id'], type=item['type'], format=item['format'], location=item['location'],
                              name=item['name'], file_name=item['file_name'],
                              created=item['created'], account_id=item['account_id'], task_id=item['task_id'],
                              reason=item['reason'], status=item['status'])
                list.append(report)
            util.json_formatter(list)
            return list
        except Exception as e:
            logger.error(
                f"Exception occurred while preparing report list from the report data obtained from DB. Error: {e}")
            return None

    def download_report(self, req_data):
        """
        fetched the local location from db and returns the file to download.
        :return:
        """
        try:
            logger.debug("fetching local location of a given id")
            report_details_object = ReportDetails()
            reports_list = report_details_object.get_report_location(req_data)
            location = [item['location'] for item in reports_list]
            return send_file(location[0], as_attachment=True)
        except Exception as e:
            logger.error(
                f"Exception occurred while preparing report list from the report data obtained from DB. Error: {e}")
            return None
