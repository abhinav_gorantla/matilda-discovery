import datetime
import os

import pandas as pd
from numpy.core.defchararray import capitalize

from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db import queries_helper
from matilda_discovery.db.connection import get_engine
from matilda_discovery.db.handler import host as host_handler
from matilda_discovery.db.handler import task as task_handler
from matilda_discovery.db.handler.task import HandleTaskList, HandleReinitiate
from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.services.host import HostReInitiate

logger = logger_py_file.Logger()

from matilda_discovery.utils import util

class TaskCreation:
    """
    contains methods that update the database when task is created.
    """
    def __init__(self):
        pass

    def create_discovery_task(self, request_data):
        """
        creates task-list, host_name,ip_list,task_id for all the given ip's in new discovery
        :param request_data: dictionary, pay load from the post call
        :return: dictionary of task_id and list of host_id and host_ip's.
        """
        logger.debug("Task creation service invoked. Creating tasks.")
        response = {}
        try:
            if request_data['login_mode'] == 'credentials':
                request_data['host_username'] = request_data.get('username').__repr__()
                task_id = self.build_task_host(request_data)
                request_data['task_id'] = task_id

                host_creation_handler_object = host_handler.HandleHostDetails()
                ip_list_data=[]
                for item in request_data.get("ip_list"):
                    ip_list = util.get_ip_list(item['ip_address'])
                    for ip in ip_list:
                        host_info = dict(ip=ip, task_id=request_data.get('task_id'), status="submitted", job_entry=0, community=request_data.get('community'), historic=0,device_identification_flag=0,login_flag=0)
                        host_id = host_creation_handler_object.create_individual_host(host_info)
                        credentials_data = dict\
                            (
                                username=request_data.get('username'),
                                password=util.simple_encrypt(request_data.get('password')),
                                host_id=host_id
                            )
                        host_creation_handler_object.create_credentials(credentials_data)

                        resp = dict(
                            ip_list=ip,
                            host_id=host_id,
                            type=item['type'],
                            task_id=task_id,
                            data_center=item['datacenter'],
                            region=item['region'],
                            project=item['project'],
                            line_of_business=item['line_of_business'],
                            environment=item['environment'],
                            application=item.get('application', "Others")
                        )
                        if request_data.get('utilization'): resp['utilization'] = 'Y' if 'Y' in request_data.get('utilization') else 'N'
                        resp['utilization_interval'] = int(request_data.get('utilization_interval')) if request_data.get('utilization_interval') else 0
                        resp['utilization_period'] = int(request_data.get('utilization_period')) if request_data.get('utilization_period') else 0
                        ip_list_data.append(resp)

                task_creation_handler_object = task_handler.HandleTaskCreation()
                task_creation_handler_object.create_iplist(ip_list_data)

                host_list = []
                host_list_response = host_creation_handler_object.get_host_list(task_id)
                for host in host_list_response:
                    host_list.append(dict(host_id=host.get('id'), host_ip_address=host.get('ip')))
                response['task_id'] = task_id
                response['host_list'] = host_list


                logger.debug(f"Successfully created tasks")

            else:
                logger.error(f"Task creation failed. Entered login_mode is {request_data['login_mode']}. This functionality is currently not available")
                return {'response': "Only Credentials login mode is enabled. {request_data['login_mode']} mode is not available"}

        except Exception as e:
            logger.error(f"create_discovery_task service failed. Failed to create tasks. Error: {e}")

        return response

    def build_task_host(self, request_data):
        """
        creates discovery_task_list, credential, ip_list,user_port_input and stores  in various DB tables from payload
        :param request_data: dictionary,payload obtained from post request
        :return: list and integer, list of IP's and tak_id
        """
        logger.debug(f"creating task_list and storing in DB")

        try:
            #total_ip_list = []
            # for item in request_data.get("ip_list"):
            #     ip_list = util.get_ip_list(item['ip_address'])
            #     total_ip_list.extend(ip_list)

            task_creation_handler_object = task_handler.HandleTaskCreation()

            time_info = util.get_datetime()
            if request_data.get('execution_mode') != "immediate":
                time_info = datetime.datetime.strptime(request_data.get('scheduled_time'), '%m-%d-%Y %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')

            task_request = dict\
                        (
                            name=request_data.get('name'),
                            discovery_type="Host Discovery",
                            created_by=request_data.get('created_by'),
                            status="submitted",
                            login_mode=request_data.get('login_mode'),
                            account_id=request_data.get('account_id'),
                            execution_mode=request_data.get('execution_mode').lower(),
                            scheduled_time=time_info,
                            created_datetime=util.get_datetime()
                        )
            task_id = task_creation_handler_object.create_discovery_task_list(task_request)

            # ip_list_data = []
            # for item in request_data.get('ip_list'):
            #     resp = dict(
            #         ip_list=item['ip_address'],
            #         type=item['type'],
            #         task_id=task_id,
            #         data_center=item['datacenter'],
            #         region=item['region'],
            #         project=item['project'],
            #         line_of_business=item['line_of_business'],
            #         environment=item['environment']
            #     )
            #     if request_data.get('utilization'): resp['utilization'] = 'Y' if 'Y' in request_data.get('utilization') else 'N'
            #     resp['utilization_interval'] = int(request_data.get('utilization_interval')) if request_data.get('utilization_interval') else 0
            #     resp['utilization_period'] = int(request_data.get('utilization_period')) if request_data.get('utilization_period') else 0
            #     ip_list_data.append(resp)

            #task_creation_handler_object.create_iplist(ip_list_data)
            # user_port_data = []
            # if request_data.get("user_port_input") not in [None, []]:
            #     for item in request_data.get("user_port_input"):
            #         resp = dict(name=item['service_type'], port=item['service_port'], task_id=task_id)
            #         user_port_data.append(resp)
            #     task_creation_handler_object.create_user_port_input(user_port_data)
            #
            # logger.debug(f"Successfully updated all the task related data into DB")
            return  task_id

        except Exception as e:
            logger.error(f"Failed creating task list details and storing in DB . Error: {e}")
            return None

    def file_input_data(self, request):
        try:
            logger.debug("Updating the applications Information from excel file... ")
            path = os.path.normpath(os.path.join(os.path.dirname(__file__), CreateInfra.input.value + request.get('filename')))
            inputfiledata = pd.ExcelFile(path)
            sheetlist = inputfiledata.sheet_names  # see all sheet names
            task_creation_handler_object = task_handler.HandleTaskCreation()
            for sheetname in sheetlist:
                time_info = util.get_datetime()
                if request.get('execution_mode') != "immediate":
                    time_info = datetime.datetime.strptime(request.get('scheduled_time'), '%m-%d-%Y %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')  # need to work on this format
                task_request = dict \
                        (
                        name=request.get('name'),
                        discovery_type="Host Discovery",
                        created_by=request.get('created_by'),
                        status="submitted",
                        login_mode=request.get('login_mode'),
                        account_id=request.get('account_id'),
                        execution_mode=request.get('execution_mode'),
                        scheduled_time=time_info,
                        created_datetime=util.get_datetime()
                    )
                task_id = task_creation_handler_object.create_discovery_task_list(task_request)

                resp = dict(ip_list='Excel file input', type='single ip', task_id=task_id)
                if request.get('utilization'): resp['utilization'] = 'Y' if 'Y' in request.get('utilization') else 'N'
                resp['utilization_interval'] = int(request.get('utilization_interval')) if request.get('utilization_interval') else 0
                resp['utilization_period'] = int(request.get('utilization_period')) if request.get('utilization_period') else 0

                template_data = inputfiledata.parse(sheetname)
                for index, row in template_data.iterrows():
                    val = row.to_dict()
                    host_info = dict(ip=val.get('IP'),
                                     task_id=task_id,
                                     status="submitted",
                                     job_entry=0,
                                     community = val.get('community'),
                                     historic=0,
                    )
                    host_creation_handler_object = host_handler.HandleHostDetails()
                    host_id = host_creation_handler_object.create_individual_host(host_info)
                    credentials_data = dict(username=val.get('Username'), password=util.simple_encrypt(val.get('Password')), host_id=host_id)
                    host_creation_handler_object.create_credentials(credentials_data)
                    datacenterinfo=dict \
                                     (
                                        host_id=host_id,
                                        data_center=val.get('Datacenter') if val.get('Datacenter') else 'Others',
                                        region=val.get('Region') if val.get('Region') else 'Others',
                                        project=val.get('Project') if val.get('Project') else 'Others',
                                        line_of_business=val.get('Line_of_Business') if val.get('Line_of_Business') else 'Others',
                                        environment=val.get('Environment') if val.get('Environment') else 'Others',
                                        application = val.get('Application') if val.get('Application') else 'Others'
                                     )
                    resp.update(datacenterinfo)
                    task_creation_handler_object.create_iplist([resp])  # need to change the logic by sending the whole list at a time to update table rather updating individual host everytime.
            return None
        except Exception as e:
            logger.debug(f"Exception occurred while Updating the applications Information from excel file. Error: {e}")
            return None


class TaskList:
    """
    contains methods that retrieve data related to task from database.
    """
    def __init__(self):
        pass

    def fetch_task_list(self, account_id):
        """
        fetches all the task_details from hsot_discovery_task_list present under the given account_id
        :param account_id: integer, unique id for each user
        :return: dictionary, list of task_details and essence of all the task's statuses.
        """
        logger.debug(f"In task list service. Fetching list of tasks")
        try:
            task_list_handler_object = task_handler.HandleTaskList()
            task_list_resp = task_list_handler_object.get_discovery_task_list(account_id)
            status_list = []
            discovery_list = []

            for task in task_list_resp:
                status_list.append(task['status'])

                range_list = []
                cidr_list = []
                for key in task.ip_information:
                    if key['type'] == 'range':
                        range_list.append(key['ip_list'])
                    if key['type'] == 'cidr':
                        cidr_list.append(key['ip_list'])

                ip_list_input = {'range': range_list, 'cidr': cidr_list}

                user_port_input = []
                for key in task.user_port_information:
                    user_port_input.append(dict(user_port_data = key.get('name'), service_port=key.get('port')))

                host_count_query= util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Host_Count")
                x=get_engine()
                host_count=queries_helper.execute_dict_of_queries(host_count_query, x, task_id=task['id'], info="host count")['host_count'][0]

                discovery = dict\
                    (
                        task_id=task['id'],
                        host_discovery_name=task['name'],
                        hosts_count=host_count['host_count'], # need to query this later
                        status=task['status'],
                        created_by=task['created_by'],
                        updated_date=None,  # need to add this column in models later
                        updated_by=task['updated_by'],
                        login_mode=task['login_mode'],
                        account_id=task['account_id'],
                        discovery_type=task['discovery_type'],
                        discovered_ip_list=ip_list_input,
                        user_entered_ports=user_port_input,
                        execution_mode=task['execution_mode'],
                        created_date=str(task['created_datetime'].strftime(CreateInfra.us_datetime_format.value)) if task['created_datetime'] is not None else None,
                        start_date=str(task['start_date'].strftime(CreateInfra.us_datetime_format.value)) if task['start_date'] is not None else None,
                        end_date=str(task['end_date'].strftime(CreateInfra.us_datetime_format.value)) if task['end_date'] is not None else None,
                        scheduled_time=str(task['scheduled_time'].strftime(CreateInfra.us_datetime_format.value)) if task['scheduled_time'] is not None else None
                    )

                if task['status'] == "completed":

                    host_status_list = []
                    for key in task.host_information:
                        if key['historic'] == 0:
                            host_status_list.append(key['status'])
                    host_success_count = dict(success=host_status_list.count("success"), failed=host_status_list.count("failed"))

                    discovery["success"] = host_success_count.get('success')
                    discovery["failed"] = host_success_count.get('failed')
                else:
                    discovery["success"] = None
                    discovery["failed"] = None

                discovery_list.append(discovery)

            task_status = dict\
                (
                    total_tasks=status_list.__len__(),
                    submitted=status_list.count("submitted"),
                    in_progress=status_list.count("in_progress"),
                    completed=status_list.count("completed")
                )

            response = dict(response=discovery_list, summary=task_status)

            logger.debug(f"successfully called task_list service")
            return response

        except Exception as e:
            logger.error(f"task_list service failed. Error: {e}")
            return None


#NEW UI
#TASK LIST - DEVICES DETAILS PENDING
    def new_fetch_task_list(self, account_id):
        """
        fetches all the task_details from hsot_discovery_task_list present under the given account_id
        :param account_id: integer, unique id for each user
        :return: dictionary, list of task_details and essence of all the task's statuses.
        """
        logger.debug(f"In task list service. Fetching list of tasks")
        try:
            task_list_handler_object = task_handler.HandleTaskList()
            task_list_resp = task_list_handler_object.get_discovery_task_list(account_id)
            status_list = []
            discovery_list = []
            device_list = []

            for task in task_list_resp:
                status_list.append(task['status'])

                host_count_query = util.get_properties(property_file_path=CreateInfra.db_queries_path.value,
                                                       section="Host_Count")
                x = get_engine()
                host_count = \
                queries_helper.execute_dict_of_queries(host_count_query, x, task_id=task['id'], info="host count")[
                    'host_count'][0]

                discovery = dict \
                        (
                        task_id=task['id'],
                        name=task['name'],
                        status=task['status'],
                        db_created_date=str(task['created_datetime'].strftime(CreateInfra.us_datetime_format.value)) if
                        task['created_datetime'] is not None else None,
                        start_date=str(task['start_date'].strftime(CreateInfra.us_datetime_format.value)) if task[
                                                                                                                 'start_date'] is not None else None,
                        end_date=str(task['end_date'].strftime(CreateInfra.us_datetime_format.value)) if task[
                                                                                                             'end_date'] is not None else None,
                        hosts_count=host_count['host_count'],
                    )
                if task.get('status'): discovery['status'] = capitalize(task.get('status'))
                if discovery['status'] == "In_progress": discovery['status'] = "Inprogress"

                if task['status'] == "completed":

                    host_status_list = []
                    for key in task.host_information:
                        if key['historic'] == 0:
                            host_status_list.append(key['status'])
                    host_success_count = dict(success=host_status_list.count("success"),
                                              failed=host_status_list.count("failed"))

                    discovery["success"] = host_success_count.get('success')
                    discovery["failed"] = host_success_count.get('failed')
                else:
                    discovery["success"] = None
                    discovery["failed"] = None

                discovery_list.append(discovery)

            discovery_details = \
                {
                    "discoveries_status":
                        [
                            {"count": status_list.__len__(), "title": "Discoveries"},
                            {"count": status_list.count("completed"), "title": "Completed"},
                            {"count": status_list.count("submitted"), "title": "Submitted"},
                            {"count": status_list.count("in_progress"), "title": "In Progress"},
                            {"count": status_list.count("scheduled"), "title": "Scheduled"}

                        ]
                }

            response = dict(kpiActivities=discovery_details, discoveryList=discovery_list)

            logger.debug(f"successfully called task_list service")
            return response

        except Exception as e:
            logger.error(f"task_list service failed. Error: {e}")
            return None

    def completed_task_list(self,account_id):
        """
        fetchs the complete list of all the completed tasks present under the account_id
        :return: list of dictionaries,list of all the completed tasks and its details
        """
        try:
            logger.debug("Fetching details of completed tasks in services")

            task_handler_object=HandleTaskList()
            completed_tasks_response=task_handler_object.get_completed_task_list(account_id)
            completed_task_list=[]

            for each in completed_tasks_response:
                task={}
                task['name']=each['name']
                task['task_id']=each['id']
                task['ip_list']=each.ip_information[0]['ip_list']
                task['region'] = each.ip_information[0]['region']
                task['data_center'] = each.ip_information[0]['data_center']
                task['line_of_business'] = each.ip_information[0]['line_of_business']
                task['environment'] = each.ip_information[0]['environment']
                completed_task_list.append(task)

            logger.debug("Successfully fetched details of completed tasks in services ")
            return completed_task_list
        except Exception as e:
            logger.error(f"Exception occurred while preparing completed task list for tha account_id {account_id}. Error: {e}")
            return None


    def reinitiate_task(self,reinitiate_data):
        """
        fetches task_id, status and reason sent from UI and make caluclations for the
        :param reinitiate_data: post body sent by UI for reinitiating task
        :return:
        """
        try:
            logger.debug("Fetching all the hosts under the given task_id for the given status and reasons. Task_id: ")
            job_ids_list = []
            task_handler = HandleReinitiate()
            host_reinitiate_handler = HostReInitiate()

            if reinitiate_data['status'] not in [None,[]]:
                reinitiate_data['status'] = [x.lower() for x in reinitiate_data.get('status')]
            # if reinitiate_data['reason'] not in [None, []]:
            #     reinitiate_data['reason'] = [x.lower() for x in reinitiate_data.get('reason')]

            #For now we are re-initiating only the discoveries based on status of the hosts.

            #if len(reinitiate_data["reason"])==0:
            task_id = reinitiate_data['task_id']
            for each_status in reinitiate_data['status']:
                host_list = task_handler.fetch_host_list(task_id, status=each_status)
                for each_host in host_list:
                    req_data = dict(host_id=each_host.get('id'), username=reinitiate_data.get('username'), password=reinitiate_data.get('password'))
                    job_host_mapping_data = host_reinitiate_handler.host_reinitiate(req_data, logger_ip=each_host.get('ip'))
                    if job_host_mapping_data not in [None, []]:
                        job_details = job_host_mapping_data[0]
                        job_id = job_details.get('job_id')
                        job_ids_list.append(job_id)

            # for each_reason in reinitiate_data['reason']:
            #     host_list=task_handler.fetch_host_list(task_id,reason=each_reason)
            #     for each_host in host_list:
            #         req_data = dict(host_id=each_host.get('id'), username=reinitiate_data.get('username'), password=reinitiate_data.get('password'))
            #         host_reinitiate_handler.host_reinitiate(req_data)
            return job_ids_list

        except Exception as e:
            logger.error(f"Exception occurred while fetching all hsots under teh given task_id. Exception: {e}")
            return None
