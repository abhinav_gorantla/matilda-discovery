import inspect
import json
import os
import time
from _datetime import datetime

import pandas as pd

from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db.handler import host as host_handler
from matilda_discovery.db.handler.host import HandleHostDetails
from matilda_discovery.db.models import model
from matilda_discovery.logger_engine import logger as logger_py_file

logger = logger_py_file.Logger()

from matilda_discovery.services.service_discovery.initiate_service_discovery import initiate_service_discovery
from matilda_discovery.utils import util
from matilda_discovery.utils.util import shell_to_json, number_formatter


class Linux:
    def __init__(self, connector):
        self.connector = connector
        self.host_handler = HandleHostDetails()
        pass

    def host_info(self, logger_ip=None):  # done
        """
        Collects all the information of the host and updates all the collected information into DB . Collects, memory, cpu,interface,ports,packages,storage, user_groups, services
        :return: returns nothing
        """
        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Collecting linux information for host_ip: {self.connector.ip}, host_id: {self.connector.host_id} ")

            self.cpu(logger_ip=logger_ip)
            self.memory(logger_ip=logger_ip)
            self.storage(logger_ip=logger_ip)
            interface_info = self.interface(logger_ip=logger_ip)
            self.ports(interface_info, logger_ip=logger_ip)
            self.packages(logger_ip=logger_ip)
            self.user_groups(logger_ip=logger_ip)
            self.login_history(logger_ip=logger_ip)
            self.certificates(logger_ip=logger_ip)
            self.services(logger_ip=logger_ip)
            self.docker_info(logger_ip=logger_ip)

            directory = self.generate_raw_text_files(logger_ip=logger_ip)
            self.generate_raw_excel_files(directory, logger_ip=logger_ip)
            self.raw_data_to_db(logger_ip=logger_ip)
            logger.debug(logger_ip=logger_ip,
                         message=f" ******************Successfully collected linux information for the host ip: {self.connector.ip} , Host ID: {self.connector.host_id}************** ")

        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Failed to collect linux information for the host ip: {self.connector.ip} , Host ID: {self.connector.host_id}. Error: {e}")
            return None

    def host(self, logger_ip=None):  # done
        """
        Discovers basic host summary information by running a shell script
        :param self.connector: an object of CMDHandler class.contains payload info, OS details and functions to run   shell scripts
        :return: dictionary, basic summary details of a host
        """
        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Collecting host information for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")
            host_info = dict(ip=self.connector.ip, task_id=self.connector.task_id, host_id=self.connector.host_id)
            start_time = time.time()

            host_data, err = self.connector.execute_shell(
                util.get_script(name=inspect.stack()[0][3], operatingsystem=self.connector.operatingsystem),
                cmd_info="Host", logger_ip=logger_ip)
            host_results = shell_to_json(host_data, info=inspect.stack()[0][3], logger_ip=logger_ip)[0]

            host_results['memory'] = number_formatter(number_type='float', value=host_results['memory'])
            host_results.update(host_info)

            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully collected host information for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")
            logger.info(logger_ip=logger_ip,
                        message=f"Time taken to get host Details of the host: {self.connector.ip} is: {round((time.time() - start_time), 2)} seconds")
            return host_results

        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Exception occurred while collecting host details for the host ip: {self.connector.ip} , Host ID: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "CPU", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)
            return {}

    def memory(self, logger_ip=None):  # done
        """
        Discovery of memory details of the host by running a shell script
        :param self.connector: an object of CMDHandler class.contains payload info, OS details and functions to run powershell and shell scripts
        :return: dictionary, memory details of a host
        """
        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Collecting memory information for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

            start_time = time.time()
            host_info = dict(host_id=self.connector.host_id)
            memory_data, err = self.connector.execute_shell(
                util.get_script(name=inspect.stack()[0][3], operatingsystem=self.connector.operatingsystem),
                cmd_info="Memory", logger_ip=logger_ip)
            memory_results = shell_to_json(memory_data, info=inspect.stack()[0][3], logger_ip=logger_ip)[0]

            memory_results['used'] = number_formatter(number_type='float', value=memory_results['used'])
            memory_results['free'] = number_formatter(number_type='float', value=memory_results['free'])
            memory_results['available'] = number_formatter(number_type='float', value=memory_results['available'])
            memory_results['total'] = number_formatter(number_type='float', value=memory_results['total'])
            memory_results['used_percent'] = number_formatter(number_type='float', value=memory_results['used_percent'])

            memory_results.update(host_info)

            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully collected memory information for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")
            logger.info(logger_ip=logger_ip,
                        message=f"Time taken to get memory details of the host: {self.connector.ip} is: {round((time.time() - start_time), 2)} seconds")

            self.host_handler.update_table(table_info=memory_results, table_model=model.Memory, logger_ip=logger_ip)

        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Exception occurred while collecting memory details for the host ip: {self.connector.ip} , Host ID: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "CPU", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)



    def cpu(self, logger_ip=None):  # done
        """
       Discovery of cpu details of the host by running a shell script
       :param self.connector: an object of CMDHandler class.contains payload info, OS details and functions to run power shell and shell scripts
       :return: dictionary, CPU details of a host
        """
        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Collecting CPU information for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

            start_time = time.time()
            host_info = dict(host_id=self.connector.host_id)
            cpu_data, err = self.connector.execute_shell(
                util.get_script(name=inspect.stack()[0][3], operatingsystem=self.connector.operatingsystem),
                cmd_info="CPU", logger_ip=logger_ip)
            cpu_results = shell_to_json(cpu_data, info=inspect.stack()[0][3], logger_ip=logger_ip)[0]

            cpu_results['utilization_percent'] = number_formatter(number_type='float',
                                                                  value=cpu_results['utilization_percent'])
            cpu_results['physical_processors'] = number_formatter(number_type='int',
                                                                  value=cpu_results['physical_processors'])
            cpu_results['physical_cores'] = number_formatter(number_type='int', value=cpu_results['physical_cores'])
            cpu_results['logical_processors'] = number_formatter(number_type='int',
                                                                 value=cpu_results['logical_processors'])
            cpu_results.update(host_info)

            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully collected CPU information for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")
            logger.info(logger_ip=logger_ip,
                        message=f"Time taken to get CPU details of the host: {self.connector.ip} is: {round((time.time() - start_time), 2)} seconds")

            self.host_handler.update_table(table_info=cpu_results, table_model=model.CPU, logger_ip=logger_ip)

        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Exception occurred while collecting CPU details for the host ip: {self.connector.ip} , Host ID: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "CPU", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)

    def interface(self, logger_ip=None, utiliztion=False):  # done
        """
        Discovery of interface details of the host by running a power shell script
        :param self.connector: an object of CMDHandler class.contains payload info, OS details and functions to run powershell and shell scripts
        :return: dictionary, interface details of a host
        """
        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Collecting interfaces information for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")
            interface_info_array = []
            desc_details = util.get_properties(property_file_path=CreateInfra.resource_properties_path.value,
                                               section='InterfaceDiscription')
            host_info = dict(host_id=self.connector.host_id)
            interface_data, err = self.connector.execute_shell(
                util.get_script(name=inspect.stack()[0][3], operatingsystem=self.connector.operatingsystem),
                cmd_info="interfaces", logger_ip=logger_ip)
            if interface_data in [None, [], '']:
                interface_data, err = self.connector.execute_shell(
                    util.get_script(name='interface_sudo', operatingsystem=self.connector.operatingsystem),
                    cmd_info="interfaces", logger_ip=logger_ip)

            interface_results = util.shell_to_json(interface_data, info=inspect.stack()[0][3], logger_ip=logger_ip)

            for each_interface in interface_results:

                if each_interface['ip_address'] not in [None, [], '']:
                    if each_interface.get('speed') in ['', None]:
                        each_interface['speed'] = None
                        each_interface['packet_max_size'] = number_formatter(number_type='int',
                                                                             value=each_interface['packet_max_size'])
                        each_interface['received_packets'] = number_formatter(number_type='int',
                                                                              value=each_interface['received_packets'])
                        each_interface['sent_packets'] = number_formatter(number_type='int',
                                                                          value=each_interface['sent_packets'])

                    elif str.lower(each_interface.get('speed')) == "unknown":
                        each_interface['speed'] = None
                        each_interface['packet_max_size'] = number_formatter(number_type='int',
                                                                             value=each_interface['packet_max_size'])
                        each_interface['received_packets'] = number_formatter(number_type='int',
                                                                              value=each_interface['received_packets'])
                        each_interface['sent_packets'] = number_formatter(number_type='int',
                                                                          value=each_interface['sent_packets'])

                    if each_interface.get('Flg') not in [None, []]:
                        each_interface['description'] = ','.join(
                            [desc_details.get(desc_item) for desc_item in desc_details if
                             desc_item in each_interface.get('Flg')])
                        each_interface['status'] = 'Up' if 'U' in each_interface.get('Flg') else 'Down'
                    if each_interface.get('name') != 'lo':
                        each_interface['dhcp_enabled'] = 'Y' if each_interface.get('dhcp_enabled') in ['dhcp',
                                                                                                       'auto'] else 'N'

                    each_interface.update(host_info)

                if each_interface['ip_address'] not in [None, [], '']:
                    interface_info_array.append(each_interface)

            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully collected interfaces information for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")
            if not utiliztion:
                self.host_handler.update_table(table_info=interface_info_array, table_model=model.Interface,
                                               logger_ip=logger_ip)
            return interface_info_array

        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Exception occurred while collecting interface details for the host ip: {self.connector.ip} , Host ID: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "interface", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)
            return []

    def ports(self, interfaces_info, logger_ip=None, utiliztion=False):  # done
        """
        Discovery of ports details of the host by running a power shell script
        :param self.connector: an object of CMDHandler class.contains payload info, OS details and functions to run powershell and shell scripts
        :return: dictionary, port details of a host
        """
        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Collecting ports information for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

            ports_info_array = []
            host_info = dict(host_id=self.connector.host_id)
            ports_data, err = self.connector.execute_shell(
                util.get_script(name=inspect.stack()[0][3], operatingsystem=self.connector.operatingsystem),
                cmd_info="ports", logger_ip=logger_ip)
            if "aix" in self.connector.operatingsystem or 'solaris' in self.connector.operatingsystem:
                ports_results = util.shell_to_json(ports_data, info=inspect.stack()[0][3], logger_ip=logger_ip)
            else:
                ports_results = util.shell_to_jsonlist(ports_data, info=inspect.stack()[0][3])

            self.host_handler_object = HandleHostDetails()
            user_port_data_resp = self.host_handler_object.get_user_port_input(self.connector.task_id, logger_ip=None)

            user_input_port_data = {}
            for item in user_port_data_resp:
                user_input_port_data[item.get('name')] = item.get('port')

            # build listenign and established port details
            local_listening_ports_list, local_listening_ports_data_list, local_established_ports_list, local_established_ports_data_list = [], [], [], []
            for each_port in ports_results:
                if each_port['status'] == "LISTEN":
                    local_listening_ports_data_list.append(each_port)
                    local_listening_ports_list.append(each_port['number'])
                elif each_port['status'] == "ESTABLISHED":
                    local_established_ports_data_list.append(each_port)
                    local_established_ports_list.append(each_port['number'])

            for port in ports_results:
                # Incomming and outgoing Direction
                if port['status'] == "ESTABLISHED":
                    if port["number"] in local_listening_ports_list:
                        port["direction"] = "incoming"
                    if port["number"] not in local_listening_ports_list:
                        port["direction"] = "outgoing"

                port['host_id'] = self.connector.host_id
                port['status'] = 'ACTIVE' if port.get('status') == 'ESTABLISHED' else 'OPEN'
                for ifaceitem in interfaces_info:
                    if ifaceitem.get('ip_address') == port['local_address']:
                        port['used_interface'] = ifaceitem.get('name')

                # Local_Service
                if str(port['number']) in user_input_port_data.values():
                    for service_name, each_port in user_input_port_data.items():
                        if each_port == str(port['number']):
                            port['local_service'] = service_name
                else:
                    port['local_service'] = util.get_properties(
                        property_file_path=CreateInfra.resource_properties_path.value, section='PortConfig',
                        property_name=port['number']) or 'Unknown'

                ports_info_array.append(port)

            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully collected ports information for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")
            if not utiliztion:
                host_handler_object = host_handler.HandleHostDetails()
                host_handler_object.update_ports_info(port_info=ports_info_array, logger_ip=logger_ip)
            else:
                return ports_info_array

        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Exception occurred while collecting ports details for the host ip: {self.connector.ip} , Host ID: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "ports", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)

    def packages(self, logger_ip=None):  # done
        """
          Discovery of package details of the host by running a  shell script
          :param self.connector: an object of CMDHandler class.contains payload info, OS details and functions to run powershell and shell scripts
          :return: dictionary, package details of a host
        """
        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Collecting packages information for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

            packages_info_array = []
            host_info = dict(host_id=self.connector.host_id)
            packages_data, err = self.connector.execute_shell(
                util.get_script(name=inspect.stack()[0][3], operatingsystem=self.connector.operatingsystem),
                cmd_info="package", logger_ip=logger_ip)
            packages_results = util.shell_to_jsonlist(packages_data)
            for each_package in packages_results:
                each_package.update(host_info)
                packages_info_array.append(each_package)

            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully collected packages information for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")
            self.host_handler.update_table(table_info=packages_info_array, table_model=model.Package,
                                           logger_ip=logger_ip)

        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Exception occurred while collecting packages details for the host ip: {self.connector.ip} , Host ID: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data={"host_id":self.connector.host_id,"ip":self.connector.ip,"type":path +"packages","error":e}
            self.host_handler.update_error_table(error_data=error_data,logger_ip=logger_ip)

    def storage(self, logger_ip=None):
        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Collecting storage information for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")
            start_time = time.time()
            host_info = dict(host_id=self.connector.host_id)
            storage_data, err = self.connector.execute_shell(
                util.get_script(name=inspect.stack()[0][3], operatingsystem=self.connector.operatingsystem),
                cmd_info="storage", logger_ip=logger_ip)
            storage_details = shell_to_json(storage_data, info=inspect.stack()[0][3], logger_ip=logger_ip)
            storage_info = []

            for each_storage in storage_details:
                each_storage['used'] = number_formatter(number_type='float', value=each_storage['used'])
                each_storage['total'] = number_formatter(number_type='float', value=each_storage['total'])
                each_storage['available'] = number_formatter(number_type='float', value=each_storage['available'])
                each_storage['used_percent'] = number_formatter(number_type='float', value=each_storage['used_percent'])
                each_storage.update(host_info)
                storage_info.append(each_storage)

            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully collected storage information for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")
            logger.info(logger_ip=logger_ip,
                        message=f"Time taken to get storage Details of the host: {self.connector.ip} is : {round((time.time() - start_time), 2)} seconds")
            self.host_handler.update_table(table_info=storage_info, table_model=model.Storage, logger_ip=logger_ip)

        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Exception occurred while collecting storage details for the host ip: {self.connector.ip} , Host ID: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "storage", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)

    def user_groups(self, logger_ip=None):
        """
        Discovery of user group details of the host by running a power shell script
        :param self.connector: an object of CMDHandler class.contains payload info, OS details and functions to run powershell and shell scripts
        :return: dictionary, user group details of a host
        """
        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Collecting user_group information for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

            user_groups_info_array = []
            host_info = dict(host_id=self.connector.host_id)
            user_groups_data, err = self.connector.execute_shell(
                util.get_script(name=inspect.stack()[0][3], operatingsystem=self.connector.operatingsystem),
                cmd_info="user_group", logger_ip=logger_ip)
            user_groups_results = util.shell_to_json(user_groups_data, info=inspect.stack()[0][3], logger_ip=logger_ip)
            if "windows" not in self.connector.operatingsystem:
                for each_user_group in user_groups_results:
                    if each_user_group['password_status'].strip() in ['L', 'LK']:
                        each_user_group['password_status'] = "password locked"
                    elif each_user_group['password_status'].strip() in ['N', 'NP']:
                        each_user_group['password_status'] = "no password"
                    elif each_user_group['password_status'].strip() in ['P', 'PS']:
                        each_user_group['password_status'] = "password set"
                    if each_user_group['min_days_for_pwd_change'] in [' ']:
                        each_user_group['min_days_for_pwd_change'] = None
                    if each_user_group['max_days_for_pwd_change'] in [' ']:
                        each_user_group['max_days_for_pwd_change'] = None
                    if each_user_group['warning_days_for_pwd_change'] in [' ']:
                        each_user_group['warning_days_for_pwd_change'] = None
                    each_user_group.update(host_info)
                    user_groups_info_array.append(each_user_group)
            else:
                for each_user in user_groups_results:
                    each_user['host_id'] = self.connector.host_id
                    user_groups_info_array.append(each_user)
                logger.debug(logger_ip=logger_ip,
                             message=f"Successfully collected user_group information for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

            self.host_handler.update_table(table_info=user_groups_info_array, table_model=model.User_Group,
                                           logger_ip=logger_ip)

        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Exception occurred while collecting user_groups details for the host ip: {self.connector.ip} , Host ID: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "user_groups", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)

    # For Bourne shell script if service_paths is empty.
    # We can remove this once we updated the script
    def bourne_services_paths_update(self, service_paths_data, logger_ip=None):
        service_paths = []
        servicefiledict = {
            'nginx.conf': 'nginx',
            'domain-registry.xml': 'weblogic',
            'domain.xml': 'jboss',
            'server.xml': 'tomcat',
            'httpd.conf': 'apache',
            'my.cnf': 'mysql',
            'tnsnames.ora': 'oracle',
            'version.sh': 'tomcat_version',
            'weblogic.jar': 'weblogic_version',
            'domain.sh': 'jboss_version'
        }

        for pathitem in service_paths_data:
            key, val = next(iter(pathitem.items()))
            service_paths.append({servicefiledict[key]: val})
        return service_paths

    def services(self, logger_ip=None):
        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"service check initiated for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

            serviceexistance = []

            # fetching core services list
            coreservicesdata = self.host_handler.get_core_services(logger_ip=logger_ip)
            coreservices = []
            for coreitem in coreservicesdata:
                coreservices.append(coreitem.get('service'))

            # system services
            system_services_data, err = self.connector.execute_shell(
                util.get_script(name="sys_services", operatingsystem=self.connector.operatingsystem),
                cmd_info="services info", logger_ip=logger_ip)
            system_services_results = util.shell_to_json(system_services_data, info=inspect.stack()[0][3],
                                                         logger_ip=logger_ip)

            service_types = util.get_properties(property_file_path=CreateInfra.resource_properties_path.value,
                                                section='Servicetype')
            serviceavailabilitylist = self.status(logger_ip=logger_ip)

            service_paths_data, err = self.connector.execute_shell(
                util.get_script(name="services", operatingsystem=self.connector.operatingsystem),
                cmd_info="services info", logger_ip=logger_ip)
            service_paths = util.shell_to_json(service_paths_data, info=inspect.stack()[0][3], logger_ip=logger_ip)
            # For Bourne shell script if service_paths is empty
            if service_paths in [None, [], '']:
                logger.debug(logger_ip=logger_ip,
                             message=f"Finding service paths for bourne script host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")
                service_paths_data, err = self.connector.execute_shell(
                    util.get_script(name='servicesbourne', operatingsystem=self.connector.operatingsystem),
                    cmd_info="services info", logger_ip=logger_ip)
                service_paths_result = util.shell_to_json(service_paths_data, info=inspect.stack()[0][3],
                                                          logger_ip=logger_ip)
                service_paths = self.bourne_services_paths_update(service_paths_result, logger_ip=logger_ip)

            service_list = []

            # jboss
            jbosspathlist = []
            jbossversiondict = {}
            jbossignorepaths = ['weblogic', 'tomcat', 'websphere']

            # weblogic
            weblogicpathlist = []
            weblogicversiondict = {}
            weblogicignorepaths = ['tomcat', 'jboss', 'websphere']

            # tomcat
            tomcatpathlist = []
            tomcatversiondict = {}
            tomcatignorepaths = ['weblogic', 'jboss', 'websphere']

            for pathitem in service_paths:
                key, val = next(iter(pathitem.items()))
                # jboss
                if key == 'jboss':
                    if not any(substring in val for substring in jbossignorepaths):
                        if '/domain/configuration/domain.xml' in val:
                            jbossxmlpath = val.split('/domain/configuration/domain.xml')
                            jbosstemp = {jbossxmlpath[0]: val}
                            jbosspathlist.append(jbosstemp)
                if key == 'jboss_version':
                    if not any(substring in val for substring in jbossignorepaths):
                        if '/bin/domain.sh' in val:
                            jbossversionxmlpath = val.split('/bin/domain.sh')
                            version_data, err = self.connector.execute_shell(
                                util.get_script(name=key, operatingsystem=self.connector.operatingsystem,
                                                arguments=val), cmd_info="version check", logger_ip=logger_ip)
                            version_result = util.shell_to_json(version_data, info=key, logger_ip=logger_ip)
                            if util.nullcheck_return_list_firstentry(version_result):
                                version = version_result[0].get(key)
                                jbossversiontemp = {jbossversionxmlpath[0]: version}
                                jbossversiondict.update(jbossversiontemp)

                # weblogic
                if key == 'weblogic':
                    if not any(substring in val for substring in weblogicignorepaths):
                        if '/domain-registry.xml' in val:
                            weblogicxmlpath = val.split('/domain-registry.xml')
                            weblogictemp = {weblogicxmlpath[0]: val}
                            weblogicpathlist.append(weblogictemp)
                if key == 'weblogic_version':
                    if not any(substring in val for substring in weblogicignorepaths):
                        if '/lib/weblogic.jar' in val:
                            weblogicversionxmlpath = val.split('/lib/weblogic.jar')
                            version_data, err = self.connector.execute_shell(
                                util.get_script(name=key, operatingsystem=self.connector.operatingsystem,
                                                arguments=val), cmd_info="version check", logger_ip=logger_ip)
                            version_result = util.shell_to_json(version_data, info=key, logger_ip=logger_ip)
                            if util.nullcheck_return_list_firstentry(version_result):
                                version = version_result[0].get(key)
                                weblogicversiontemp = {weblogicversionxmlpath[0]: version}
                                weblogicversiondict.update(weblogicversiontemp)

                # tomcat
                if key == 'tomcat':
                    if not any(substring in val for substring in tomcatignorepaths):
                        if '/conf/server.xml' in val:
                            tomcatxmlpath = val.split('/conf/server.xml')
                            tomcattemp = {tomcatxmlpath[0]: val}
                            tomcatpathlist.append(tomcattemp)
                if key == 'tomcat_version':
                    if not any(substring in val for substring in tomcatignorepaths):
                        if '/bin/version.sh' in val:
                            tomcatversionxmlpath = val.split('/bin/version.sh')
                            version_data, err = self.connector.execute_shell(
                                util.get_script(name=key, operatingsystem=self.connector.operatingsystem,
                                                arguments=val), cmd_info="version check", logger_ip=logger_ip)
                            version_result = util.shell_to_json(version_data, info=key, logger_ip=logger_ip)
                            if util.nullcheck_return_list_firstentry(version_result):
                                version = version_result[0].get(key)
                                tomcatversiontemp = {tomcatversionxmlpath[0]: version}
                                tomcatversiondict.update(tomcatversiontemp)

                # from here we started preparing service_list
                # nginx and mysql
                if key == 'mysql' or key == 'nginx':
                    response = dict(task_id=self.connector.task_id, host_id=self.connector.host_id)
                    version_data, err = self.connector.execute_shell(
                        util.get_script(name=key + '_version', operatingsystem=self.connector.operatingsystem),
                        cmd_info="version check", logger_ip=logger_ip)
                    version_result = util.shell_to_json(version_data, info=key, logger_ip=logger_ip)
                    if util.nullcheck_return_list_firstentry(version_result):
                        version = version_result[0].get(key + '_version')
                        response['version'] = version
                    response['name'] = key
                    response['path'] = val
                    response['type'] = service_types.get(key)
                    response['discovery_id'] = initiate_service_discovery(self.connector, servicetype=key, path=val,
                                                                          platform='Linux')
                    response['status'] = 'Down'
                    response['core_service'] = 1
                    activeservicedetails = serviceavailabilitylist.get(key)
                    if activeservicedetails:
                        response['cpu_percent'] = activeservicedetails.get('cpu_percent')
                        response['memory_percent'] = activeservicedetails.get('memory_percent')
                        response['port'] = activeservicedetails.get('port')
                        response['status'] = 'Up'
                    serviceexistance.append(key)
                    service_list.append(response)
                    logger.debug(logger_ip=logger_ip,
                                 message=f"Discovered service details for service: {key} Details: {response} host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

                # apache and oracle
                if key == 'apache' or key == 'oracle':
                    response = dict(task_id=self.connector.task_id, host_id=self.connector.host_id)
                    response['name'] = key
                    response['path'] = val
                    response['type'] = service_types.get(key)
                    response['discovery_id'] = initiate_service_discovery(self.connector, servicetype=key, path=val,
                                                                          platform='Linux')
                    response['status'] = 'Down'
                    response['core_service'] = 1
                    serviceexistance.append(key)
                    service_list.append(response)
                    logger.debug(logger_ip=logger_ip,
                                 message=f"Discovered service details for service: {key} Details: {response} host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

            # Jboss
            activejbossdetails = serviceavailabilitylist.get('jboss')

            for jbosspathlistitem in jbosspathlist:
                response = dict(task_id=self.connector.task_id, host_id=self.connector.host_id)
                key, val = next(iter(jbosspathlistitem.items()))
                response['name'] = 'jboss'
                response['path'] = val
                response['version'] = jbossversiondict.get(key)
                response['type'] = service_types.get('jboss')
                response['discovery_id'] = initiate_service_discovery(self.connector, servicetype='jboss', path=val,
                                                                      platform='Linux')
                response['status'] = 'Down'
                response['core_service'] = 1
                if activejbossdetails:
                    if activejbossdetails.get('home_path'):
                        if activejbossdetails.get('home_path') == key:
                            response['cpu_percent'] = activejbossdetails.get('cpu_percent')
                            response['memory_percent'] = activejbossdetails.get('memory_percent')
                            response['port'] = activejbossdetails.get('port')
                            response['status'] = 'Up'
                serviceexistance.append('jboss')
                service_list.append(response)
                logger.debug(logger_ip=logger_ip,
                             message=f"Discovered service details for service: JBoss, Details: {response} host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

            # weblogic
            activeweblogicdetails = serviceavailabilitylist.get('weblogic')

            for weblogicpathlistitem in weblogicpathlist:
                response = dict(task_id=self.connector.task_id, host_id=self.connector.host_id)
                key, val = next(iter(weblogicpathlistitem.items()))
                response['name'] = 'weblogic'
                response['path'] = val
                relatedversionpath = next((sitem for sitem in weblogicversiondict.keys() if key in sitem), None)
                response['version'] = weblogicversiondict.get(relatedversionpath)
                response['type'] = service_types.get('weblogic')
                response['discovery_id'] = initiate_service_discovery(self.connector, servicetype='weblogic', path=val,
                                                                      platform='Linux')
                response['status'] = 'Down'
                response['core_service'] = 1
                if activeweblogicdetails:
                    if activeweblogicdetails.get('home_path'):
                        if activeweblogicdetails.get('home_path') == relatedversionpath:
                            response['cpu_percent'] = activeweblogicdetails.get('cpu_percent')
                            response['memory_percent'] = activeweblogicdetails.get('memory_percent')
                            response['port'] = activeweblogicdetails.get('port')
                            response['status'] = 'Up'
                serviceexistance.append('weblogic')
                service_list.append(response)
                logger.debug(logger_ip=logger_ip,
                             message=f"Discovered service details for service: Weblogic, Details: {response} host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

            # tomcat
            activetomcatdetails = serviceavailabilitylist.get('tomcat')

            for tomcatpathlistitem in tomcatpathlist:
                response = dict(task_id=self.connector.task_id, host_id=self.connector.host_id)
                key, val = next(iter(tomcatpathlistitem.items()))
                response['name'] = 'tomcat'
                response['path'] = val
                response['version'] = tomcatversiondict.get(key)
                response['type'] = service_types.get('tomcat')
                response['discovery_id'] = initiate_service_discovery(self.connector, servicetype='tomcat', path=val,
                                                                      platform='Linux')
                response['status'] = 'Down'
                response['core_service'] = 1
                if activetomcatdetails:
                    if activetomcatdetails.get('home_path'):
                        if activetomcatdetails.get('home_path') == key:
                            response['cpu_percent'] = activetomcatdetails.get('cpu_percent')
                            response['memory_percent'] = activetomcatdetails.get('memory_percent')
                            response['port'] = activetomcatdetails.get('port')
                            response['status'] = 'Up'
                serviceexistance.append('tomcat')
                service_list.append(response)
                logger.debug(logger_ip=logger_ip,
                             message=f"Discovered service details for service: Tomcat, Details: {response} host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

            for system_services_item in system_services_results:
                if not system_services_item.get('system_service_name') in ['']:
                    response = dict(task_id=self.connector.task_id, host_id=self.connector.host_id,
                                    name=system_services_item.get('system_service_name'), core_service=0)
                    if not system_services_item.get('cpu_usage') in ['']:
                        response['cpu_percent'] = system_services_item.get('cpu_usage')
                    if not system_services_item.get('memory_usage') in ['']:
                        response['memory_percent'] = system_services_item.get('memory_usage')
                    if not system_services_item.get('port_number') in ['']:
                        response['port'] = system_services_item.get('port_number')
                    response['status'] = 'Up' if system_services_item.get(
                        'system_service_status') == 'enabled' else 'Down'
                    for itemcoreservice in coreservices:
                        if itemcoreservice in str.lower(system_services_item.get('system_service_name')):
                            response['core_service'] = 1
                            response['name'] = itemcoreservice
                            break
                    if not response['name'] in serviceexistance:
                        service_list.append(response)

            self.host_handler.update_table(table_info=service_list, table_model=model.Service, logger_ip=logger_ip)


        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Exception occurred while collecting services details for the host ip: {self.connector.ip} , Host ID: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "services", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)

    def login_history(self, logger_ip=None):
        """
        Fetches the details of the login history of a particular host in linux platform instances
        :param logger_ip: string, ip_address of a host
        :return: list, list of user_name , ip_address, server name, of the user who logged into the host
        """

        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Fetching login histroy of the host by running shell script for the host_ip :- {logger_ip}")
            login_history_shell_response, err = self.connector.execute_shell(
                util.get_script(name=inspect.stack()[0][3], operatingsystem=self.connector.operatingsystem),
                cmd_info="login_history", logger_ip=logger_ip)
            login_history_data = util.shell_to_json(login_history_shell_response, info=inspect.stack()[0][3],
                                                    logger_ip=logger_ip)
            login_history_list = []
            if not login_history_data == []:
                for record in login_history_data:
                    login_details = {}
                    login_details['host_id'] = self.connector.host_id
                    try:
                        login_details['time'] = datetime.strptime(record['time'], '%b %d  %H:%M:%S %Y')
                    except Exception as e:
                        logger.error(f"Exception occurered while converting date format. {e}")
                        login_details['time'] = datetime.strptime(record['time'] + " 2020", '%b %d  %H:%M %Y')
                    login_details['user_name'] = record["user_name"]
                    login_details['server_ip'] = record["server_ip"]
                    login_history_list.append(login_details)

            self.host_handler.update_table(table_info=login_history_list, table_model=model.Login_History,
                                           logger_ip=logger_ip)

        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Exception occred while fetching login history by executing shell for IP :- {logger_ip}. Exception : {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "login_history", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)


    def certificates(self, logger_ip=None):
        """
        Fetches the certificate details of the linuc instances
        :param logger_ip: ip_address of the host
        :return: list of certificates details
        """
        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Fetching certificates details of the host by running shell script for the host_ip :- {logger_ip}")
            temp_dict = dict(host_id=self.connector.host_id)
            certificates_shell_response, err = self.connector.execute_shell(
                util.get_script(name=inspect.stack()[0][3], operatingsystem=self.connector.operatingsystem),
                cmd_info="certificates info", logger_ip=logger_ip)
            certificates_data = util.shell_to_json(certificates_shell_response, info=inspect.stack()[0][3],
                                                   logger_ip=logger_ip)
            certificates_list = []
            if not certificates_data == []:
                for record in certificates_data:
                    certificates_details = {}
                    certificates_details['host_id'] = self.connector.host_id
                    certificates_details['path'] = record["path"]
                    certificates_details['certificate_name'] = record["certificate_name"]
                    certificates_details['issuer'] = record["issuer"]
                    not_after = util.date_time_formatter(req=record["not_after"], platform="Linux", logger_ip=logger_ip)
                    certificates_details['not_after'] = not_after
                    not_before = util.date_time_formatter(req=record["not_before"], platform="Linux",
                                                          logger_ip=logger_ip)
                    certificates_details['not_before'] = not_before
                    certificates_details['serial_number'] = record["serial_number"]
                    # certificates_details['thumb_print'] = record["thumb_print"]
                    # certificates_details['dns_name_list'] = record["dns_name_list"]
                    # certificates_details['subject'] = record["subject"]
                    # certificates_details['version'] = record["version"]
                    # certificates_details['ps_provider'] = record["ps_provider"]

                    certificates_list.append(certificates_details)
            else:
                certificates_list.append(temp_dict)
            self.host_handler.update_table(table_info=certificates_list, table_model=model.Certificates,
                                           logger_ip=logger_ip)

        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Exception occred while fetching certificates information by executing shell script for IP :- {logger_ip}. Exception : {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "certificates", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)

    def port_finding(self, servicename, logger_ip=None):
        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Finding the ports host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

            self.host_handler_object = HandleHostDetails()
            resp = self.host_handler_object.get_user_port_input(self.connector.task_id, logger_ip=logger_ip)
            default_ports = util.get_properties(property_file_path=CreateInfra.resource_properties_path.value,
                                                section='defaultports')
            port = next((item['port'] for item in resp if item["name"] == servicename), default_ports.get(servicename))

            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully found port for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")
            return port

        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Exception occurred while finding port for the host ip: {self.connector.ip} , Host ID: {self.connector.host_id}. Error: {e} ")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "port finding", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)

            return None

    def generate_raw_text_files(self, logger_ip=None):
        """
        generates RAW text files and stores the data for every IP by where each RAW files contain the  obtained by running in shell
        :param logger_ip: string, IP_Address of the host that is being discovered
        :return: NA,
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Generating raw Text files host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")
            directory, hosted_platform = util.raw_reports_dir(self.connector.discoveryname, logger_ip)
            commands = []
            file = ""
            if "ubuntu" in self.connector.operatingsystem:
                os = self.connector.operatingsystem[:-1]
            else:
                os = self.connector.operatingsystem
            os_commands = util.get_properties(property_file_path=CreateInfra.resource_properties_path.value, section=os)
            for key, value in os_commands.items():
                commands.append(dict(name=key, command=value))
            for command in commands:
                ssh_stdout = self.connector.execute_shell_raw(command['command'], cmd_info=command['name'], logger_ip=logger_ip)
                if hosted_platform == 'Linux':
                    file = directory + "/" + command['name'] + ".txt"
                if hosted_platform == 'Windows':
                    file = directory + "\\" + command['name'] + ".txt"
                if ssh_stdout:
                    with open(file, 'x') as f:
                        f.writelines(ssh_stdout)
            return directory
        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Exception occurred while generating raw text files for the host ip: {self.connector.ip} , Host ID: {self.connector.host_id}. Error: {e}")
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": "raw text files", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)
            return None

    def generate_raw_excel_files(self, directory, logger_ip=None):
        """
        generates RAW excel files and stores the data for every IP by where each RAW files contain the  obtained by running in shell
        :param logger_ip: string, IP_Address of the host that is being discovered
        :return: NA,
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Generating raw Excel files host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")
            writer = ""
            if os.path.exists(directory + "/" + self.connector.ip + '_raw_report.xlsx'):
                try:
                    os.remove(directory + "/" + self.connector.ip + '_raw_report.xlsx')
                except OSError as e:
                    error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": "raw excel file path ", "error": e}
                    self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)
            else:
                writer = pd.ExcelWriter(directory + "/" + self.connector.ip + '_raw_report.xlsx', engine='xlsxwriter')
            os_commands = ['environment_variables', 'processes', 'system_drivers', 'system_services']  # file names
            for i in os_commands:
                command = dict(name=i, command=util.get_script(name=i, operatingsystem=self.connector.operatingsystem, version=self.connector.version))
                ssh_stdout, ssh_stderr = self.connector.execute_shell(command['command'], cmd_info=command['name'], logger_ip=logger_ip)
                if "port" == command['name'] and 'solaris' in str.lower(self.connector.operatingsystem):
                    resp = util.shell_to_json(ssh_stdout)
                elif "package" == command['name'] or "port" == command['name']:
                    resp = util.shell_to_jsonlist(ssh_stdout)
                else:
                    resp = util.shell_to_json(ssh_stdout)
                if resp in [[], None]:
                    continue
                pd.read_json(json.dumps(resp)).to_excel(writer, sheet_name=command['name'], index=False)
            writer.save()
            writer.close()
            logger.debug(logger_ip=logger_ip, message=f"Successfully generated summary text for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Exception occurred while generating summary text for the host ip: {self.connector.ip} , Host ID: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "raw files", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)
            return None

    def raw_data_to_db(self, logger_ip=None):
        """
        generates RAW data and stores the data to DB Tables for every IP by where each RAW files contain the  obtained by running in shell
        :param logger_ip: string, IP_Address of the host that is being discovered
        :return: NA,
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Generating Raw data to store in DB for the host ip: {self.connector.ip} ,host_id: {self.connector.host_id} ")
            commmands = \
                {
                    'environment_variables': model.RawEnvVariables,
                    'processes': model.RawProcess,
                }
            os_commands = commmands.keys()
            for i in os_commands:
                command = dict(name=i, command=util.get_script(name=i, operatingsystem=self.connector.operatingsystem, version=self.connector.version))
                ssh_stdout, ssh_stderr = self.connector.execute_shell(command['command'], cmd_info=command['name'], logger_ip=logger_ip)
                logger.debug(logger_ip=logger_ip, message=f"COMMAND = {command}")
                resp = util.shell_to_json(ssh_stdout)
                for item in resp:
                    item.update(dict(host_id=self.connector.host_id))
                self.host_handler.update_table(table_info=resp, table_model=commmands.get(i), logger_ip=logger_ip)
        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Exception occurred while dumping raw data to db for the host ip: {self.connector.ip} , Host ID: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "raw files", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)
            return None

    def version(self, service_paths, logger_ip=None):
        try:
            versions = {}
            versionpathargs = {}
            final_versions = {}
            no_paths_req_services = ["mysql_version", "nginx_version"]
            for path in service_paths:
                key, val = next(iter(path.items()))
                if "_version" in key:
                    if key in versionpathargs.keys():
                        versionpathargs[key] = versionpathargs[key] + ' ' + '"' + val + '"'
                    else:
                        versionpathargs[key] = '"' + val + '"'
            for key in versionpathargs:
                val = versionpathargs[key]
                version_data, err = self.connector.execute_shell(
                    util.get_script(name=key, operatingsystem=self.connector.operatingsystem, arguments=val),
                    cmd_info="version check", logger_ip=logger_ip)
                version_result = util.shell_to_json(version_data, info=key, logger_ip=logger_ip)
                if util.nullcheck_return_list_firstentry(version_result):
                    version = version_result[0].get(key)
                    servicename = key.replace('_version', '')
                    versions[servicename] = version
            for item in no_paths_req_services:
                version_data, err = self.connector.execute_shell(
                    util.get_script(name=item, operatingsystem=self.connector.operatingsystem),
                    cmd_info="version check", logger_ip=logger_ip)
                version_result = util.shell_to_json(version_data, info=item, logger_ip=logger_ip)
                if util.nullcheck_return_list_firstentry(version_result):
                    version = version_result[0].get(item)
                    servicename = item.replace('_version', '')
                    versions[servicename] = version
            for veritem in versions.keys():
                if versions[veritem] != '':
                    final_versions[veritem] = versions[veritem]
            return final_versions
        except Exception as e:
            return None

    def status(self, logger_ip=None):
        try:
            status_data = self.connector.execute_shell(
                util.get_script(name=inspect.stack()[0][3], operatingsystem=self.connector.operatingsystem),
                cmd_info="services status")
            status_output = util.shell_to_json(status_data[0], info='Service_Status_Check', logger_ip=logger_ip)
            result = {}
            for statusitem in status_output:
                result[statusitem['service']] = dict(cpu_percent=statusitem['cpu'], memory_percent=statusitem['memory'],
                                                     port=statusitem['port'], home_path=statusitem.get('home_path'))
            return result
        except Exception as e:
            return {}

    def docker_info(self, logger_ip=None):
        """
        Collects docker information by running docker_info.sh shell script and saves information to database
        :param logger_ip: string, IP_Address of the host that is being discovered
        :return: Docker Information,
        """
        docker_info = []
        try:
            dock_info_obj = ""
            info = self.connector.execute_shell_docker(
                util.get_script(name=inspect.stack()[0][3], operatingsystem=self.connector.operatingsystem),
                cmd_info="docker_info")
            version = self.connector.execute_shell_docker(
                util.get_script(name="docker_version", operatingsystem=self.connector.operatingsystem),
                cmd_info="docker_info")
            if info and version:
                docker_info = dict(
                    host_id=self.connector.host_id,
                    name=version['Client']['Platform']['Name'],
                    version=version['Client']['Version'],
                    build_time=version['Client']['BuildTime'],
                    root_dir=info.get('DockerRootDir'),
                    images=info.get('Images'),
                    containers=info.get('Containers'),
                    containers_running=info.get('ContainersRunning'),
                    containers_paused=info.get('ContainersPaused'),
                    containers_stopped=info.get('ContainersStopped'),
                    # volumes=info['Plugins']['Volume'],
                    # networks=info['Plugins']['Network']
                )
                dock_info_obj = self.host_handler.update_table(table_info=docker_info, table_model=model.DockerInfo,
                                                               logger_ip=logger_ip)
            if dock_info_obj:
                self.docker_img_cont(dock_info_obj, logger_ip=logger_ip)
        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Exception occurred while generating Docker Info for the host ip: {self.connector.ip} , Host ID: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "docker_info", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)
            return docker_info

    def docker_img_cont(self, dock_info_obj, logger_ip=None):
        """
        Generates information of docker images, docker containers using docker_img_cont.sh and container stats using docker_stats.sh shell script
        :param logger_ip: string, IP_Address of the host that is being discovered
        :return: None
        """
        images = []
        cont_info = []
        try:
            data = self.connector.execute_shell_docker(
                util.get_script(name=inspect.stack()[0][3], operatingsystem=self.connector.operatingsystem),
                cmd_info="images and containers")
            stats, err = self.connector.execute_shell(
                util.get_script(name='docker_stats', operatingsystem=self.connector.operatingsystem),
                cmd_info="container stats", logger_ip=logger_ip)
            stats = shell_to_json(stats, info="docker_stats", logger_ip=logger_ip)
            image_details = data.get('Images')
            container_details = data.get('Containers')

            if image_details:
                for image in image_details:
                    image.update(docker_info_id=dock_info_obj[0].get('id'), image_id=image.get('ID').partition(":")[2])
                    image.pop('ID', None)
                    image = {k.lower(): v for k, v in image.items()}
                    images.append(image)
                dock_img_obj = self.host_handler.update_table(table_info=images, table_model=model.DockerImage,
                                                              logger_ip=logger_ip)
                for img in dock_img_obj:
                    img_name = img.get('repository') + ":" + img.get('tag')
                    if container_details:
                        for container in container_details:
                            if img_name == container.get('Image'):
                                container.update(docker_image_id=img.get('id'), container_id=container.get('ID'))
                                ls = ['ID', 'Command']
                                [container.pop(key) for key in ls]
                                container = {k.lower(): v for k, v in container.items()}
                                for stat in stats:
                                    if stat.get('container_id') == container.get('container_id'):
                                        ls = ['name', 'container_id']
                                        [stat.pop(key) for key in ls]
                                        stat.update(container)
                                        cont_info.append(stat)

                self.host_handler.update_table(table_info=cont_info, table_model=model.DockerContainer,
                                               logger_ip=logger_ip)

        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Exception occurred while generating images and containers for the host ip: {self.connector.ip} , Host ID: {self.connector.host_id}. Error: {e}")

            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "dockr image cont", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)
        return None
