import datetime
import inspect
import os
import time

import pandas
from kombu.utils import json

from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db.handler import host as host_handler
from matilda_discovery.db.handler.host import HandleHostDetails
from matilda_discovery.db.models import model
from matilda_discovery.logger_engine import logger as logger_py_file

logger = logger_py_file.Logger()

from matilda_discovery.services.service_discovery.initiate_service_discovery import initiate_service_discovery
from matilda_discovery.utils import util


class Windows:
    def __init__(self, connector):
        self.connector = connector
        self.host_handler = HandleHostDetails()
        self.physical_processors = None
        pass

    def host_info(self, logger_ip=None):
        """
        Fetches the whole windows host information using power shells and updates the whole discovered information into the DB tables at one strech.
        Discovered info is memory_info , storage_info,interface_info,user_groups_info,cpu_info,packages_info,ports_info,services_info
        """
        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Collecting windows information for host_ip: {self.connector.ip}, host_id: {self.connector.host_id} ")

            self.cpu(logger_ip=logger_ip)
            self.memory(logger_ip=logger_ip)
            self.storage(logger_ip=logger_ip)
            interface_info = self.interface(logger_ip=logger_ip)
            service_availability_list = self.ports(interface_info, logger_ip=logger_ip)
            self.packages(logger_ip=logger_ip)
            self.user_groups(logger_ip=logger_ip)
            self.login_history(logger_ip=logger_ip)
            self.certificates(logger_ip=logger_ip)
            self.services(service_availability_list=service_availability_list, logger_ip=logger_ip)

            directory, hosted_platform= self.generate_raw_text_files(logger_ip=logger_ip)
            self.generate_raw_excel_files(directory, hosted_platform, logger_ip=logger_ip)
            self.raw_data_to_db(logger_ip=logger_ip)
            # self.host_handler.update_host_info(self.connector.host_id,memory_info, cpu_info, storage_info, interface_info, packages_info, user_groups_info, ports_info, services_info, login_history_info,certificates_info,logger_ip=logger_ip)

            logger.debug(logger_ip=logger_ip, message=f"  *******************Successfully collected windows information for the host_ip: {self.connector.ip}, host_id: {self.connector.host_id}******************* ")
            return None

        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Failed to collect windows information for the host_ip: {self.connector.ip}, host_id: {self.connector.host_id}. Error: {e}")
            return None

    def host(self, logger_ip=None):
        """
        Discovers basic host summary information by running a power shell script
        :param self.connector: an object of CMDHandler class.contains payload info, OS details and functions to run powershell and shell scripts
        :return: dictionary, basic summary details of a host
        """
        try:
            start_time = time.time()
            logger.debug(logger_ip=logger_ip,
                         message=f"Collecting host information for host_ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

            host_info = dict(name=self.connector.discoveryname, ip=self.connector.ip, task_id=self.connector.task_id,
                             host_id=self.connector.host_id, platform="Windows", operating_system="Windows", stage='3')
            host_powershell_resp = self.execute_powershell_script(logger_ip=logger_ip,
                                                                  cmd=util.get_script(name=inspect.stack()[0][3],
                                                                                      version=self.connector.version),
                                                                  info="host")[0]
            if host_powershell_resp.get("physical_processors"):
                self.physical_processors = host_powershell_resp.get("physical_processors")
                host_powershell_resp.pop('physical_processors')

            host_info.update(host_powershell_resp)

            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully collected host information for the host_ip: {self.connector.ip}, host_id: {self.connector.host_id}")
            logger.info(logger_ip=logger_ip,
                        message=f"Time taken to get host details of the host: {self.connector.ip} is: {round((time.time() - start_time), 2)} seconds")
            return host_info

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to collect host information for the host_ip: {self.connector.ip}, host_id: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "host", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)
            return {}

    def memory(self, logger_ip=None):
        """
        Discovery of memory details of the host by running a power shell script
        :param self.connector: an object of CMDHandler class.contains payload info, OS details and functions to run powershell and shell scripts
        :return: dictionary, memory details of a host
        """
        try:
            start_time = time.time()
            logger.debug(logger_ip=logger_ip,
                         message=f"Collecting memory information for host_ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

            memory_info = dict(host_id=self.connector.host_id)
            memory_powershell_response = self.execute_powershell_script(logger_ip=logger_ip,
                                                                        cmd=util.get_script(name=inspect.stack()[0][3],
                                                                                            version=self.connector.version),
                                                                        info="memory")[0]

            mem_type, mem_speed = None, None

            if memory_powershell_response.get("type"):
                mem_type = util.get_properties(property_file_path=CreateInfra.resource_properties_path.value,
                                               section='MemoryType_LookUp',
                                               property_name=memory_powershell_response["type"][0])
            if memory_powershell_response.get("speed"):
                mem_speed = memory_powershell_response["speed"][0]

            memory_powershell_response["type"] = mem_type
            memory_powershell_response["speed"] = mem_speed
            memory_info.update(memory_powershell_response)

            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully collected memory information for the host_ip: {self.connector.ip} , host_id: {self.connector.host_id}")
            logger.info(logger_ip=logger_ip,
                        message=f"Time taken to get memory details of the host: {self.connector.ip} is: {round((time.time() - start_time), 2)} seconds")
            self.host_handler.update_table(table_info=memory_info, table_model=model.Memory, logger_ip=logger_ip)

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to collect memory information for the host_ip: {self.connector.ip} , host_id: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "Memory", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)

    def storage(self, logger_ip=None):
        """
        Discovery of storage details of the host by running a power shell script
        :param self.connector: an object of CMDHandler class.contains payload info, OS details and functions to run powershell and shell scripts
        :return: dictionary, storage details of a host
        """
        try:
            start_time = time.time()
            logger.debug(logger_ip=logger_ip,
                         message=f"Collecting storage information for host_ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

            storage_list = []
            storage_info = dict(host_id=self.connector.host_id)
            storage_powershell_response = self.execute_powershell_script(logger_ip=logger_ip,
                                                                         cmd=util.get_script(name=inspect.stack()[0][3],
                                                                                             version=self.connector.version),
                                                                         info="storage")
            for record in storage_powershell_response:
                record.update(storage_info)
                storage_list.append(record)

            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully collected storage information for the host_ip: {self.connector.ip} , host_id: {self.connector.host_id}")
            logger.info(logger_ip=logger_ip,
                        message=f"Time taken to get storage details of the host: {self.connector.ip} is: {round((time.time() - start_time), 2)} seconds")
            self.host_handler.update_table(table_info=storage_list, table_model=model.Storage, logger_ip=logger_ip)

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to collect storage information for the host_ip: {self.connector.ip} , host_id: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "Storage", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)

    def user_groups(self, logger_ip=None):
        """
        Discovery of user_group details of the host by running a power shell script
        :param self.connector: an object of CMDHandler class.contains payload info, OS details and functions to run powershell and shell scripts
        :return: dictionary, user group details of a host
        """
        try:
            start_time = time.time()
            logger.debug(logger_ip=logger_ip,
                         message=f"Collecting user groups information for host_ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

            user_groups_info = []
            temp_dict = dict(host_id=self.connector.host_id)
            user_groups_powershell_response = self.execute_powershell_script(logger_ip=logger_ip, cmd=util.get_script(
                name=inspect.stack()[0][3], version=self.connector.version), info="user_groups")

            if not user_groups_powershell_response == []:
                for record in user_groups_powershell_response:
                    if str(record['user_type']) == "1":
                        record['user_type'] = "Local"
                    record.update(temp_dict)
                    user_groups_info.append(record)
            else:
                user_groups_info.append(temp_dict)

            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully collected user groups information for the host_ip: {self.connector.ip} , host_id: {self.connector.host_id}")
            logger.info(logger_ip=logger_ip,
                        message=f"Time taken to get user_group details of the host: {self.connector.ip} is: {round((time.time() - start_time), 2)} seconds")
            self.host_handler.update_table(table_info=user_groups_info, table_model=model.User_Group,
                                           logger_ip=logger_ip)

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to collect user groups information for the host_ip: {self.connector.ip} , host_id: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "user groups", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)

    def cpu(self, logger_ip=None):
        """
       Discovery of cpu details of the host by running a power shell script
       :param self.connector: an object of CMDHandler class.contains payload info, OS details and functions to run power shell and shell scripts
       :return: dictionary, CPU details of a host
        """
        try:
            start_time = time.time()
            logger.debug(logger_ip=logger_ip,
                         message=f"Collecting cpu information for host_ip: {self.connector.ip} ,host_id: {self.connector.host_id} ")

            cpu_info = dict(host_id=self.connector.host_id, physical_processors=self.physical_processors)
            cpu_powershell_response = self.execute_powershell_script(logger_ip=logger_ip,
                                                                     cmd=util.get_script(name=inspect.stack()[0][3],
                                                                                         version=self.connector.version),
                                                                     info="CPU")[0]
            cpu_info.update(cpu_powershell_response)

            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully collected   cpu information for the host_ip: {self.connector.ip} , host_id: {self.connector.host_id}")
            logger.info(logger_ip=logger_ip,
                        message=f"Time taken to get CPU details of the host: {self.connector.ip} is: {round((time.time() - start_time), 2)} seconds")
            self.host_handler.update_table(table_info=cpu_info, table_model=model.CPU, logger_ip=logger_ip)

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to collect cpu information for the host_ip: {self.connector.ip} , host_id: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "CPU", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)


    def packages(self, logger_ip=None):
        """
        Discovery of package details of the host by running a power shell script
        :param self.connector: an object of CMDHandler class.contains payload info, OS details and functions to run powershell and shell scripts
        :return: dictionary, package details of a host
        """

        try:
            start_time = time.time()
            logger.debug(logger_ip=logger_ip,
                         message=f"Collecting packages information for host_ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

            packages_info = []
            temp_dict = dict(host_id=self.connector.host_id)
            packages_powershell_response = self.execute_powershell_script(logger_ip=logger_ip, cmd=util.get_script(
                name=inspect.stack()[0][3], version=self.connector.version), info="packages")

            if not packages_powershell_response == []:
                for record in packages_powershell_response:
                    record.update(temp_dict)
                    packages_info.append(record)
            else:
                packages_info.append(temp_dict)

            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully collected packages information for the host_ip: {self.connector.ip} , host_id: {self.connector.host_id}")
            logger.info(logger_ip=logger_ip,
                        message=f"Time taken to get host details of the host: {self.connector.ip} is: {round((time.time() - start_time), 2)} seconds")
            self.host_handler.update_table(table_info=packages_info, table_model=model.Package, logger_ip=logger_ip)

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to collect packages information for the host_ip: {self.connector.ip} , host_id: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "packages", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)

    def interface(self, logger_ip=None, utiliztion=False):
        """
       Discovery of interface details of the host by running a power shell script
       :param self.connector: an object of CMDHandler class.contains payload info, OS details and functions to run powershell and shell scripts
       :return: dictionary, interface details of a host
        """

        try:
            start_time = time.time()
            logger.debug(logger_ip=logger_ip,
                         message=f"Collecting interface information for host_ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

            interfaces_info = []
            temp_dict = dict(host_id=self.connector.host_id)
            interfaces_powershell_response = self.execute_powershell_script(logger_ip=logger_ip, cmd=util.get_script(
                name=inspect.stack()[0][3], version=self.connector.version), info="interface")

            if not interfaces_powershell_response == []:
                for record in interfaces_powershell_response:
                    # del record['packet_max_size'] # power shell is returning empty string. to make it work we removed this element here. need to fix powershell
                    record.update(temp_dict)
                    interfaces_info.append(record)
            else:
                interfaces_info.append(temp_dict)

            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully collected interface information for the host_ip: {self.connector.ip} , host_id: {self.connector.host_id}")
            logger.info(logger_ip=logger_ip,
                        message=f"Time taken to get interface details of the host: {self.connector.ip} is: {round((time.time() - start_time), 2)} seconds")
            if not utiliztion:
                self.host_handler.update_table(table_info=interfaces_info, table_model=model.Interface,
                                               logger_ip=logger_ip)
            return interfaces_info

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to collect interface information for the host_ip: {self.connector.ip} , host_id: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "interface", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)

    def ports(self, interface, logger_ip=None):
        """
       Discovery of interface details of the host by running a power shell script
       :param self.connector: an object of CMDHandler class.contains payload info, OS details and functions to run powershell and shell scripts
       :return: dictionary, interface details of a host
       """
        try:
            serviceavailabilitylist = {}
            serviceslist = dict(iis='iis', weblogic='weblogic', jboss='jboss', tomcat='tomcat', nginx='nginx',
                                httpd='apache', sqlser='mssql', mysql='mysql', oracle='oracle', system='iis')
            start_time = time.time()
            logger.debug(logger_ip=logger_ip,
                         message=f"Collecting ports information for host_ip: {self.connector.ip} , host_id: {self.connector.host_id} ")

            ports_list = []
            ports_results = self.execute_powershell_script(logger_ip=logger_ip,
                                                           cmd=util.get_script(name=inspect.stack()[0][3],
                                                                               version=self.connector.version),
                                                           info="ports")

            self.host_handler_object = HandleHostDetails()
            user_port_data_resp = self.host_handler_object.get_user_port_input(self.connector.task_id,
                                                                               logger_ip=logger_ip)

            user_input_port_data = {}
            for item in user_port_data_resp:
                user_input_port_data[item.get('name')] = item.get('port')

            if ports_results not in [None, []]:

                # build listenign and established port details
                local_listening_ports_list, local_established_ports_list = [], []
                for each_port in ports_results:
                    if each_port['status'] == "LISTENING":
                        local_listening_ports_list.append(each_port['number'])
                    elif each_port['status'] == "ESTABLISHED":
                        local_established_ports_list.append(each_port['number'])

                    # below loop is to prepare service availability list for services details
                    for serviceitem in serviceslist.keys():
                        if serviceitem in each_port['service'].lower():
                            if each_port['service'] != 'System':
                                serviceavailabilitylist[serviceslist[serviceitem]] = dict(port=each_port['number'],
                                                                                          cpu_percent=each_port[
                                                                                              'cpu_percent'],
                                                                                          memory_percent=each_port[
                                                                                              'memory_percent'])
                                serviceslist.pop(serviceitem)
                                break
                            elif each_port['number'] == '80':
                                serviceavailabilitylist[serviceslist[serviceitem]] = dict(port=each_port['number'],
                                                                                          cpu_percent=each_port[
                                                                                              'cpu_percent'],
                                                                                          memory_percent=each_port[
                                                                                              'memory_percent'])
                                serviceslist.pop(serviceitem)
                                break

                for port in ports_results:
                    # Incomming and outgoing Direction
                    if port['status'] == "ESTABLISHED":
                        if port["number"] in local_listening_ports_list:
                            port["direction"] = "incoming"
                        if port["number"] not in local_listening_ports_list:
                            port["direction"] = "outgoing"

                    port['host_id'] = self.connector.host_id
                    port['status'] = 'ACTIVE' if port.get('status') == 'ESTABLISHED' else 'OPEN'
                    for interface_item in interface:
                        if interface_item.get('ip_address') == port['local_address']:
                            port['used_interface'] = interface_item.get('name')

                    # Local_Service
                    if str(port['number']) in user_input_port_data.values():
                        for service_name, each_port in user_input_port_data.items():
                            if each_port == str(port['number']):
                                port['local_service'] = service_name
                    else:
                        port['local_service'] = util.get_properties(
                            property_file_path=CreateInfra.resource_properties_path.value, section='PortConfig',
                            property_name=port['number']) or 'Unknown'
                    # with normal user pernetages are coming as empty string
                    if port.get('cpu_percent') == '':
                        port.pop('cpu_percent')
                    if port.get('memory_percent') == '':
                        port.pop('memory_percent')

                    ports_list.append(port)
            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully collected ports information for the host_ip: {self.connector.ip} , host_id: {self.connector.host_id}")
            logger.info(logger_ip=logger_ip,
                        message=f"Time taken to get port details of the host: {self.connector.ip} is: {round((time.time() - start_time), 2)} seconds")

            host_handler_object = host_handler.HandleHostDetails()
            host_handler_object.update_ports_info(port_info=ports_list, logger_ip=logger_ip)
            return serviceavailabilitylist

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to collect ports information for the host_ip: {self.connector.ip} , host_id: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "ports", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)
            return []

    def services(self, service_availability_list, logger_ip=None):
        """
        Discovery of service details of the host by running a power shell script
        :param self.connector: an object of CMDHandler class.contains payload info, OS details and functions to run powershell and shell scripts
        :return: dictionary, service details of a host
        """
        try:
            # fetching core services list
            coreservicesdata = self.host_handler.get_core_services(logger_ip=logger_ip)
            coreservices = []
            for coreitem in coreservicesdata:
                coreservices.append(coreitem.get('service'))

            serviceexistance = []

            # system services
            sys_service_data = self.execute_powershell_script(logger_ip=logger_ip,
                                                              cmd=util.get_script(name='sys_services',
                                                                                  version=self.connector.version),
                                                              info="services")

            serviceavailabilitylist = service_availability_list
            start_time = time.time()
            servicepath = None
            logger.debug(logger_ip=logger_ip,
                         message=f"Collecting services information for host_ip: {self.connector.ip} , host_id: {self.connector.host_id} ")
            service_list = []
            file_name = dict(iis='applicationhost.config', weblogic='domain-registry.xml', tomcat='server.xml',
                             apache='httpd.exe', jboss='standalone.bat',
                             nginx='nginx.conf', oracle='tnsnames.ora', mysql_version='mysql.exe', mysql='mysql.exe',
                             jboss_version='standalone.bat',
                             weblogic_version='weblogic.jar', tomcat_version='version.bat', oracle_version='oraclient')
            service_types = util.get_properties(property_file_path=CreateInfra.resource_properties_path.value,
                                                section='Servicetype')
            service_paths = self.execute_powershell_script(logger_ip=logger_ip,
                                                           cmd=util.get_script(name=inspect.stack()[0][3],
                                                                               version=self.connector.version),
                                                           info="services")
            versions = self.versions(service_paths=service_paths, logger_ip=logger_ip)
            if 'mssql' in versions:
                service_paths.append(dict(mssql=None))
            for pathitem in service_paths:
                response = dict(task_id=self.connector.task_id, host_id=self.connector.host_id)
                key, val = next(iter(pathitem.items()))
                if key == "mysql_version":
                    key = "mysql"
                # if key in versions.keys() or key in serviceavailabilitylist.keys():
                if not '_version' in key:
                    response['name'] = key
                    if val:
                        response['path'] = os.path.join(val, file_name[
                            key])  # appending file name to send path with file_name for service discovey
                        servicepath = response['path'].replace('\\', '/')
                    response['version'] = versions.get(key)
                    response['type'] = service_types.get(key)
                    response['core_service'] = 1
                    response['discovery_id'] = initiate_service_discovery(self.connector, servicetype=response['name'],
                                                                          path=servicepath, platform='Windows')
                    if key in serviceavailabilitylist.keys():
                        addtional_details = serviceavailabilitylist[key]
                        response['cpu_percent'] = addtional_details['cpu_percent']
                        response['memory_percent'] = addtional_details['memory_percent']
                        response['port'] = addtional_details['port']
                        response['status'] = 'Up'
                    else:
                        response['status'] = 'Down'
                    serviceexistance.append(key)
                    service_list.append(response)

            for system_services_item in sys_service_data:
                if not system_services_item.get('Name') in ['']:
                    response = dict(task_id=self.connector.task_id,
                                    host_id=self.connector.host_id,
                                    name=system_services_item.get('Name'),
                                    path=system_services_item.get('Path'),
                                    core_service=0)
                    if not system_services_item.get('cpu') in ['']:
                        response['cpu_percent'] = system_services_item.get('cpu')
                    if not system_services_item.get('memory(%)') in ['']:
                        response['memory_percent'] = system_services_item.get('memory(%)')
                    # if not system_services_item.get('port_number') in ['']:
                    #     response['port'] = system_services_item.get('port_number')
                    response['status'] = 'Up' if system_services_item.get('Status') == 'OK' else 'Down'
                    # if any(substring in system_services_item.get('Name') for substring in coreservices):
                    #     response['core_service'] = 1
                    # service_list.append(response)
                    for itemcoreservice in coreservices:
                        if itemcoreservice in str.lower(system_services_item.get('Name')):
                            response['core_service'] = 1
                            response['name'] = itemcoreservice
                            break
                    if not response['name'] in serviceexistance:
                        service_list.append(response)

            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully collected   services information for the host_ip: {self.connector.ip} , host_id: {self.connector.host_id}")
            logger.info(logger_ip=logger_ip,
                        message=f"Time taken to get services_list  details of the host: {self.connector.ip} is: {round((time.time() - start_time), 2)} seconds")
            self.host_handler.update_table(table_info=service_list, table_model=model.Service, logger_ip=logger_ip)

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to collect    services information for the host_ip: {self.connector.ip} , host_id: {self.connector.host_id}. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "services", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)

    def login_history(self, logger_ip=None):
        """
        Fetches the details of the login history of a particular host
        :param logger_ip: string, ip_address of a host
        :return: list, list of user_name , ip_address, server name, of the user who logged into the host
        """

        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Fetching login histroy of the host by running powershell script for the host_ip :- {logger_ip}")
            temp_dict = dict(host_id=self.connector.host_id)
            login_history_powershell_response = self.execute_powershell_script(logger_ip=logger_ip, cmd=util.get_script(
                name=inspect.stack()[0][3], version=self.connector.version), info="login history")
            login_history_list = []
            if not login_history_powershell_response == [None, []]:

                for record in login_history_powershell_response:
                    login_details = {}
                    login_details['host_id'] = self.connector.host_id
                    if record.get("TimeCreated"):
                        login_details['time'] = datetime.datetime.strptime(record["TimeCreated"],
                                                                           '%m/%d/%Y %I:%M:%S %p')

                    login_details['user_name'] = record["User"]
                    login_details['server_ip'] = record["IPAddress"]
                    login_details['server_name'] = record["ServerName"]
                    login_details['action'] = record["Action"]
                    login_history_list.append(login_details)
            else:
                login_history_list.append(temp_dict)
            self.host_handler.update_table(table_info=login_history_list, table_model=model.Login_History,
                                           logger_ip=logger_ip)

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Exception occred while fetching login history by executing powershell for IP :- {logger_ip}. Exception : {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "login history", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)

    def certificates(self, logger_ip=None):
        """
        Fetches the details of the certificates details of a particular host
        :param logger_ip: string, ip_address of a host
        :return:
        """

        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Fetching certificates details of the host by running power shell script for the host_ip :- {logger_ip}")
            temp_dict = dict(host_id=self.connector.host_id)
            certificates_powershell_response = self.execute_powershell_script(logger_ip=logger_ip, cmd=util.get_script(
                name=inspect.stack()[0][3], version=self.connector.version), info="login history")
            certificates_list = []
            if not certificates_powershell_response == []:
                for record in certificates_powershell_response:
                    certificates_details = {}
                    certificates_details['host_id'] = self.connector.host_id

                    certificates_details['path'] = record["path"]
                    certificates_details['certificate_name'] = record["certificate_name"]
                    certificates_details['issuer'] = record["issuer"]
                    not_after = util.date_time_formatter(req=record["not_after"], platform="Windows",
                                                         logger_ip=logger_ip)
                    certificates_details['not_after'] = not_after
                    not_before = util.date_time_formatter(req=record["not_before"], platform="Windows",
                                                          logger_ip=logger_ip)
                    certificates_details['not_before'] = not_before
                    certificates_details['serial_number'] = record["serial_number"]
                    certificates_details['thumb_print'] = record["thumb_print"]
                    certificates_details['dns_name_list'] = record["dns_name_list"]
                    certificates_details['subject'] = record["subject"]
                    certificates_details['version'] = record["version"]
                    certificates_details['ps_provider'] = record["ps_provider"]

                    certificates_list.append(certificates_details)
            else:
                certificates_list.append(temp_dict)
            self.host_handler.update_table(table_info=certificates_list, table_model=model.Certificates,
                                           logger_ip=logger_ip)

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Exception occred while fetching login history by executing powershell for IP :- {logger_ip}. Exception : {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "certificates", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)


    def port_finding(self, servicename, logger_ip=None):  # done
        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Finding the ports host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")
            self.host_handler_object = HandleHostDetails()
            resp = self.host_handler_object.get_user_port_input(self.connector.task_id, logger_ip=logger_ip)
            default_ports = util.get_properties(property_file_path=CreateInfra.resource_properties_path.value,
                                                section='defaultports')
            port = next((item['port'] for item in resp if item["name"] == servicename), default_ports.get(servicename))
            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully found port for host ip: {self.connector.ip} , host_id: {self.connector.host_id} ")
            return port
        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Exception occurred while finding port for the host ip: {self.connector.ip} , Host ID: {self.connector.host_id}. Error: {e} ")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "port finding", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)

            return None

    def generate_raw_text_files(self, logger_ip=None):
        """
        generates RAW text files and stores the data for every IP by where each RAW files contain the data obtained by running commands in power shell.
        :param logger_ip: string, IP_Address of the host that is being discovered
        :return: directory and hosted platform,
        """
        try:
            logger.info(logger_ip=logger_ip, message=f"Preparing RAW Text files for the IP : {logger_ip} ")
            os_commands = util.get_properties(property_file_path=CreateInfra.resource_properties_path.value,
                                              section=CreateInfra.Windows.value)
            directory, hosted_platform = util.raw_reports_dir(self.connector.discoveryname, logger_ip)

            commands = []
            file = ""

            for key, value in os_commands.items():
                commands.append(dict(name=key, command=value))

            for command in commands:
                powershell_response = self.connector.execute_power_shell(cmd=command['command'],
                                                                         cmd_info=command['name'], logger_ip=logger_ip)

                if hosted_platform == 'Linux':
                    file = directory + "/" + command['name'] + ".txt"
                if hosted_platform == 'Windows':
                    file = directory + "\\" + command['name'] + ".txt"
                if powershell_response:
                    with open(file, 'x') as f:
                        f.writelines(powershell_response)
            return directory, hosted_platform
        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to generate Raw Text files. Error: {e}")
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": "Raw Text Files", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)
            return None

    def generate_raw_excel_files(self, directory, hosted_platform, logger_ip=None):
        """
        generates RAW Excel files and stores the data for every IP by where each RAW files contain the data obtained by running commands in power shell.
        :param logger_ip: string, IP_Address of the host that is being discovered
        :return: NA,
        """
        try:
            logger.info(logger_ip=logger_ip, message=f"Preparing RAW Excel files for the IP : {logger_ip} ")
            commands = []
            os_commands = util.get_properties(property_file_path=CreateInfra.resource_properties_path.value,
                                              section=CreateInfra.Windows.value)
            comd_list = ['win_uptime_services', 'win_scheduled', 'win_programs', 'win_processes', 'win_system_drivers']
            for key, value in os_commands.items():
                commands.append(dict(name=key, command=value))

            for command in commands:
                powershell_response = self.connector.execute_power_shell(cmd=command['command'],
                                                                         cmd_info=command['name'], logger_ip=logger_ip)

                if any(name in command['name'] for name in comd_list):
                    response = util.powershell_to_json(powershell_response)
                else:
                    response = util.powershell_to_json_raw_files_format(powershell_response)

                if hosted_platform == 'Linux':
                    pandas.read_json(json.dumps(response)).to_csv(directory + "/" + command['name'] + ".csv")

                if hosted_platform == 'Windows':
                    pandas.read_json(json.dumps(response)).to_csv(directory + "\\" + command['name'] + ".csv")

            logger.info(logger_ip=logger_ip, message=f"Completed creation of RAW Files for the Host {logger_ip}")

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to generate RAW Excel Files. Error: {e}")
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": "Excel Files", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)
            return None

    def raw_data_to_db(self, logger_ip=None):
        """
        generates RAW data and stores the data to DB tables for every IP by where each RAW files contain the data obtained by running commands in power shell.
        :param logger_ip: string, IP_Address of the host that is being discovered
        :return: NA,
        """
        try:
            logger.info(logger_ip=logger_ip, message=f"Preparing RAW data to store in DB for the IP : {logger_ip} ")
            commmands = \
                {
                    'raw_env': model.RawEnvVariables,
                    'raw_process': model.RawProcess,
                    'raw_programs': model.RawPrograms,
                    'raw_services': model.RawServices,
                    'raw_uptime': model.RawUptime
                }

            os_commands = commmands.keys()

            for i in os_commands:
                command = dict(name=i,
                               command=util.get_script(name=i, operatingsystem=self.connector.operatingsystem,
                                                       version=self.connector.version))

                powershell_response = self.connector.execute_power_shell(cmd=command['command'],
                                                                         cmd_info=command['name'],
                                                                         logger_ip=logger_ip)

                comd_list = ['raw_services', 'raw_programs', 'raw_process']

                if any(name in command['name'] for name in comd_list):
                    response = util.powershell_to_json(powershell_response)

                else:
                    response = util.powershell_to_json_raw_files_format(powershell_response)

                for item in response:
                    item.update(dict(host_id=self.connector.host_id))

                self.host_handler.update_table(table_info=response, table_model=commmands.get(i),
                                               logger_ip=logger_ip)

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to save Raw data to DB . Error: {e}")
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": "Raw Data to DB", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)
            return None

    def execute_powershell_script(self, cmd=None, info=None, logger_ip=None):
        """
        executes any powershell sent into the function as a string and returns json formatted output
        :param cmd: string,powershell script as a single string
        :param info:string,  information about the kind of script
        :return:Dictionary, output of power shell script
        """
        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Executing {info} powershell script  for IP : {self.connector.ip}")
            response = self.connector.execute_power_shell(cmd=cmd, cmd_info=info, logger_ip=logger_ip)
            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully executed the power shell script. POWER SHELL RESPONSE: {response}")
            if response:
                result = util.powershell_to_json(response, info=info, logger_ip=logger_ip)
                logger.debug(logger_ip=logger_ip,
                             message=f"Successfully executed the power shell script and converted into json response. POWER SHELL TO JSON RESPONSE: {result}")
                return result
            else:
                raise Exception(f"Failed to get response. response = {response}")

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to execute the Power shell script. Error: {e}")
            path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
            error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "executing powershell"+ "info", "error": e}
            self.host_handler.update_error_table(error_data=error_data, logger_ip=logger_ip)
            return None

    def versions(self, service_paths=None, logger_ip=None):
        versions = {}
        versionpathargs = {}
        final_versions = {}
        no_script_versions = ["nginx"]  # we don't have version check scripts for these servers
        try:
            for path in service_paths:
                key, val = next(iter(path.items()))
                if "_version" in key:
                    if key in versionpathargs.keys():
                        versionpathargs[key] = versionpathargs[key] + ',"' + val.replace('\\', '/') + '"'
                    else:
                        versionpathargs[key] = '"' + val.replace('\\', '/') + '"'
                if key == "iis":
                    versionpathargs["iis_version"] = None
                if key in no_script_versions:
                    versionpathargs[key] = None

            for key in versionpathargs:
                val = versionpathargs[key]
                if key in no_script_versions:
                    versions[key] = val
                else:
                    version_data = self.execute_powershell_script(logger_ip=logger_ip, cmd=util.get_script(name=key,
                                                                                                           version=self.connector.version,
                                                                                                           arguments=val),
                                                                  info="versionsinfo " + key)
                    if util.nullcheck_return_list_firstentry(version_data):
                        version = version_data[0].get(key)
                        servicename = key.replace('_version', '')
                        versions[servicename] = version
            mssql_version = self.execute_powershell_script(logger_ip=logger_ip,
                                                           cmd=util.get_script(name="mssql_version",
                                                                               version=self.connector.version,
                                                                               arguments=None),
                                                           info="versionsinfo mssql")
            if util.nullcheck_return_list_firstentry(mssql_version):
                version = mssql_version[0].get("mssql_version")
                versions["mssql"] = version
            for veritem in versions.keys():
                if versions[veritem] != '':
                    final_versions[veritem] = versions[veritem]
        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed at finding versions. Error: {e}")
        return final_versions
