import inspect

from matilda_discovery.connector import cmd_handler
from matilda_discovery.services.discovery.linux import Linux
from matilda_discovery.services.discovery.windows import Windows
from matilda_discovery.services.utilizations.utilization import utilization
from matilda_discovery.utils import util
from matilda_discovery.db.handler.host import HandleHostDetails
from matilda_discovery.db.handler.task import HandleTaskList

from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()



class Discovery:
    """
    contains methods that handle discovery
    """

    def __init__(self, req_data=None):
        self.req_data = req_data
        self.linuxflag, self.winflag, self.win_error, self.linux_error = None, None, None, None
        self.connector = cmd_handler.CMDHandler(req_data)
        self.handle_host_object = HandleHostDetails()
        self.task_handler_object = HandleTaskList()
        self.logger_ip = req_data['current_ip']

        if req_data.get("servertype") == 'Linux':
            ssh, out, self.linuxflag, self.linux_error = self.connector.ssh_connect(logger_ip=self.logger_ip)
            self.discovery_object = Linux(self.connector)

        if req_data.get("servertype") == 'Windows':
            session, host_resp, self.winflag, self.win_error, e = self.connector.windows_session(logger_ip=self.logger_ip)
            self.discovery_object = Windows(self.connector)

        if req_data.get("servertype") == 'UknownServer':
            ssh, out, self.linuxflag, self.linux_error = self.connector.ssh_connect(logger_ip=self.logger_ip)
            if self.linuxflag:
                self.discovery_object = Linux(self.connector)
            else:
                session, host_resp, self.winflag, self.win_error, e = self.connector.windows_session(logger_ip=self.logger_ip)
                if self.winflag:
                    self.discovery_object = Windows(self.connector)

    def discovery(self):
        logger.debug(logger_ip=self.logger_ip, message=f"\n ****************")
        logger.debug(logger_ip=self.logger_ip, message=f"\n   ")
        logger.debug(logger_ip=self.logger_ip, message=f"Discovery Initiated for IP {self.logger_ip}")
        logger.debug(logger_ip=self.logger_ip, message=f"\n   ")
        logger.debug(logger_ip=self.logger_ip, message=f"\n ****************")

        logger.debug(logger_ip=self.logger_ip, message=f"Initiating {self.connector.operatingsystem} discovery for discovery ip: {self.logger_ip} host_id: {self.connector.host_id} and updating task status to in_progress")

        self.task_handler_object.update_discovery_task_list(task_id=self.connector.task_id, status="in_progress", start_date=util.get_datetime())#task status update
        self.handle_host_object.update_host_table(host_id=self.connector.host_id, status="in_progress", reason="Authenticated", stage="3", device_type="compute")
        try:
            if self.linuxflag or self.winflag:
                self.handle_host_object.update_host_table(host_id=self.connector.host_id,device_identification_flag=1,login_flag=1, logger_ip=self.logger_ip)

                host_information = self.discovery_object.host(logger_ip=self.logger_ip)
                self.handle_host_object.update_host_table(logger_ip=self.logger_ip, **host_information) #host information update

                self.discovery_object.host_info(logger_ip=self.logger_ip)

                utilization_object = utilization(self.req_data)
                utilization_object.utilization(first_utilization=True,logger_ip=self.logger_ip)

                self.handle_host_object.update_host_table(host_id=self.connector.host_id, status="success", stage=4, end_date=util.get_datetime())
                self.task_handler_object.task_status_update(task_id=self.connector.task_id, logger_ip=self.logger_ip)#task status update
                self.connector.clearall()
                return self.req_data
            else:
                logger.debug(logger_ip=self.logger_ip, message=f"Unknow discovery: {self.connector.ip} ")
                host_information = {'host_id': self.connector.host_id, 'stage': "3", 'status': "failed",'end_date': util.get_datetime()}
                if 'Authentication failure' in [self.win_error, self.linux_error]:
                    host_information['device_type'] = 'compute'
                    host_information['reason'] = 'Authentication Failed'
                    host_information['login_flag'] = 2
                    host_information['device_identification_flag'] = 1
                else:
                    host_information['device_type'] = 'Unknown Device'
                    host_information['reason'] = 'Unable to Connect'
                    host_information['device_identification_flag'] = 2

                if self.connector.discovery_type == 'vcenter':
                    host_information["status"] = 'success'
                    host_information['device_type'] = 'compute'


                logger.error(logger_ip=self.logger_ip, message=f"Unable to access. Error: {host_information.get('reason')} ")

                host_handler = HandleHostDetails()
                path =  "path:- " + str(inspect.stack()[1][3] if inspect.stack()[1][3] else "") + " //" + str(inspect.stack()[0][3]) + "  "
                error_data = {"host_id": self.connector.host_id, "ip": self.connector.ip, "type": path + " ," + "Authentication problem", "error": host_information.get('reason')}
                host_handler.update_error_table(error_data=error_data, logger_ip=self.logger_ip)

                self.handle_host_object.update_host_table(logger_ip=self.logger_ip, **host_information)
                self.task_handler_object.task_status_update(task_id=self.connector.task_id, logger_ip=self.logger_ip)#task status update
                self.connector.clearall()

        except Exception as e:
            host_information = dict(host_id=self.connector.host_id, stage="4", status="failed", reason='Runtime Error', end_date=util.get_datetime())
            logger.error(logger_ip=self.logger_ip, message=f"Exception occurred while initiating discovery for ip: {self.connector.ip} Exception: {e}")
            self.handle_host_object.update_host_table(logger_ip=self.logger_ip, **host_information)
            self.task_handler_object.task_status_update(task_id=self.connector.task_id, logger_ip=self.logger_ip)
            self.connector.clearall()





