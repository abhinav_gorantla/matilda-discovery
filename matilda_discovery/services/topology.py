import math
import os
from random import randint
import base64

from sqlalchemy.sql import text
from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db.connection import get_engine
from matilda_discovery.db.handler.topology import TopologyDetails as topology_handler
from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.utils import util

logger = logger_py_file.Logger()


class NetworkTopology:

    def __int__(self):
        self.engine = get_engine()

    def get_network_dependency(self, ip_range=None, ip_range_list=None):
        """
        Accepts a ip range or task id or account id and returns network level topology
        :param ip_range: ip range for which dependency needs to be mapped
        :param ip_range_list: list of ip's in the range for which dependency needs to be mapped
        :return: dict, network level topology for the given range
        """
        try:
            logger.debug(f"Building network level dependency map for given ip_range: {ip_range}, ip_range_list: {ip_range_list}")

            temp_nodes = {}  # used to store {ip : host name} which will be used to create list of nodes
            nodes_list = []  # used to store list of dict of nodes with their properties
            links_list = []  # used to store all the links or connection between nodes
            link_id = 0  # used to create a unique link id for every link
            every_ip_node = []  # used to store all ip's that we are showing in the mapping regardless of weather its in the network or not

            # used to store counts of different server types
            server_type_count = \
                {
                    "Web Server": 0,
                    "Application Server": 0,
                    "Database": 0,
                    "Out of Network": 0,
                    "Undiscovered": 0,
                }

            if ip_range_list is None and ip_range is not None:
                ip_range_list = util.get_ip_list(ip_range)
            elif ip_range_list is None and ip_range is None:
                raise Exception(f"Missing IP range. Please provide ip range or ip list. Given input: ip_range: {ip_range}, ip_range_list: {ip_range_list}")

            topology_handler_object = topology_handler()

            # get list of all IPs in the network
            in_network_nodes = []
            temp_in_network_nodes = topology_handler_object.get_in_network_nodes()
            for item in temp_in_network_nodes:
                in_network_nodes.append(item.get('ip'))

            # get list of all incoming and outgoing connections
            network_connections = topology_handler_object.get_network_stream(ip_range_list=ip_range_list)

            for item in network_connections:
                every_ip_node.append(item['local_address'])
                every_ip_node.append(item['foreign_address'])

                # add all nodes into temp nodes in this form: {ip : host name}
                temp_nodes[item['local_address']] = item['local_name']

                if item.get('foreign_name'):
                    temp_nodes[item['foreign_address']] = item['foreign_name']

                else:
                    temp_nodes[item['foreign_address']] = item['foreign_address']

                # add source and targets to links_list based on direction of the link
                if item['direction'] == 'outgoing':
                    temp_link = \
                        {
                            "id": "link_" + str(link_id),
                            "name": 'link_name_' + str(randint(1, 100000)),
                            "source": item['local_address'],
                            "target": item['foreign_address'],
                            "properties":
                                {
                                    "process": item['service'],
                                    "local_port": item['local_port'],
                                    "foreign_port": item['foreign_port'],
                                }
                        }

                else:
                    temp_link = \
                        {
                            "id": "link_" + str(link_id),
                            "name": 'link_name_' + str(randint(1, 100000)),
                            "target": item['local_address'],
                            "source": item['foreign_address'],
                            "properties":
                                {
                                    "process": item['service'],
                                    "local_port": item['foreign_port'],
                                    "foreign_port": item['local_port'],
                                }
                        }

                link_id += 1
                links_list.append(temp_link)

            # get properties for all the nodes we are displaying
            every_ip_node = list(set(every_ip_node))  # to remove duplicates

            # get properties for the nodes that will be displayed
            node_properties = topology_handler_object.get_node_properties(every_ip_node=every_ip_node)

            node_property = {}

            # convert properties coming in as a list to a dict so it can be accessed easily
            for item in node_properties:
                node_property[item["local_address"]] = \
                    {
                        "IP": item["local_address"],
                        "service": item['name']+item['version'],
                        "status": item['status'],
                        "server_type": item['server_type'],
                    }

            # categories_set is used to collect distinct categories of all nodes
            categories_set = set()

            for ip_key, name_value in temp_nodes.items():
                temp_property = {}
                if node_property.get(ip_key):
                    temp_property = node_property.get(ip_key)

                # temp_node = {"x": coordinate[temp_index][0], "y": coordinate[temp_index][1], "id": ip_key, "name": name_value}
                temp_node = {"id": ip_key, "name": name_value}

                if ip_key in ip_range_list:
                    if temp_property.get('server_type') is not None:
                        temp_node["attributes"] = {"server_type": temp_property.pop('server_type')}

                        if "Application Server" in temp_node["attributes"]["server_type"]:
                            server_type_count["Application Server"] += 1
                        elif "Database" in temp_node["attributes"]["server_type"]:
                            server_type_count["Database"] += 1
                        elif "Web Server" in temp_node["attributes"]["server_type"]:
                            server_type_count["Web Server"] += 1
                        else:
                            server_type_count["Out of Network"] += 1

                        categories_set.add(temp_node["attributes"]["server_type"])
                    else:
                        server_type_count["Undiscovered"] += 1
                        temp_node["attributes"] = {"server_type": "Undiscovered"}

                elif ip_key in in_network_nodes:
                    server_type_count["Out of Network"] += 1
                    temp_node["attributes"] = {"server_type": "Out of range"}

                elif ip_key in every_ip_node:
                    server_type_count["Out of Network"] += 1
                    temp_node["attributes"] = {"server_type": "Out of network"}

                else:
                    temp_node["attributes"] = {"server_type": "Look into this"}

                temp_node["properties"] = temp_property
                temp_node["symbolSize"] = 25

                nodes_list.append(temp_node)

            # generate coordinates based on server type and count

            # get the max number of nodes in any category along x axis
            max_x_nodes = max(server_type_count.values())

            x_scaling = 1
            y_scaling = max(1, int(max_x_nodes / 2))

            y_coordinates =\
            {
                "Out of Network": 0 * y_scaling,
                "Web Server": 1.5 * y_scaling,
                "Application Server": 2.5 * y_scaling,
                "Database": 3.5 * y_scaling,
                "Undiscovered": 5 * y_scaling,
            }

            out_of_network_coordinates = self.generate_x_coordinates(max_x=max_x_nodes, number_of_nodes=server_type_count["Out of Network"])
            web_server_coordinates = self.generate_x_coordinates(max_x=max_x_nodes, number_of_nodes=server_type_count["Web Server"])
            app_server_coordinates = self.generate_x_coordinates(max_x=max_x_nodes, number_of_nodes=server_type_count["Application Server"])
            db_server_coordinates = self.generate_x_coordinates(max_x=max_x_nodes, number_of_nodes=server_type_count["Database"])
            undiscovered_coordinates = self.generate_x_coordinates(max_x=max_x_nodes, number_of_nodes=server_type_count["Undiscovered"])

            max_y_limit = y_coordinates["Out of Network"]
            max_x_limit = max\
                (
                    max\
                        (
                            max(out_of_network_coordinates) if out_of_network_coordinates else 0,
                            max(web_server_coordinates) if web_server_coordinates else 0,
                            max(app_server_coordinates) if app_server_coordinates else 0,
                            max(db_server_coordinates) if db_server_coordinates else 0,
                            max(undiscovered_coordinates) if undiscovered_coordinates else 0
                        ),
                    abs(min\
                        (
                            min(out_of_network_coordinates) if out_of_network_coordinates else 0,
                            min(web_server_coordinates) if web_server_coordinates else 0,
                            min(app_server_coordinates) if app_server_coordinates else 0,
                            min(db_server_coordinates) if db_server_coordinates else 0,
                            min(undiscovered_coordinates) if undiscovered_coordinates else 0
                        ))
                )

            for node_item in nodes_list:

                node_item["itemStyle"] = {'normal': {'borderColor': 'grey', 'borderWidth': 0, 'shadowBlur': 2, 'shadowColor': 'black'}}

                if str.lower(node_item["attributes"]["server_type"]) in ["out of range", "out of network"]:
                    node_item["x"] = (x_scaling * out_of_network_coordinates.pop()) + max_x_limit
                    node_item["y"] = y_coordinates["Out of Network"] - max_y_limit
                    node_item["itemStyle"] = {'normal': {'borderColor': 'grey', 'borderWidth': 0, 'shadowBlur': 2, 'shadowColor': 'black'}}

                if node_item["attributes"]["server_type"] == "Web Server":
                    node_item["x"] = (x_scaling * web_server_coordinates.pop()) + max_x_limit
                    node_item["y"] = y_coordinates["Web Server"] - max_y_limit
                    node_item["itemStyle"] = {'normal': {'borderColor': 'purple', 'borderWidth': 0, 'shadowBlur': 2, 'shadowColor': 'black'}}

                if node_item["attributes"]["server_type"] == "Application Server":
                    node_item["x"] = (x_scaling * app_server_coordinates.pop()) + max_x_limit
                    node_item["y"] = y_coordinates["Application Server"] - max_y_limit
                    node_item["itemStyle"] = {'normal': {'borderColor': 'maroon', 'borderWidth': 0, 'shadowBlur': 2, 'shadowColor': 'black'}}

                if node_item["attributes"]["server_type"] == "Database":
                    node_item["x"] = (x_scaling * db_server_coordinates.pop()) + max_x_limit
                    node_item["y"] = y_coordinates["Database"] - max_y_limit
                    node_item["itemStyle"] = {'normal': {'borderColor': 'orange', 'borderWidth': 0, 'shadowBlur': 2, 'shadowColor': 'black'}}

                if node_item["attributes"]["server_type"] == "Undiscovered":
                    node_item["x"] = (x_scaling * undiscovered_coordinates.pop()) + max_x_limit
                    node_item["y"] = y_coordinates["Undiscovered"] - max_y_limit
                    node_item["itemStyle"] = {'normal': {'borderColor': 'black', 'borderWidth': 0, 'shadowBlur': 2, 'shadowColor': 'black'}}


            # create a categories list for UI
            categories_list = [{"name": "Undiscovered"}, {"name": "Out of network"}]
            for item in categories_set:
                categories_list.append({"name": item})

            network_map = \
                    {
                        "nodes": nodes_list,
                        "links": links_list,
                        "categories": categories_list
                    }

            logger.debug(f"Successfully built topology for given ip_range: {ip_range}")

            return network_map

        except Exception as e:
            print("error: ", e)
            logger.error(f"Failed to build topology for given ip_range: {ip_range}. Error: {e}")
            return None

    def generate_x_coordinates(self, max_x, number_of_nodes):
        """
        Accepts maximum number of nodes and number of nodes, generates coordinates which are equidistant from center and each other.
        """
        try:
            logger.debug(f"Generating coordinates for max_x: {max_x}, number_of_nodes: {number_of_nodes}.")

            # x_scale = (max_x / number_of_nodes)*2
            # temp_x starts with [-max_x]  i.e., temp_x = [- max_x]
            # temp_x(n+1) = temp_x(n) + x_scale
            # x(n) = (temp_x(n) + temp_x(n+1) ) / 2

            x_scale = round(((max_x / number_of_nodes) * 2), 2)
            temp_x = [-max_x]
            x_coordinates = []

            for i in range(1, number_of_nodes + 1):
                temp_x.append(temp_x[i-1] + x_scale)
                x_coordinates.append( round(((temp_x[i - 1] + temp_x[i - 1] + x_scale) / 2), 2) )

            logger.debug(f"Successfully generate coordinates for max_x: {max_x}, number_of_nodes: {number_of_nodes}.")
            return x_coordinates

        except Exception as e:
            logger.error(f"Failed to generate coordinates for max_x: {max_x}, number_of_nodes: {number_of_nodes}. Error: {e}")
            return []


class HostTopology:

    def __int__(self):
        self.engine = get_engine()

    # @staticmethod
    # def get_ip_topology(ip):
    #     """
    #     Accepts a host ip and returns network topology around the given host ip
    #     :param ip: starting ip around which topology needs to be built
    #     :return: dict, network topology around the given host ip
    #     """
    #     try:
    #         logger.debug(f"Building topology for ip: {ip}")
    #
    #         topology_handler_object = topology_handler()
    #
    #         down_stream_topology = {'name': ip, 'children': []}
    #         down_stream_ip = {}
    #         down_stream_topology['children'] = [topology_handler_object.get_ip_stream(ip, down_stream_topology, down_stream_ip, "Outgoing")]
    #
    #         up_stream_topology = {'name': ip, 'children': []}
    #         up_stream_ip = {}
    #         up_stream_topology['children'] = [topology_handler_object.get_ip_stream(ip, up_stream_topology, up_stream_ip, "Incoming")]
    #
    #         up_stream_children = up_stream_topology['children'][0].get('children')
    #         if up_stream_children is None:
    #             up_stream_children = []
    #
    #         down_stream_children = down_stream_topology['children'][0].get('children')
    #         if down_stream_children is None:
    #             down_stream_children = []
    #
    #         topology = {'name': ip, 'children': up_stream_children + down_stream_children}
    #
    #         if topology['children'] in [None, []]:
    #             raise Exception('No dependencies found')
    #
    #         logger.debug(f"Successfully built topology for ip: {ip}")
    #         return topology
    #
    #     except Exception as e:
    #         logger.error(f"Failed to build topology for ip: {ip}. Error: {e}")
    #         return None

    @staticmethod
    def get_ip_stream(engine, starting_ip, direction):

        try:
            logger.debug(f"Getting host level ip stream for ip: {starting_ip}")

            scaling_factor = 0
            hierarchy = 0
            ip_queue = [[starting_ip, hierarchy, scaling_factor]]

            visited_nodes = {}

            temp_nodes = {}
            connections_list = []
            nodes_list = []
            link_counter = 0

            secondary_direction = 'incoming'
            if direction == 'incoming':
                secondary_direction = 'outgoing'

            while ip_queue:
                current_ip, hierarchy, scaling_factor = ip_queue.pop()

                if not visited_nodes.get(current_ip):
                    visited_nodes[current_ip] = True
                else:
                    continue

                temp_nodes[current_ip] = {"parents": set([]), "hierarchy": hierarchy}

                section_name = "Host_Level_Dependency"
                host_level_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name)

                query = host_level_queries.get('ip_dependency')
                final_query_output = engine.execute(text(query), local_address=current_ip, primary_direction=direction, secondary_direction=secondary_direction).fetchall()

                all_connections = []
                for item in final_query_output:
                    all_connections.append(item)

                temp_nodes[current_ip]["scaling factor"] = scaling_factor
                scaling_factor = len(all_connections) + scaling_factor - 1

                for item in final_query_output:
                    child_ip = item[0]
                    temp_nodes[current_ip]["parents"].add(child_ip)

                    if direction == "outgoing":
                        ip_queue.insert(0, [child_ip, hierarchy - 1, len(all_connections) + scaling_factor])
                        connections_list.append \
                            (
                                {
                                    "id": "link " + str(link_counter),
                                    "name": current_ip + " --> " + child_ip,
                                    "source": current_ip,
                                    "target": child_ip,
                                    "properties": {}
                                }
                            )

                    else:
                        ip_queue.insert(0, [child_ip, hierarchy + 1, len(all_connections) + scaling_factor])
                        connections_list.append \
                            (
                                {
                                    "id": "link " + str(link_counter),
                                    "name": child_ip + " --> " + current_ip,
                                    "source": child_ip,
                                    "target": current_ip,
                                    "properties": {}
                                }
                            )

                    link_counter += 1

            new_ip_queue = [starting_ip]
            temp_nodes[starting_ip]["x"] = 0

            visited_nodes = {}

            while new_ip_queue:

                current_ip = new_ip_queue.pop()

                if not visited_nodes.get(current_ip):
                    visited_nodes[current_ip] = True

                    parent_list = list(temp_nodes.get(current_ip).get("parents"))

                    if parent_list:
                        no_of_parents = len(parent_list)

                        x_list = [0]

                        if no_of_parents:
                            temp = int(no_of_parents / 2)
                            x_list = []
                            for i in range(-temp, temp + 1):
                                if no_of_parents % 2 == 0 and i == 0:
                                    continue
                                else:
                                    x_list.append(i)

                        for i in range(len(parent_list)):
                            if temp_nodes[parent_list[i]].get("x"):
                                continue
                            elif abs(temp_nodes[current_ip]["hierarchy"]) < abs(temp_nodes[parent_list[i]]["hierarchy"]):
                                temp_nodes[parent_list[i]]["x"] = x_list[i] + temp_nodes[current_ip]["x"]
                                new_ip_queue.insert(0, parent_list[i])

            for k, v in temp_nodes.items():

                if k == starting_ip:
                    nodes_list.append \
                        (
                            {
                                "id": k,
                                "name": k,
                                "symbolSize": 30,
                                "x": v.get("x"),
                                "y": v.get("hierarchy"),
                                'itemStyle': {'normal': {'color': 'orange'}},
                                "attributes": {}
                            }
                        )

                else:
                    nodes_list.append \
                        (
                            {
                                "id": k,
                                "name": k,
                                "symbolSize": 20,
                                "x": v.get("x"),
                                "y": v.get("hierarchy"),
                                'itemStyle': {'normal': {'color': 'black'}},
                                "attributes": {}
                            }
                        )

            logger.debug(f"Successfully got host level ip stream for ip: {starting_ip}")
            return nodes_list, connections_list

        except Exception as e:
            logger.error(f"Failed to get host level ip stream for ip: {starting_ip}. Error: {e}")
            return [], []

    @staticmethod
    def parse_network_map(incoming_nodes, incoming_connections, outgoing_nodes, outgoing_connections):
        try:
            logger.debug(f"Parsing network map.")
            final_connections = incoming_connections + outgoing_connections
            x_offset = 0
            y_offset = 0
            incoming_ip = []
            final_nodes = []

            for item in incoming_nodes:
                item['y'] = -item['y']

                if abs(item['x']) > x_offset:
                    x_offset = abs(item['x'])
                if abs(item['y']) > y_offset:
                    y_offset = abs(item['y'])

                item['y'] = -item['y']
                final_nodes.append(item)
                incoming_ip.append(item['id'])

            for item in outgoing_nodes:
                if item['id'] not in incoming_ip:

                    if abs(item['x']) > x_offset:
                        x_offset = abs(item['x'])
                    if abs(item['y']) > y_offset:
                        y_offset = abs(item['y'])

                    final_nodes.append(item)

            for nodes in final_nodes:
                nodes['x'] += x_offset
                nodes['y'] = 2 * (-nodes['y'] - y_offset)

            logger.debug(f"Successfully parsed the network map.")
            return {'nodes': final_nodes, 'links': final_connections}

        except Exception as e:
            logger.error(f"Failed to parse network map. Error: {e}")
            return {}