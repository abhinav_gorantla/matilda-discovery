import math

from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db import queries_helper
from matilda_discovery.db.handler.host import HandleHostDetails
from matilda_discovery.db.handler.recommendation import CreateRecommendation
from matilda_discovery.db.handler.topology import TopologyDetails
from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()
from datetime import datetime
from dateutil.relativedelta import relativedelta
from matilda_discovery.utils import util
from matilda_discovery.db.connection import get_engine
from matilda_discovery.db.handler import host as host_handler
from matilda_discovery.services.host import HostDetails
from matilda_discovery.utils.util import json_formatter
import json,requests
from requests.exceptions import ConnectionError

class Recommendation:

    def __init__(self):
        self.engine = get_engine()
        pass

    @staticmethod
    def infrastructure_migration_assessment(req_data, logger_ip=None):
        """
        analyses the infrastructure and provide recommendation for a specific cloud provider for evey host.
        req_data : dict, contains info of the whole host
        return : None, updates DB with the migration analysis in os_recommendation table
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"fetching os recommendations data for the host {req_data['ip']} for the cloud_provider : {req_data['cloud_provider']}")

            recommendation_handler_object = CreateRecommendation()
            os_lookup_data = recommendation_handler_object.fetch_operating_system_lookup(req_data, logger_ip=logger_ip)

            if os_lookup_data not in [[], None]:

                # priority, compatible, migration_strategy and compatibility_score
                priority = "Migrate soon"
                compatible = "Yes"
                compatibility_score = 50
                migration_strategy = "Rebuild"

                current_date = datetime.strptime(util.get_datetime().split(' ')[0], '%Y-%m-%d')
                support_date = os_lookup_data.get("support_date")
                if support_date:
                    support_date = datetime.strptime(support_date, '%Y-%m-%d')

                    eol = relativedelta(support_date, current_date).years

                    compatible = CreateInfra.os_cloud_compatible.value[0]  # "No"
                    compatibility_score = 0
                    priority = CreateInfra.os_cloud_move_priority.value[0]  # "Migrate later"
                    migration_strategy = CreateInfra.os_cloud_migration_strategy.value[0]  # "Rehost"

                    if eol <= 0:
                        priority = CreateInfra.os_cloud_move_priority.value[1]  # "Migrate immediately"
                        migration_strategy = CreateInfra.os_cloud_migration_strategy.value[1]  # "Rebuild"
                    elif eol <= 3:
                        priority = CreateInfra.os_cloud_move_priority.value[2]  # "Migrate soon"
                        # migration_strategy = CreateInfra.os_cloud_migration_strategy.value[2]  # "Rehost with optional upgrade"

                    if os_lookup_data.get("available_image_name"):
                        compatible = CreateInfra.os_cloud_compatible.value[1]  # "Yes"
                        compatibility_score += 50

                    if eol >= 0:
                        compatibility_score += eol * 2

                # criticality
                criticality = "Moderate"
                dependency_service_object = TopologyDetails()
                dependents = dependency_service_object.get_dependents_count(req_data["ip"])

                if dependents:

                    criticality = CreateInfra.os_cloud_criticality.value[2]  # "High"

                    if dependents <= 10:
                        criticality = CreateInfra.os_cloud_criticality.value[0]  # "Low"
                    elif dependents <= 20:
                        criticality = CreateInfra.os_cloud_criticality.value[1]  # "Moderate"

                # update everything in os_lookup_dat and return it
                os_lookup_data.update\
                    (
                        {
                            "criticality": criticality,
                            "compatible": compatible,
                            "compatibility_score": compatibility_score,
                            "priority": priority,
                            "migration_strategy": migration_strategy,
                            "migration_complexity": "Moderate", # default value...mock data
                        }
                    )

                logger.debug(logger_ip=logger_ip, message=f"successfully fetched os recommendation data. Data: {os_lookup_data}")
                return os_lookup_data
            else:
                logger.info(logger_ip=logger_ip, message=f"Empty data obtained for the os_lookup data for eol to make a recommendation for the host")
                return None

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to fetch support_date details. Error: {e}")
            return {}


    def infrastructure_price_recommendation(self,req_data, eol_recommendation_data,logger_ip=None):
        recommendation_data = {}

        try:
            logger.debug(logger_ip=logger_ip, message=f"Initiating price recommendation for the migration process : {req_data['id']}, cloud_provider:{req_data['cloud_provider']}")

            host_handler_object = host_handler.HandleHostDetails()
            host_data = host_handler_object.get_host_results(req_data['id'])[0]

            host_service_object = HostDetails()
            storage_info = host_service_object.get_disk_usage_info(host_data)
            cpu_information = host_service_object.get_cpu_info(host_data)
            memory_information = host_service_object.get_memory_info(host_data)

            json_formatter(storage_info)
            json_formatter(cpu_information)
            json_formatter(memory_information)

            vcpuCount = int(cpu_information.get('logical_processors'))
            memory = util.round_up(memory_information.get('capacity'),4)
            storage = float(storage_info.get('capacity')) if storage_info else None
            operating_system = req_data.get('platform')
            actual_storage_price = None

            #take region from user input and get the recommendation

            region = "westus"  # need to add this in future

            # cpuUtil = cpu_information['utilization_percent']
            # memUtil = memory_information['used_percent']
            # storageUtil = storage_info['used_percent']


            if req_data['platform'] == "Solaris":
                operating_system = "Linux"
            if req_data['cloud_provider'] == "Azure" and req_data['platform'] in ["Linux","Solaris"]:
                operating_system = "centos"
                # data in Azure DB is with centos. so the operating system is being sent as CENTOS.
            data = \
                {
                    "cloudProviderName": req_data.get('cloud_provider'),
                    "vcpuCount": vcpuCount,
                    "memory": memory,
                    "operatingSystem": operating_system
                }

            logger.debug(logger_ip=logger_ip, message=f"Fetching details from cost api for the exact instance configurations : {req_data['id']},with req_body :{data}")

            cloud_instance_price_details = self.price_recommendation_data(data,logger_ip=logger_ip)

            if cloud_instance_price_details is None:
                suggested_vcpuCount,suggested_memory=self.configuration_recommendation(memory=memory,vCPU=vcpuCount,cloud_provider=req_data.get('cloud_provider'),operating_system=operating_system,logger_ip=logger_ip)
                data['vcpuCount'] = suggested_vcpuCount
                data['memory'] = suggested_memory
                cloud_instance_price_details = self.price_recommendation_data(data, logger_ip=logger_ip)

            if req_data['cloud_provider']== "AWS":
                actual_storage_price = CreateInfra.aws_EBS_hdd_price.value * 12 * int(storage) if storage else None

            elif req_data['cloud_provider']== "Azure":
                nth_power_of_2 = math.ceil(math.log2(storage)) if storage else None
                azure_manage_disk_storage = pow(2, nth_power_of_2) if nth_power_of_2 else None
                if azure_manage_disk_storage:
                    azure_manage_disk_storage = 32 if azure_manage_disk_storage <= 32 else azure_manage_disk_storage
                    actual_storage_price=CreateInfra.azure_manged_disk_hdd_price.value[str(azure_manage_disk_storage)] * 12

            base_instance_price = \
                {
                    'instance_type': cloud_instance_price_details.get('instance_type') if cloud_instance_price_details else None,
                    'cloud_provider': req_data['cloud_provider'],
                    'yearly_onpremise_cost': CreateInfra.on_premise_vm_price.value,
                    'on_demand_price': (math.ceil(cloud_instance_price_details.get('on_demand_price'))if cloud_instance_price_details.get('on_demand_price') else None ) if cloud_instance_price_details else None,
                    'reserved_1_year_price': (math.ceil(cloud_instance_price_details.get('reserved_1_year_price'))if cloud_instance_price_details.get('reserved_1_year_price') else None ) if cloud_instance_price_details else None ,
                    'reserved_3_year_price': (math.ceil(cloud_instance_price_details.get('reserved_3_year_price'))if cloud_instance_price_details.get('reserved_3_year_price') else None ) if cloud_instance_price_details else None,
                    'actual_storage_price' : math.ceil(actual_storage_price) if actual_storage_price else None
                }
            recommendation_data.update(base_instance_price)

            #recommended vcpu, memmory adn cloud details are being caluclated and updated in DB.

            host_handler_object = HandleHostDetails()
            utilization_averages = host_handler_object.get_utilization_averages(req_data['id'], logger_ip=logger_ip)[0]

            recommended_vcpuCount = None
            recommended_memory = None
            recommended_storage_price= None

            if utilization_averages.get("cpu_percent"):
                recommended_vcpuCount = math.ceil(vcpuCount * ((10 + utilization_averages.get("cpu_percent")) / 100))

            if utilization_averages.get("memory_percent"):
                recommended_memory = util.round_up(math.ceil(memory * ((10 + utilization_averages.get("memory_percent")) / 100)), 4)

            recommended_storage= None
            if utilization_averages.get("storage_percent"):
                recommended_storage = util.round_up(storage * ((10 + utilization_averages.get("storage_percent")) / 100), 4)

            recommendation_post_body = \
                {
                    "cloudProviderName": req_data.get('cloud_provider'),
                    "vcpuCount": recommended_vcpuCount,
                    "memory": recommended_memory,
                    "operatingSystem": operating_system
                }
            logger.debug(logger_ip=logger_ip, message=f"Fetching recommended price details from cost api for the recommended instance configurations : {req_data['id']}, with req_body :{recommendation_post_body}")
            cloud_instance_price_details = self.price_recommendation_data(recommendation_post_body, logger_ip=logger_ip)

            if cloud_instance_price_details is None:
                suggested_vcpuCount_2, suggested_memory_2 = self.configuration_recommendation(memory=recommended_memory, vCPU=recommended_vcpuCount,cloud_provider=req_data.get('cloud_provider'),operating_system=operating_system, logger_ip=logger_ip)
                recommendation_post_body['vcpuCount'] = suggested_vcpuCount_2
                recommendation_post_body['memory'] = suggested_memory_2
                cloud_instance_price_details = self.price_recommendation_data(recommendation_post_body, logger_ip=logger_ip)

            if req_data['cloud_provider'] == "AWS":
                recommended_storage_price = CreateInfra.aws_EBS_hdd_price.value * 12 * int(recommended_storage) if recommended_storage else None

            elif req_data['cloud_provider'] == "Azure":
                nth_power_of_2 = math.ceil(math.log2(recommended_storage)) if recommended_storage else None
                azure_manage_disk_recommended_storage = pow(2, nth_power_of_2) if nth_power_of_2 else None
                if azure_manage_disk_recommended_storage:
                    azure_manage_disk_recommended_storage = 32 if azure_manage_disk_recommended_storage < 32 else azure_manage_disk_recommended_storage
                    recommended_storage_price = CreateInfra.azure_manged_disk_hdd_price.value[str(azure_manage_disk_recommended_storage)] *12

            recommended_instance_price =\
                {
                    'recommended_vcpu_count': recommended_vcpuCount,
                    'recommended_memory' : recommended_memory,
                    'recommended_storage' : recommended_storage,
                    'recommended_instance_type': cloud_instance_price_details.get('instance_type') if cloud_instance_price_details else None,
                    'recommended_on_demand_price' :( math.ceil(cloud_instance_price_details.get('on_demand_price')) if cloud_instance_price_details.get('on_demand_price') else None )if cloud_instance_price_details else None,
                    'recommended_1_year_price' : (math.ceil(cloud_instance_price_details.get('reserved_1_year_price')) if cloud_instance_price_details.get('reserved_1_year_price') else None ) if cloud_instance_price_details else None,
                    'recommended_3_year_price' :( math.ceil(cloud_instance_price_details.get('reserved_3_year_price')) if cloud_instance_price_details.get('reserved_3_year_price') else None ) if cloud_instance_price_details else None,
                    'recommended_storage_price' : math.ceil(recommended_storage_price) if recommended_storage_price else None
                }
            recommendation_data.update(recommended_instance_price)

        except ConnectionError as e:
            logger.error(logger_ip=req_data['ip'], message=f"Connection error encountered. Price recommendation is not updated. Error: {e}")
            pass

        except Exception as e:
            logger.error(logger_ip=req_data['ip'], message=f"Exception occurred while fetching recommendation details for host_id: {req_data['id']}, cloud_provider: {req_data['cloud_provider']} . Exception : {e} ")

        try:
            if eol_recommendation_data not in [{}, [], None]:
                recommendation_data.update(eol_recommendation_data)
            recommendation_data['host_id'] = req_data['id']
            recommendation_object = CreateRecommendation()
            recommendation_object.update_recommendation_info(os_recommendation_info=recommendation_data, logger_ip=req_data['ip'])
            logger.debug(logger_ip=req_data['ip'], message=f"Successfully updated price recommendation info")

        except Exception as e:
            logger.error(logger_ip=req_data['ip'], message=f"Exception occurred while fetching recommendation details for host_id: {req_data['id']}, cloud_provider: {req_data['cloud_provider']} . Exception : {e} ")
            return None


    def price_recommendation_data(self,body,logger_ip=None):
        """
        fetches the pricing details for a particular instance for a specific configuration and cloud provider.
        """
        try:
            body["region"] = CreateInfra.datacenter_region.value
            logger.debug(logger_ip=logger_ip, message=f"Hitting the price api to get the price recommendation for the ip : {logger_ip}")
            cost_analysis_data = util.get_properties(property_file_path=CreateInfra.etc_properties_path.value, section='cost_analysis_api')
            url = 'http://' + cost_analysis_data['url'] + ':' + cost_analysis_data['port'] + '/api/v1/compute/selectBaseVirtualMachine'


            on_demand_price, reserved_1_year_price, reserved_3_year_price, instance_type = None,None,None,None

            r = requests.post(url=url, json=body, headers={'content-type': 'application/json'})

            if r.status_code == 200:
                response = json.loads(r.text)  # for now we are taking only the first record of the obtained pricing list
                if response not in [None, []]:
                    logger.debug(logger_ip=logger_ip, message=f"Successfully obtained data from price api end point. Data :{json.loads(r.text)}")
                    response = response[0]

                    instance_type = response['instanceType']

                    for each in response['pricingList']:
                        if each['pricingType'] == "ondemand":
                            on_demand_price = float(each['price']) * 12
                        elif each['pricingType'] == "reserved" and each['reservedLength'] in ["1yr","1 yr"]:  # update names based on response from price api
                            reserved_1_year_price = float(each['price'])
                        elif each['pricingType'] == "reserved" and each['reservedLength'] in ["3yr","3 yr"]:
                            reserved_3_year_price = round(float(each['price']) / 3, 2)

                else:
                    logger.debug(logger_ip=logger_ip, message=f"Obtained empty Data for the configuration :{body}. Status code : {r.status_code}")
                    return None

            else:
                logger.debug(logger_ip=logger_ip, message=f"Negative response from price api. Response code : {r.status_code}")
                return None
            price_recommendation_data = {'instance_type': instance_type, 'yearly_onpremise_cost': CreateInfra.on_premise_vm_price.value, 'on_demand_price': on_demand_price, 'reserved_1_year_price': reserved_1_year_price, 'reserved_3_year_price': reserved_3_year_price}
            logger.debug(logger_ip=logger_ip, message=f"Successfully returned price recommendation data {price_recommendation_data}")
            return price_recommendation_data

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Exception occurred while hitting and getting response from pricing endpoint. Exception : {e}")
            return {'instance_type': None, 'yearly_onpremise_cost': CreateInfra.on_premise_vm_price.value, 'on_demand_price': None, 'reserved_1_year_price': None, 'reserved_3_year_price': None}


    def service_recommendation(self, host_id, logger_ip=None):
        """
        Accepts host_id, completes service recommendation for that host and updates the database
        :param host_id: host details with job entry 3
        :return: None
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"fetching support_date details from service_lookup table")

            section_name = "Service_Recommendation_Queries"
            service_recommendation_query = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name, property_name="service_lookup")
            dictionary_of_queries = {"service_lookup": service_recommendation_query}
            query_results = queries_helper.execute_dict_of_queries\
                (
                    dictionary_of_queries,
                    self.engine,
                    info="service and version",
                    host_id=host_id
                )
            service_data = query_results['service_lookup']

            if service_data:
                logger.debug(logger_ip=logger_ip, message=f"Successfully fetched all services running on host id: {host_id}. Services running: {service_data}")

                service_recommendation_data = []

                while service_data:
                    service_item = service_data.pop()

                    service_recommendation_query = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name, property_name="service_eol_query")
                    dictionary_of_queries = {"service_eol_lookup": service_recommendation_query}
                    query_results = queries_helper.execute_dict_of_queries \
                        (
                            dictionary_of_queries,
                            self.engine,
                            info="service and version",
                            host_id=host_id,
                            service_name=service_item["service"],
                            service_version=service_item["version"],
                            service_id=service_item["service_id"]
                        )
                    if query_results:
                        service_recommendation_data.append(query_results['service_eol_lookup'][0])

                for item in service_recommendation_data:

                    # criticality
                    item["criticality"] = CreateInfra.service_overview_criticality.value[0]  # Low
                    if item.get("applications_count") > 10:
                        item["criticality"] = CreateInfra.service_overview_criticality.value[2]  # high
                    elif item.get("applications_count") > 5:
                        item["criticality"] = CreateInfra.service_overview_criticality.value[1]  # moderate

                    # eosl, move_priority, migration_strategy
                    item["eosl"] = CreateInfra.service_overview_eosl.value[0]  # active
                    item["move_priority"] = CreateInfra.service_overview_move_priority.value[0]  # migrate later
                    item["migration_strategy"] = CreateInfra.service_overview_migration_strategy.value[0]  # rehost

                    current_date = datetime.strptime(util.get_datetime().split(' ')[0], '%Y-%m-%d')
                    support_date = datetime.strptime(item["support_date"].split(' ')[0], '%Y-%m-%d')
                    eol = relativedelta(support_date, current_date).years

                    if eol <= 0:
                        item["eosl"] = CreateInfra.service_overview_eosl.value[1]  # expired
                        item["move_priority"] = CreateInfra.service_overview_move_priority.value[1]  # migrate immideately
                        item["migration_strategy"] = CreateInfra.service_overview_migration_strategy.value[1]  # refactor

                    elif eol <= 3:
                        item["move_priority"] = CreateInfra.service_overview_move_priority.value[2]  # migrate soon

                recommendation_object = CreateRecommendation()
                recommendation_object.update_service_recommendation_info(service_recommendation_info=service_recommendation_data, host_id=host_id, logger_ip=logger_ip)

            else:
                logger.debug(logger_ip=logger_ip, message=f"Did not find any services running on host id: {host_id}")

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to fetch service recommendation details for services. Error: {e}")


    def configuration_recommendation(self,memory=None,vCPU=None,cloud_provider=None,operating_system=None,logger_ip=None):
        """
        fetches the exact configuration of the instance and recommends a cloud supported configuration
        """
        try:
            logger.debug(logger_ip=logger_ip,message=f"Calculating the configuration for the instance  : vCPU :{vCPU},Memory {memory}")
            recommended_vCPU ,recommended_memory= None,None

            nth_power_of_2 = math.ceil(math.log2(memory)) if memory else None
            if nth_power_of_2 == 0:
                memory=1
            elif nth_power_of_2:
                memory = pow(2, nth_power_of_2) if nth_power_of_2 else None

            nth_power_of_2 = math.ceil(math.log2(vCPU)) if vCPU else None
            if nth_power_of_2 == 0:
                vCPU=2
            elif nth_power_of_2:
                vCPU = pow(2, nth_power_of_2) if nth_power_of_2 else None

            lookup_data = CreateInfra.cloud_vcpu_memory_mapping.value

            memory_choices= lookup_data[str(vCPU)]

            for each in memory_choices:
                if each-memory>=0:
                    recommended_memory=each
                    recommended_vCPU = vCPU
                    break
            if recommended_memory is None:
                for key,value in lookup_data.items():
                    if memory in value:
                        recommended_vCPU=int(key)
                        recommended_memory=memory
                        break

            return recommended_vCPU,recommended_memory
        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Exception occured while calculating the configuration for the instance  : vCPU :{vCPU},Memory {memory}.Exception : {e}")
            return None,None


