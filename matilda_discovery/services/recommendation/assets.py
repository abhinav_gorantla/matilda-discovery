import copy
import math
from datetime import datetime
from dateutil.relativedelta import relativedelta

from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db.connection import get_engine
from matilda_discovery.db.handler.assets import HandleAssets
from matilda_discovery.db.handler.host import HandleHostDetails
from matilda_discovery.db.handler.topology import TopologyDetails
from matilda_discovery.db.queries_helper import execute_dict_of_queries
from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.db.handler.recommendation import CreateRecommendation
from matilda_discovery.utils import util
from matilda_discovery.utils.util import get_properties
from matilda_discovery.services.host import HostDetails

logger = logger_py_file.Logger()


class Infrastructure:

    def __init__(self):
        self.engine = get_engine()

    def infrastructure_assets_dashboard(self, cloud_provider, logger_ip=None):
        """

        """
        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Fetching infrastructure_assets_details for cloud_provider:{cloud_provider}")

            response = {}
            asset_handler_object = HandleAssets()
            infrastructure_assets_info = asset_handler_object.infrastructure_assets_data(cloud_provider=cloud_provider,
                                                                                         logger_ip=logger_ip)

            try:
                if infrastructure_assets_info.get('infrastructure_count_details'):
                    temp_physical, temp_virtual = 0, 0
                    infrastructure_count_details = infrastructure_assets_info['infrastructure_count_details'][
                        'infrastructure_count_query']
                    for item in infrastructure_count_details:
                        if str.lower(item.get('instance_type')) in ['physical', 'server']:
                            temp_physical += int(item.get('count'))
                        else:
                            temp_virtual += int(item.get('count'))

                    response['physical'] = temp_physical
                    response['virtual'] = temp_virtual
                    response['physical_percent'] = round(
                        (response['physical'] / (response['physical'] + response['virtual'])) * 100, 2)
                    response['virtual_percent'] = round((100 - response['physical_percent']), 2)

            except Exception as e:
                logger.error(logger_ip=logger_ip, message=f"Error in infrastructure_count_details. Error: {e}")
                response['physical'] = ""
                response['virtual'] = ""
                response['physical_percent'] = ""
                response['virtual_percent'] = ""

            try:
                existing, recommended = [], []
                if infrastructure_assets_info.get('prices_details'):
                    prices_details = infrastructure_assets_info['prices_details']['prices_query'][0]
                    existing_on_prem = prices_details.pop('existing_on_prem')

                    existing = \
                        [
                            {
                                "name": "Yearly on Premise",
                                "value": "$" + str(existing_on_prem),
                                "Savings": ""
                            },
                            {
                                "name": "Yearly on demand",
                                "value": "$" + str(prices_details.get('existing_on_demand')),
                                "Savings": str(
                                    round((1 - (prices_details.get('existing_on_demand') / existing_on_prem)) * 100,
                                          2)) + "%"
                            },
                            {
                                "name": "Reserved 1yr",
                                "value": "$" + str(prices_details.get('existing_1_year')),
                                "Savings": str(
                                    round((1 - (prices_details.get('existing_1_year') / existing_on_prem)) * 100,
                                          2)) + "%"
                            },
                            {
                                "name": "Reserved 3yr",
                                "value": "$" + str(prices_details.get('existing_3_year')),
                                "Savings": str(
                                    round((1 - (prices_details.get('existing_3_year') / existing_on_prem)) * 100,
                                          2)) + "%"
                            }
                        ]

                    recommended = \
                        [
                            {
                                "name": "Yearly on Premise",
                                "value": "$" + str(prices_details.get('recommended_on_prem')),
                                "Savings": ""
                            },
                            {
                                "name": "Yearly on demand",
                                "value": "$" + str(prices_details.get('recommended_on_demand')),
                                "Savings": str(
                                    round((1 - (prices_details.get('recommended_on_demand') / existing_on_prem)) * 100,
                                          2)) + "%"
                            },
                            {
                                "name": "Reserved 1yr",
                                "value": "$" + str(prices_details.get('recommended_1_year')),
                                "Savings": str(
                                    round((1 - (prices_details.get('recommended_1_year') / existing_on_prem)) * 100,
                                          2)) + "%"
                            },
                            {
                                "name": "Reserved 3yr",
                                "value": "$" + str(prices_details.get('recommended_3_year')),
                                "Savings": str(
                                    round((1 - (prices_details.get('recommended_3_year') / existing_on_prem)) * 100,
                                          2)) + "%"
                            }
                        ]
                response['existing'] = existing
                response['recommended'] = recommended

            except Exception as e:
                logger.error(logger_ip=logger_ip, message=f"Error in prices_details. Error: {e}")
                response['existing'] = \
                    [
                        {
                            "name": "Yearly on Premise",
                            "value": "$0",
                            "Savings": ""
                        },
                        {
                            "name": "Yearly on demand",
                            "value": None,
                            "Savings": None
                        },
                        {
                            "name": "Reserved 1yr",
                            "value": None,
                            "Savings": None
                        },
                        {
                            "name": "Reserved 3yr",
                            "value": None,
                            "Savings": None
                        }
                    ]
                response['recommended'] = \
                    [
                        {
                            "name": "Yearly on Premise",
                            "value": "$0",
                            "Savings": ""
                        },
                        {
                            "name": "Yearly on demand",
                            "value": None,
                            "Savings": None
                        },
                        {
                            "name": "Reserved 1yr",
                            "value": None,
                            "Savings": None
                        },
                        {
                            "name": "Reserved 3yr",
                            "value": None,
                            "Savings": None
                        }
                    ]

            try:
                cloud_suitability = ""
                if infrastructure_assets_info.get('compatible_count_details'):

                    cloud_suitability = []
                    for item in infrastructure_assets_info['compatible_count_details']['compatible_count_query']:
                        if item.get('compatible') == "Yes":
                            cloud_suitability.append({"name": "Yes", "value": item.get('count')})
                        elif item.get('compatible') == "No":
                            cloud_suitability.append({"name": "No", "value": item.get('count')})

                response["cloud_suitability"] = cloud_suitability

            except Exception as e:
                logger.error(logger_ip=logger_ip, message=f"Error in cloud_suitability. Error: {e}")
                response["cloud_suitability"] = ""

            description = \
                [
                    "Rehost: Everything is good. Lift and shift should work.",
                    "Rebuild: Operating System is outdated. Upgrade to a later version for migrating.",
                    "Refactor: Underlying middleware services are outdated. Upgrade middleware services for migrating",
                ]
            try:

                migration_strategy = {}
                chart_data = ""

                if infrastructure_assets_info.get('migration_strategy_count_details'):

                    chart_data = {"Rehost": 0, "Rebuild": 0, "Refactor": 0}

                    for item in infrastructure_assets_info['migration_strategy_count_details'][
                        'migration_strategy_count_query']:
                        if item.get('migration_strategy') == 'Rebuild':
                            chart_data["Rebuild"] = item["count"]
                        elif item.get('migration_strategy') == 'Rehost':
                            chart_data["Rehost"] = item["count"]
                        elif item.get('migration_strategy') == 'Refactor':
                            chart_data["Refactor"] = item["count"]

                migration_strategy["chart_data"] = chart_data
                migration_strategy["description"] = description
                response["migration_strategy"] = migration_strategy

            except Exception as e:
                logger.error(logger_ip=logger_ip, message=f"Error in migration_strategy. Error: {e}")
                migration_strategy = None
                migration_strategy["chart_data"] = ""
                migration_strategy["description"] = description
                response["migration_strategy"] = migration_strategy

            description = \
                [
                    "Immediate: Operating System EOL is expired. Has to be migrated immediately.",
                    "Soon: Operating System EOL is expiring soon (< 3 yr). Has to be migrated in the near future.",
                    "Later: Operating System EOL is valid for few more years. Can be migrated later.",
                ]
            try:
                migration_priority = {}
                chart_data = ""

                if infrastructure_assets_info.get('migration_priority_count_details'):
                    chart_data = {"Immediate": 0, "Soon": 0, "Later": 0}

                    for item in infrastructure_assets_info['migration_priority_count_details'][
                        'migration_priority_count_query']:
                        if item.get('priority') == 'Migrate immediately':
                            chart_data["Immediate"] = item["count"]
                        elif item.get('priority') == 'Migrate soon':
                            chart_data["Soon"] = item["count"]
                        elif item.get('priority') == 'Migrate later':
                            chart_data["Later"] = item["count"]

                migration_priority["chart_data"] = chart_data
                migration_priority["description"] = description
                response["migration_priority"] = migration_priority

            except Exception as e:
                logger.error(logger_ip=logger_ip, message=f"Error in migration_priority. Error: {e}")
                migration_priority = None
                migration_priority["chart_data"] = ""
                migration_priority["description"] = description
                response["migration_priority"] = migration_priority

            description = \
                [
                    "Active: EOL is valid for few more years.",
                    "Soon: EOL expires in less than 3 years",
                    "Expired: EOL has been expired."
                ]
            try:

                response['os_details'] = None

                if infrastructure_assets_info.get('operating_system_count_details'):
                    bar, pie, os_details = {}, [], {}

                    for item in infrastructure_assets_info['operating_system_count_details'][
                        'operating_system_count_query']:
                        if bar.get(item['operating_system']):
                            if bar[item['operating_system']].get(item['eol_status']) is not None:
                                bar[item["operating_system"]][item["eol_status"]] += item["count"]
                            else:
                                bar[item["operating_system"]] = {"Active": 0, "Soon": 0, "Expired": 0}
                                bar[item["operating_system"]][item["eol_status"]] += item["count"]
                        else:
                            bar[item["operating_system"]] = {"Active": 0, "Soon": 0, "Expired": 0}
                            bar[item["operating_system"]][item["eol_status"]] += item["count"]

                        if not pie:
                            temp = {'name': item["operating_system"], 'value': item['count'], 'bar': {}}
                            temp['bar'][item['version']] = {"Active": 0, "Soon": 0, "Expired": 0}
                            temp['bar'][item['version']][item['eol_status']] += item['count']
                            pie.append(temp)
                        else:
                            flag = 0
                            for pie_item in pie:
                                if pie_item.get('name') == item["operating_system"]:
                                    pie_item['value'] += item['count']
                                    pie_item['bar'][item['version']] = {"Active": 0, "Soon": 0, "Expired": 0}
                                    pie_item['bar'][item['version']][item['eol_status']] += item['count']
                                    flag = 1
                            if flag == 0:
                                temp = {'name': item["operating_system"], 'value': item['count'], 'bar': {}}
                                temp['bar'][item['version']] = {"Active": 0, "Soon": 0, "Expired": 0}
                                temp['bar'][item['version']][item['eol_status']] += item['count']
                                pie.append(temp)

                    os_details["bar"] = bar
                    os_details["pie"] = pie
                    os_details["description"] = description
                    response["os_details"] = os_details

            except Exception as e:
                logger.error(logger_ip=logger_ip, message=f"Error in os_details. Error: {e}")
                os_details = None
                os_details["bar"] = ""
                os_details["pie"] = []
                os_details["description"] = description
                response["os_details"] = os_details

            description = \
                [
                    "Active: EOL is valid for few more years.",
                    "Soon: EOL expires in less than 3 years",
                    "Expired: EOL has been expired."
                ]
            try:
                response['services_details'] = None

                if infrastructure_assets_info.get('server_type_count_details'):
                    bar, pie, service_details = {}, [], {}

                    for item in infrastructure_assets_info['server_type_count_details']['server_type_count_query']:
                        if bar.get(item['type']):
                            if bar[item['type']].get(item['eol_status']) is not None:
                                bar[item["type"]][item["eol_status"]] += item["count"]
                            else:
                                bar[item["type"]] = {"Active": 0, "Soon": 0, "Expired": 0}
                                bar[item["type"]][item["eol_status"]] += item["count"]
                        else:
                            bar[item["type"]] = {"Active": 0, "Soon": 0, "Expired": 0}
                            bar[item["type"]][item["eol_status"]] += item["count"]

                        if not pie:
                            temp = {'name': item["type"], 'value': item['count'], 'bar': {}}
                            temp['bar'][item['name']] = {"Active": 0, "Soon": 0, "Expired": 0}
                            temp['bar'][item['name']][item['eol_status']] += item['count']
                            pie.append(temp)
                        else:
                            flag = 0
                            for pie_item in pie:
                                if pie_item.get('name') == item["type"]:
                                    pie_item['value'] += item['count']
                                    pie_item['bar'][item['name']] = {"Active": 0, "Soon": 0, "Expired": 0}
                                    pie_item['bar'][item['name']][item['eol_status']] += item['count']
                                    flag = 1
                            if flag == 0:
                                temp = {'name': item["type"], 'value': item['count'], 'bar': {}}
                                temp['bar'][item['name']] = {"Active": 0, "Soon": 0, "Expired": 0}
                                temp['bar'][item['name']][item['eol_status']] += item['count']
                                pie.append(temp)

                    service_details["bar"] = bar
                    service_details["pie"] = pie
                    service_details["description"] = description
                    response["services_details"] = service_details

            except Exception as e:
                logger.error(logger_ip=logger_ip, message=f"Error in service_details. Error: {e}")
                service_details = None
                service_details["bar"] = ""
                service_details["pie"] = []
                service_details["description"] = description
                response["services_details"] = service_details

            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully fetched infrastructure_assets_details for cloud_provider:{cloud_provider}")
            return response

        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Failed to fetch infrastructure_assets_details for cloud_provider:{cloud_provider}. Errorr: {e}")
            return {}

    # NEW UI
    def new_infrastructure_assets_dashboard(self, cloud_provider, logger_ip=None):
        """

        """
        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Fetching infrastructure_assets_details for cloud_provider:{cloud_provider}")

            response = {}
            asset_handler_object = HandleAssets()
            infrastructure_assets_info = asset_handler_object.infrastructure_assets_data(cloud_provider=cloud_provider,
                                                                                         logger_ip=logger_ip)

            try:
                if infrastructure_assets_info.get('infrastructure_count_details'):
                    temp_physical, temp_virtual = 0, 0
                    infrastructure_count_details = infrastructure_assets_info['infrastructure_count_details'][
                        'infrastructure_count_query']
                    for item in infrastructure_count_details:
                        if str.lower(item.get('instance_type')) in ['physical', 'server']:
                            temp_physical += int(item.get('count'))
                        else:
                            temp_virtual += int(item.get('count'))

                    response['physical'] = temp_physical
                    response['virtual'] = temp_virtual
                    response['physical_percent'] = round(
                        (response['physical'] / (response['physical'] + response['virtual'])) * 100, 2)
                    response['virtual_percent'] = round((100 - response['physical_percent']), 2)

            except Exception as e:
                logger.error(logger_ip=logger_ip, message=f"Error in infrastructure_count_details. Error: {e}")
                response['physical'] = ""
                response['virtual'] = ""
                response['physical_percent'] = ""
                response['virtual_percent'] = ""

            try:
                existing, recommended = [], []
                if infrastructure_assets_info.get('prices_details'):
                    prices_details = infrastructure_assets_info['prices_details']['prices_query'][0]
                    existing_on_prem = prices_details.pop('existing_on_prem')

                    existing = \
                        [
                            {
                                "name": "Yearly on Premise",
                                "value": "$" + str(existing_on_prem),
                                "Savings": ""
                            },
                            {
                                "name": "Yearly on demand",
                                "value": "$" + str(prices_details.get('existing_on_demand')),
                                "Savings": str(
                                    round((1 - (prices_details.get('existing_on_demand') / existing_on_prem)) * 100,
                                          2)) + "%"
                            },
                            {
                                "name": "Reserved 1yr",
                                "value": "$" + str(prices_details.get('existing_1_year')),
                                "Savings": str(
                                    round((1 - (prices_details.get('existing_1_year') / existing_on_prem)) * 100,
                                          2)) + "%"
                            },
                            {
                                "name": "Reserved 3yr",
                                "value": "$" + str(prices_details.get('existing_3_year')),
                                "Savings": str(
                                    round((1 - (prices_details.get('existing_3_year') / existing_on_prem)) * 100,
                                          2)) + "%"
                            }
                        ]

                    recommended = \
                        [
                            {
                                "name": "Yearly on Premise",
                                "value": "$" + str(prices_details.get('recommended_on_prem')),
                                "Savings": ""
                            },
                            {
                                "name": "Yearly on demand",
                                "value": "$" + str(prices_details.get('recommended_on_demand')),
                                "Savings": str(
                                    round((1 - (prices_details.get('recommended_on_demand') / existing_on_prem)) * 100,
                                          2)) + "%"
                            },
                            {
                                "name": "Reserved 1yr",
                                "value": "$" + str(prices_details.get('recommended_1_year')),
                                "Savings": str(
                                    round((1 - (prices_details.get('recommended_1_year') / existing_on_prem)) * 100,
                                          2)) + "%"
                            },
                            {
                                "name": "Reserved 3yr",
                                "value": "$" + str(prices_details.get('recommended_3_year')),
                                "Savings": str(
                                    round((1 - (prices_details.get('recommended_3_year') / existing_on_prem)) * 100,
                                          2)) + "%"
                            }
                        ]
                response['existing'] = existing
                response['recommended'] = recommended

            except Exception as e:
                logger.error(logger_ip=logger_ip, message=f"Error in prices_details. Error: {e}")
                response['existing'] = \
                    [
                        {
                            "name": "Yearly on Premise",
                            "value": "$0",
                            "Savings": ""
                        },
                        {
                            "name": "Yearly on demand",
                            "value": None,
                            "Savings": None
                        },
                        {
                            "name": "Reserved 1yr",
                            "value": None,
                            "Savings": None
                        },
                        {
                            "name": "Reserved 3yr",
                            "value": None,
                            "Savings": None
                        }
                    ]
                response['recommended'] = \
                    [
                        {
                            "name": "Yearly on Premise",
                            "value": "$0",
                            "Savings": ""
                        },
                        {
                            "name": "Yearly on demand",
                            "value": None,
                            "Savings": None
                        },
                        {
                            "name": "Reserved 1yr",
                            "value": None,
                            "Savings": None
                        },
                        {
                            "name": "Reserved 3yr",
                            "value": None,
                            "Savings": None
                        }
                    ]

            try:
                cloud_suitability = ""
                if infrastructure_assets_info.get('compatible_count_details'):

                    cloud_suitability = []
                    for item in infrastructure_assets_info['compatible_count_details']['compatible_count_query']:
                        if item.get('compatible') == "Yes":
                            cloud_suitability.append({"name": "Yes", "value": item.get('count')})
                        elif item.get('compatible') == "No":
                            cloud_suitability.append({"name": "No", "value": item.get('count')})

                response["cloud_suitability"] = cloud_suitability

            except Exception as e:
                logger.error(logger_ip=logger_ip, message=f"Error in cloud_suitability. Error: {e}")
                response["cloud_suitability"] = ""

            description = \
                [
                    "Rehost: Everything is good. Lift and shift should work.",
                    "Rebuild: Operating System is outdated. Upgrade to a later version for migrating.",
                    "Refactor: Underlying middleware services are outdated. Upgrade middleware services for migrating",
                ]
            try:

                migration_strategy = {}
                chart_data = ""

                if infrastructure_assets_info.get('migration_strategy_count_details'):

                    chart_data = {"Rehost": 0, "Rebuild": 0, "Refactor": 0}

                    for item in infrastructure_assets_info['migration_strategy_count_details'][
                        'migration_strategy_count_query']:
                        if item.get('migration_strategy') == 'Rebuild':
                            chart_data["Rebuild"] = item["count"]
                        elif item.get('migration_strategy') == 'Rehost':
                            chart_data["Rehost"] = item["count"]
                        elif item.get('migration_strategy') == 'Refactor':
                            chart_data["Refactor"] = item["count"]

                migration_strategy["chart_data"] = chart_data
                migration_strategy["description"] = description
                response["migration_strategy"] = migration_strategy

            except Exception as e:
                logger.error(logger_ip=logger_ip, message=f"Error in migration_strategy. Error: {e}")
                migration_strategy = None
                migration_strategy["chart_data"] = ""
                migration_strategy["description"] = description
                response["migration_strategy"] = migration_strategy

            description = \
                [
                    "Immediate: Operating System EOL is expired. Has to be migrated immediately.",
                    "Soon: Operating System EOL is expiring soon (< 3 yr). Has to be migrated in the near future.",
                    "Later: Operating System EOL is valid for few more years. Can be migrated later.",
                ]
            try:
                migration_priority = {}
                chart_data = ""

                if infrastructure_assets_info.get('migration_priority_count_details'):
                    chart_data = {"Immediate": 0, "Soon": 0, "Later": 0}

                    for item in infrastructure_assets_info['migration_priority_count_details'][
                        'migration_priority_count_query']:
                        if item.get('priority') == 'Migrate immediately':
                            chart_data["Immediate"] = item["count"]
                        elif item.get('priority') == 'Migrate soon':
                            chart_data["Soon"] = item["count"]
                        elif item.get('priority') == 'Migrate later':
                            chart_data["Later"] = item["count"]

                migration_priority["chart_data"] = chart_data
                migration_priority["description"] = description
                response["migration_priority"] = migration_priority

            except Exception as e:
                logger.error(logger_ip=logger_ip, message=f"Error in migration_priority. Error: {e}")
                migration_priority = None
                migration_priority["chart_data"] = ""
                migration_priority["description"] = description
                response["migration_priority"] = migration_priority

            description = \
                [
                    "Active: EOL is valid for few more years.",
                    "Soon: EOL expires in less than 3 years",
                    "Expired: EOL has been expired."
                ]
            # Mock data
            try:

                donutDetails = []
                osChart = \
                    [
                        {
                            "name": "CentOS",
                            "value": 5,
                            "bar_stack_data": [{
                                "category": "7 (Core)",
                                "values": [{
                                    "key": "Expired",
                                    "value": 0
                                },
                                    {
                                        "key": "Active",
                                        "value": 5
                                    },
                                    {
                                        "key": "Soon",
                                        "value": 0
                                    }
                                ]
                            }]
                        },
                        {
                            "name": "Ubuntu",
                            "value": 7,
                            "bar_stack_data": [
                                {
                                    "category": "16.04  LTS (Bionic Beaver)",
                                    "values": [{
                                        "key": "Expired",
                                        "value": 0
                                    },
                                        {
                                            "key": "Active",
                                            "value": 0
                                        },
                                        {
                                            "key": "Soon",
                                            "value": 2
                                        }
                                    ]
                                },
                                {
                                    "category": "16.04.6 LTS (Xenial Xerus)",
                                    "values": [{
                                        "key": "Expired",
                                        "value": 1
                                    },
                                        {
                                            "key": "Active",
                                            "value": 0
                                        },
                                        {
                                            "key": "Soon",
                                            "value": 0
                                        }
                                    ]
                                },
                                {
                                    "category": "18.04.2 LTS (Bionic Beaver)",
                                    "values": [{
                                        "key": "Expired",
                                        "value": 0
                                    },
                                        {
                                            "key": "Active",
                                            "value": 0
                                        },
                                        {
                                            "key": "Soon",
                                            "value": 2
                                        }
                                    ]
                                },
                                {
                                    "category": "18.04.3 LTS (Bionic Beaver)",
                                    "values": [{
                                        "key": "Expired",
                                        "value": 0
                                    },
                                        {
                                            "key": "Active",
                                            "value": 0
                                        },
                                        {
                                            "key": "Soon",
                                            "value": 1
                                        }
                                    ]
                                },
                                {
                                    "category": "18.04.4 LTS",
                                    "values": [{
                                        "key": "Expired",
                                        "value": 0
                                    },
                                        {
                                            "key": "Active",
                                            "value": 0
                                        },
                                        {
                                            "key": "Soon",
                                            "value": 1
                                        }
                                    ]
                                }

                            ],

                        }

                    ]

                osDescription = \
                    [
                        "Active: EOL is valid for few more years.",
                        "Soon: EOL expires in less than 3 years",
                        "Expired: EOL has been expired."
                    ]
                os_summary_description = \
                    [
                        "5 instances where EOL is active",
                        "6 instances where EOL is expiring soon",
                        "1 instances where EOL is already expired."
                    ]

                serviceChart = \
                    [
                        {
                            "name": "Application Server",
                            "value": 5,
                            "bar_stack_data": [
                                {
                                    "category": "jboss",
                                    "values": [{
                                        "key": "Expired",
                                        "value": 0
                                    },
                                        {
                                            "key": "Active",
                                            "value": 1
                                        },
                                        {
                                            "key": "Soon",
                                            "value": 0
                                        }
                                    ]
                                },
                                {
                                    "category": "Tomcat",
                                    "values": [{
                                        "key": "Expired",
                                        "value": 0
                                    },
                                        {
                                            "key": "Active",
                                            "value": 1
                                        },
                                        {
                                            "key": "Soon",
                                            "value": 0
                                        }
                                    ]
                                },
                                {
                                    "category": "Weblogic",
                                    "values": [{
                                        "key": "Expired",
                                        "value": 3
                                    },
                                        {
                                            "key": "Active",
                                            "value": 0
                                        },
                                        {
                                            "key": "Soon",
                                            "value": 0
                                        }
                                    ]
                                }]
                        },
                        {
                            "name": "Database",
                            "value": 7,
                            "bar_stack_data": [
                                {
                                    "category": "Mssql",
                                    "values": [{
                                        "key": "Expired",
                                        "value": 0
                                    },
                                        {
                                            "key": "Active",
                                            "value": 1
                                        },
                                        {
                                            "key": "Soon",
                                            "value": 0
                                        }
                                    ]
                                },
                                {
                                    "category": "Oracle",
                                    "values": [{
                                        "key": "Expired",
                                        "value": 0
                                    },
                                        {
                                            "key": "Active",
                                            "value": 2
                                        },
                                        {
                                            "key": "Soon",
                                            "value": 0
                                        }
                                    ]
                                },
                                {
                                    "category": "Mysql",
                                    "values": [{
                                        "key": "Expired",
                                        "value": 0
                                    },
                                        {
                                            "key": "Active",
                                            "value": 0
                                        },
                                        {
                                            "key": "Soon",
                                            "value": 4
                                        }
                                    ]
                                }]
                        },
                        {
                            "name": "Web Server",
                            "value": 7,
                            "bar_stack_data": [
                                {
                                    "category": "Apache",
                                    "values": [{
                                        "key": "Expired",
                                        "value": 0
                                    },
                                        {
                                            "key": "Active",
                                            "value": 1
                                        },
                                        {
                                            "key": "Soon",
                                            "value": 0
                                        }
                                    ]
                                },
                                {
                                    "category": "Nginx",
                                    "values": [{
                                        "key": "Expired",
                                        "value": 0
                                    },
                                        {
                                            "key": "Active",
                                            "value": 4
                                        },
                                        {
                                            "key": "Soon",
                                            "value": 0
                                        }
                                    ]
                                },
                                {
                                    "category": "IIS",
                                    "values": [{
                                        "key": "Expired",
                                        "value": 0
                                    },
                                        {
                                            "key": "Active",
                                            "value": 2
                                        },
                                        {
                                            "key": "Soon",
                                            "value": 0
                                        }
                                    ]
                                }],

                        }

                    ]
                serviceDescription = \
                    [
                        "Active: EOL is valid for few more years.",
                        "Soon: EOL expires in less than 3 years",
                        "Expired: EOL has been expired."
                    ]

                service_summary_description = \
                    [
                        "12 instances where EOL is active",
                        "4 instances where EOL is expiring soon",
                        "3 instances where EOL is already expired."
                    ]

                donutDetails = \
                    {
                        "osChart": osChart,
                        "osDescription": osDescription,
                        "os_summary_description": os_summary_description,
                        "serviceChart": serviceChart,
                        "serviceDescription": serviceDescription,
                        "service_summary_description": service_summary_description
                    }
                response['donutDetails'] = donutDetails



            except Exception as e:
                logger.error(logger_ip=logger_ip, message=f"Error in service_details. Error: {e}")
                response["donutDetails"] = donutDetails

            logger.debug(logger_ip=logger_ip,
                         message=f"Successfully fetched infrastructure_assets_details for cloud_provider:{cloud_provider}")
            return response

        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Failed to fetch infrastructure_assets_details for cloud_provider:{cloud_provider}. Errorr: {e}")
            return {}

    @staticmethod
    def infrastructure_overview(host_req, logger_ip=None):
        """
        Updates infrastructure overview columns in host table
        """
        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Updating infrastructure overview information for id: {host_req['id']}")
            if host_req:

                recommendation_object = CreateRecommendation()

                # revision
                current_date = datetime.strptime(util.get_datetime().split(' ')[0], '%Y-%m-%d')

                operating_system_lookup_data = recommendation_object.fetch_operating_system_lookup(host_req,
                                                                                                   logger_ip=logger_ip)

                revision = None

                if operating_system_lookup_data:

                    if operating_system_lookup_data.get("support_date") != "0000-00-00":

                        support_date = datetime.strptime(operating_system_lookup_data.get("support_date"), '%Y-%m-%d')

                        eol = relativedelta(support_date, current_date).years

                        instance_type = host_req["instance_type"]

                        revision = CreateInfra.os_overview_revision.value[0]  # "LTS"
                        if eol:
                            if instance_type in ["VM", "Virtual"] and eol <= 0:
                                revision = CreateInfra.os_overview_revision.value[1]  # "Decommission"
                            elif eol <= 0:
                                revision = CreateInfra.os_overview_revision.value[2]  # "Upgrade immediately"
                            elif eol <= 3:
                                revision = CreateInfra.os_overview_revision.value[3]  # "Upgrade recommended"

                # dependents
                dependency_service_object = TopologyDetails()
                dependents = dependency_service_object.get_dependents_count(host_req["ip"])

                # usage
                cpu_usage = None
                memory_usage = None
                storage_usage = None

                host_handler_object = HandleHostDetails()
                utilization_averages = \
                host_handler_object.get_utilization_averages(host_req['id'], logger_ip=logger_ip)[0]

                if utilization_averages.get("cpu_percent"):
                    cpu_usage = CreateInfra.os_overview_cpu_usage.value[2]  # "High"

                    if utilization_averages.get("cpu_percent") <= 33.3:
                        cpu_usage = CreateInfra.os_overview_cpu_usage.value[0]  # "Low"
                    elif utilization_averages.get("cpu_percent") <= 66.6:
                        cpu_usage = CreateInfra.os_overview_cpu_usage.value[1]  # "Moderate"

                if utilization_averages.get("memory_percent"):
                    memory_usage = CreateInfra.os_overview_memory_usage.value[2]  # "High"

                    if utilization_averages.get("memory_percent") <= 33.3:
                        memory_usage = CreateInfra.os_overview_memory_usage.value[0]  # "Low"
                    elif utilization_averages.get("memory_percent") <= 66.6:
                        memory_usage = CreateInfra.os_overview_memory_usage.value[1]  # "Moderate"

                if utilization_averages.get("storage_percent"):
                    storage_usage = CreateInfra.os_overview_storage_usage.value[2]  # "High"

                    if utilization_averages.get("storage_percent") <= 33.3:
                        storage_usage = CreateInfra.os_overview_storage_usage.value[0]  # "Low"
                    elif utilization_averages.get("storage_percent") <= 66.6:
                        storage_usage = CreateInfra.os_overview_storage_usage.value[1]  # "Moderate"

                # activity
                activity = None

                login_count = host_handler_object.get_login_activity(host_req['id'], days=30, logger_ip=logger_ip)

                if login_count:

                    activity = CreateInfra.os_overview_activity.value[3]  # "High"

                    if login_count:
                        if login_count == 0:
                            activity = CreateInfra.os_overview_activity.value[0]  # "Idle"
                        elif login_count <= 5:
                            activity = CreateInfra.os_overview_activity.value[1]  # "Low"
                        elif login_count <= 20:
                            activity = CreateInfra.os_overview_activity.value[2]  # "Moderate"

                # memory type
                memory_type = None

                if host_req.get("memory"):
                    memory = host_req["memory"]

                    memory_type = CreateInfra.os_overview_memory_type.value[2]  # "Macro"

                    if 0 < memory <= 8:
                        memory_type = CreateInfra.os_overview_memory_type.value[0]  # "Micro"
                    elif memory <= 16:
                        memory_type = CreateInfra.os_overview_memory_type.value[1]  # "Regular"

                host_handler_object.update_host_table \
                        (
                        host_id=host_req["id"],
                        revision=revision,
                        cpu_usage=cpu_usage,
                        memory_usage=memory_usage,
                        storage_usage=storage_usage,
                        dependents=dependents,
                        activity=activity,
                        memory_type=memory_type
                    )
                logger.debug(logger_ip=logger_ip,
                             message=f"Successfully updated infrastructure overview information for id: {host_req['id']}. Information: revision={revision}, storage_usage={storage_usage}, cpu_usage={cpu_usage}, memory_usage={memory_usage}, dependents={dependents}")

            else:
                raise Exception(f"Empty host_req given")

        except Exception as e:
            logger.error(logger_ip=logger_ip,
                         message=f"Failed to update infrastructure overview information for id: {host_req['id']}. Error: {e}")

    def fetch_infrastructure_overview_details(self, account_id, cloud_provider):
        """
        Prepares assessment for the on_premise data.
        :return:
        """
        try:
            logger.debug(f"Preparing infrastructure assessment for the account_id : {account_id}")
            if str.lower(cloud_provider) not in ["aws", "azure", "gcp"]:
                raise Exception(f"Unsupported cloud provider. Given cloud provider: {cloud_provider}")

            infrastructure_assets = {}

            infrastructure_overview_query = get_properties(property_file_path=CreateInfra.db_queries_path.value,
                                                           section="Assets",
                                                           property_name="infrastructure_on_premise_overview")
            infrastructure_on_premise_overview_details = execute_dict_of_queries(
                {"infrastructure_on_premise_overview": infrastructure_overview_query}, self.engine,
                account_id=account_id, cloud_provider=cloud_provider,
                info="query for infrastructure onpermise assesment")

            infrastructure_assets['infrastructure_overview'] = infrastructure_on_premise_overview_details[
                'infrastructure_on_premise_overview']

            return infrastructure_assets

        except Exception as e:
            logger.error(
                f"Exception occurred while preparing infrastructure overview details for the account_id : {account_id}.Exception :{e}")
            return None

    # NEW UI
    # Instance List
    def new_fetch_infrastructure_overview_details(self, account_id, cloud_provider):
        """
        Prepares assessment for the on_premise data.
        :return:
        """
        try:
            logger.debug(f"Preparing infrastructure assessment for the account_id : {account_id}")
            if str.lower(cloud_provider) not in ["aws", "azure", "gcp"]:
                raise Exception(f"Unsupported cloud provider. Given cloud provider: {cloud_provider}")

            infrastructure_assets = {}

            infrastructure_overview_query = get_properties(property_file_path=CreateInfra.db_queries_path.value,
                                                           section="Assets",
                                                           property_name="new_infrastructure_on_premise_overview")
            infrastructure_on_premise_overview_details = execute_dict_of_queries(
                {"infrastructure_on_premise_overview": infrastructure_overview_query}, self.engine,
                account_id=account_id, cloud_provider=cloud_provider,
                info="query for infrastructure onpermise assesment")

            infrastructure_assets['infrastructure_overview'] = infrastructure_on_premise_overview_details[
                'infrastructure_on_premise_overview']

            return infrastructure_assets

        except Exception as e:
            logger.error(
                f"Exception occurred while preparing infrastructure overview details for the account_id : {account_id}.Exception :{e}")
            return None

    def fetch_infrastructure_migration_details(self, account_id=None, cloud_provider=None):
        """
        Prepares infrastructure migration details assets for an account_id
        :return:
        """
        try:
            logger.debug(
                f"Preparing infrastructure migration assessment and pricing details for the account_id : {account_id}")

            aws_yearly_on_premise_cost, aws_on_demand_price, aws_reserved_1_year_price, aws_reserved_3_year_price, \
            aws_recommended_on_demand_price, aws_recommended_reserved_1_year_price, aws_recommended_reserved_3_year_price, \
            aws_actual_storage_price, aws_recommended_storage_price, summary, infrastructure_assets = 0, 0, 0, 0, 0, 0, 0, 0, 0, {}, {}

            infrastructure_migration_query = get_properties(property_file_path=CreateInfra.db_queries_path.value,
                                                            section="Assets",
                                                            property_name="infrastructure_migration_overview")
            infrastructure_migration_overview_details = execute_dict_of_queries(
                {"infrastructure_migration_overview": infrastructure_migration_query}, self.engine,
                account_id=account_id, cloud_provider=cloud_provider,
                info="query for infrastructure migration assesment")

            migration_overview_data = infrastructure_migration_overview_details['infrastructure_migration_overview']
            host_handlerobject = HandleHostDetails()

            if migration_overview_data:
                for each_record in migration_overview_data:
                    each_record['services'] = host_handlerobject.get_services_list(host_ip=each_record['ip_address'],
                                                                                   host_id=each_record['host_id'])

                    aws_yearly_on_premise_cost = aws_yearly_on_premise_cost + (
                        each_record['yearly_onpremise_cost'] if each_record['yearly_onpremise_cost'] is not None else 0)

                    aws_on_demand_price = aws_on_demand_price + (
                        each_record['on_demand_price'] if each_record['on_demand_price'] is not None else 0)
                    aws_reserved_1_year_price = aws_reserved_1_year_price + (
                        each_record['reserved_1_year_price'] if each_record['reserved_1_year_price'] is not None else 0)
                    aws_reserved_3_year_price = aws_reserved_3_year_price + (
                        each_record['reserved_3_year_price'] if each_record['reserved_3_year_price'] is not None else 0)
                    aws_actual_storage_price = aws_actual_storage_price + (
                        each_record['actual_storage_price'] if each_record['actual_storage_price'] is not None else 0)

                    aws_recommended_on_demand_price = aws_recommended_on_demand_price + (
                        each_record['recommended_on_demand_price'] if each_record[
                                                                          'recommended_on_demand_price'] is not None else 0)
                    aws_recommended_reserved_1_year_price = aws_recommended_reserved_1_year_price + (
                        each_record['recommended_1_year_price'] if each_record[
                                                                       'recommended_1_year_price'] is not None else 0)
                    aws_recommended_reserved_3_year_price = aws_recommended_reserved_3_year_price + (
                        each_record['recommended_3_year_price'] if each_record[
                                                                       'recommended_3_year_price'] is not None else 0)
                    aws_recommended_storage_price = aws_recommended_storage_price + (
                        each_record['recommended_storage_price'] if each_record[
                                                                        'recommended_storage_price'] is not None else 0)

                summary["onPrem"] = math.ceil(aws_yearly_on_premise_cost)
                summary["onDemand"] = math.ceil(aws_on_demand_price) + math.ceil(aws_actual_storage_price)
                summary["1YearReserved"] = math.ceil(aws_reserved_1_year_price) + math.ceil(aws_actual_storage_price)
                summary["3YearReserved"] = math.ceil(aws_reserved_3_year_price) + math.ceil(aws_actual_storage_price)
                summary["actual_storage_price"] = math.ceil(aws_actual_storage_price)
                summary["RecommendeOnDemand"] = math.ceil(aws_recommended_on_demand_price) + math.ceil(
                    aws_recommended_storage_price)
                summary["Recommended1YearReserved"] = math.ceil(aws_recommended_reserved_1_year_price) + math.ceil(
                    aws_recommended_storage_price)
                summary["Recommended3YearReserved"] = math.ceil(aws_recommended_reserved_3_year_price) + math.ceil(
                    aws_recommended_storage_price)
                summary["recommended_storage_price"] = math.ceil(aws_recommended_storage_price)

                infrastructure_assets['summary'] = summary
                infrastructure_assets['migration_overview'] = migration_overview_data
                return infrastructure_assets
            else:
                infrastructure_assets['summary'] = None
                infrastructure_assets['migration_overview'] = None
                logger.info(f"Empty data obtained while running dashboard infrastructure migration analysis")
                return infrastructure_assets
        except Exception as e:
            logger.error(
                f"Exception occurred while preparing infrastructure assessment for the account_id : {account_id}.Exception :{e}")
            return None

    def infrastructure_assets_host_list(self, account_id=None):
        """
        fetches the complete list of succesfully dicovered host list along with the recomendation data of each ip.
        :param account_id: integer, unique id assigned for every user
        :return: list of dictionaries, each dictinary contains details of host and recommendations for that host_ip
        """
        try:
            logger.debug(f"Fetching the host list containing assets details for the account_id: {account_id}")

            host_handlerobject = HandleHostDetails()
            succesfull_ip_details_reponse = host_handlerobject.get_succesfull_host_list_data(account_id)
            assets_list, aws_yearly_on_premise_cost, aws_on_demand_price, aws_reserved_1_year_price, aws_reserved_3_year_price, summary = [], 0, 0, 0, 0, {}

            for each in succesfull_ip_details_reponse:
                ip_details = {}
                ip_details['host_id'] = each["host_id"]
                ip_details['host_name'] = each['host_name']
                ip_details['host_ip'] = each['ip']
                ip_details['operating_system'] = each['operating_system']
                ip_details['version'] = each['version']
                ip_details['cloud_provider'] = each['cloud_provider']
                ip_details['instance_type'] = each['instance_type']
                ip_details['support_end_date'] = each['support_date']
                ip_details['compatible'] = each['compatible']
                ip_details['recommendation'] = each['message']
                ip_details['yearly_onpremise_cost'] = CreateInfra.on_premise_vm_price.value
                ip_details['on_demand_price'] = each['on_demand_price']
                ip_details['reserved_1_year_price'] = each['reserved_1_year_price']
                ip_details['reserved_3_year_price'] = each['reserved_3_year_price']
                ip_details['region'] = each['region']
                ip_details['data_center'] = each['data_center']
                ip_details['line_of_business'] = each['line_of_business']
                ip_details['project'] = each['project']
                ip_details['services'] = host_handlerobject.get_services_list(host_ip=each['ip'],
                                                                              host_id=each['host_id'])
                ip_details['applications'] = None
                if ip_details['compatible']:
                    assets_list.append(ip_details)
                    if ip_details['cloud_provider'] == "AWS":
                        aws_yearly_on_premise_cost = aws_yearly_on_premise_cost + (
                            ip_details['yearly_onpremise_cost'] if ip_details[
                                                                       'yearly_onpremise_cost'] is not None else 0)
                        aws_on_demand_price = aws_on_demand_price + (
                            ip_details['on_demand_price'] if ip_details['on_demand_price'] is not None else 0)
                        aws_reserved_1_year_price = aws_reserved_1_year_price + (
                            ip_details['reserved_1_year_price'] if ip_details[
                                                                       'reserved_1_year_price'] is not None else 0)
                        aws_reserved_3_year_price = aws_reserved_3_year_price + (
                            ip_details['reserved_3_year_price'] if ip_details[
                                                                       'reserved_3_year_price'] is not None else 0)

            summary["onPrem"] = math.ceil(aws_yearly_on_premise_cost)
            summary["onDemand"] = math.ceil(aws_on_demand_price)
            summary["1YearReserved"] = math.ceil(aws_reserved_1_year_price)
            summary["3YearReserved"] = math.ceil(aws_reserved_3_year_price)

            logger.debug(f"Successfully fetched assets values for all the tasks under the account_id: {account_id}")

            return {'response': assets_list, 'summary': summary}

        except Exception as e:
            logger.error(
                f"Exception occurred while processing successfully discoverd ip details in services for account_id: {account_id}. Exception :{e}")
            return None

    def onprem_actions(self, host_id=None, logger_ip=None):
        """
        fetches the host discovery details and derive recomandations.
        :param account_id: integer, unique id assigned for every user
        :return: list of dictionaries, each dictinary contains details of host and recommendations for that host_ip
        """
        try:
            logger.debug(f"Genarate the actions using host discovery results for the host_id: {host_id}")
            onprem_actions = dict(host_id=host_id,
                                  action_type="On_Premise",
                                  recommendation=None)

            host_handler_object = HandleHostDetails()
            host_data = host_handler_object.get_host_results(host_id)[0]

            # actions based on login history & ports trafic
            try:
                login_data = host_data.get('user_group_information')
                if not login_data:
                    login_actions = copy.deepcopy(onprem_actions)
                    login_actions["recommendation"] = "It is a demon server with no activity"
                    host_handler_object.update_onprem_action(onprem_actions=login_actions, logger_ip=logger_ip)
            except Exception as e:
                logger.error(f"Exception occurred while processing onprem_actions host_id: {host_id}. Exception :{e}")


            ############################################  Need to check  ###########################
            port_data = host_data.get('port_information')
            port_result = []
            # need to check about the CreateInfra.port_service_filter.value values
            try:
                for portitem in port_data:
                    if int(portitem.get('number')) not in CreateInfra.port_number_filter.value and portitem.get(
                            'service') not in CreateInfra.port_service_filter.value:
                        if not portitem.get('foreign_address') in ['', '0.0.0.0']:
                            port_result.append('portitem')
                            break
                if not port_result:
                    ports_actions = copy.deepcopy(onprem_actions)
                    ports_actions["recommendation"] = "It is a demon server with no activity"
                    host_handler_object.update_onprem_action(onprem_actions=ports_actions, logger_ip=logger_ip)
            except Exception as e:
                logger.error(f"Exception occurred while processing onprem_actions host_id: {host_id}. Exception :{e}")

            # actions based on  Unused Files or Temp Files which are occupying most of Usage -- yet to implement this
            # host_service = HostDetails()
            # Metrics = host_service.get_utilizations(host_data)

            host_handler_object = HandleHostDetails()
            Metrics = host_handler_object.get_utilization_averages(host_id, logger_ip=logger_ip)[0]

            # actions based on storage usage
            try:
                storgare_val = Metrics.get("storage_percent")
                storage_actions = copy.deepcopy(onprem_actions)
                if storgare_val:
                    if storgare_val <= 10:
                        storage_actions["recommendation"] = "Storage usage is less.Reduce the storage capacity"
                        host_handler_object.update_onprem_action(onprem_actions=storage_actions, logger_ip=logger_ip)
                    elif storgare_val >= 90:
                        storage_actions["action_type"] = "Storage usage is high.Increase the storage capacity"
                        host_handler_object.update_onprem_action(onprem_actions=storage_actions, logger_ip=logger_ip)
                    else:
                        pass
            except Exception as e:
                logger.error(f"Exception occurred while processing onprem_actions host_id: {host_id}. Exception :{e}")

            # actions based on the performance of memory usage
            try:
                memory_val = Metrics.get("memory_percent")
                memory_actions = copy.deepcopy(onprem_actions)
                if memory_val:
                    if memory_val >= 90:
                        memory_actions["recommendation"] = "Memory usage is high.Increase the memory capacity"
                        host_handler_object.update_onprem_action(onprem_actions=memory_actions, logger_ip=logger_ip)
                    elif memory_val <= 10:
                        memory_actions["recommendation"] = "Memory usage is less.Reduce the memory capacity"
                        host_handler_object.update_onprem_action(onprem_actions=memory_actions, logger_ip=logger_ip)
                    else:
                        pass
            except Exception as e:
                logger.error(f"Exception occurred while processing onprem_actions host_id: {host_id}. Exception :{e}")

            # actions based on the performance of cpu usage
            try:
                cpu_val = Metrics.get("cpu_percent")
                cpu_actions = copy.deepcopy(onprem_actions)
                if cpu_val:
                    if cpu_val >= 90:
                        cpu_actions["recommendation"] = "CPU usage is high.Increase the core capacity"
                        host_handler_object.update_onprem_action(onprem_actions=cpu_actions, logger_ip=logger_ip)
                    elif cpu_val >= 10:
                        cpu_actions["recommendation"] = "CPU usage is less.Reduce the core capacity"
                        host_handler_object.update_onprem_action(onprem_actions=cpu_actions, logger_ip=logger_ip)
                    else:
                        pass
            except Exception as e:
                logger.error(f"Exception occurred while processing onprem_actions host_id: {host_id}. Exception :{e}")
            return None
        except Exception as e:
            logger.error(
                f"Exception occurred while generating the actions for host_id: {host_id}. Exception :{e}")
            return None
