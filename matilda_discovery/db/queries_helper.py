from sqlalchemy.sql import text

from matilda_discovery.logger_engine import logger as logger_py_file

logger = logger_py_file.Logger()


def execute_dict_of_queries(list_of_queries, engine, **kwargs):
    """
    Accepts list of queries and returns list of queries output.
    :param list_of_queries: dictionary , keys as the query names and queries as the values of that dictionary.
    :param engine: checks if a session is present with MySQL, if not it will create a new session
    :param kwargs: parameters to be passed required by all the queries
    :return: list of dictionaries where key is query and the value is the output of that query.
    """
    logger.debug(f"Executing each query from the {kwargs['info']} query list")
    final_dump = {}

    try:
        logger.debug(f"List of queries = {list_of_queries}, parameters = {kwargs}")
        for query_name, query in list_of_queries.items():
            response = engine.execute(text(query), kwargs).fetchall()
            if len(response) == 0:
                logger.debug(f"Returned empty data for the query: {query_name} ")
                pass
            else:
                # results from SQLAlchemy are returned as a list of tuples; this procedure converts it into a list of dicts
                result = query_response_formatting(response)
                final_dump[query_name] = result

        logger.debug(f"Successfully executed every query from the {kwargs['info']}query list")

    except Exception as e:
        logger.error(f"Failed to execute all queries from the {kwargs['info']} query list. Error: {e}")

    return final_dump


def execute_report_queries(list_of_queries, engine, **kwargs):
    """
    Accepts list of queries and returns list of queries output.
    :param list_of_queries: queries from a particular section
    :param engine: checks if a session is present with MySQL, if not it will create a new session
    :param kwargs: parameters to be passed required by all the queries
    :return: list of dictionaries where key is query and the value is the output of that query.
    """
    final_dump = []
    try:
        logger.debug(f"Executing each query from the {kwargs['info']} query list")
        for query_name, query in list_of_queries.items():
            results = []
            logger.debug(f"Executing Query: {query_name} through SQLAlchemy")
            response = engine.execute(text(query), kwargs).fetchall()
            if len(response) == 0:
                logger.debug(f"Returned Empty data for the Query: {query_name} through SQLAlchemy")
                pass
            else:
                # results from SQLAlchemy  are returned as a list of tuples; this procedure converts it into a list of dicts
                for row_number, row in enumerate(response):
                    results.append({})
                    for column_number, value in enumerate(row):
                        results[row_number][row.keys()[column_number]] = value
                final_dump.append(dict(name=query_name, query_data=results))
            logger.debug(f"Successfully executed list of queries.")
    except Exception as e:
        logger.error(f"Failed to execute all queries from the {kwargs['info']} query list. \n EXCEPTION: {e}")
    return final_dump


def query_response_formatting(response):
    """
    Accepts the response from an executed query which is a list of tuples and returns a list of dicts for ease of use.
    :param response:query output in the tuples form
    :return: query output in dictionary format
    """
    logger.debug(f"Response formatting")

    result = []
    try:
        for row_number, row in enumerate(response):
            result.append({})
            for column_number, value in enumerate(row):
                result[row_number][row.keys()[column_number]] = value
        # for each in result: # check if this is really needed
        #     for key, value in each.items():
        #         if each[key] is None:
        #             each[key] = 0
        logger.debug(f"Response formatting successful")

    except Exception as e:
        logger.error(f"Response formatting failed. Error: {e}")

    return result
