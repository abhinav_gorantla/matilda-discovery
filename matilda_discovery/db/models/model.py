import datetime
from tokenize import String

from oslo_config import cfg
from oslo_db.sqlalchemy import models
from sqlalchemy import (Column, Float)
from sqlalchemy import Integer, Text, ForeignKey, DateTime, CHAR
from sqlalchemy import orm
from sqlalchemy.dialects.mysql import INTEGER, MEDIUMTEXT, SMALLINT, TINYINT, MEDIUMINT, VARCHAR
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref

CONF = cfg.CONF
BASE = declarative_base()


def MediumText():
    return Text().with_variant(MEDIUMTEXT(), 'mysql')


class MatildaNetworkBase(models.ModelBase):
    metadata = None

    def __copy__(self):
        session = orm.Session()

        copy = session.merge(self, load=False)
        session.expunge(copy)
        return copy

    def save(self, session=None):
        from matilda_discovery.db import connection
        if session is None:
            session = connection.get_session()

        super(MatildaNetworkBase, self).save(session=session)

    def delete(self, session=None):
        from matilda_discovery.db import connection
        if session is None:
            session = connection.get_session()

        super(MatildaNetworkBase, self).delete(session=session)

    def __repr__(self):
        items = ['%s=%r' % (col.name, getattr(self, col.name))
                 for col in self.__table__.columns]
        return "<%s.%s[object at %x] {%s}>" % (self.__class__.__module__,
                                               self.__class__.__name__,
                                               id(self), ','.join(items))

    def to_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


############################### HOST DISCOVERY MODELS ##################################################

# it creates tables for the hostname in the database
class Task(BASE, MatildaNetworkBase):
    __tablename__ = 'task'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    status = Column(Text)
    name = Column(Text)
    created_by = Column(Text)
    start_date = Column(DateTime)
    end_date = Column(DateTime)
    updated_by = Column(Text)
    login_mode = Column(Text)
    account_id = Column(MEDIUMINT)
    discovery_type = Column(Text)
    created_datetime = Column(DateTime)
    scheduled_time = Column(DateTime)
    execution_mode = Column(Text)
    url = Column(Text)
    port = Column(Text)
    username = Column(Text)
    password = Column(Text)
    type = Column(Text)


    ip_information = relationship('IP', backref=backref('task', cascade='save-update'))
    user_port_information = relationship('User_Port')
    host_information = relationship('Host')
    datacenter_information = relationship('Datacenter')



class IP(BASE, MatildaNetworkBase):
    __tablename__ = 'ip'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    task_id = Column(MEDIUMINT, ForeignKey('task.id', ondelete="cascade"))
    host_id = Column(MEDIUMINT)
    ip_list = Column(Text)
    type = Column(Text)
    region = Column(Text)
    data_center = Column(Text)
    line_of_business = Column(Text)
    project = Column(Text)
    environment = Column(Text)
    application = Column(Text)
    utilization = Column(CHAR)
    utilization_interval = Column(Integer)
    utilization_period = Column(Integer)


class User_Port(BASE, MatildaNetworkBase):
    __tablename__ = 'user_port'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    task_id = Column(MEDIUMINT, ForeignKey('task.id'))
    name = Column(Text)
    port = Column(Text)


class Host(BASE, MatildaNetworkBase):
    __tablename__ = 'host'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    task_id = Column(MEDIUMINT, ForeignKey('task.id'))
    ip = Column(VARCHAR(15))
    platform = Column(Text)
    operating_system = Column(Text)
    name = Column(Text)
    bios_configuration = Column(Text)
    version = Column(Text)
    release_version = Column(Text)
    system_date = Column(Text)
    instance_type = Column(Text)
    service_provider = Column(Text)
    device_type = Column(Text)
    memory = Column(Float(8, 2))
    domain = Column(Text)
    status = Column(Text)
    reason = Column(Text)
    stage = Column(Text)
    device_identification_flag = Column(TINYINT)
    login_flag = Column(TINYINT)
    gateway_ip = Column(VARCHAR(15))
    community = Column(Text)
    instruction_set = Column(Text)
    sku = Column(Text)
    timezone = Column(Text)
    uptime = Column(Text)
    power_status = Column(Text)
    installed_on = Column(Text)
    pagefile_size = Column(Text)
    internet_access = Column(Text)
    job_entry = Column(TINYINT)
    revision = Column(VARCHAR(50))
    cpu_usage = Column(Text)
    memory_usage = Column(Text)
    storage_usage = Column(Text)
    dependents = Column(SMALLINT)
    memory_type = Column(Text)
    activity = Column(VARCHAR(50))
    start_date = Column(DateTime)
    end_date = Column(DateTime)
    historic = Column(TINYINT)

    storage_information = relationship('Storage')
    memory_information = relationship('Memory')
    service_information = relationship('Service')
    interface_information = relationship('Interface')
    package_information = relationship('Package')
    cpu_information = relationship('CPU')
    user_group_information = relationship('User_Group')
    port_information = relationship('Port')
    recommendation_information = relationship('OS_Recommendation')
    service_recommendation_information = relationship('Service_Recommendation')
    application_information = relationship('ApplicationHostMapping')
    utilization_information = relationship('Utilization')
    login_history_information = relationship("Login_History")
    certificate_information = relationship("Certificates")
    credentials_information = relationship('Credentials')
    docker_information = relationship('DockerInfo')
    error_information = relationship('Error')


class Credentials(BASE, MatildaNetworkBase):
    __tablename__ = 'host_credentials'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    username = Column(Text)
    password = Column(Text)
    key = Column(Text)
    domain = Column(Text)


class CPU(BASE, MatildaNetworkBase):
    __tablename__ = 'cpu'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    utilization_percent = Column(Float(5, 2))
    physical_processors = Column(Integer)
    physical_cores = Column(Integer)
    logical_processors = Column(Integer)
    model_name = Column(Text)


class Memory(BASE, MatildaNetworkBase):
    __tablename__ = 'memory'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    used = Column(Float(8, 2))
    free = Column(Float(8, 2))
    available = Column(Float(8, 2))
    total = Column(Float(8, 2))
    used_percent = Column(Float(5, 2))
    type = Column(Text)
    speed = Column(Text)


class Storage(BASE, MatildaNetworkBase):
    __tablename__ = 'storage'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    disk = Column(Text)
    type = Column(Text)
    partition = Column(Text)
    logical_volume = Column(Text)
    owner = Column(Text)
    group = Column(Text)
    permissions = Column(Text)
    file_system = Column(Text)
    total = Column(Float(8, 2))
    used = Column(Float(8, 2))
    available = Column(Float(8, 2))
    used_percent = Column(Float(5, 2))
    mounted = Column(Text)
    model = Column(Text)
    boot_partition = Column(Text)
    logical_disk = Column(Text)
    volume_serial_number = Column(Text)
    compressed = Column(Text)


class Interface(BASE, MatildaNetworkBase):
    __tablename__ = 'interface'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    name = Column(Text)
    ip_address = Column(Text)
    gateway = Column(Text)
    additional_ip = Column(Text)
    packet_max_size = Column(Text)
    received_packets = Column(Text)
    sent_packets = Column(Text)
    status = Column(Text)
    description = Column(Text)
    mac_address = Column(Text)
    type = Column(Text)
    speed = Column(Text)
    ip_subnet = Column(Text)
    dhcp_enabled = Column(Text)
    dns_server = Column(Text)


class Port(BASE, MatildaNetworkBase):
    __tablename__ = 'port'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    recv_queue = Column(Text)
    send_queue = Column(Text)
    local_address = Column(VARCHAR(15))
    foreign_address = Column(VARCHAR(60))
    foreign_port = Column(Text)
    process_id = Column(Text)
    number = Column(Text)
    local_service = Column(Text)
    status = Column(Text)
    used_interface = Column(Text)
    service = Column(Text)
    connected_service = Column(Text)
    direction = Column(Text)
    cpu_time = Column(Text)
    protocol = Column(Text)
    cpu_percent = Column(Float(5, 2))
    memory_percent = Column(Float(5, 2))


class Package(BASE, MatildaNetworkBase):
    __tablename__ = 'package'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    name = Column(Text)
    version = Column(Text)
    repository = Column(Text)
    basedir = Column(Text)
    category = Column(Text)
    arch = Column(Text) # rename the column
    vendor = Column(Text)
    insdate = Column(Text) # rename the column
    status = Column(Text)


class Login_History(BASE, MatildaNetworkBase):
    __tablename__ = 'login_history'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    time = Column(DateTime)
    user_name = Column(Text)
    server_name = Column(Text)
    server_ip = Column(Text)
    action = Column(Text)

class Certificates(BASE, MatildaNetworkBase):
    __tablename__ = 'certificate'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    path = Column(Text)
    certificate_name = Column(Text)
    issuer = Column(Text)
    not_after = Column(DateTime)
    not_before = Column(DateTime)
    serial_number = Column(Text)
    thumb_print= Column(Text)
    dns_name_list = Column(Text)
    subject = Column(Text)
    version = Column(Text)
    ps_provider = Column(Text)



class Service(BASE, MatildaNetworkBase):
    __tablename__ = 'service'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    name = Column(Text)
    status = Column(VARCHAR(4))
    type = Column(Text)
    path = Column(Text)
    discovery_id = Column(Text)
    version = Column(Text)
    port = Column(Text)
    cpu_percent =Column(Text)
    memory_percent =Column(Text)
    core_service = Column(TINYINT)

    service_recommendation_information = relationship('Service_Recommendation')


class Application(BASE, MatildaNetworkBase):
    __tablename__ = 'application'

    id = Column(SMALLINT, primary_key=True, autoincrement=True)
    name = Column(VARCHAR(250))
    discovered_name = Column(VARCHAR(250))
    app_code = Column(Text)
    app_owner = Column(Text)
    business_group = Column(Text)
    environment = Column(Text)
    business_critical = Column(Text)
    availability = Column(Text)
    performance = Column(Text)
    cloud_suitability = Column(Text)
    target_deployment = Column(Text)
    migration_strategy = Column(Text)
    integration_complexity = Column(Text)
    security = Column(Text)
    compliance = Column(Text)
    elasticity = Column(Text)
    utilization = Column(Text)
    maturity = Column(Text)
    migration_complexity = Column(Text)
    migration_priority = Column(Text)

    Application_Recommendation_Information = relationship("Application_Recommendation")


class Application_Recommendation(BASE, MatildaNetworkBase):
    __tablename__ = 'application_recommendation'

    id = Column(SMALLINT, primary_key=True, autoincrement=True)
    application_id = Column(SMALLINT, ForeignKey('application.id'))
    cloud_provider = Column(Text)
    migration_priority = Column(Text)
    migration_suitability = Column(Text)
    migration_strategy = Column(Text)
    on_demand_price = Column(Float(10,2))
    reserved_1_year_price = Column(Float(10,2))
    reserved_3_year_price = Column(Float(10,2))
    recommended_on_demand_price = Column(Float(10, 2))
    recommended_1_year_price = Column(Float(10, 2))
    recommended_3_year_price = Column(Float(10, 2))


class ApplicationHostMapping(BASE, MatildaNetworkBase):
    __tablename__ = 'application_host_mapping'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    application_id = Column(SMALLINT, ForeignKey('application.id'))
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    service_discovery_id = Column(MEDIUMINT)


class User_Group(BASE, MatildaNetworkBase):
    __tablename__ = 'user_group'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    group_name = Column(Text)
    username = Column(Text)
    group_description = Column(Text)
    user_type = Column(Text)
    user_id = Column(MEDIUMINT)
    home_directory = Column(Text)
    last_password_change = Column(Text)
    account_expiry_date = Column(Text)
    min_days_for_pwd_change = Column(MEDIUMINT)
    max_days_for_pwd_change = Column(MEDIUMINT)
    warning_days_for_pwd_change = Column(MEDIUMINT)
    password_inactive = Column(Text)
    password_status = Column(Text)
    password_expiry_date = Column(Text)
    last_login = Column(Text)


class Reports(BASE, MatildaNetworkBase):
    __tablename__ = 'report'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    type = Column(Text)
    format = Column(Text)
    location = Column(Text)
    file_name = Column(Text)
    cloud_provider = Column(Text)
    name = Column(Text)
    account_id = Column(MEDIUMINT)
    task_id = Column(MEDIUMINT)
    reason = Column(Text)
    status = Column(Text)
    created = Column(Text)
    report_job_entry = Column(TINYINT)


class Utilization(BASE, MatildaNetworkBase):
    __tablename__ = 'utilization'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    executiontime = Column(Text)
    memory_percent = Column(Float(5, 2))
    cpu_percent = Column(Float(5, 2))
    storage_percent = Column(Float(5, 2))

class Error(BASE, MatildaNetworkBase):
    __tablename__ = 'error'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    ip = Column(Text)
    type = Column(Text)
    error = Column(Text)


class Device_Lookup(BASE, MatildaNetworkBase):
    __tablename__ = 'device_lookup'

    id = Column(SMALLINT, primary_key=True, autoincrement=True)
    mib = Column(Text)
    device_type = Column(Text)
    platform = Column(Text)


class OS_Lookup(BASE, MatildaNetworkBase):
    __tablename__ = 'os_lookup'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    operating_system = Column(Text)
    base_name = Column(Text)
    version = Column(Text)
    platform = Column(Text)
    cloud_provider = Column(Text)
    available_image_name = Column(Text)
    support_date = Column(DateTime)
    recommendation = Column(Text)
    action = Column(Text)


class OS_Recommendation(BASE, MatildaNetworkBase):
    __tablename__ = 'os_recommendation'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    support_date = Column(DateTime)
    cloud_provider = Column(Text)
    compatible = Column(VARCHAR(3))
    message = Column(VARCHAR(50))
    criticality = Column(VARCHAR(30))
    compatibility_score = Column(Float(10,2))
    action = Column(Text)
    priority = Column(VARCHAR(30))
    migration_strategy = Column(VARCHAR(30))
    migration_complexity = Column(VARCHAR(50))
    yearly_onpremise_cost = Column(Float(10,2))
    on_demand_price = Column(Float(10,2))
    reserved_1_year_price = Column(Float(10, 2))
    reserved_3_year_price = Column(Float(10, 2))
    actual_storage_price = Column(Float(10, 2))
    no_upfront_fee_1_year = Column(Float(10,2))
    partial_upfront_fee_1_year = Column(Float(10,2))
    full_upfront_fee_1_year = Column(Float(10,2))
    no_upfront_fee_3_year = Column(Float(10,2))
    partial_upfront_fee_3_year = Column(Float(10,2))
    full_upfront_fee_3_year = Column(Float(10,2))
    instance_type = Column(Text)
    recommended_vcpu_count = Column(MEDIUMINT)
    recommended_memory = Column(MEDIUMINT)
    recommended_storage = Column(MEDIUMINT)
    recommended_on_demand_price = Column(Float(10,2))
    recommended_1_year_price = Column(Float(10,2))
    recommended_3_year_price = Column(Float(10,2))
    recommended_instance_type = Column(Float(10,2))
    recommended_storage_price = Column(Float(10,2))


class Service_Lookup(BASE, MatildaNetworkBase):
    __tablename__ = 'service_lookup'

    id = Column(SMALLINT, primary_key=True, autoincrement=True)
    software = Column(Text)
    name = Column(Text)
    version = Column(Text)
    type = Column(Text)
    cloud_provider = Column(Text)
    support_date = Column(DateTime)
    recommendation = Column(Text)
    action = Column(Text)


class Service_Recommendation(BASE, MatildaNetworkBase):
    __tablename__ = 'service_recommendation'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    service_id = Column(MEDIUMINT, ForeignKey('service.id'))
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    support_date = Column(DateTime)
    eosl = Column(VARCHAR(20))
    criticality = Column(VARCHAR(20))
    move_priority = Column(VARCHAR(20))
    migration_strategy = Column(VARCHAR(20))
    migration_complexity = Column(VARCHAR(50))
    message = Column(VARCHAR(50))
    action = Column(Text)


class Service_Port(BASE, MatildaNetworkBase):
    __tablename__ = 'service_port'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    service_name = Column(Text)
    port = Column(Text)

class Datacenter(BASE, MatildaNetworkBase):
    __tablename__ = 'datacenter'

    id = Column(INTEGER, primary_key=True, autoincrement=True)
    task_id = Column(MEDIUMINT, ForeignKey('task.id'))
    name = Column(Text)
    status = Column(Text)
    network_count = Column(Text)
    vcenter_host_count = Column(INTEGER)
    cluster_count = Column(INTEGER)

class Cluster(BASE, MatildaNetworkBase):
    __tablename__ = 'cluster'

    id = Column(INTEGER, primary_key=True, autoincrement=True)
    datacenter_id = Column(INTEGER, ForeignKey('datacenter.id'))
    name = Column(Text)
    status = Column(Text)
    vcenter_host_count = Column(INTEGER)
    effective_vcenter_host_count = Column(INTEGER)
    vm_count = Column(INTEGER)
    power_off_vm_count = Column(INTEGER)
    cpu_core_count = Column(INTEGER)
    cpu_thread_count = Column(INTEGER)
    effective_cpu_count = Column(INTEGER)
    total_cpu = Column(INTEGER)
    effective_memory = Column(INTEGER)
    total_memory = Column(INTEGER)


class Vcenter_Host(BASE, MatildaNetworkBase):
    __tablename__ = 'vcenter_host'
    id = Column(INTEGER, primary_key=True, autoincrement=True)
    cluster_id = Column(INTEGER, ForeignKey('cluster.id'))
    ip = Column(Text)
    status = Column(Text)
    cpu_usage = Column(Text)
    host_count = Column(INTEGER)
    memory_usage = Column(Text)
    cpu_clock_speed = Column(Text)
    cpu_core_count = Column(INTEGER)
    cpu_model = Column(Text)
    cpu_thread_count = Column(INTEGER)
    interface_count = Column(INTEGER)
    uuid = Column(Text)
    vendor = Column(Text)
    model = Column(Text)
    hypervisor_version = Column(Text)

    # put FK here



class DockerInfo(BASE, MatildaNetworkBase):
    __tablename__ = 'docker_info'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    name = Column(Text)
    version = Column(Text)
    build_time = Column(Text)
    os_type = Column(Text)
    root_dir = Column(Text)
    images = Column(Text)
    containers = Column(Text)
    containers_running = Column(Integer)
    containers_paused = Column(Integer)
    containers_stopped = Column(Integer)
    docker_images = relationship('DockerImage')


class DockerImage(BASE, MatildaNetworkBase):
    __tablename__ = 'docker_image'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    docker_info_id = Column(MEDIUMINT, ForeignKey('docker_info.id'))
    containers = Column(Text)
    createdat = Column(Text)
    createdsince = Column(Text)
    digest = Column(Text)
    image_id = Column(Text)
    repository = Column(Text)
    sharedsize = Column(Text)
    size = Column(Integer)
    tag = Column(Integer)
    uniquesize = Column(Integer)
    virtualsize = Column(Integer)
    docker_containers = relationship('DockerContainer')


class DockerContainer(BASE, MatildaNetworkBase):
    __tablename__ = 'docker_container'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    docker_image_id = Column(MEDIUMINT, ForeignKey('docker_image.id'))
    command = Column(Text)
    createdat = Column(Text)
    container_id = Column(Text)
    image = Column(Text)
    labels = Column(Text)
    localvolumes = Column(Text)
    mounts = Column(Text)
    names = Column(Text)
    networks = Column(Text)
    ports = Column(Text)
    runningfor = Column(Text)
    size = Column(Text)
    status = Column(Text)
    cpu_percentage = Column(Text)
    memory_usage = Column(Text)
    memory_limit = Column(Text)
    memory_percentage = Column(Text)
    net_in = Column(Text)
    net_out = Column(Text)
    block_in = Column(Text)
    block_out = Column(Text)
    pid = Column(Text)
    user_id = Column(Text)
    process_id = Column(Text)
    cpu_percent = Column(Text)
    image_id = Column(Text)


class Job_Host_Mapping(BASE, MatildaNetworkBase):
    __tablename__ = 'job_host_mapping'
    id = Column(INTEGER, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT)
    job_id = Column(Text)
    job_type = Column(Text)
    job_status = Column(Text)


class Core_Services(BASE, MatildaNetworkBase):
    __tablename__ = 'core_services'
    id = Column(INTEGER, primary_key=True, autoincrement=True)
    service = Column(VARCHAR(250))


class Other_Services(BASE, MatildaNetworkBase):
    __tablename__ = 'other_services'
    id = Column(INTEGER, primary_key=True, autoincrement=True)
    service = Column(VARCHAR(250))

class Action(BASE, MatildaNetworkBase):
    __tablename__ = 'action'
    id = Column(INTEGER, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT)
    action_type = Column(Text)
    recommendation = Column(Text)

class Validation(BASE, MatildaNetworkBase):
    __tablename__ = 'validation'
    id = Column(INTEGER, primary_key=True, autoincrement=True)
    ip = Column(Text)
    task_name = Column(Text)
    community = Column(Text)
    username = Column(Text)
    password = Column(Text)
    task_id = Column(Text)
    ping = Column(Text)
    telnet = Column(Text)
    login = Column(Text)
    cmd_execution = Column(Text)
    reason = Column(Text)



class RawEnvVariables(BASE, MatildaNetworkBase):
    __tablename__ = 'raw_env_variables'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    name = Column(Text)
    value = Column(Text)



class RawProcess(BASE, MatildaNetworkBase):
    __tablename__ = 'raw_process'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    name = Column(Text)
    description = Column(Text)
    process_name = Column(Text)
    product = Column(Text)
    path = Column(Text)
    cpu_percent = Column(Text)
    file_version = Column(Text)
    product_version = Column(Text)
    process_id = Column(Text)
    total_processor_time = Column(Text)
    start_time = Column(Text)
    user = Column(Text)
    memory_percent = Column(Text)
    vsz = Column(Text)
    rss = Column(Text)
    tty = Column(Text)
    stat = Column(Text)
    command = Column(Text)

class RawPrograms(BASE, MatildaNetworkBase):
    __tablename__ = 'raw_programs'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    name = Column(Text)
    publisher = Column(Text)
    install_location = Column(Text)
    version = Column(Text)
    display_version = Column(Text)
    version_major = Column(Text)
    version_minor = Column(Text)
    install_date = Column(Text)
    install_source = Column(Text)

class RawServices(BASE, MatildaNetworkBase):
    __tablename__ = 'raw_services'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    name = Column(Text)
    display_name = Column(Text)
    status = Column(Text)
    dependent_services = Column(Text)
    services_depended_on = Column(Text)
    service_type = Column(Text)


class RawUptime(BASE, MatildaNetworkBase):
    __tablename__ = 'raw_uptime'

    id = Column(MEDIUMINT, primary_key=True, autoincrement=True)
    host_id = Column(MEDIUMINT, ForeignKey('host.id'))
    command = Column(Text)
    user = Column(Text)
    caption = Column(Text)


