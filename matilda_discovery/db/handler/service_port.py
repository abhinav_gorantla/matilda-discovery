from matilda_discovery.db import connection
from matilda_discovery.db.models import model

from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()



class ServicePortHandler:

    def __init__(self):
        self.session = connection.get_session()


    def get_service_port_details(self):
        """
        fetches services and respective ports from service_port table
        :return: dictionary
        """
        logger.debug(f"Fetching services and ports from service_port table")

        try:
            with self.session.begin():
                query = self.session.query(model.Service_Port)
                resp = query.all()

            logger.debug(f"Successfully fetched services and port numbers from service_port table")
            return resp


        except Exception as e:
            logger.error(f"Failed to fetch services and port numbers from service_port table. Error: {e}")
            return None

