from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db import connection
from matilda_discovery.db.connection import get_engine
from matilda_discovery.db.models import model
from matilda_discovery.db.queries_helper import execute_dict_of_queries
from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.utils.util import get_properties

logger = logger_py_file.Logger()


class HandleValidations:
    """
    """

    def __init__(self):
        self.session = connection.get_session()
        self.engine = get_engine()


    def update_validations(self, validations=None, logger_ip=None):
        """
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Updating validations")
            with self.session.begin():
                req_data = model.Validation()
                req_data.update(validations)
                req_data.save(session=self.session)
                logger.debug(logger_ip=logger_ip, message=f"Sucessfully updated validations")
                return None

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Exceptions occured while updating validations: Error: {e}")
            return None

    def get_validation_task_list(self, logger_ip=None):
        """
        """
        try:
            property_name = 'validation_task_list'
            table_data_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Validation_Queries", property_name=property_name)
            table_data = execute_dict_of_queries({"validation_task_list": table_data_query}, self.engine, info="validation_task_list queries")
            return table_data.get('validation_task_list')

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to get validation_task_list. Error: {e}")
            return None

    def get_validation_list(self,task_id, logger_ip=None):
        """
        """
        try:
            table_data_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Validation_Queries", property_name='validation_list')
            validation_list = execute_dict_of_queries({"validation_list": table_data_query}, self.engine, info="validation_list queries",task_id=task_id)

            validation_list_summary_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Validation_Queries", property_name='validation_list_summary')
            validation_list_summary = execute_dict_of_queries({"validation_list_summary": validation_list_summary_query}, self.engine, info="validation_list queries", task_id=task_id)

            return validation_list.get('validation_list') , validation_list_summary.get('validation_list_summary')

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to get validation_list. Error: {e}")
            return None

    def get_validation_password(self, id, logger_ip=None):
        """
        """
        try:
            with self.session.begin():
                query = self.session.query(model.Validation)
                if id is not None:
                    query = query.filter_by(id=id)
                resp = query.all()
                return resp[0]

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to get password. Error: {e}")
            return None
