import requests
from sqlalchemy import insert

from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db import connection, queries_helper
from matilda_discovery.db.connection import get_engine
from matilda_discovery.db.models import model
from matilda_discovery.db.queries_helper import execute_dict_of_queries

from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.utils import util

logger = logger_py_file.Logger()

from matilda_discovery.utils.util import get_properties
from sqlalchemy.sql import text


class HandleApplicationDetails:
    """
    contains methods that handle task related data retrieval
    """

    def __init__(self):
        self.session = connection.get_session()
        self.engine = get_engine()

    def delete_application_recommendation_details(self):
        """
        Deletes all the data present in the application_recommendation_details table
        :return:
        """
        try:
            logger.debug(f"Deleting data from application recommendation data")

            with self.session.begin():
                self.session.query(model.Application_Recommendation).delete()
            logger.debug("Successfully deleted all records from application recommendation table")

        except Exception as e:
            logger.error(f"Exception occurred while deleting data from application recommendation table. Exception {e}")
            return None

    def fetch_application_detail_list(self, account_id=None):
        """
        fetches all the list of application details from application table
        return : list, list of dictinary of all the application and details
        """
        try:
            logger.debug(f"Fetching application list details from the DB")
            resp = []

            with self.session.begin():
                query = self.session.query(model.Application)
                temp_resp = query.all()
                for item in temp_resp:
                    resp.append(dict(item))
            logger.debug(f"Successfully returned list of application details")

            return resp

        except Exception as e:
            logger.error(f"Failed in fetching application list. Error: {e}")
            return None

    # def testing_fetch_application_recommendation_detail(self, application_id=None,cloud_provider=None):
    #     """
    #     fetches application recommendation data for a particular id and cloud provider
    #     return : dict, application recommendation details
    #     """
    #     try:
    #         logger.debug(f"Fetching application recommendation details for application_id :{application_id}, cloud_provider :{cloud_provider}from the DB")
    #
    #         application_recommendation_details_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Application_Queries", property_name="cloud_provider_based_Application_recommendation")
    #         application_recommendation_details = execute_dict_of_queries({"application_recommendation_details": application_recommendation_details_query}, self.engine, application_id=1,cloud_provider="AWS", info="Application recommendation details")
    #         application_recommendation_details = application_recommendation_details[0] if application_recommendation_details else None
    #         logger.debug(f"Successfully returned application details")
    #         return application_recommendation_details
    #
    #     except Exception as e:
    #         logger.error(f"Failed in fetching application recommendation details. Error: {e}")
    #         return None


    def fetch_application_recommendation_detail(self, application_id=None,cloud_provider=None):
        """
        fetches application recommendation data for a particular id and cloud provider
        return : dict, application recommendation details
        """
        try:
            logger.debug(f"Fetching application recommendation details for application_id :{application_id}, cloud_provider :{cloud_provider}from the DB")

            with self.session.begin():
                query = self.session.query(model.Application_Recommendation)
                query_result = query.filter_by(application_id=application_id,cloud_provider=cloud_provider)
                query_result=query_result.all()
                response=dict(query_result[0]) if query_result else None

            logger.debug(f"Successfully returned list of application details. Response for application id {application_id} : {response}")

            return response

        except Exception as e:
            logger.error(f"Failed in fetching application recommendation details. Error: {e}")
            return None

    #app assessment update
    def update_application_assessment_data(self, applicationinfo=None, logger_ip=None):
        """
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Updating the application assessment into the application table")
            with self.session.begin():
                for item in applicationinfo:
                    query = self.session.query(model.Application)
                    id = item.get('id')
                    application_query = query.filter_by(id=id)
                    application_query.update(item)

                logger.debug(logger_ip=logger_ip, message=f"Successfully updated the application table")
                return None
        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Exception occurred while updating applications table. Eception : {e}")
            return None


    def update_application_recommendation_data(self, applicationinfo=None, logger_ip=None):
        """
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Updating the application assessment into the application table")
            with self.session.begin():
                for item in applicationinfo:
                    application_recommendation = model.Application_Recommendation()
                    application_recommendation.update(item)
                    application_recommendation.save(session=self.session)

                logger.debug(logger_ip=logger_ip, message=f"Successfully updated the application table")
                return None
        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Exception occurred while updating applications table. Eception : {e}")
            return None

    def fetch_applications_infra_sevices_details(self,application_id=None):
        """
        Fetches services and infra details for a particular application
        :param application_id: int, unique id for evey application
        :return: count of host, services and databases individually
        """
        try:
            logger.debug(f"Fetching the infra, DB,services details present under a particular application.")

            db_services_count_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Stand_Alone_Queries", property_name="Application_services")
            services_details = execute_dict_of_queries({"services_types_count": db_services_count_query}, self.engine, application_id=application_id, info="services and DB count under application")

            logger.debug(f"Successfully returned application details")
            return services_details
        except Exception as e:
            logger.debug(f"Exception occurred while fetching details from DB. Exception {e}")
            return None, None

    def get_application_details(self, app_id, logger_ip=None):
        """
        returns application details for the given app id
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Fetching application details for app_id: {app_id}")

            application_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Application_Queries", property_name="application_query")
            application_details = execute_dict_of_queries({"application_query": application_query}, self.engine, app_id=app_id, info="Application_Queries application_query")
            if application_details:
                logger.debug(f"Successfully returned application prices")
                return application_details['application_query'][0]
            else:
                raise Exception(f"Received no output for the application price query")

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to fetch application details for app_id: {app_id}. Error: {e}")
            return None

    def get_application_prices(self, host_id_list, cloud_provider):
        """
        accepts host_id list and cloud provider. Returns prices for the app id in the cloud provider
        """
        try:
            logger.debug(f"Fetching the application prices for host_id_list: {host_id_list} and cloud_provider: {cloud_provider}")

            price_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Application_Queries", property_name="price_query")
            price_details = execute_dict_of_queries({"price_query": price_query}, self.engine, host_id_list=host_id_list, cloud_provider=cloud_provider, info="Application_Queries price query")
            if price_details:
                logger.debug(f"Successfully returned application prices")
                return price_details['price_query'][0]
            else:
                raise Exception(f"Received no output for the application price query")

        except Exception as e:
            logger.debug(f"Failed to fetch application prices for host_list: {host_id_list} and cloud_provicer: {cloud_provider}. Error: {e}")
            return None, None

    def application_dashboard_details(self,cloud_provider=None):
        """
        run the queries of dashboard analysis and returns the data
        :return: list, list of dictinaries of queires.
        """
        try:
            logger.debug(f"Running quries to fetch application dashboard details.")


            section_name = "Application_Dashboard"

            application_dashboard_quries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name)
            query_results = queries_helper.execute_dict_of_queries(application_dashboard_quries, self.engine, info="Application Dashboard",cloud_provider=cloud_provider)

            return query_results
        except Exception as e:
            logger.error(f"Exception occurred while running application dashboard queries. Exception :{e}")