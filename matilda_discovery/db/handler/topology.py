from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db import queries_helper
from matilda_discovery.db.connection import get_engine

from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()

from matilda_discovery.utils import util


class TopologyDetails:

    def __init__(self):
        self.engine = get_engine()

    def get_ip_stream(self, ip, topology, stream_ip, direction):
        """
        Accepts an initial ip and returns network topology around it with down stream and up stream flows
        :param ip: starting ip
        :param topology: empty dictionary structure to which up stream and down stream data is added
        :param stream_ip: buffer maintained to avoid running into network cycles
        :param direction: contains "Outgoing" or "Incoming". Indicates direction for flow
        :return: dict, network topology around starting ip with down stream and up stream flows
        """
        try:
            logger.debug(f"Collecting {direction} data for ip: {ip}")

            stream_ip[ip] = True
            section_name = "Network_IP_Topology"
            host_level_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name)
            if host_level_queries:
                query_results = queries_helper.execute_dict_of_queries(host_level_queries, self.engine, info="topology test", local_address=ip, direction=direction)
                if query_results != {}:
                    temp_stream = []
                    for items in query_results['ip_topology']:

                        if items['foreign_address'] in stream_ip:
                            temp_stream = temp_stream[:] + topology.get('children')[:] + [{'name': items['foreign_address'], 'colorCode': direction}]
                            continue

                        temp_stream = temp_stream[:] + topology.get('children')[:] + [self.get_ip_stream(items['foreign_address'], {'name': items.get('foreign_address'), 'children': topology.get('children')[:]}, stream_ip, direction)][:]

                    logger.debug(f"Successfully collected {direction} data for ip: {ip}")
                    return {'name': ip, 'colorCode': direction, 'children': temp_stream[:]}
                else:
                    logger.debug(f"Given ip: {ip} is outside the network range or has no {direction} data")
                    return {'name': ip, 'colorCode': direction}

        except Exception as e:
            logger.error(f"Failed to collect {direction} data for ip: {ip}. Error: {e}")
            return {}


    def get_network_stream(self, ip_range_list):
        """
        Accepts a ip range or task id or account id and returns list of dictionary containing all incoming and outgoing connections in the given range
        :param ip_range_list: starting ip for which topology needs to be built
        :return: list, containing all incoming and outgoing connections in the given range
        """
        try:

            if ip_range_list:
                logger.debug(f"Fetching network level incoming and outgoing connections for ip_range_list: {ip_range_list}")

                section_name = "network_level_topology"
                network_level_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name)
                if network_level_queries:
                    query_results = queries_helper.execute_dict_of_queries(network_level_queries, self.engine,
                                                                           port_number_filter=CreateInfra.port_number_filter.value,
                                                                           port_service_filter=CreateInfra.port_service_filter.value,
                                                                           info="fetching network level connections", ip_range=ip_range_list)

                    logger.debug(f"Successfully fetched network level incoming and outgoing connections for ip_range_list: {ip_range_list}")
                    return query_results["network_topology"]

            else:
                logger.debug(f"No ip found in the given ip_range_list: {ip_range_list}")
                return []

        except Exception as e:
            logger.error(f"Failed to fetch network level incoming and outgoing connections for given ip_range_list: {ip_range_list}. Error: {e}")
            return None


    def get_in_network_nodes(self):
        """
        """
        try:

            logger.debug(f"Fetching all ips in the network")

            section_name = "In_Network_IP"
            network_level_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name)
            if network_level_queries:
                query_results = queries_helper.execute_dict_of_queries(network_level_queries, self.engine, info="fetching all IPs in range")

                logger.debug(f"Successfully fetched all IPs in the network")
                return query_results["query_1"]

        except Exception as e:
            logger.error(f"Failed to fetch all IPs in the network. Error: {e}")
            return None


    def get_node_properties(self, every_ip_node):
        """
        Accepts a ip range or task id or account id and returns list of dictionary containing all incoming and outgoing connections in the given range
        :param every_ip_node: list of ip's
        :return: list, containing all incoming and outgoing connections in the given range
        """
        try:
            if every_ip_node:
                logger.debug(f"Fetching node properties")

                section_name = "network_node_properties"
                node_property_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name)
                if node_property_queries:
                    query_results = queries_helper.execute_dict_of_queries(node_property_queries, self.engine, info="fetching node properties", every_ip_node=every_ip_node)

                    if query_results.get("query_1"):
                        logger.debug(f"Successfully fetched node properties")
                        return query_results["query_1"]
                    else:
                        logger.error(f"No output from query:network_node_properties-query_1. Returned Null value")
                        return []

            else:
                logger.debug(f"No incoming or outgoing connection found for the given every_ip_node: {every_ip_node}")
                return []

        except Exception as e:
            logger.error(f"Failed to fetch node properties. Error: {e}")
            return []


    def get_dependents_count(self, host_ip):
        """
        Accepts an IP adress and returns total number of dependents based on incoming and outgoing connections in port table
        """
        try:
            logger.debug(f"Fetching number of dependents for ip: {host_ip}")

            section_name = "IP_Dependents"
            dependents_count_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name)
            query_results = queries_helper.execute_dict_of_queries(dependents_count_queries, self.engine, info="fetching number of dependents", ip=host_ip)

            if query_results not in [None, []]:
                logger.debug(f"Found {query_results['number_of_dependents']} dependents for ip {host_ip}")
                return query_results["number_of_dependents"][0].get("dependents")

            else:
                logger.debug(f"Found no dependents for ip {host_ip}")
                return None

        except Exception as e:
            logger.error(f"Failed to fetch number of dependents for ip {host_ip}. Error: {e}")
            return None


    def get_node_property(self, ip):
        """
        Accepts a ip and returns list of dictionary containing number of incoming and outgoing connectiosn, and other properties
        :param ip: ip address
        :return: list, containing all incoming and outgoing connections in the given range
        """
        try:
            if ip:
                logger.debug(f"Fetching node property")

                section_name = "network_node_property"
                node_property_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name)
                if node_property_queries:
                    query_results = queries_helper.execute_dict_of_queries(node_property_queries, self.engine, info="fetching node properties", ip=ip)

                    logger.debug(f"Successfully fetched node properties")
                    return query_results["node_property"]
            else:
                logger.debug(f"No property found for the given ip: {ip}")
                return []

        except Exception as e:
            logger.error(f"Failed to fetch node properties. Error: {e}")
            return None

