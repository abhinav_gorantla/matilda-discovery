from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db import queries_helper
from matilda_discovery.utils import util
from matilda_discovery.db.connection import get_engine

from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()



class DashboardData:

    def __init__(self):
        self.engine = get_engine()

    def task_status_details(self, task_id):
        """
        Accepts only task_id and returns task status of a particular task
        :param task_id: int,  task_id an optional parameter
                    every discovery will have a unique task_id, where the data of a particular discovery can be retrieved using task_id
        :return:  tuple containing app_status, started_time, elapsed_time of the task
        """
        logger.debug(f"Fetching task status details queries")

        try:
            section_name = "Dashboard_Task_Status_Details"
            logger.debug(f"Fetching status details of the task:  {task_id}")
            task_status_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name)
            if task_status_queries:
                query_results = queries_helper.execute_dict_of_queries(task_status_queries, self.engine, info="task status", task_id=task_id)
                logger.debug(f"Successfully executed task status details queries")
                return query_results

            else:
                logger.error(f"Task status query list is empty for task_id :{task_id}")
                return None
        except Exception as e:
            logger.error(f"Failed to execute task status details queries. Error: {e}")
            return None

    def utilization_details(self, account_id, task_id=None):
        """
        Accepts account_id, optional task_id and returns utilization details.
        :param account_id: int, mandatory field
                        every user will have a different account_id with which the whole users data can be retrieved.
        :param task_id: int,  task_id an optional parameter
                    every discovery will have a unique task_id, where the data a particular discovery cna be retrieved using task_id
        :return: dictionary, contains results of executing utilization details queries
        """
        logger.debug(f"Fetching utilization details queries")
        try:
            if task_id is not None:
                section_name = "Dashboard_Utilization_Details_Using_task_id"
            else:
                section_name = "Dashboard_Utilization_Details_Using_account_id"

            utilization_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name)
            if utilization_queries:
                query_results = queries_helper.execute_dict_of_queries(utilization_queries, self.engine, info="utilization", account_id=account_id, task_id=task_id)
                logger.debug("Successfully executed utilization details queries")
            else:
                logger.error(" Dashboard utilization query list is empty for account_id :{account_id}, task_id :{task_id}")
                query_results=None
            return query_results
        except Exception as e:
            logger.error(f"Failed to execute utilization details queries. Error: {e}")
            return None

    def stage_details(self, account_id, task_id=None):
        """
        Accepts account_id, optional task_id and returns stage details of the all the host_id's under the account_id or task_id details.
        :param account_id: int, mandatory field
                        every user will have a different account_id with which the whole users data can be retrieved.
        :param task_id: int,  task_id an optional parameter
                    every discovery will have a unique task_id, where the data a particular discovery cna be retrieved using task_id
        :return: dictionary, contains results of executing stage details queries
        """
        logger.debug(f"Fetching stage details details queries")
        try:
            if task_id is not None:
                section_name = "Dashboard_Stage_Details_Using_task_id"
                logger.debug(f"Fetching stage details queries of task_id:  {task_id}")
            else:
                section_name = "Dashboard_Stage_Details_Using_account_id"
                logger.debug(f"Fetching stage details queries of account_id:  {account_id}")

            stage_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name)
            if stage_queries:
                query_results = queries_helper.execute_dict_of_queries(stage_queries, self.engine, info="stage", account_id=account_id, task_id=task_id)
                logger.debug(f"Successfully executed stage details queries")
            else:
                logger.error(f"Dashboard_Stage_Details_Using_task_id query list is empty")
                query_results=None

            return query_results

        except Exception as e:
            logger.error(f"Failed to executed stage details details queries. Error: {e}")
            return None

    def host_distribution_details(self, account_id):
        """
        Accepts account_id, optional task_id and returns host_distribution of the all the host_id's under the account_id or task_id details.
        :param account_id: int, mandatory field every user will have a different account_id with which the whole users data can be retrieved.
        :return: dictionary, contains results of executing host distribution details queries
        """
        try:
            logger.debug(f"Fetching host distribution details for accoutn_id: {account_id}.")
            section_name = "Host_Distribution_Using_Account_ID"

            host_distribution_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name)
            if host_distribution_queries:
                query_results = queries_helper.execute_dict_of_queries(host_distribution_queries, self.engine, info="host distribution", account_id=account_id)
                logger.debug(f"Successfully executed host distribution queries")
            else:
                logger.error(f"Host_Distribution query list is empty")
                query_results = None

            return query_results

        except Exception as e:
            logger.error(f"Failed to execute host distribution queries. Error: {e}")
            return None

    def operating_system_details(self, account_id, task_id=None):
        """
        Accepts account_id, optional task_id and returns operating system of the all the host_id's under the account_id or task_id details.
        :param account_id: int, mandatory field
                        every user will have a different account_id with which the whole users data can be retrieved.
        :param task_id: int, task_id an optional parameter
                    every discovery will have a unique task_id, where the data a particular discovery cna be retrieved using task_id
        :return: dictionary, contains results of executing operating system details queries
        """
        logger.debug(f"Fetching operating system details queries")

        try:

            if task_id is not None:
                section_name = "Operating_System_Details_Using_task_id"
                logger.debug(f"Fetching operating system details queries of task_id {task_id}")
            else:
                section_name = "Operating_System_Details_Using_account_id"
                logger.debug(f"Fetching operating system details of account_id :  {account_id}")

            operating_system_details_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name)
            query_results = queries_helper.execute_dict_of_queries(operating_system_details_queries, self.engine, info="operating system", account_id=account_id, task_id=task_id)['Operating_System_Details']

            logger.debug(f"Successfully executed operating system details queries")
            return query_results

        except Exception as e:
            logger.error(f"Failed to execute operating system queries. Error: {e}")
            return None

    def application_service_details(self, account_id, task_id=None):
        """
        Accepts account_id, optional task_id and returns application services details of the all the host_id's under the account_id or task_id details.
        :param account_id: int, mandatory field
                        every user will have a different account_id with which the whole users data can be retrieved.
        :param task_id: int, task_id an optional parameter
                    every discovery will have a unique task_id, where the data a particular discovery cna be retrieved using task_id
        :return: dictionary, contains results of executing application service details queries
        """
        logger.debug(f"Fetching application service details queries")

        service_details = []
        try:

            if task_id is not None:
                section_name = "Service_Details_Using_task_id"
                logger.debug(f"Fetching application service details queries of task_id= {task_id}")
            else:
                section_name = "Service_Details_Using_account_id"
                logger.debug(f"Fetching application service details queries of account_id= {account_id}")

            service_details_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name)
            query_results = queries_helper.execute_dict_of_queries(service_details_queries, self.engine, account_id=account_id, info="application services", task_id=task_id)['Service_Details']

            logger.debug("Successfully executed application service details")
            return query_results

        except Exception as e:
            logger.error(f"Failed to fetch and execute application service queries. Error: {e}")
            return None

    def device_details(self, account_id, task_id=None):
        """
        Accepts account_id, optional task_id and returns device details of the all the host_id's under the account_id or task_id details.
        :param account_id: int, mandatory field
                        every user will have a different account_id with which the whole users data can be retrieved.
        :param task_id: int, task_id an optional parameter
                    every discovery will have a unique task_id, where the data a particular discovery cna be retrieved using task_id
        :return: dictionary, contains results of executing device details queries
        """
        logger.debug(f"Fetching device details")
        try:

            if task_id is not None:
                section_name = "Device_Type_Details_Using_task_id"
                logger.debug(f"Fetching device details queries of task_id= {task_id}")
            else:
                section_name = "Device_Type_Details_Using_account_id"
                logger.debug(f"Fetching device details queries of acount_id= {account_id}")

            device_details_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name)
            query_results = queries_helper.execute_dict_of_queries(device_details_queries, self.engine, account_id=account_id, info="device details", task_id=task_id)['Device_Details']

            logger.debug("Successfully executed device details queries")
            return query_results

        except Exception as e:
            logger.error(f"Failed to fetch and execute device details queries. Error: {e}")
            return None

class TaskSummary:

    def __init__(self):
        self.engine = get_engine()
        pass

    def task_services_summary(self,account_id,task_id):
        """
        fetches the summary of the services summary present under the given task_id.
        :return: dictionary, name os service as key and count of services as the value in hte dictionary
        """

        try:
            logger.debug(f"fetching the task service summary  from DB of the task_id : {task_id}")
            section_name="Task_Service_Summary"
            device_details_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name)
            query_results = queries_helper.execute_dict_of_queries(device_details_queries, self.engine, account_id=account_id, info="task services summary", task_id=task_id)['service_summary']
            logger.debug(f"Successfully collected task summary details of services from DB for task_id: {task_id}.")
            return query_results
        except Exception as e:
            logger.error(f"Exception occurred while fetching the summary of the services from DB for the task_id: {task_id}. Exception : {e}")
            return None


