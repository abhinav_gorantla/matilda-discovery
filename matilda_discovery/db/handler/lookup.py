from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db.connection import get_engine
from matilda_discovery.db.queries_helper import execute_dict_of_queries
from matilda_discovery.utils.util import get_properties
from matilda_discovery.logger_engine import logger as logger_py_file

logger = logger_py_file.Logger()


class HandleLookup:

    def __init__(self):
        self.engine = get_engine()

    def get_table(self, property_name="os_lookup_query", logger_ip=None):
        """
        accepts the query name under the section Lookup_table_queries and returns the executed output
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Fetching table data for the query {property_name}")

            table_data_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Lookup_table_queries", property_name=property_name)
            table_data = execute_dict_of_queries({"actions_list": table_data_query}, self.engine,info="Lookup_table_queries os_lookup_query")

            if table_data.get('actions_list'):
                logger.debug(logger_ip=logger_ip, message=f"Successfully fetched table data for the query {property_name}")
                return table_data['actions_list']
            else:
                raise Exception(f"Failed to get data in handler. received output: {table_data}")

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to fetch data for the query: {property_name}. Error: {e}")
            return None
