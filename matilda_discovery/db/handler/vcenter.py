from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.utils import util

from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()

from matilda_discovery.db import connection
from matilda_discovery.db.models import model


class Vcenter:
    """
    contains methods that handle updating database with information of created task
    """

    def __init__(self):
        self.session = connection.get_session()



    def create_datacenter(self, datacenter_info):


        logger.debug(logger_ip="vcenter", message=f"Storing Vcenter_Datacenter Details Into Datacenter Table")

        try:
            with self.session.begin():
                request_data = model.Datacenter()
                request_data.update(datacenter_info)
                request_data.save(session=self.session)
            logger.debug(logger_ip="vcenter", message=f"Successfully stored Vcenter_Datacenter Details Into Datacenter Table")
            return request_data.get('id')

        except Exception as e:
            logger.error(logger_ip="vcenter", message=f"Failed to store datacenter details. Error: {e}")
            return None


    def create_cluster(self, cluster_info):


        logger.debug(logger_ip="vcenter", message=f"Storing Vcenter_Cluster Details Into Cluster Table")

        try:
            with self.session.begin():
                request_data = model.Cluster()
                request_data.update(cluster_info)
                request_data.save(session=self.session)
            logger.debug(logger_ip="vcenter", message=f"Successfully stored Vcenter_Cluster Details Into Cluster Table")
            return request_data.get('id')

        except Exception as e:
            logger.error(logger_ip="vcenter", message=f"Failed to store cluster details. Error: {e}")
            return None

    def create_vcenterhost(self, vcenterhost_info):

        logger.debug(logger_ip="vcenter", message=f"Storing Vcenter_Host Details Into Vcenter_Host Table")

        try:
            with self.session.begin():
                request_data = model.Vcenter_Host()
                request_data.update(vcenterhost_info)
                request_data.save(session=self.session)
            logger.debug(logger_ip="vcenter", message=f"Succesfully stored Vcenter_Host Details Into Vcenter_Host Table")
            return request_data.get('id')

        except Exception as e:
            logger.error(logger_ip="vcenter", message=f"Failed to store vcenter_host details. Error: {e}")
            return None

    def get_new_vcenter_requests(self):
        """
        fetches new requests from DB based on status and discovery_type. Fetches vcenter requests
        :return:  list. one with vcenter requests
        """
        try:
            logger.debug(logger_ip=CreateInfra.scheduler_log_name.value, message=f"Fetching vcenter requests from DB based on status and discovery_type")
            with self.session.begin():
                query = self.session.query(model.Task)

                vcenter_query = query.filter_by(status='submitted',discovery_type='vcenter')
                vcenter_requests_data = vcenter_query.all()
                vcenter_query.update(dict(status='in_progress'))


            if len(vcenter_requests_data) != 0:
                logger.debug(logger_ip=CreateInfra.scheduler_log_name.value, message="Successfully fetched requests from DB based on job_entry")
            return vcenter_requests_data

        except Exception as e:
            logger.error(f"Exception occurred while fetching new vcenter requests from DB based on status and discovery_type . Error:  {e}")
            return None













