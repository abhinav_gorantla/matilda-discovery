from matilda_discovery.db import connection
from matilda_discovery.db.models import model
from matilda_discovery.constant.matilda_enum import CreateInfra

from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()



class ReportDetails:
    """
    contains methods that handle task related data retrieval
    """

    def __init__(self):
        self.session = connection.get_session()

    def create_new_report_requests(self, data):
        """
        Creating new requests to DB
        :param data:
        :return:
        """
        try:
            logger.debug(f"Creating new request for generating reports in DB")
            with self.session.begin():
                req_data = model.Reports()
                req_data.update(data)
                req_data.save(session=self.session)
            return req_data.get('id')
        except Exception as e:
            logger.error(f"Failed to create new requests in DB for report generation.  Exception: {e}")

    def update_reports(self, req_data):
        """

        :return:
        """
        try:
            # logger.debug(f"updating reports table for account_id: {req_data['id']}, task_id: {req_data['task_id']}")
            with self.session.begin():
                query = self.session.query(model.Reports)
                query = query.filter_by(id=req_data['id'])
                query.update(req_data)
                logger.debug(f"Successfully Updated Report Details to Reports table")
            return query
        except Exception as e:
            logger.error(
                f"Exception occurred while updating the reports table for account_id: {req_data['account_id']},task_id: {req_data['task_id']}.Exception: {e}")
            return None

    def get_reports_list(self, req_data):
        """
        fetches all the reports details from the DB
        :return:
        """
        try:
            logger.debug("Fectching report list details from DB")
            with self.session.begin():
                query = self.session.query(model.Reports)
                query = query.filter_by(account_id=req_data['account_id'])
            return query.all()
        except Exception as e:
            logger.error(f"Exception occurred while fetching report list details from DB. Error: {e}")
            return None

    def get_report_location(self, req_data):
        """
        fetched the report location for a given id
        :return:
        """
        try:
            logger.debug("Fetching report location for the given id")
            with self.session.begin():
                query = self.session.query(model.Reports)
                query = query.filter_by(id=req_data['id'])
            return query.all()
        except Exception as e:
            logger.error(f"Exception occurred while fetching report list details from DB. Error: {e}")
            return None
