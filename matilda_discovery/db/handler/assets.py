from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db.connection import get_engine
from matilda_discovery.db.queries_helper import execute_dict_of_queries
from matilda_discovery.utils import util
from matilda_discovery.utils.util import get_properties
from matilda_discovery.logger_engine import logger as logger_py_file

logger = logger_py_file.Logger()


class HandleAssets:

    def __init__(self):
        self.engine = get_engine()

    def infrastructure_assets_data(self, cloud_provider, logger_ip=None):
        """
        accepts the cloud provider name under the section Lookup_table_queries and returns the executed output
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Fetching table data for the queries under Infrastructure_Assessment_Queries")

            response = {}

            prices_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Infrastructure_Assessment_Queries",
                                          property_name="prices_query")
            prices_details = execute_dict_of_queries({"prices_query": prices_query}, self.engine, cloud_provider=cloud_provider, info="")
            if prices_details:
                logger.debug(f"Successfully executed prices_query under Infrastructure_Assessment_Queries")
                response['prices_details'] = prices_details
            else:
                logger.error(logger_ip=logger_ip, message=f"Failed to get prices_details data in handler. received output: {prices_details}")

            infrastructure_count_query = get_properties(property_file_path=CreateInfra.db_queries_path.value,
                                                        section="Infrastructure_Assessment_Queries", property_name="infrastructure_count_query")
            infrastructure_count_details = execute_dict_of_queries({"infrastructure_count_query": infrastructure_count_query}, self.engine,
                                                                   cloud_provider=cloud_provider, info="")
            if infrastructure_count_details:
                logger.debug(f"Successfully executed infrastructure_count_query under Infrastructure_Assessment_Queries")
                response['infrastructure_count_details'] = infrastructure_count_details
            else:
                logger.error(logger_ip=logger_ip,
                             message=f"Failed to get infrastructure_count_details data in handler. received output: {infrastructure_count_details}")

            compatible_count_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Infrastructure_Assessment_Queries",
                                                    property_name="compatible_count_query")
            compatible_count_details = execute_dict_of_queries({"compatible_count_query": compatible_count_query}, self.engine,
                                                               cloud_provider=cloud_provider, info="")
            if compatible_count_details:
                logger.debug(f"Successfully executed compatible_count_query under Infrastructure_Assessment_Queries")
                response['compatible_count_details'] = compatible_count_details
            else:
                logger.error(logger_ip=logger_ip,
                             message=f"Failed to get compatible_count_details data in handler. received output: {compatible_count_details}")

            migration_strategy_count_query = get_properties(property_file_path=CreateInfra.db_queries_path.value,
                                                            section="Infrastructure_Assessment_Queries",
                                                            property_name="migration_strategy_count_query")
            migration_strategy_count_details = execute_dict_of_queries({"migration_strategy_count_query": migration_strategy_count_query},
                                                                       self.engine, cloud_provider=cloud_provider, info="")
            if migration_strategy_count_details:
                logger.debug(f"Successfully executed migration_strategy_count_query under Infrastructure_Assessment_Queries")
                response['migration_strategy_count_details'] = migration_strategy_count_details
            else:
                logger.error(logger_ip=logger_ip,
                             message=f"Failed to get migration_strategy_count_details data in handler. received output: {migration_strategy_count_details}")

            migration_priority_count_query = get_properties(property_file_path=CreateInfra.db_queries_path.value,
                                                            section="Infrastructure_Assessment_Queries",
                                                            property_name="migration_priority_count_query")
            migration_priority_count_details = execute_dict_of_queries({"migration_priority_count_query": migration_priority_count_query},
                                                                       self.engine, cloud_provider=cloud_provider, info="")
            if migration_priority_count_details:
                logger.debug(f"Successfully executed migration_priority_count_query under Infrastructure_Assessment_Queries")
                response['migration_priority_count_details'] = migration_priority_count_details
            else:
                logger.error(logger_ip=logger_ip,
                             message=f"Failed to get migration_priority_count_details data in handler. received output: {migration_priority_count_details}")

            operating_system_count_query = get_properties(property_file_path=CreateInfra.db_queries_path.value,
                                                          section="Infrastructure_Assessment_Queries", property_name="operating_system_count_query")
            operating_system_count_details = execute_dict_of_queries({"operating_system_count_query": operating_system_count_query}, self.engine,
                                                                     cloud_provider=cloud_provider, info="")
            if operating_system_count_details:
                logger.debug(f"Successfully executed operating_system_count_query under Infrastructure_Assessment_Queries")
                response['operating_system_count_details'] = operating_system_count_details
            else:
                logger.error(logger_ip=logger_ip,
                             message=f"Failed to get operating_system_count_details data in handler. received output: {operating_system_count_details}")

            server_type_count_query = get_properties(property_file_path=CreateInfra.db_queries_path.value,
                                                     section="Infrastructure_Assessment_Queries", property_name="server_type_count_query")
            server_type_count_details = execute_dict_of_queries({"server_type_count_query": server_type_count_query}, self.engine,
                                                                cloud_provider=cloud_provider, info="")
            if server_type_count_details:
                logger.debug(f"Successfully executed server_type_count_query under Infrastructure_Assessment_Queries")
                response['server_type_count_details'] = server_type_count_details
            else:
                logger.error(logger_ip=logger_ip,
                             message=f"Failed to get server_type_count_details data in handler. received output: {server_type_count_details}")

            if response == {}:
                logger.error(logger_ip=logger_ip, message=f"Failed to fetch all of the details under Infrastructure_Assessment_Queries")
                return {}

            logger.debug(logger_ip=logger_ip, message=f"Successfully ran all queries under Infrastructure_Assessment_Queries")
            return response

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to fetch data for the queries under Infrastructure_Assessment_Queries. Error: {e}")
            return {}

