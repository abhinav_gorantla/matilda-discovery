from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db.connection import get_engine
from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.utils import util

logger = logger_py_file.Logger()

from matilda_discovery.db import connection, queries_helper
from matilda_discovery.db.models import model

from sqlalchemy.sql import text


class CreateRecommendation:

    def __init__(self):
        self.session = connection.get_session()
        self.engine = get_engine()
        pass


    def create_recommendation_record(self,new_record):
        """
        creates a new recommendation record for the request obtained from scheduler.
        :return:
        """
        try:
            logger.debug("Creating new records for the request obtained from scheduler in recommendations table")
            with self.session.begin():
                request_data = model.OS_Recommendation()
                request_data.update(new_record)
                request_data.save(session=self.session)

            logger.debug(f"Successfully created new record for host_id : {new_record['host_id']}")

        except Exception as e:
            logger.error(f"Exception occured while storing credentials for task_id: {new_record['host_id']}. Error: {e}")


    def fetch_os_recommendation(self,host_id=None,cloud_provider=None):
        """
        fetches the os_recommendation record for a particular host_id and cloud provider.
        :param host_id: unique id for infrastructure.
        :return:
        """
        try:
            logger.debug(f"Fetching os_recommendation data for host_id :{host_id}, cloud_provider :{cloud_provider}")
            with self.session.begin():
                data = self.session.query(model.OS_Recommendation).filter_by(host_id=int(host_id), cloud_provider=cloud_provider).all()

                # query = self.session.query(model.OS_Recommendation)
                # query = query.filter_by(host_id=host_id, cloud_provider=cloud_provider)
                # data=query.all()
                return data
        except Exception as e:
            logger.error(f"Exception occurred while fetching os_recommendation record based on cloud_provider and host_id .Exception {e}")
            return None
    def update_support_date_recommendation_info(self, host_id, support_date_recommendation_info, logger_ip=None):
        """
        accepts list of dictionaries for recommendation table with host id and dumps it into the database
        :param host_id: id for which the data is being inserted
        :param support_date_recommendation_info: data that needs to be inserted into recommendation table
        """

        try:
            logger.debug(logger_ip=logger_ip, message=f"updating the recommendation table for host id: {host_id} with support_date recommendation data: {support_date_recommendation_info}")
            if support_date_recommendation_info in [None, []]:
                raise Exception(f"Missing support_date recommendation data: {support_date_recommendation_info}")

            with self.session.begin():

                query = self.session.query(model.OS_Recommendation)
                query = query.filter_by(host_id=host_id, cloud_provider=support_date_recommendation_info['cloud_provider'])
                output = query.all()[0]
                output.update(support_date_recommendation_info)
                output.save(session=self.session)

                logger.debug(logger_ip=logger_ip, message=f"Successfully updated the recommendation table")
                return True

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to insert data into the recommendation table. Error: {e}")
            return False

    def update_recommendation_info(self, os_recommendation_info, logger_ip=None):
        """
        accepts list of dictionaries for recommendation table with host id and dumps it into the database
        :param host_id: id for which the data is being inserted
        :param support_date_recommendation_info: data that needs to be inserted into recommendation table
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Updating the os recommendation table for host id: {os_recommendation_info['host_id']} with support_date recommendation data: {os_recommendation_info}")
            if os_recommendation_info in [None, {}]:
                raise Exception(f"Missing os recommendation data: {os_recommendation_info}")

            with self.session.begin():
                recommendation_data = model.OS_Recommendation()
                recommendation_data.update(os_recommendation_info)
                recommendation_data.save(session=self.session)

                logger.debug(logger_ip=logger_ip,message=f"Successfully created and stored host records for host_id: {os_recommendation_info['host_id']}, cloud_provider={os_recommendation_info['cloud_provider']}")

                return True

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to insert os recommendation data into the recommendation table host_id: {os_recommendation_info['host_id']}, cloud_provider={os_recommendation_info['cloud_provider']} . Error: {e}")
            return False

    def update_service_recommendation_info(self, service_recommendation_info, host_id, logger_ip=None):
        """
        accepts list of dictionaries for recommendation table with host id and dumps it into the database
        :param service_recommendation_info: data that needs to be inserted into recommendation table
        :param host_id: corresponding host id of services being updated in service_recommendation table
        :param logger_ip: ip address for ip level logging
        :return: None
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"updating the service recommendation table for host id: {host_id} with service recommendation data: {service_recommendation_info}")

            if service_recommendation_info in [None, []]:
                raise Exception(f"Missing service recommendation data.")

            for items in service_recommendation_info:
                with self.session.begin():
                    service_recommendation_model = model.Service_Recommendation()
                    service_recommendation_model.update(items)
                    service_recommendation_model.save(session=self.session)

            logger.debug(logger_ip=logger_ip,message=f"Successfully created and stored host records for host_id: {host_id}")

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to insert service recommendation data into the recommendation table host_id: {host_id}. Error: {e}")


    def fetch_operating_system_lookup(self, req_data, logger_ip=None):
        """
        Accepts operating system name and returns recommendation details using os_lookup table
        :param logger_ip: ip address required for ip level logging
        :return: list of recommendation details for every cloud provider
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"fetching support_date details from os_lookup table using operating system name")
            cloud_provider = req_data['cloud_provider'] if req_data['cloud_provider'] else None

            os_lookup_query = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Stand_Alone_Queries", property_name="OS_Lookup_query")
            dictionary_of_queries = {"operating_system_lookup": os_lookup_query}

            if str.lower(req_data['platform']) == "windows":
                temp_version = req_data["version"]
            else:
                temp_version = req_data['operating_system'] + ' ' + req_data['release_version']

            query_results = queries_helper.execute_dict_of_queries\
                (
                    dictionary_of_queries,
                    self.engine,
                    info="os lookup status",
                    temp_value=temp_version,
                    cloud_provider=cloud_provider
                )

            if query_results.get('operating_system_lookup') and query_results['operating_system_lookup'][0]:
                logger.debug(logger_ip=logger_ip, message=f"Successfully fetched support_date details using operating system name. Results: {query_results['operating_system_lookup']}")
                return query_results['operating_system_lookup'][0]
            else:
                logger.info(logger_ip=logger_ip, message=f"Returned empty data for the support date query.")
                return None

            # else:
            #     raise Exception("Query returned empty data")
            #     return None

        except KeyError as e:
            logger.error(logger_ip=logger_ip, message=f"fetching support_date details from os_lookup table using operating system name failed. Error: {e}")
            return {}

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to fetch support_date details. Error: {e}")
            return {}


    def fetch_application_upstream_downstream_ips(self, application_ip=None, logger_ip=None):
            """
            accepts list of dictionaries for recommendation table with host id and dumps it into the database
            :param service_recommendation_info: data that needs to be inserted into recommendation table
            :param host_id: corresponding host id of services being updated in service_recommendation table
            :param logger_ip: ip address for ip level logging
            :return: None
            """
            try:
                logger.debug(logger_ip=logger_ip, message=f"fetching support_date details from os_lookup table using operating system name")

                section_name = "application_upstream_downstream_ips"

                task_status_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name)
                if task_status_queries:
                    query_results = queries_helper.execute_dict_of_queries(task_status_queries, self.engine, info="task status", application_ip=application_ip)

                    logger.debug(f"Successfully executed task status details queries")
                    return query_results

            except Exception as e:
                logger.error(logger_ip=logger_ip, message=f"Failed to insert service recommendation data into the recommendation table application_ip: {application_ip}. Error: {e}")