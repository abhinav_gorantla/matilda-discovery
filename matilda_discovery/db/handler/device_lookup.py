

from matilda_discovery.db import connection
from matilda_discovery.db.models import model
from matilda_discovery.utils import util


from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()




class Device_Type:
    """
    contains methods that handle task related data retrieval
    """

    def __init__(self):
        pass

        self.session = connection.get_session()

    def get_device_type(self, mib, host_id=None, logger_ip=None): # done
        """
        fetches details of the type of the device from device_type table.
        :param host_id: integer
        :return: dictionary, type of the device of the host obtained
        """
        logger.debug(logger_ip=logger_ip, message=f"fetching device type details of the host from device_type_lookup table using MIB  :  {host_id}")

        try:
            with self.session.begin():
                query = self.session.query(model.Device_Lookup)
                if mib is not None:
                    query = query.filter_by(mib=mib)
                return query.all()

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to get device_type of the host {host_id} with. Error: {e}")
            return None