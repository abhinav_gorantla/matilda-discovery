from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db.connection import get_engine
from matilda_discovery.db.queries_helper import execute_dict_of_queries
from matilda_discovery.utils.util import get_properties
from matilda_discovery.logger_engine import logger as logger_py_file

logger = logger_py_file.Logger()


class EOL_Date:

    def __init__(self):
        self.engine = get_engine()

    def fetch_service_os_eol(self, name, version, query_name, logger_ip=None):
        """
        accepts the query name, name and version and returns corresponding EOL date from lookup tables in the DB
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Fetching EOL data for query_name: {query_name}, name: {name}, version: {version}")

            eol_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="OS_Service_EOL", property_name=query_name)
            query_output = execute_dict_of_queries({"oel_date": eol_query}, self.engine, name=name, version=version, info="service_oel_date")

            if query_output:
                logger.debug(logger_ip=logger_ip, message=f"Successfully got the EOL date. Returning: {query_output.get('oel_date')[0]}")
                return query_output.get('oel_date')[0]
            else:
                raise Exception(f"Failed to get the EOL date. Received empty output. query_output = {query_output}")

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to fetch eol date. Error: {e}")
            return None


