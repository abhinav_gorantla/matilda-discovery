from matilda_discovery.utils import util

from matilda_discovery.logger_engine import logger as logger_py_file
logger = logger_py_file.Logger()

from matilda_discovery.db import connection
from matilda_discovery.db.models import model


class HandleTaskCreation:
    """
    contains methods that handle updating database with information of created task
    """

    def __init__(self):
        self.session = connection.get_session()


    def create_discovery_task_list(self, resp_hosts):
        """
        Accepts request data, number of hosts and session abject. Updates the database and returns the updated request data along with newly created task id.
        """
        logger.debug("Creating task list obtained from payload")

        try:
            with self.session.begin():
                request_data = model.Task()
                request_data.update(resp_hosts)
                request_data.save(session=self.session)

            logger.debug(f"Successfully created discovery task list with task_id: {request_data.get('id')}")
            return request_data.get('id')

        except Exception as e:
            logger.error(f"Failed to create discover task list. Error: {e}")
            return None


    def create_credentials(self, credentials_data):
        """
        Accepts request data, task id and session abject. Updates the database with user credentials.
        """
        try:
            logger.debug(f"Storing credentials into credentials table in DB for task_id: {credentials_data['task_id']} ")
            with self.session.begin():
                request_data = model.Credentials()
                request_data.update(credentials_data)
                request_data.save(session=self.session)

            logger.debug(f"Successfully stored credentials for task_id: {credentials_data['task_id']}")

        except Exception as e:
            logger.error(f"Exception occured while storing credentials for task_id: {credentials_data['task_id']}. Error: {e}")

    def create_iplist(self, ip_list_data):
        """
        Accepts request data, task id and session abject. Updates the database with ip list.
        """
        try:
            logger.debug(f"Storing ip list data into IP table in DB for task_id: {ip_list_data[0]['task_id']}")
            with self.session.begin():
                for key in ip_list_data:
                    request_data = model.IP()
                    request_data.update(key)
                    request_data.save(session=self.session)
            logger.debug(f"Successfully stored ip list data for task_id: {ip_list_data[0]['task_id']}")

        except Exception as e:
            logger.error(f"Failed to store ip list data for task_id: {ip_list_data[0]['task_id']}. Error: {e}")


    def create_user_port_input(self, user_port_data):
        """
        Accepts request data, task id and session abject. Updates the database with port details.
        """
        logger.debug(f"Storing user_port_input data into user_port table. Data: {user_port_data}")

        try:
            with self.session.begin():
                for key in user_port_data:
                    request_data = model.User_Port()
                    request_data.update(key)
                    request_data.save(session=self.session)

            logger.debug(f"Successfully stored user_port_input data into user_port table. Data: {user_port_data}")

        except Exception as e:
            logger.error(f"Failed to store user port data into user_port table. Data: {user_port_data}. Error: {e}")


class HandleTaskList:
    """
    contains methods that handle task related data retrieval
    """

    def __init__(self):
        self.session = connection.get_session()


    def get_discovery_task_list(self, account_id):
        """
        fetches list of tasks under the account id
        :param account_id:id for every user
        :return:list of tasks
        """
        logger.debug(f"Fetching list of task from task table for the account_id:  {account_id}")
        try:
            with self.session.begin():
                query = self.session.query(model.Task)
                if account_id is not None:
                    query = query.filter_by(account_id=account_id)
                resp = query.all()

            logger.debug(f"Successfully fetched task list details for the account id: {account_id}")
            return resp

        except Exception as e:
            logger.error(f"Failed in fetching task list under the account: {account_id} db handler. Error: {e}")
            return None

    def get_completed_task_list(self,account_id):
        """
        fetches list of completed tasks under the account id
        :param account_id:id for every user
        :return:list,list of completed tasks
        """
        try:
            logger.debug(f"Fetching completed task list from DB for the account_id: {account_id}")
            with self.session.begin():
                query = self.session.query(model.Task)
                if account_id is not None:
                    query = query.filter_by(account_id=account_id,status='completed')
                resp = query.all()
            logger.debug(f"Successfully fetched completed task list details for the account id: {account_id}")
            return resp

        except Exception as e:
            logger.debug(f"Exception occurred while fetching completed task_list from DB for accoutn_id: {account_id}. Exception : {e}")
            return None


    def get_task_details(self, task_id):
        """

        """
        logger.debug(f"Fetchign task details from task table fro task id: {task_id}")

        try:
            with self.session.begin():
                query = self.session.query(model.Task)
                query = query.filter_by(id=task_id)
            logger.debug(f"Successfully fetched task details from DB for task_id:  {task_id}")
            return query.all()

        except Exception as e:
            logger.error(f"Exception occurred while fetching task details fro task_id: {task_id}. Error: {e}")
            return None


    def update_discovery_task_list(self,logger_ip=None, **kwargs): # done
        """
        updates task table with all the data points sent in kwargs
        :return: N/A
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Updating task details into task table for task_id {kwargs['task_id']}.Details: {kwargs}")
            with self.session.begin():
                query = self.session.query(model.Task)
                query = query.filter_by(id=kwargs['task_id'])
                #if query.all()[0]['status'] != 'in_progress':
                temp_dict = {}
                for item in kwargs:
                    if item != 'task_id':
                        temp_dict[item] = kwargs[item]
                query.update(temp_dict)
            logger.debug(logger_ip=logger_ip, message=f"Successfully updated details into task table for task_id {kwargs['task_id']}.Details: {kwargs}")

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Exception occurred while updating task table for task_id {kwargs['task_id']}.Error {e}")

    def task_status_update(self, task_id, logger_ip=None): # done
        """
        returns true if all the host_id's under the given task_id are in completed stage else returns false
        :param task_id: int, unique id for a particular discovery
        :return: boolean, true or false based on all the host_id's completed status
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Fetching the status of the task whether all the hosts of the task were successfully finished discovery for task_id: {task_id} ")
            Flag = False
            if task_id is None:
                raise Exception("Missing task_id")

            with self.session.begin():
                query = self.session.query(model.Host)
                query = query.filter_by(task_id=task_id, historic=0)
                query = query.filter(((model.Host.status == 'submitted') | (model.Host.status == 'in_progress') | (model.Host.status == 'scheduled')))

                if len(query.all()) == 0:
                    logger.debug(logger_ip=logger_ip, message=f"Updating task_id: {task_id} status to completed")
                    task_handler_object = HandleTaskList()
                    task_handler_object.update_discovery_task_list(logger_ip=logger_ip, task_id=task_id, end_date=util.get_datetime(), status="completed")
                    logger.debug(logger_ip=logger_ip, message=f"$$$$$$$$$ All the host's under the task_id {task_id} were successfully discovered $$$$$$$$$")
                    Flag = True

            logger.debug(logger_ip=logger_ip, message=f"Successfully fetched the status of the task for task_id: {task_id} ")
            return Flag

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Exception occurred while fetching the status of the task id: {task_id}. Exception : {e}")
            return None

class HandleReinitiate():
    """
    handles all the functionalities for reinitiating a task
    """
    def __init__(self):
        self.session = connection.get_session()

    def fetch_host_list(self,task_id,status=None,reason=None):
        """
        Fetches all the hosts under the task for the given reason
        :param task_id: Unique id for every task given
        :param reason:reason for failure of the discovery for every host.
        :return:list, list of host_id's with the given host id and the respective reason.
        """
        try:
            logger.debug((f"Fetchign all the hosts under the given task_id: {task_id} and the reason for failure : {reason}"))
            with self.session.begin():
                query= self.session.query(model.Host)
                query=query.filter_by(task_id=task_id, historic=0)
                if reason:
                    query=query.filter_by(reason=reason, historic=0)
                if status:
                    query = query.filter_by(status=status, historic=0)
                host_list=query.all()
            logger.debug(f"Successfully fetched all the host under the reason :{reason} under the task_id :{reason} were fetched.")
            return host_list
        except Exception as e:
            logger.error(f"Exception occured while fetching the host list for reinitite task :{task_id}.Exception:{e}")
            return None

