from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db.connection import get_engine
from matilda_discovery.db.queries_helper import execute_dict_of_queries
from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.utils.util import get_properties

logger = logger_py_file.Logger()


class HandleHostDelta:

    def __init__(self):
        self.engine = get_engine()

    def host_delta_data(self, current_id, previous_id, logger_ip=None):
        """
        accepts current_id, previous_id and runs individual queries to fetch delta between them
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Fetching table data delta for the queries under Host_Details_Delta")

            response = {}

            cpu_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Host_Details_Delta",
                                          property_name="cpu_delta")
            cpu_details = execute_dict_of_queries({"cpu_query": cpu_query}, self.engine, current_id=current_id, previous_id=previous_id, info="")
            if cpu_details:
                logger.debug(f"Successfully executed cpu_delta under Host_Details_Delta")
                response['cpu_details'] = cpu_details
            else:
                logger.error(logger_ip=logger_ip, message=f"Failed to get cpu_details data in handler. received output: {cpu_details}")

            memory_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Host_Details_Delta",
                                          property_name="memory_delta")
            memory_details = execute_dict_of_queries({"memory_query": memory_query}, self.engine, current_id=current_id, previous_id=previous_id, info="")
            if memory_details:
                logger.debug(f"Successfully executed memory_delta under Host_Details_Delta")
                response['memory_details'] = memory_details
            else:
                logger.error(logger_ip=logger_ip, message=f"Failed to get memory_details data in handler. received output: {memory_details}")

            storage_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Host_Details_Delta",
                                          property_name="storage_delta")
            storage_details = execute_dict_of_queries({"storage_query": storage_query}, self.engine, current_id=current_id, previous_id=previous_id, info="")
            if storage_details:
                logger.debug(f"Successfully executed storage_delta under Host_Details_Delta")
                response['storage_details'] = storage_details
            else:
                logger.error(logger_ip=logger_ip, message=f"Failed to get storage_details data in handler. received output: {storage_details}")

            interface_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Host_Details_Delta",
                                          property_name="interface_delta")
            interface_details = execute_dict_of_queries({"interface_query": interface_query}, self.engine, current_id=current_id, previous_id=previous_id, info="")
            if interface_details:
                logger.debug(f"Successfully executed interface_delta under Host_Details_Delta")
                response['interface_details'] = interface_details
            else:
                logger.error(logger_ip=logger_ip, message=f"Failed to get interface_details data in handler. received output: {interface_details}")


            ports_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Host_Details_Delta",
                                          property_name="ports_delta")
            ports_details = execute_dict_of_queries({"ports_query": ports_query}, self.engine, current_id=current_id, previous_id=previous_id, info="")
            if ports_details:
                logger.debug(f"Successfully executed ports_delta under Host_Details_Delta")
                response['ports_details'] = ports_details
            else:
                logger.error(logger_ip=logger_ip, message=f"Failed to get ports_details data in handler. received output: {ports_details}")


            packages_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Host_Details_Delta",
                                          property_name="packages_delta")
            packages_details = execute_dict_of_queries({"packages_query": packages_query}, self.engine, current_id=current_id, previous_id=previous_id, info="")
            if packages_details:
                logger.debug(f"Successfully executed packages_delta under Host_Details_Delta")
                response['packages_details'] = packages_details
            else:
                logger.error(logger_ip=logger_ip, message=f"Failed to get packages_details data in handler. received output: {packages_details}")


            user_groups_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Host_Details_Delta",
                                          property_name="user_groups_delta")
            user_groups_details = execute_dict_of_queries({"user_groups_query": user_groups_query}, self.engine, current_id=current_id, previous_id=previous_id, info="")
            if user_groups_details:
                logger.debug(f"Successfully executed user_groups_delta under Host_Details_Delta")
                response['user_groups_details'] = user_groups_details
            else:
                logger.error(logger_ip=logger_ip, message=f"Failed to get user_groups_details data in handler. received output: {user_groups_details}")


            login_history_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Host_Details_Delta",
                                          property_name="login_history_delta")
            login_history_details = execute_dict_of_queries({"login_history_query": login_history_query}, self.engine, current_id=current_id, previous_id=previous_id, info="")
            if login_history_details:
                logger.debug(f"Successfully executed login_history_delta under Host_Details_Delta")
                response['login_history_details'] = login_history_details
            else:
                logger.error(logger_ip=logger_ip, message=f"Failed to get login_history_details data in handler. received output: {login_history_details}")


            certificates_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Host_Details_Delta",
                                          property_name="certificates_delta")
            certificates_details = execute_dict_of_queries({"certificates_query": certificates_query}, self.engine, current_id=current_id, previous_id=previous_id, info="")
            if certificates_details:
                logger.debug(f"Successfully executed certificates_delta under Host_Details_Delta")
                response['certificates_details'] = certificates_details
            else:
                logger.error(logger_ip=logger_ip, message=f"Failed to get certificates_details data in handler. received output: {certificates_details}")


            services_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Host_Details_Delta",
                                          property_name="services_delta")
            services_details = execute_dict_of_queries({"services_query": services_query}, self.engine, current_id=current_id, previous_id=previous_id, info="")
            if services_details:
                logger.debug(f"Successfully executed services_delta under Host_Details_Delta")
                response['services_details'] = services_details
            else:
                logger.error(logger_ip=logger_ip, message=f"Failed to get services_details data in handler. received output: {services_details}")


            docker_info_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Host_Details_Delta",
                                          property_name="docker_info_delta")
            docker_info_details = execute_dict_of_queries({"docker_info_query": docker_info_query}, self.engine, current_id=current_id, previous_id=previous_id, info="")
            if docker_info_details:
                logger.debug(f"Successfully executed docker_info_delta under Host_Details_Delta")
                response['docker_info_details'] = docker_info_details
            else:
                logger.error(logger_ip=logger_ip, message=f"Failed to get docker_info_details data in handler. received output: {docker_info_details}")

            if response == {}:
                logger.error(logger_ip=logger_ip, message=f"Failed to fetch all of the details under Host_Details_Delta")
                return {}

            logger.debug(logger_ip=logger_ip, message=f"Successfully ran all queries under Host_Details_Delta")
            return response

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to fetch data for the queries under Host_Details_Delta. Error: {e}")
            return {}

