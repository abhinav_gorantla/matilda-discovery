from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.db import connection, queries_helper
from matilda_discovery.db.connection import get_engine
from matilda_discovery.db.models import model
from matilda_discovery.db.queries_helper import execute_dict_of_queries
from matilda_discovery.logger_engine import logger as logger_py_file
from matilda_discovery.utils import util

logger = logger_py_file.Logger()

from matilda_discovery.utils.util import get_properties
from sqlalchemy.sql import text


class HandleHostDetails:
    """
    contains methods that handle task related data retrieval
    """

    def __init__(self):
        self.session = connection.get_session()
        self.engine = get_engine()

    def create_host_list(self, host_records):
        """
        obtain the req data and inserts into host_table by auto incrementing the primary key.
        :param host_records:list of dictionaries, host details of each host
        :return:nothing, it creates records in host table
        """
        logger.debug(f"Creating host records for task_id: {host_records[0]['task_id']} ")

        try:
            with self.session.begin():
                for key in host_records:
                    request_data = model.Host()
                    request_data.update(key)
                    request_data.save(session=self.session)

            logger.debug(f"Successfully created and stored host records for task_id: {host_records[0]['task_id']}")

        except Exception as e:
            logger.error(f"Failed to create and store Host Records for task_id: {host_records[0]['task_id']}. Error: {e}")

    def create_credentials(self, credentials_data):
        """
        Accepts request data, task id and session abject. Updates the database with user credentials.
        """
        try:
            logger.debug(f"Storing credentials into credentials table in DB for task_id: {credentials_data['host_id']} ")
            with self.session.begin():
                request_data = model.Credentials()
                request_data.update(credentials_data)
                request_data.save(session=self.session)

            logger.debug(f"Successfully stored credentials for host_id: {credentials_data['host_id']}")

        except Exception as e:
            logger.error(f"Exception occured while storing credentials for host_id: {credentials_data['host_id']}. Error: {e}")

    def create_individual_host(self, resp_hosts):
        """
        """
        logger.debug("Creating host obtained from payload")

        try:
            with self.session.begin():
                request_data = model.Host()
                request_data.update(resp_hosts)
                request_data.save(session=self.session)

            logger.debug(f"Successfully created discovery host with host_id: {request_data.get('id')}")
            return request_data.get('id')

        except Exception as e:
            logger.error(f"Failed to create discover host. Error: {e}")
            return None

    def insert_host_and_credentials_data(self, host_data=None, credentials_data=None,ip_details=None, logger_ip=None):
        """
        inserts host data,credentials,ip_data into host table without auto increment of host_id.
        :param host_data: dictionary,the data to be inserted into the host table.
        :param credentials_data: dictionary,the data to be inserted into the credentials table.
        :return:None,updates DB with the inserted data and returns nothing
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Inserting  host and credentials data into host_table and credentials. Host data: {host_data}.")

            if host_data is None or credentials_data is None:
                raise Exception(f"Missing host_data or credentials_data. Inputs received: host_data = {host_data}. credentials_data = {credentials_data}")

            host_table_object = model.Host()
            host_table_object.update(host_data)
            host_table_object.save(session=self.session)

            credentials_data['host_id'] = host_table_object.__dict__.get('id')
            credentials_table_object = model.Credentials()
            credentials_table_object.update(credentials_data)
            credentials_table_object.save(session=self.session)

            ip_details['host_id'] = host_table_object.__dict__.get('id')
            ip_table_object = model.IP()
            ip_table_object.update(ip_details)
            ip_table_object.save(session=self.session)

            logger.debug(logger_ip=logger_ip, message=f"Successfully inserted data into host table host and credentials data")
            return None

        except Exception as e:
            logger.debug(f"Exception occurred while inserting data into host table. Exception : {e}")
            return None

    def update_host_table(self, logger_ip=None, **kwargs):  # done
        """
        updates the ping status of the device in host_summary table about status,reason,startdata
        :param task_id: integer,under which list of host_id's present
        :return: list of dictionaries,list of host_details of each host present under task_id
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Updating host details into host table for host_id : {kwargs['host_id']}. Details: {kwargs}")
            temp_dict = {}
            for item in kwargs:
                if item != 'host_id':
                    temp_dict[item] = kwargs[item]
            with self.session.begin():
                query = self.session.query(model.Host)
                if kwargs["host_id"] is not None:
                    query = query.filter_by(id=kwargs['host_id'], historic=0)
                    query.update(temp_dict)

            logger.debug(logger_ip=logger_ip, message=f"Successfully updated host details into host table for host_id: {kwargs['host_id']}.")

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed in updating host_id details into host table for host_id: {kwargs['host_id']}. Error: {e}")
            return None

    def update_table(self, table_info, table_model, truncate_first=False, logger_ip=None):
        """
        accepts data to be inserted into table_model in table_info and updates the table
        """

        try:
            logger.debug(logger_ip=logger_ip, message=f"Updating table : {table_model} with table_info: {table_info} ")

            session = connection.get_session()

            if truncate_first:
                self.session.query(table_model).delete()
                logger.debug(f"Successfully truncated the table : {table_model} before inserting")

            with session.begin():
                if table_info:
                    table_data = []
                    if type(table_info) == list:
                        for key in table_info:
                            table_object = table_model()
                            table_object.update(key)
                            table_object.save(session=session)
                            table_data.append(table_object.__dict__)

                    else:
                        table_object = table_model()
                        table_object.update(table_info)
                        table_object.save(session=session)
                        table_data.append(table_object.__dict__)

                    logger.debug(logger_ip=logger_ip, message=f"Successfully updated table_model: {table_model}")
                    resp = util.update_db_obj(table_data)
                    return resp

                else:
                    logger.error(logger_ip=logger_ip, message=f"Empty data received. Input table_info: {table_info}")

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to update table : {table_model}. Error: {e}")

    def update_error_table(self,error_data,logger_ip=None):
        """
        Updates the exception table with the obtained exception, ip, host_id obtained for a particular host_id.
        """
        try:
            with self.session.begin():
                req_data = model.Error()
                req_data.update(error_data)
                req_data.save(session=self.session)

                return None
        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Exception occurred while updating Error table with the obtained exception for the host_ip :{error_data['ip']}. Exception {e}")
            return None


    def update_utilization_info(self, utilization_info, logger_ip=None):
        """
        accepts models of multiple tables with host id and dumps it into the database
        :param host_id: id for which the data is being inserted
        :param utilization_info: data that needs to be inserted into utilization table
        :return: None
        """

        try:
            logger.debug(logger_ip=logger_ip, message=f"updating database for host id: {utilization_info['host_id']} with utilization data: {utilization_info}")

            with self.session.begin():
                req_data = model.Utilization()
                req_data.update(utilization_info)
                req_data.save(session=self.session)

                logger.debug(logger_ip=logger_ip, message=f"Successfully updated the database")
                return None

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to insert data into the database. Error: {e}")
            return None

    def update_ports_info(self, port_info, logger_ip=None):
        """
        accepts ports table data with host id and dumps it into the database
        :param port_info: data that needs to be inserted into ports table
        :return: None
        """

        try:
            logger.debug(logger_ip=logger_ip, message=f"updating database with port info")

            with self.session.begin():

                if port_info:
                    for key in port_info:
                        if key.get("number"):
                            if key.get("number") in CreateInfra.port_number_filter.value:
                                continue
                        if key.get("service"):
                            if key.get("service") in CreateInfra.port_service_filter.value:
                                continue

                            query = self.session.query(model.Port).filter_by\
                                (
                                    host_id=key.get("host_id"),
                                    local_address=key.get("local_address"),
                                    foreign_address=key.get("foreign_address"),
                                    number=key.get("number"),
                                    direction=key.get("direction"),
                                    service=key.get("service")
                                ).all()
                            if not query:
                                port_model = model.Port()
                                port_model.update(key)
                                port_model.save(session=self.session)

                logger.debug(logger_ip=logger_ip, message=f"Successfully updated the port table")
                return None

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to insert data into the ports table. Error: {e}")
            return None

    def recently_discovered_host_details(self, host_ip):
        """
        accepts host_ip and returns the status and host_id of hte most recent discovery happened on that IP.
        :param host_ip: string, ip_address of a host
        :return:  dictionary,most recent successfully discovered ip's host_id and details
        """
        try:
            logger.debug(f"Fetching host status details for the ip: {host_ip}")
            host_status_details_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Stand_Alone_Queries", property_name="host_ip_details")
            successfully_discovered_ip_details = execute_dict_of_queries({"host_ip_details":host_status_details_query}, self.engine, host_ip=host_ip, info="Stand_Alone_Queries host_ip_details")

            logger.debug(f"Successfully fetched host_ip details from DB for ip : {host_ip}")
            return successfully_discovered_ip_details['host_ip_details'][0]
        except Exception as e:
            logger.error(f"Exception occurred while fetching host_ip status details from DB for host_ip : {host_ip}. Error : {e}")
            return None

    def get_new_discovery_requests(self):
        """
        fetches new requests from DB based on job entry. Fetches SNMP requests if job_entry=1, else fetches DiscoveryReq if job_entry id=3
        :return: 2 list. one with SNMP request , other with Discovery Requests
        """
        try:
            logger.debug(logger_ip=CreateInfra.scheduler_log_name.value, message=f"Fetching request from DB based on job_entry")
            with self.session.begin():
                query = self.session.query(model.Host)

                snmp_query = query.filter_by(job_entry=0, historic=0)
                snmp_requests_data = snmp_query.all()
                snmp_query.update(dict(job_entry=1))

                discovery_query = query.filter_by(job_entry=2, historic=0)
                discovery_requests_data = discovery_query.all()
                discovery_query.update(dict(job_entry=3, status="scheduled", stage=2))

                recommendation_query = query.filter_by(status="success", job_entry=3, device_type="compute", historic=0)
                recommendation_request_data = recommendation_query.all()
                recommendation_query.update(dict(job_entry=4))

            if len(snmp_requests_data) != 0 or len(discovery_requests_data) != 0 or len(recommendation_request_data) !=0:
                logger.debug(logger_ip=CreateInfra.scheduler_log_name.value, message="Successfully fetched requests from DB based on job_entry")
            return snmp_requests_data, discovery_requests_data, recommendation_request_data

        except Exception as e:
            logger.error(f"Exception occurred while fetching new requests from DB based on job_entry . Error:  {e}")
            return None

    def reset_recommendation(self, host_id=None, logger_ip=None):
        """
        deletes all successful os recommendation and service recommendation and reruns the code.
        """
        try:
            if host_id:
                logger.debug(logger_ip=logger_ip, message=f"Deleting successful os recommendation and service recommendation records and resetting job entry to 3 for host id : {host_id}")

                self.session.query(model.Host).filter((model.Host.id == host_id) & (model.Host.historic == 0)).update(dict(job_entry=3))
                self.session.query(model.OS_Recommendation).filter_by(host_id=host_id).delete()
                self.session.query(model.Service_Recommendation).filter_by(host_id=host_id).delete()

                logger.debug(logger_ip=logger_ip, message=f"Successfully restarted recommendations for host id: {host_id}")

            else:
                logger.debug(f"Deleting all successful os recommendation and service recommendation records and resetting job entry to 3")

                recommendation_reset_query = self.session.query(model.Host.id).filter_by(job_entry=4, historic=0)
                recommendation_completed_ids = recommendation_reset_query.all()

                for recommendation_completed_id in recommendation_completed_ids:
                    self.session.query(model.OS_Recommendation).filter_by(host_id=recommendation_completed_id).delete()
                    self.session.query(model.Service_Recommendation).filter_by(host_id=recommendation_completed_id).delete()

                recommendation_reset_query.update(dict(job_entry=3))

                logger.debug(f"Successfully restarted all successful os recommendation and service recommendation records and resetting job entry to 3")

        except Exception as e:
            if host_id:
                logger.error(logger_ip=logger_ip, message=f"Failed to restart recommendations. Error: {e}")
            else:
                logger.error(f"Failed to restart recommendations. Error: {e}")

    def get_host_results(self, host_id):
        """
        fetches details of the host from host_table of the given host_id
        :param host_id: integer
        :return: dictionary, host details obtained from host_table table
        """
        try:
            logger.debug(f"Fetching  host_id details from host table for host_id: {host_id}")
            if host_id is None:
                raise Exception(f"Missing host id. host_id = {host_id}")

            with self.session.begin():
                query = self.session.query(model.Host)
                query = query.filter_by(id=host_id, historic=0)
                resp = query.all()
                logger.debug(f"Successfully fetched host_details from host table for host_id: {host_id}")
                return resp

        except Exception as e:
            logger.error(f"Failed to get host_details from DB for host_id: {host_id}. Error: {e}")
            return None

    def update_utilization_job_host_mapping(self, job_id=None, host_id=None, job_type=None,job_status=None):
        """
        """
        try:
            logger.debug(f"Fetching utilizations job host mapping update for host_id: {host_id}")
            with self.session.begin():
                query = self.session.query(model.Job_Host_Mapping)
                query = query.filter_by(host_id=host_id)
                utilization_mapping_query = query.all()
                if utilization_mapping_query:
                    utilization_mapping_query=utilization_mapping_query[0]
                    utilization_mapping_query.update(dict(job_status=job_status))
                else:
                    request_data = model.Job_Host_Mapping()
                    request_data.update(dict(host_id=host_id,job_id=job_id,job_type=job_type,job_status=job_status))
                    request_data.save(session=self.session)
                    logger.debug(f"Successfully utilizations job host mapping udpated for host_id: {host_id}")
                return None

        except Exception as e:
            logger.error(f"Failed to utilizations job host mapping udpate for host_id: {host_id}. Error: {e}")
            return None

    def get_utilization_request_details(self, host_id=None):
        """
        """
        try:
            logger.debug(f"Fetching utilizations request details for host_id: {host_id}")
            with self.session.begin():
                query = self.session.query(model.IP)
                query = query.filter_by(host_id=host_id)
                resp = query.all()
                logger.debug(f"Successfully utilizations request details for host_id: {host_id}")
                return resp

        except Exception as e:
            logger.error(f"Failed to utilizations request details for host_id: {host_id}. Error: {e}")
            return None

    def get_job_status(self, host_id=None):
        """
        """
        try:
            logger.debug(f"Fetching utilizations job status for host_id: {host_id}")
            with self.session.begin():
                query = self.session.query(model.Job_Host_Mapping)
                query = query.filter_by(host_id=host_id)
                resp = query.all()
                logger.debug(f"Successfully utilizations job status for host_id: {host_id}")
                return resp

        except Exception as e:
            logger.error(f"Failed to utilizations job status for host_id: {host_id}. Error: {e}")
            return None

    def get_job_id(self, host_id=None, job_type=None):
        """
        """
        try:
            logger.debug(f"Fetching utilizations job host mapping update for host_id: {host_id}")
            with self.session.begin():
                query = self.session.query(model.Job_Host_Mapping)
                query = query.filter_by(host_id=host_id,job_type=job_type)
                resp = query.all()
                logger.debug(f"Successfully utilizations job host mapping udpated for host_id: {host_id}")
                return resp[0]

        except Exception as e:
            logger.error(f"Failed to utilizations job host mapping udpate for host_id: {host_id}. Error: {e}")
            return None

    def get_docker_images(self, docker_info_id):
        """
        fetches details of the docker images from docker_image of the given docker_info_id
        :param docker_info_id: integer
        :return: dictionary, docker images details obtained from docker_image table
        """
        try:
            logger.debug(f"Fetching  docker image details from docker_image table for docker_info_id: {docker_info_id}")
            if docker_info_id is None:
                raise Exception(f"Missing docker info id. docker_info_id = {docker_info_id}")

            with self.session.begin():
                query = self.session.query(model.DockerImage)
                query = query.filter_by(docker_info_id=docker_info_id)
                resp = query.all()
                logger.debug(f"Successfully fetched docker images from docker_image table for docker_info_id: {docker_info_id}")
                return resp
        except Exception as e:
            logger.error(f"Failed to get docker images from DB for docker_info_id {docker_info_id}. Error: {e}")
            return None

    def get_docker_containers(self, docker_image_id):
        """
        fetches details of the docker containers from docker_container of the given image_id
        :param image_id: integer
        :return: dictionary, docker container details obtained from docker_container table
        """
        try:
            logger.debug(f"Fetching  docker container details from docker_container table for image_id: {docker_image_id}")
            if docker_image_id is None:
                raise Exception(f"Missing image id. image_id = {docker_image_id}")

            with self.session.begin():
                query = self.session.query(model.DockerContainer)
                query = query.filter_by(docker_image_id=docker_image_id)
                resp = query.all()
                logger.debug(f"Successfully fetched docker container details from docker_container table for image_id: {docker_image_id}")
                return resp

        except Exception as e:
            logger.error(f"Failed to get docker container details from DB for image_id: {docker_image_id}. Error: {e}")
            return None

    def get_host_list(self, task_id):
        """
        fetches whole list of details of each host present under the given task_id
        :param task_id: integer,under which list of host_id's present
        :return: list of dictionaries,list of host_details of each host present under task_id
        """
        logger.debug(f"Fetching host list from host table for task_id:  {task_id}")

        try:
            with self.session.begin():
                query = self.session.query(model.Host)
                if task_id is not None:
                    query = query.filter_by(task_id=task_id, historic=0)
                resp = query.all()

            logger.debug(f"Successfully returned list of host details of all hosts under task_id {task_id}")
            return resp

        except Exception as e:
            logger.error(f"Failed in fetching host_list from DB in db handler for task_id {task_id}. Error: {e}")
            return None

    def get_succesfull_host_list_data(self, account_id=None):
        """
        fetches details of the succesfully discovered hosts under the given account_id containing recommendations and all the fields required for the assets list.
        :param account_id: integer, unique id for a user.
        :return: list of dictionary,  each dictionary contains details regarding the host and os recommendations.
        """
        try:
            logger.debug(f"Fetching host_id details for all the successfully discovered host's under the account_id: {account_id}")

            successfully_discovered_ip_list_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Stand_Alone_Queries", property_name="successfully_discovered_ip_details")

            successfully_discovered_ip_list_details = execute_dict_of_queries({"successfully_discovered_ip_details": successfully_discovered_ip_list_query}, self.engine, account_id=account_id, info="Stand_Alone_Query for successfully discovered ip details")

            logger.debug(f"Successfully fetched list of all the successfully discovered ip's for the account_id: {account_id}")

            return successfully_discovered_ip_list_details["successfully_discovered_ip_details"]
        except Exception as e:
            logger.error(f"Failed to get list of all discovered ip's details under the account_id: {account_id}. Error: {e}")
            return None

    def get_service_recommendations_list(self,account_id):
        """
        fetches services recommendation list under the account_id.
        :return: list, service recommendation list for all the host under the account_id
        """
        try:
            logger.debug(f"Fetching services recommendation list")
            service_recomendation_list_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Stand_Alone_Queries", property_name="service_recommendation_list")
            service_recomendation_list_data = execute_dict_of_queries({"service_recommendations": service_recomendation_list_query}, self.engine, account_id=account_id, info="Stand_Alone_Query for service recommendation list")
            logger.debug(f"Successfully fetched the service recommendation details")

            return service_recomendation_list_data['service_recommendations']

        except Exception as e:
            logger.error(f"Exception occurred while fetching service recommendation details fro teh account_id:{account_id}.Exception :{e} ")
            return None

    def get_services_list(self,host_id,host_ip):
        """
        fetches all the services present under the ip of the given host_ip
        :param host_id: unique id for every new discovery of a host
        :return: list of services
        """
        try:
            logger.debug(f"Fetching services list under the host_id: {host_id}.Host_IP= {host_ip} ")
            if host_id is not None:
                services_list_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Stand_Alone_Queries", property_name="services_list")

                services_list_details = execute_dict_of_queries({"services_list": services_list_query}, self.engine, host_id=host_id, info="Stand_Alone_Query for services list of an ip")

                logger.debug(f"Successfully fetched services list for the ip: {host_ip}")

                return services_list_details['services_list'] if len(services_list_details['services_list']) !=0 else None
        except Exception as e:
            logger.debug(f"Exception occurred while fetching services data for the ip: {host_ip}.Exception :{e}")
            return None

    def get_user_port_input(self, task_id, logger_ip=None):  # done
        try:
            logger.debug(logger_ip=logger_ip, message=f"Fetching user_port information for task_id {task_id}")
            with self.session.begin():
                query = self.session.query(model.User_Port)
                if task_id is not None:
                    query = query.filter_by(task_id=task_id)
                    logger.debug(logger_ip=logger_ip, message=f"Successfully fetched user port info from DB for task_id:{task_id}")
                return query.all()

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Exception occurred while fetching user port information. Exception as {e}")
            return None

    def fetch_host_details(self, host_id, logger_ip=None):
        """
        fetches host_table,credentials_table,ip_table details of the host from host_table of the given host_id
        :param host_id: integer
        :return: dictionary, host details obtained from host_table table after deleting the records in the db
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Fetching the host record and credentials records and deleting the whole host record.")

            with self.session.begin():
                host_query = self.session.query(model.Host)
                host_query = host_query.filter_by(id=host_id, historic=0)
                host_data = host_query.all()
                if not host_data:
                    raise Exception(f"Did not find any current record in host table for host id: {host_id}")
                host_query.update(dict(historic=1))

                credentials_query = self.session.query(model.Credentials)
                credentials_query = credentials_query.filter_by(host_id=host_id)
                credentials_data = credentials_query.all()

                ip_details_query = self.session.query(model.IP)
                ip_details_query = ip_details_query.filter_by(host_id=host_id)
                ip_details = ip_details_query.all()

                job_host_mapping_query = self.session.query(model.Job_Host_Mapping)
                job_host_mapping_query = job_host_mapping_query.filter_by(host_id=host_id, job_type='utilization')
                job_host_mapping_data = job_host_mapping_query.all()

                job_host_mapping_query.delete()

            logger.debug(logger_ip=logger_ip, message=f"Successfully fetched the host and credentials records and  deleted the whole host record")
            return host_data[0], credentials_data[0], job_host_mapping_data,ip_details[0]

        except Exception as e:
            logger.error(f"Failed to reinitiate for host_id: {host_id}. Error: {e}")
            return None

    def get_utilization_averages(self, host_id, logger_ip=None):
        """
        returns average utilization metrics for a given host_id
        """
        try:

            logger.debug(logger_ip=logger_ip, message=f"Fetching average utilization metrics for host_id {host_id}")

            section_name = "Average_Utilization"
            average_utilization_count_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name)
            query_results = queries_helper.execute_dict_of_queries(average_utilization_count_queries, self.engine, info="fetching average_utilization_count", host_id=host_id)
            if query_results.get("query_1"):
                logger.debug(logger_ip=logger_ip, message=f"Successfully fetched average utilization metrics for host_id {host_id}")
                return query_results["query_1"]

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to fetch average utilization metrics for host_id {host_id}. Error: {e}")
            return None

    def update_application_list(self,application_list=None,logger_ip=None):
        """
        updates the applications table with the list of applications for a particular ip.
        host_id: unique id for every host
        application_list : list of applications in the a particular ip.
        :return: None
        """
        try:
            logger.debug(logger_ip=logger_ip,message= f"Updating the application list into the application table for the host_id ")
            with self.session.begin():

                if application_list:
                    for each_application in application_list:
                        req_data = model.Application()
                        req_data.update(each_application)
                        req_data.save(session=self.session)
                logger.debug(logger_ip=logger_ip, message=f"Successfully updated the application table")
                return None
        except Exception as e:
            logger.debug(logger_ip=logger_ip, message=f"Exception occured while updating applications table. Eception : {e}")

    def update_application_host_mapping(self, application_host_mapping_info=None, logger_ip=None):
        """

        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Updating the ApplicationHostMapping table with host_id and application_id ")
            if application_host_mapping_info:
                with self.session.begin():
                    query = self.session.query(model.ApplicationHostMapping)
                    query = query.filter_by(host_id=application_host_mapping_info.get('host_id'),application_id=application_host_mapping_info.get('application_id'))
                    data = query.all()
                    if not data:
                        req_data = model.ApplicationHostMapping()
                        req_data.update(application_host_mapping_info)
                        req_data.save(session=self.session)
                    logger.debug(logger_ip=logger_ip, message=f"Successfully updated the ApplicationHostMapping table")

        except Exception as e:
            logger.debug(logger_ip=logger_ip, message=f"Exception occured while updating ApplicationHostMapping table. Eception : {e}")

    def update_application_information(self, application_details=None, logger_ip=None):
        """

        """
        try:
            logger.debug(logger_ip=logger_ip,
                         message=f"Updating the Application table with host_id and application_id ")
            with self.session.begin():
                query = self.session.query(model.Application)
                query = query.filter_by(name=application_details.get('name'))
                data = query.all()
                app_info = dict(name=None, app_code=None,business_group=None,business_importance=None,maturity = None,
                                periodic_processing = None, periodic_availability=None, complexity = None, code_size = None,
                                priority=None, strategy=None,interfaces_count=None, environment=None)
                app_info.update(application_details)
                if data:
                    for dic in data:
                        dic.update(app_info)
                else:
                    query = model.Application()
                    query.update(application_details)
                    query.save(session=self.session)
                    logger.debug(logger_ip=logger_ip, message=f"Successfully updated the application table")
                    return query.id
                return data[0].get('id')
        except Exception as e:
            logger.debug(logger_ip=logger_ip,
                         message=f"Exception occured while updating applications table. Eception : {e}")

    def get_hostid(self, ip=None,logger_ip=None):
        """

        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"fetching host_id using ip ")
            with self.session.begin():
                query = self.session.query(model.Host)
                query = query.filter_by(ip=ip, historic=0)
                data = query.all()
                if data:
                 return data[0].get("id")

        except Exception as e:
            logger.debug(logger_ip=logger_ip, message=f"Exception occured while fetching host_id using ip. Eception : {e}")

    def import_application_information(self, application_details=None, logger_ip=None):
        """
        Dumps data obtained from the excel file, by maintaining consistent data in application table.
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Updating the Application table with host_id and application_id ")

            with self.session.begin():
                query = self.session.query(model.Application)

                if application_details.get('discovered_name'):
                    query = query.filter_by(discovered_name=application_details.get('discovered_name'))
                    data = query.all()

                    if data:
                        query.update(application_details)
                    else:
                        logger.info("Discovered name of the application is not matching with the DB records. User changed the discovered name.")

                elif application_details.get('name'):
                    query = query.filter_by(name=application_details.get('name'))
                    data_1 = query.all()
                    if data_1:
                        query.update(application_details)
                    else:
                        query = model.Application()
                        query.update(application_details)
                        query.save(session=self.session)

                logger.debug(logger_ip=logger_ip, message=f"Successfully updated the application table")

        except Exception as e:
            logger.debug(logger_ip=logger_ip,
                         message=f"Exception occured while updating applications table. Eception : {e}")

    def get_login_activity(self, host_id, days, logger_ip=None):
        """
        returns average utilization metrics for a given host_id
        """
        try:

            logger.debug(logger_ip=logger_ip, message=f"Fetching number of login in past {days} days for host_id: {host_id}")

            section_name = "Login_History_Count"
            login_history_count_queries = util.get_properties(property_file_path=CreateInfra.db_queries_path.value, section=section_name)
            query_results = queries_helper.execute_dict_of_queries(login_history_count_queries, self.engine, info="fetching login_history_count", host_id=host_id, days=days, start_date=util.get_datetime())

            if query_results["query_1"][0]:
                logger.debug(logger_ip=logger_ip, message=f"Successfully fetched number of login in past {days} days for host_id: {host_id}")
                return query_results["query_1"][0].get("count")

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to fetch number of login in past {days} days for host_id: {host_id}. Error: {e}")
            return None

    def get_hosts_in_app(self, app_id, logger_ip=None):
        """
        fetch list of host id that have the corresponding app_id associated with them
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Fetching the list of host_id that have app_id: {app_id} running")
            with self.session.begin():
                data = self.session.query(model.ApplicationHostMapping.host_id, model.Host.ip)\
                    .filter(model.ApplicationHostMapping.host_id == model.Host.id and model.Host.historic == 0)\
                    .filter_by(application_id=app_id).distinct().all()

                host_id_list = []
                host_ip_list = []
                for item in data:
                    host_id_list.append(item[0])
                    host_ip_list.append(item[1])

                logger.debug(logger_ip=logger_ip, message=f"Successfully fetched the list of host_id that have app_id: {app_id} running")
                return host_id_list, host_ip_list

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to fetch the list of host_id that have app_id: {app_id} running. Error: {e}")
            return None

    def get_actions(self, host_id_list, cloud_provider, logger_ip=None):
        """
        returns list of actions and counts for a list of hosts
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Fetching all unique actions for host_id: {host_id_list}")

            actions_list_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Application_Queries", property_name="actions_list")
            actions_list = execute_dict_of_queries({"actions_list": actions_list_query}, self.engine, host_id_list=host_id_list, cloud_provider=cloud_provider, info="Application_Queries actions list")

            logger.debug(f"Successfully fetched all unique actions for host_id: {host_id_list}")
            return actions_list['actions_list']

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to all unique actions for host_id: {host_id_list}. Error: {e}")
            return None

    def get_infrastructure(self, host_id_list, cloud_provider, logger_ip=None):
        """
        returns list of infrastructure for a list of host ids
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Fetching all infrastructure for host_id: {host_id_list}")

            infrastructure_list_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Application_Queries", property_name="infrastructure_list")
            infrastructure_list = execute_dict_of_queries({"infrastructure_list": infrastructure_list_query}, self.engine, host_id_list=host_id_list, cloud_provider=cloud_provider, info="Application_Queries infrastructure_list")

            logger.debug(f"Successfully fetched all infrastructure for host_id: {host_id_list}")
            return infrastructure_list['infrastructure_list']

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to all infrastructure host_id: {host_id_list}. Error: {e}")
            return None

    def get_service(self, host_id_list, logger_ip=None):
        """
        returns list of service for a list of host ids
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Fetching all service for host_id: {host_id_list}")

            service_list_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Application_Queries", property_name="service_list")
            service_list = execute_dict_of_queries({"service_list": service_list_query}, self.engine, host_id_list=host_id_list, info="Application_Queries service list")

            logger.debug(f"Successfully fetched all service for host_id: {host_id_list}")
            return service_list['service_list']

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to all service host_id: {host_id_list}. Error: {e}")
            return None

    def get_database(self, host_id_list, logger_ip=None):
        """
        returns list of database for a list of host ids
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Fetching all database for host_id: {host_id_list}")

            database_list_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Application_Queries", property_name="database_list")
            database_list = execute_dict_of_queries({"database_list": database_list_query}, self.engine, host_id_list=host_id_list, info="Application_Queries database list")

            logger.debug(f"Successfully fetched all database for host_id: {host_id_list}")
            return database_list['database_list']

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to all database host_id: {host_id_list}. Error: {e}")
            return None

    def get_core_services(self,logger_ip=None):
        """
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Fetching core services")
            with self.session.begin():
                query = self.session.query(model.Core_Services)
                resp = query.all()
                logger.debug(logger_ip=logger_ip, message=f"Sucessfully fetched all core services")
                return resp

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to all fetch the core services: Error: {e}")
            return None

    def update_core_service_list(self, service=None, logger_ip=None):
        """
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Fetching core services")
            if service:
                with self.session.begin():
                    query = self.session.query(model.Core_Services)
                    service_query = query.filter_by(service=service)
                    service_query_data = service_query.all()
                    if not service_query_data:
                        req_data = model.Core_Services()
                        req_data.update(dict(service=service))
                        req_data.save(session=self.session)
                    logger.debug(logger_ip=logger_ip, message=f"Sucessfully fetched all core services")
                    return None

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to all fetch the core services: Error: {e}")
            return None

    def update_onprem_action(self, onprem_actions=None, logger_ip=None):
        """
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Updating onprem acgtions")
            with self.session.begin():
                req_data = model.Action()
                req_data.update(onprem_actions)
                req_data.save(session=self.session)
                logger.debug(logger_ip=logger_ip, message=f"Sucessfully updated onprem actions")
                return None

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Exceptions occured while updating onprem actions: Error: {e}")
            return None

    def update_as_core_service(self, service=None, logger_ip=None):
        """
        returns list of database for a list of host ids
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Updating as core service : {service}")

            core_service_update_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Core_Service_Queries", property_name="mark_core_service")

            service_data = dict(coreservice=service)
            self.engine.execute(text(core_service_update_query), service_data)

            logger.debug(f"Successfully updated as core service: {service}")
            return {'response':'Updated service sucessfully'}

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to update as core service: {service}. Error: {e}")
            return {'response':'Failed'}

    def update_as_other_service(self, host_id=None, logger_ip=None):
        """
        returns list of database for a list of host ids
        """
        try:
            logger.debug(logger_ip=logger_ip, message=f"Updating as other service : {host_id}")

            core_service_update_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Core_Service_Queries", property_name="mark_other_service")

            host_data=dict(host_id=host_id)

            self.engine.execute(text(core_service_update_query), host_data)

            logger.debug(f"Successfully updated as other service: {host_id}")
            return None

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to update as other service: {host_id}. Error: {e}")
            return None

    def get_error_list(self, account_id, logger_ip=None):
        """
        """
        try:
            table_data_query = get_properties(property_file_path=CreateInfra.db_queries_path.value, section="Error_Queries", property_name='errors_list')
            error_list = execute_dict_of_queries({"error_list": table_data_query}, self.engine, info="errors_list queries", account_id=account_id)

            return error_list

        except Exception as e:
            logger.error(logger_ip=logger_ip, message=f"Failed to get errors_list. Error: {e}")
            return None
