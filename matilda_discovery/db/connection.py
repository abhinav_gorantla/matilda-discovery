from oslo_config import cfg
from oslo_db import options as oslo_db_options
from oslo_db.sqlalchemy import session as db_session

from matilda_discovery.constant.matilda_enum import CreateInfra
from matilda_discovery.utils import util

import threading

data = util.get_properties(property_file_path=CreateInfra.etc_properties_path.value, section='database')
api_db_opts = [
    cfg.StrOpt('db_connection',
               secret=True,
               default='mysql+mysqldb://' + data['username'] + ':' + data['password'] + '@' + data['url'] + '/' + data['dbname']
               )]

CONF = cfg.CONF
opt_group = cfg.OptGroup(name='database')
CONF.register_group(opt_group)
CONF.register_opts(oslo_db_options.database_opts, opt_group)
CONF.register_opts(api_db_opts, opt_group)


_ENGINE_FACADE = {'matilda_discovery': None}
_CSS_FACADE = 'matilda_discovery'
_LOCK = threading.Lock()


def _create_facade(conf_group):
    return db_session.EngineFacade(
        sql_connection=conf_group.db_connection,
        autocommit=True,
        expire_on_commit=False,
        mysql_sql_mode=conf_group.mysql_sql_mode,
        connection_recycle_time=conf_group.connection_recycle_time,
        connection_debug=conf_group.connection_debug,
        max_pool_size=conf_group.max_pool_size,
        max_overflow=conf_group.max_overflow,
        pool_timeout=conf_group.pool_timeout,
        sqlite_synchronous=conf_group.sqlite_synchronous,
        connection_trace=conf_group.connection_trace,
        max_retries=conf_group.max_retries,
        retry_interval=conf_group.retry_interval)


def _create_facade_lazily(facade, conf_group):
    global _LOCK, _ENGINE_FACADE
    if _ENGINE_FACADE[facade] is None:
        with _LOCK:
            if _ENGINE_FACADE[facade] is None:
                _ENGINE_FACADE[facade] = _create_facade(conf_group)
    return _ENGINE_FACADE[facade]


def get_session(use_slave=False, **kwargs):
    conf_group = CONF.database
    facade = _create_facade_lazily(_CSS_FACADE, conf_group)
    return facade.get_session(use_slave=use_slave, **kwargs)


def get_engine(use_slave=False):
    conf_group = CONF.database
    facade = _create_facade_lazily(_CSS_FACADE, conf_group)
    return facade.get_engine(use_slave=use_slave)
