import os
from fnmatch import fnmatch
os.system("rm -rf build")
os.system("rm -rf dist")
os.system("rm -rf cython_debug")
os.system("rm -rf matilda_discovery.egg-info")
root = os.path.dirname(os.path.abspath(__file__))
pattern = "*.c"
pattern1 = "*.so"
ommit = ['__pycache__','.git','.scannerwork','.idea','migrate_repo']
#ommit_work=['test','api','db','__pycache__']
#ommit_plug=['api','test','__pycache__']
s1 = set(ommit)
temp = []
for path, subdirs, files in os.walk(root):

    for name in files:
        s2 = set(path.split('/'))
        if (fnmatch(name, pattern) or fnmatch(name, pattern1)) and not s1.intersection(s2):
            print(os.path.join(path, name))
            os.system('rm ' + os.path.join(path, name))

