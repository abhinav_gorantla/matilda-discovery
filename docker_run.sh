#!/bin/bash

export PYTHONPATH=$PYTHONPATH:/opt/matilda/matilda-discovery
sed "s/url =.*/url = $DB_HOST/g" /opt/matilda/matilda-discovery/matilda_discovery/etc/matilda_discovery.ini
cd /opt/matilda/matilda-discovery/matilda_discovery/api/controller
python3 api.py
